const settings = {
  "name": "frontity",
  "state": {
    "frontity": {
      "url": "http://f-de-formentera.vercel.app/",
      "backend" : "https://gestion.fdeformentera.com/",
      "title": "F de Formentera",
      "description": "la ginebra con sabor a Mediterráneo"
    },
    analytics: {
      pageviews: {
        googleTagManagerAnalytics: true,
      },
      events: {
        googleTagManagerAnalytics: true,
      }
    },

  },
  "packages": [
    {
      "name": "@frontity/mars-theme",
      "state": {
        "theme": {

          "featured": {
            "showOnList": false,
            "showOnPost": false
          }
        }
      }
    },
    {
      "name": "@frontity/wp-source",
      "state": {
        "source": {
          "api": "https://gestion.fdeformentera.com/wp-json",
          "url": "https://gestion.fdeformentera.com/",
          "homepage": "/pagina-ejemplo",
          postTypes: [
            {
              type: "movies",
              endpoint: "movies",
              archive: "/movies_archive",
            },{
              type: "posts", // type slug
              endpoint: "/wp/v2/posts"
            },
          ]
        }

      }
    },
    "analytics-pixel",
    "wp-graphql-pixel",
    "@frontity/tiny-router",
    "@frontity/html2react",
    "@frontity/yoast",
    {
      "name": "@frontity/google-tag-manager-analytics",
      "state": {
        "googleTagManagerAnalytics": {
          containerId: "GTM-MTDJNKJ",
        },
      },
    },

  ]
};

export default settings;
