import 'cross-fetch/polyfill';
import {ApolloClient, ApolloError, InMemoryCache} from '@apollo/client';
import {HttpLink} from 'apollo-link-http';
import {ApolloLink} from 'apollo-link';
import {gql} from '@apollo/client';
import CartFragments from './fragments/Cart/cart';
import {getAuthToken, getCustomerId, getRefreshToken, getUserAuthId, isLogged, processAuthToken, registerJwtToken, registerNewAuthToken, unregisterJwtToken} from "./lib/auth-functions";

/**
 * Middleware operation
 * If we have a session token in localStorage, add it to the GraphQL request as a Session header.
 */
export const middleware = new ApolloLink((operation, forward) => {
  /**
   * If session data exist in local storage, set value as session header.
   */
  const session = localStorage.getItem('woo-session');
  if (session) {
    operation.setContext(({headers = {}}) => ({
      headers: {
        'woocommerce-session': `Session ${session}`,
      }
    }));
  }
  const authToken = getAuthToken();
  operation.setContext(({headers = {}}) => ({
    headers: {
      ...headers,
      'Authorization': authToken ? `Bearer ${authToken}` : "",
    }
  }));

  return forward(operation);
});

/**
 * Afterware operation
 * This catches the incoming session token and stores it in localStorage, for future GraphQL requests.
 */
export const afterware = new ApolloLink((operation, forward) => {
  return forward(operation).map((response) => {
    /**
     * Check for session header and update session in local storage accordingly.
     */
    const context = operation.getContext();
    const {response: {headers}} = context;
    const session = headers.get('woocommerce-session');
    if (session) {
      if (localStorage.getItem('woo-session') !== session) {
        localStorage.setItem('woo-session', headers.get('woocommerce-session'));
      }
    }

    return response;
  });
});


const httpLink = new HttpLink({
  uri: 'https://gestion.fdeformentera.com/graphql',
});

const client = new ApolloClient({
  link: middleware.concat(afterware.concat(httpLink)),
  cache: new InMemoryCache(),
  clientState: {},
});


export default {
  actions: {
    auth: {
      authProcessAuthToken: async ({state, libraries}) => {
        switch (processAuthToken()) {
          case -1:
            state.tokenStatus = -1;
            unregisterJwtToken()
            break;
          case 0:
            state.tokenStatus = 0;
            const r = await libraries.register.refreshJwtAuthToken(getRefreshToken());

            if (r == null || r instanceof ApolloError) {
              unregisterJwtToken()
            } else {
              registerNewAuthToken(r.authToken);
              state.tokenStatus = 1;
            }

            break;
          default:
            state.tokenStatus = 1;
            break;
        }
      },
    }
  },
  libraries: {
    auth: {
      authRegisterJwtToken: (authToken, refreshToken, userId) => {
        registerJwtToken(authToken, refreshToken, userId);
      },
      authUnregisterJwtToken: () => {
        unregisterJwtToken();
      },
      authIsLogged: () => {
        return isLogged();
      },
      authGetUserAuthId: () => {
        return getUserAuthId();
      },
      authGetCustomerId: () => {
        return getCustomerId();
      },
      authGetRefreshToken: () => {
        return getRefreshToken();
      }
    },
    login: {
      login: () => {
        return 'JSON DEVUELTO'
      }
    },
    products: {
      getProduct: (id) => {
        return client
        .query({
          variables: {
            id: id
          },
          query: gql`
              query getProduct($id: ID!) {
                 product(id: $id, idType: DATABASE_ID) {
                  description
                  name
                  ... on SimpleProduct {
                    price
                    regularPrice
                    salePrice
                    dateOnSaleFrom
                    dateOnSaleTo
                    taxClass
                    taxStatus
                    featuredImage {
                      node {
                        sourceUrl
                      }
                    }
                  }
                }                            
              }
            `
        })
        .catch(result => {
          return result
        })
        .then(result => {
          return result
        })


      }
    },
    cart: {
      emptyCart: () => {
        return client
        .query({
          fetchPolicy: "network-only",
          query: gql`
          
              mutation {
                emptyCart(input: {clearPersistentCart: false}) {
                  clientMutationId
                  cart {
                    ...CartContent
                  }  
                }
              }             
              ${CartFragments.cart}
            `
        })
        .catch(result => {
          return result
        })
        .then(result => {
          return result
        })
      },
      getCart: () => {
        return client
          .query({
            fetchPolicy: "network-only",
            query: gql`
              query MyQuery {
                cart {
                  ...CartContent
                }                            
              }
              ${CartFragments.cart}
            `
          })
          .catch(result => {
            return result
          })
          .then(result => {
            return result
          })

      },
      checkout: async (orderDetail  ) => {
        const CREATE_CHECKOUT = gql`
          mutation  ($address: String!, $city: String!, $email: String!, $firstName: String!, $phone: String!, $postcode: String!, $state: String!) {           
              checkout(input: {                
                billing: {
                  address1: $address, 
                  city: $city, 
                  country: ES, 
                  email: $email, 
                  firstName: $firstName,
                  postcode: $postcode,
                  phone: $phone,
                  state: $state,
                },              
                paymentMethod: "paypal",                  
                shipToDifferentAddress: false,                
              }) {
                clientMutationId
                result
                redirect
                order {
                  billing {
                    firstName
                    lastName
                    address1
                    city
                    phone
                    postcode
                    state
                    email
                  }
                  shipping {
                    firstName
                    lastName
                    address1
                    city
                    postcode
                    state
                    phone
                  }
                  status
                  id
                  total(format: FORMATTED)
                  totalTax(format: FORMATTED)
                  subtotal(format: FORMATTED)
                  shippingTotal(format: FORMATTED)
                  paymentMethodTitle
                  paymentMethod
                  needsPayment
                  needsProcessing
                  orderNumber
                  databaseId
                  orderKey
                  couponLines {
                    edges {
                      node {
                        code
                        discount
                        discountTax
                      }
                    }
                  }
                  customerNote
                  date
                  customer {
                    email
                  }
                }
              }             
            
          }`

        return await client.mutate({
          mutation: CREATE_CHECKOUT,
          variables: {
            address: orderDetail.purchase_units[0].shipping.address.address_line_1,
            city: orderDetail.purchase_units[0].shipping.address.admin_area_2,
            email: orderDetail.payer.email_address,
            firstName: orderDetail.purchase_units[0].shipping.name.full_name,
            phone: orderDetail.payer.phone.phone_number.national_number,
            postcode: orderDetail.purchase_units[0].shipping.address.postal_code,
            state: orderDetail.purchase_units[0].shipping.address.admin_area_1,
          }
        }).then(result => {
          return result
        }).catch(result => {
          return result
        })
      },
      addCoupon: (code) => {
        const ADD_COUPON = gql`
          mutation MyMutation($code: String!) {  
            applyCoupon(input: {code: $code}) {
              cart {
                  ...CartContent
              }
            }
          }
          ${CartFragments.cart}
          `;
        return client.mutate({
          mutation: ADD_COUPON,
          variables: {
            code: code
          }
        }).then(result => {
          return result
        }).catch(result => {
          return result
        });

      },
      updateItemQuantities: async (key, quantity) => {
        const ADD_TODO = gql`
            mutation updateItemQuantities($quantity: Int!, $key: ID!) {           
              updateItemQuantities(input: { items: {quantity: $quantity, key: $key}}) {
                clientMutationId
                cart {
                  ...CartContent
                }  
              }
            }
            ${CartFragments.cart}`;

        return await client.mutate({
          mutation: ADD_TODO,
          variables: {
            quantity: parseInt(quantity),
            key: key
          }
        }).then(result => {
          return result
        }).catch(result => {
          return result
        })
      },
      addItem: async (productId, quantity) => {
        const ADD_TODO = gql`
          mutation addItem($quantity: Int!, $productId: Int!) {           
            addToCart(input: {quantity: $quantity, productId: $productId}) {
              clientMutationId
              cart {
                ...CartContent
              }  
            }
          }
          ${CartFragments.cart}`;

        return await client.mutate({
          mutation: ADD_TODO,
          variables: {
            quantity: parseInt(quantity),
            productId: productId
          }
        }).then(result => {
          return result
        }).catch(result => {
          return result
        })
      },
    },
    register: {
      loginUser: async (email, password) => {
        const LOGIN_USER = gql`
          mutation LoginUser($email: String!, $password: String!) {
              login(input: {username: $email, password: $password}) {
                  authToken
                  refreshToken
                  customer {
                      id
                      firstName
                      role
                      metaData(keysIn: "user_empresa") {
                        id
                        key
                        value
                      }
                  }
              }
          }`;

        return await client.mutate({
          mutation: LOGIN_USER,
          variables: {
            email: email,
            password: password
          }
        }).then(result => {
          localStorage.setItem('UserID', result?.data?.login?.customer?.id)
          localStorage.setItem('Role', result?.data?.login?.customer?.metaData !== null ? 'empresa' : result?.data?.login?.customer?.role)
          return result?.data?.login;
        }).catch(result => {

          return result
        });
      },
      registerUser: async (firstName, lastName, email, password, cif = null, empresa = null) => {

        const variables = {
          firstName: firstName,
          lastName: lastName,
          email: email,
          password: password,
        }


        let REGISTER_USER = ''
        if(cif != null && empresa != null) {


          variables.user_cif = 'CIF';
          variables.user_empresa = 'EMPRESA';

          REGISTER_USER = gql`
            mutation RegisterEmpresa($firstName: String, $lastName: String, $email: String, $password: String, $user_cif: String!, $user_empresa: String! ) {
              registerEmpresa(input: {
                firstName: $firstName, 
                lastName: $lastName,
                email: $email, 
                password: $password,
                metaData: [
                  {key: "user_cif", value: $user_cif},
                  {key: "user_empresa", value: $user_empresa}
                ]
              }) {
                authToken
                refreshToken
                customer {
                  id
                  firstName
                  role
                }
              }
          }`;
        }else{
          REGISTER_USER = gql`
            mutation RegisterCustomer($firstName: String, $lastName: String, $email: String, $password: String) {
              registerCustomer(input: {
                firstName: $firstName, 
                lastName: $lastName,
                email: $email, 
                password: $password
              }) {
                authToken
                refreshToken
                customer {
                  id
                  firstName
                  role
                }
              }
          }`;
        }






        return await client.mutate({
          mutation: REGISTER_USER,
          variables: variables
        }).then(result => {
          if(cif != null && empresa != null) {
            localStorage.setItem('UserID', result?.data?.registerEmpresa.customer.id)
            localStorage.setItem('Role', result?.data?.registerEmpresa.customer.role)
            return result?.data?.registerEmpresa;
          }else{
            localStorage.setItem('UserID', result?.data?.registerCustomer.customer.id)
            localStorage.setItem('Role', result?.data?.registerCustomer.customer.role)
            return result?.data?.registerCustomer;
          }

        }).catch(result => {
          return result
        });
      },
      sendPasswordResetEmail: async (email) => {
        const SEND_PASSWORD_RESET = gql`
        mutation SendPasswordResetEmail($email: String!) {
          sendPasswordResetEmail(input: {username: $email}) {
            clientMutationId
          }
        }`;

        return await client.mutate({
          mutation: SEND_PASSWORD_RESET,
          variables: {
            email: email
          }
        }).then(result => {
          return result?.data?.sendPasswordResetEmail;
        }).catch(result => {
          return result
        });
      },
      resetUserPassword: async (email, password, key) => {
        const PASSWORD_RESET = gql`
          mutation resetUserPassword($email: String!, $password: String!, $key: String!) {
            resetUserPassword(input: {login: $email, password: $password, key: $key}) {
              clientMutationId
            }
          }
        `;

        return await client.mutate({
          mutation: PASSWORD_RESET,
          variables: {
            email: email,
            password: password,
            key: key
          }
        }).then(result => {
          return result?.data?.resetUserPassword;
        }).catch(result => {
          return result
        });
      },
      refreshJwtAuthToken: async (refreshToken) => {
        const REFRESH_AUTH_TOKEN = gql`
          mutation MyQuery($refreshToken: String!) {
            refreshJwtAuthToken(input: {jwtRefreshToken: $refreshToken}) {
              authToken
            }
          }
        `;

        return await client.mutate({
          mutation: REFRESH_AUTH_TOKEN,
          variables: {
            refreshToken: refreshToken
          }
        }).then(result => {
          return result?.data?.refreshJwtAuthToken;
        }).catch(result => {
          return result
        });
      },
      getDatosPersonales: async id => {
        const GET_DATOS_PERSONALES = gql`
          query getDatosPersonales($id: ID!) {
            customer(id: $id) {
              email
              firstName
              lastName
              metaData(keysIn: ["birthday_day", "birthday_month", "birthday_year"]) {
                key
                value
              }
              billing {
                extra_billing_addresses {
                  firstName
                  lastName
                  company
                  address1
                  city
                  phone
                  postcode
                  state
                  key            
                }        
              }
              shipping {
                extra_shipping_addresses {
                  firstName
                  lastName
                  company
                  address1
                  city
                  phone
                  postcode
                  state      
                  key
                }
              }
            }
          }`;

        return await client.query({
          query: GET_DATOS_PERSONALES,
          variables: {
            id: id
          }
        }).then(result => {
          return result?.data?.customer;
        }).catch(result => {
          return result
        });
      }
    },
    user: {
      getUser: async () => {

      }
    },
    mi_cuenta: {
      updateAddress: async (address, type) => {
        const firstName = user.firstName;
        const lastName = user.lastName;
        const email = user.email;
        const birthdayDay = user.birthdayDay;
        const birthdayMonth = user.birthdayMonth;
        const birthdayYear = user.birthdayYear;

        const UPDATE_DATOS_PERSONALES = gql`
          mutation UpdateDatosPersonales(
            $id: ID!, 
            $firstName: String!, 
            $lastName: String!, 
            $email: String!,
            $birthdayDay: String!,
            $birthdayMonth: String!,
            $birthdayYear: String!,
          ) {
            updateCustomer(input: {
              id: $id, 
              email: $email, 
              firstName: $firstName, 
              lastName: $lastName,
              metaData: [
                {key: "birthday_day", value: $birthdayDay}, 
                {key: "birthday_month", value: $birthdayMonth}, 
                {key: "birthday_year", value: $birthdayYear}
              ]
            }) {
              clientMutationId
            }
          }
        `;

        return await client.mutate({
          mutation: UPDATE_DATOS_PERSONALES,
          variables: {
            id: id,
            firstName: firstName,
            lastName: lastName,
            email: email,
            birthdayDay: birthdayDay,
            birthdayMonth: birthdayMonth,
            birthdayYear: birthdayYear
          }
        }).then(result => {
          return result?.data?.updateCustomer;
        }).catch(result => {
          return result
        });
      },

      updateDatosPersonales: async (id, user) => {
        const firstName = user.firstName;
        const lastName = user.lastName;
        const email = user.email;
        const birthdayDay = user.birthdayDay;
        const birthdayMonth = user.birthdayMonth;
        const birthdayYear = user.birthdayYear;

        const UPDATE_DATOS_PERSONALES = gql`
          mutation UpdateDatosPersonales(
            $id: ID!, 
            $firstName: String!, 
            $lastName: String!, 
            $email: String!,
            $birthdayDay: String!,
            $birthdayMonth: String!,
            $birthdayYear: String!,
          ) {
            updateDatosPersonales(input: {
              id: $id, 
              email: $email, 
              firstName: $firstName, 
              lastName: $lastName,
              birthdayDay: $birthdayDay, 
              birthdayMonth: $birthdayMonth, 
              birthdayYear: $birthdayYear
              
            }) {
              clientMutationId
            }
          }
        `;

        return await client.mutate({
          mutation: UPDATE_DATOS_PERSONALES,
          variables: {
            id: id,
            firstName: firstName,
            lastName: lastName,
            email: email,
            birthdayDay: birthdayDay,
            birthdayMonth: birthdayMonth,
            birthdayYear: birthdayYear
          }
        }).then(result => {
          return result?.data?.updateCustomer;
        }).catch(result => {
          return result
        });
      },
      updatePassword: async (id, password) => {
        const UPDATE_PASSWORD = gql`
          mutation updatePassword($id: ID!, $password: String!) {
            updateUser(input: {
              id: $id, 
              password: $password
            }) {
              clientMutationId
            }
          }
        `;

        return await client.mutate({
          mutation: UPDATE_PASSWORD,
          variables: {
            id: id,
            password: password,
          }
        }).then(result => {
          return result?.data?.updateUser;
        }).catch(result => {
          return result
        });
      },
      getOrders: async (id) => {
        const GET_ORDERS = gql`
          query getOrders($id: Int!) {
            orders(where: {customerId: $id}) {
              edges {
                node {
                  orderNumber
                  date
                  status
                }
              }
            }
          }
        `;

        return await client.mutate({
          mutation: GET_ORDERS,
          variables: {
            id: id
          }
        }).then(result => {
          return result?.data?.orders;
        }).catch(result => {
          return result
        });
      },
      getAddresses: async (id) => {
        const GET_ADDRESSES = gql`
          query getAddresses($id: ID!) {
            customer(id: $id) {
              email,
              billing {
                firstName
                lastName
                company
                address1
                city
                phone
                postcode
                state                
                extra_billing_addresses {
                  firstName
                  lastName
                  company
                  address1
                  city
                  phone
                  postcode
                  state             
                  key     
                  street   
                  tipoVia  
                  numero   
                  piso        
                }        
              }
              shipping {
                firstName
                address1
                lastName
                city
                company
                phone
                postcode
                state
                country
                extra_shipping_addresses {
                  firstName
                  lastName
                  company
                  address1
                  city
                  phone
                  postcode
                  state
                  key
                  street
                  tipoVia
                  numero
                  piso
                }
              }
              metaData(keysIn: [
                "shipping_tipo_via", 
                "shipping_street", 
                "shipping_numero", 
                "shipping_piso", 
                "shipping_phone",
                "billing_tipo_via", 
                "billing_street", 
                "billing_numero", 
                "billing_piso", 
              ]) {
                key
                value
              }
            }
          }
        `;

        return await client.mutate({
          mutation: GET_ADDRESSES,
          variables: {
            id: id
          }
        }).then(result => {
          return result?.data?.customer;
        }).catch(result => {
          return result
        });
      },
      deleteAddressEnvio: async (id) => {
        const DELETE_ADDRESS_ENVIO = gql`
          mutation updateDatosEnvio(
            $id: String!
          ) {
            removeShippingAddress(input: {
              id: $id,              
            }) {
              clientMutationId
            }
          }
        `;

        return await client.mutate({
          mutation: DELETE_ADDRESS_ENVIO,
          variables: {
            id: id,
          }
        }).then(result => {
          return result?.data?.updateCustomer;
        }).catch(result => {
          return result
        });
      },
      deleteAddressFacturacion: async (id) => {
        const DELETE_ADDRESS_ENVIOFActuracion = gql`
          mutation deleteAddressFacturacin {
            removeBillingAddress(input: {
              id: "1",              
            }) {
              clientMutationId
            }
          }
        `;

        return await client.mutate({
          mutation: DELETE_ADDRESS_ENVIOFActuracion,
          variables: {
            id: id,
          }
        }).then(result => {
          return result?.data?.updateCustomer;
        }).catch(result => {
          return result
        });
      },
      addExtraAddressEnvio: async ( address) => {
        const firstName = address.firstName;
        const lastName = address.lastName;
        const address1 = address.address1;
        const postcode = address.postcode;
        const city = address.city;
        const phone = address.phone;
        const state = address.state;
        const tipoVia = address.tipoVia;
        const street = address.street;
        const numero = address.numero;
        const piso = address.piso;

        const CREATE_ADDRESS_ENVIO = gql`
          mutation createDatosEnvio(            
            $firstName: String!, 
            $lastName: String!,
            $address1: String!,
            $postcode: String!,
            $city: String!,
            $state: String!,            
            $phone: String!,
            $tipoVia: String!,
            $street: String!,
            $numero: String!,
            $piso: String
          ) {
            addShippingAddress(input: {
              firstName: $firstName, 
              lastName: $lastName, 
              address1: $address1, 
              postcode: $postcode, 
              city: $city, 
              state: $state,
              phone: $phone,   
              tipoVia: $tipoVia,
              street: $street,
              numero: $numero,
              piso: $piso,      
            }) {
              clientMutationId
            }
          }
        `;

        return await client.mutate({
          mutation: CREATE_ADDRESS_ENVIO,
          variables: {
            firstName: firstName,
            lastName: lastName,
            address1: address1,
            postcode: postcode,
            city: city,
            state: state,
            phone: phone,
            tipoVia: tipoVia,
            street: street,
            numero: numero,
            piso: piso
          }
        }).then(result => {
          return result?.data?.addShippingAddress;
        }).catch(result => {
          return result
        });
      },
      updateExtraAddressEnvio: async (address) => {
        const firstName = address.firstName;
        const lastName = address.lastName;
        const address1 = address.address1;
        const postcode = address.postcode;
        const city = address.city;
        const phone = address.phone;
        const state = address.state;
        const country = "ES";
        const tipoVia = address.tipoVia;
        const street = address.street;
        const numero = address.numero;
        const piso = address.piso;
        const key = address.key;

        const UPDATE_ADDRESS_ENVIO = gql`
          mutation updateDatosEnvio(            
            $firstName: String!, 
            $lastName: String!,
            $address1: String!,
            $postcode: String!,
            $city: String!,
            $state: String!,
            $country: String!,
            $phone: String!,
            $tipoVia: String!,
            $street: String!,
            $numero: String!,
            $piso: String,
            $key: String!
          ) {
            updateExtraAddress(input: {
              firstName: $firstName, 
              lastName: $lastName, 
              address1: $address1, 
              postcode: $postcode, 
              city: $city, 
              state: $state,
              phone: $phone,
              tipoVia: $tipoVia,
              street: $street,
              numero: $numero,
              piso: $piso,
              country: $country,
              key: $key              
            }) {
              clientMutationId
            }
          }
        `;

        return await client.mutate({
          mutation: UPDATE_ADDRESS_ENVIO,
          variables: {
            firstName: firstName,
            lastName: lastName,
            address1: address1,
            postcode: postcode,
            city: city,
            state: state,
            country: country,
            phone: phone,
            tipoVia: tipoVia,
            street: street,
            numero: numero,
            piso: piso,
            key: key
          }
        }).then(result => {
          return result?.data?.updateExtraAddress;
        }).catch(result => {
          return result
        });
      },
      updateAddressEnvio: async (id, address) => {
        const firstName = address.firstName;
        const lastName = address.lastName;
        const address1 = address.address1;
        const postcode = address.postcode;
        const city = address.city;
        const phone = address.phone;
        const state = address.state;
        const country = "ES";
        const tipoVia = address.tipoVia;
        const street = address.street;
        const numero = address.numero;
        const piso = address.piso;

        const UPDATE_ADDRESS_ENVIO = gql`
          mutation updateDatosEnvio(
            $id: ID!, 
            $firstName: String!, 
            $lastName: String!,
            $address1: String!,
            $postcode: String!,
            $city: String!,
            $state: String!,
            $country: CountriesEnum!,
            $phone: String!,
            $tipoVia: String!,
            $street: String!,
            $numero: String!,
            $piso: String
          ) {
            updateCustomer(input: {
              id: $id,
              shipping: {
                firstName: $firstName, 
                lastName: $lastName, 
                address1: $address1, 
                postcode: $postcode, 
                city: $city, 
                state: $state,
                country: $country
              },
              metaData: [
                {key: "shipping_tipo_via", value: $tipoVia}, 
                {key: "shipping_street", value: $street}, 
                {key: "shipping_numero", value: $numero},
                {key: "shipping_piso", value: $piso},
                {key: "shipping_phone", value: $phone}
              ]
            }) {
              clientMutationId
            }
          }
        `;

        return await client.mutate({
          mutation: UPDATE_ADDRESS_ENVIO,
          variables: {
            id: id,
            firstName: firstName,
            lastName: lastName,
            address1: address1,
            postcode: postcode,
            city: city,
            state: state,
            country: country,
            phone: phone,
            tipoVia: tipoVia,
            street: street,
            numero: numero,
            piso: piso
          }
        }).then(result => {
          return result?.data?.updateCustomer;
        }).catch(result => {
          return result
        });
      },
      updateAddressFacturacion: async (id, address) => {
        const firstName = address.firstName;
        const lastName = address.lastName;
        const address1 = address.address1;
        const postcode = address.postcode;
        const city = address.city;
        const phone = address.phone;
        const state = address.state;
        const country = "ES";
        const tipoVia = address.tipoVia;
        const street = address.street;
        const numero = address.numero;
        const piso = address.piso;

        const UPDATE_ADDRESS_FACTURACION = gql`
          mutation updateDatosFacturacion(
            $id: ID!, 
            $firstName: String!, 
            $lastName: String!,
            $address1: String!,
            $postcode: String!,
            $city: String!,
            $state: String!,
            $country: String!,
            $phone: String!,
            $tipoVia: String!,
            $street: String!,
            $numero: String!,
            $piso: String!
          ) {
            updateDatosFacturacion(input: {
              id: $id, 
              firstName:  $firstName, 
              lastName:  $lastName,
              address1:  $address1,
              postcode: $postcode,
              city: $city,
              state:  $state,
              country:  $country,
              phone:  $phone,
              tipoVia:  $tipoVia,
              street:  $street,
              numero:  $numero,
              piso:  $piso
            }) {
              clientMutationId
            }
          }
        `;

        return await client.mutate({
          mutation: UPDATE_ADDRESS_FACTURACION,
          variables: {
            id: id,
            firstName: firstName,
            lastName: lastName,
            address1: address1,
            postcode: postcode,
            city: city,
            state: state,
            country: country,
            phone: phone,
            tipoVia: tipoVia,
            street: street,
            numero: numero,
            piso: piso
          }
        }).then(result => {
          return result?.data?.updateCustomer;
        }).catch(result => {
          return result
        });
      },
      getOrder: async (id) => {

        console.log(id, 'getORder')
        const GET_ORDER = gql`
          query getOrder($id: ID!) {
            order(id: $id, idType: DATABASE_ID) {
              id
              date
              status
              lineItems {
                nodes {
                  quantity
                  product {
                    name
                    description
                    ... on SimpleProduct {
                      price
                      regularPrice
                      salePrice
                      dateOnSaleFrom
                      dateOnSaleTo
                    }
                    image {
                      sourceUrl
                      description
                    }
                    productCategories {
                      nodes {
                        name
                      }
                    }
                  }
                }
              }
              paymentMethod
              paymentMethodTitle
              total
              taxLines {
                nodes {
                  taxTotal
                }
              }
              subtotal
              shippingTotal
              totalTax
              billing {
                address1
                address2
                city
                country
                company
                email
                firstName
                lastName
                phone
                postcode
                state
              },
              shipping {
                address1
                address2
                city
                country
                company
                email
                firstName
                lastName
                phone
                postcode
                state
              }
              couponLines {
                nodes {
                  discount
                  coupon {
                    amount
                  }
                  code
                }
              }
            }
          }
        `;

        return await client.mutate({
          mutation: GET_ORDER,
          variables: {
            id: id
          }
        }).then(result => {
          return result?.data?.order;
        }).catch(result => {
          return result
        });
      },
    },
    checkout: {
      sendCheckout: async (input) => {
        const CHECKOUT = gql`
          mutation CHECKOUT_MUTATION($input: CheckoutInput!) {
            checkout(input: $input) {
              redirect
              result
              order {
                databaseId
                id
                orderNumber
                billing {
                  firstName
                  lastName
                  address1
                  city
                  phone
                  postcode
                  state
                  email
                }
                shipping {
                  firstName
                  lastName
                  address1
                  city
                  postcode
                  state
                  phone
                  email
                }
                
                status
                total(format: FORMATTED)
                totalTax(format: FORMATTED)
                subtotal(format: FORMATTED)
                shippingTotal(format: FORMATTED)                
                paymentMethodTitle
                paymentMethod
                needsPayment
                needsProcessing
                orderNumber
                databaseId
                orderKey
                couponLines {
                  edges {
                    node {
                      code
                      coupon {
                        amount
                      }
                      discount
                      discountTax
                    }
                  }
                }
                customerNote
                date
                customer {
                  email
                }
              }
            }
          }
        `;



        return await client.mutate({
          mutation: CHECKOUT,
          variables: {
            input: input
          }
        }).then(result => {
          return result?.data?.checkout;
        }).catch(result => {
          return result
        });
      },
      setStripeCustomerId: async (id, customerId) => {
        const SET_STRIPE_CUSTOMER_ID = gql`
          mutation setStripeCustomerId(            
            $customerId: String!
          ) {
            setStripeID(input: {             
              customerId: $customerId
            }) {
              clientMutationId
            }
          }
        `;

        return await client.mutate({
          mutation: SET_STRIPE_CUSTOMER_ID,
          variables: {
            customerId: customerId
          }
        }).then(result => {
          return result?.data?.updateCustomer;
        }).catch(result => {
          return result
        });
      },
    },
    forms: {

      submitForm:  (data, id) => {
        return client
        .mutate({
          variables: {
            fields: data,
            id: id
          },
          mutation: gql`   
            mutation addForm($fields: [InputField]!, $id: Int!) {         
              submitForm(input: {data: $fields, formId: $id}) {
                success
                message
              }
            }
            `
        })
        .catch(result => {
          console.log(result, 'KORESULT')
          return result
        })
        .then(result => {
          console.log(result, 'OKRESULT')
          return result
        })
      },
      getForm:  (id) => {
        return client
        .query({
          variables: {
            id: id
          },
          query: gql`
              query getForm($id: ID!) {
                form(id: $id, idType: DATABASE_ID) {
                  title,
                  fields {
                    nodes {
                      fieldId
                      label
                      type
                    }
                  }
                }                          
              }              
            `
        })
        .catch(result => {
          return result
        })
        .then(result => {
          return result
        })
      }
    }
  }
}

