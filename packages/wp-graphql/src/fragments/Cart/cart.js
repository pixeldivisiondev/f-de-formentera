import { gql } from '@apollo/client';

const Cart =  {
  cart: gql`
    fragment CartContent on Cart {      
      contentsTax
      discountTax
      discountTotal
      displayPricesIncludeTax
      feeTax
      feeTotal
      shippingTax
      shippingTotal
      subtotal
      total
      totalTax
      subtotalTax
      availableShippingMethods {
        ... on ShippingPackage {
        
         rates {
         __typename
          cost
          id
          instanceId
          methodId
          label    
          min_amount
        }
        }
       
        packageDetails
      }
      chosenShippingMethod
      appliedCoupons {
        nodes {
          code
          description
          discountType
          amount
        }
      }
      contents {
        nodes {
          quantity
          key
          total
          subtotal
          product {
            node {
              id
              description
              productCategories {                                          
                nodes {
                  name                  
                }
              }
              name
              image {
                sourceUrl
                uri
              }
              ... on SimpleProduct {
                 price
                 regularPrice
                 salePrice
                 dateOnSaleFrom
                 dateOnSaleTo
              }
            }
          }
        }
        itemCount
      }
    }
  `,
};


export default Cart
