import React from 'react';

import Cookies from 'js-cookie';
import jwt_decode from "jwt-decode";
import {connect} from "frontity";

const USER_ID = 'react-formentera-id';
const AUTH_TOKEN_NAME = 'formentera-session-id';
const REFRESH_TOKEN_NAME = 'formentera-cart-session-id';

export const registerJwtToken = (authToken, refreshToken, userId) => {
  localStorage.setItem(USER_ID, encodeToken(userId));
  localStorage.setItem(AUTH_TOKEN_NAME, encodeToken(authToken));
  Cookies.set(REFRESH_TOKEN_NAME, encodeToken(refreshToken), {HttpOnly: false, expires: 365});
};

export const unregisterJwtToken = () => {
  localStorage.removeItem(USER_ID);
  localStorage.removeItem(AUTH_TOKEN_NAME);
  Cookies.remove(REFRESH_TOKEN_NAME);
};

export const registerNewAuthToken = authToken => {
  localStorage.setItem(AUTH_TOKEN_NAME, encodeToken(authToken));
};

/* Procesa el auth token:
 * Si no existen los tokens devuelve -1
 * Si han pasado mas de 45 minutos de inactividad devuelve -1
 * Si el token auth ha expirado (15 minutos) devuelve 0
 * Si tenemos token devuelve 1
 */

export const processAuthToken = () => {
  const authToken = getAuthToken();
  const refreshToken = getRefreshToken();

  // Si no existen debemos forzar el logout
  if (authToken === null || refreshToken === null) {
    return -1;
  }

  // Si han pasado mas de 45 minutos sin actividad debemos forzar el logout
  const qmustLogout = mustLogout(authToken);
  if (qmustLogout === true || qmustLogout === null) {
    return -1;
  }

  const qhasExpiredAuthToken = hasExpiredAuthToken(authToken);
  if (qhasExpiredAuthToken === true) {
    return 0;
  } else {
    return 1;
  }
};

export const getAuthToken = () => {
  return decodeToken(localStorage.getItem(AUTH_TOKEN_NAME));
};

export const getRefreshToken = () => {
  return decodeToken(Cookies.get(REFRESH_TOKEN_NAME));
};

export const hasExpiredAuthToken = authToken => {
  if (authToken !== null) {
    const authTokenDecoded = jwt_decode(authToken);
    const expiredAt = authTokenDecoded.exp;

    return getCurrentTimestamp() > expiredAt;
  } else {
    return null;
  }
};

export const mustLogout = authToken => {
  if (authToken !== null) {
    const authTokenDecoded = jwt_decode(authToken);
    const expiredAt = authTokenDecoded.exp;

    const timestampExtra = 1800000; // Media hora

    return getCurrentTimestamp() > (expiredAt + timestampExtra);
  } else {
    return null;
  }
};

export const getCustomerId = () => {
  const authToken = getAuthToken();

  if (authToken !== null) {
    const authTokenDecoded = jwt_decode(authToken);

    return parseInt(authTokenDecoded.data.user.id);
  } else {
    return null;
  }
};

export const getUserAuthId = () => {
  return decodeToken(localStorage.getItem(USER_ID));
};

export const isLogged = () => {
  return getAuthToken() !== null && getUserAuthId() !== null;
};

export const getCurrentTimestamp = () => {
  return Math.floor(Date.now() / 1000);
};

export const encodeToken = token => {
  if (token === null || token === undefined) {
    return null;
  }

  return btoa(token);
};

export const decodeToken = token => {
  if (token === null || token === undefined) {
    return null;
  }

  return atob(token);
};
