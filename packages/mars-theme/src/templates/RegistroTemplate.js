import React from 'react';
import {connect, styled} from "frontity";

import Intro from "../components/pages/registro/Intro";
import Wrapper from "../objects/Wrapper";
import FormRegistro from "../components/pages/registro/FormRegistro";
import LaunchLogin from "../components/pages/registro/LaunchLogin";

const RegistroTemplate = ({state, actions, ...props}) => {
  WrapperStyled.defaultProps = {
    theme: state.theme
  };

  return (
    <WrapperStyled wrapper={"Stretch"} align="flex-start" direction="column">
      <Intro/>
      <LaunchLogin/>
      <FormRegistro pageRegistro={true}/>
    </WrapperStyled>
  )
};

export default connect(RegistroTemplate);

const WrapperStyled = styled(Wrapper)`
  padding: 28px 16px;

  ${props => {
    return `  
      @media ${props.theme.breakPoints['tablet']} {
        padding: 28px 16px 60px;
      }
    `
  }}
`;
