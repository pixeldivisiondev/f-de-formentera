import React from 'react';
import {connect} from "frontity";

import MiCuentaContainer from "../components/pages/mi-cuenta/MiCuentaContainer";
import MisDirecciones from "../components/pages/mi-cuenta/MisDirecciones";

const DireccionesTemplate = ({state, actions, ...props}) => {

  return (
    <MiCuentaContainer current="mi-cuenta/mis-direcciones">
      <MisDirecciones/>
    </MiCuentaContainer>
  )
};

export default connect(DireccionesTemplate);

