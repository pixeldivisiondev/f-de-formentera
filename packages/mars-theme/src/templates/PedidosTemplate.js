import React from 'react';
import {connect} from "frontity";

import MiCuentaContainer from "../components/pages/mi-cuenta/MiCuentaContainer";
import MisPedidos from "../components/pages/mi-cuenta/MisPedidos";

const PedidosTemplate = ({state, actions, ...props}) => {

  return (
    <MiCuentaContainer current="mis-compras/mis-pedidos">
      <MisPedidos/>
    </MiCuentaContainer>
  )
};

export default connect(PedidosTemplate);

