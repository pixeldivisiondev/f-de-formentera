import React from 'react';
import {connect} from "frontity";

import MiCuentaContainer from "../components/pages/mi-cuenta/MiCuentaContainer";
import MisDatosPersonales from "../components/pages/mi-cuenta/MisDatosPersonales";

const MisDatosPersonalesTemplate = ({state, actions, ...props}) => {

  return (
    <MiCuentaContainer current="mi-cuenta/mis-datos-personales">
      <MisDatosPersonales/>
    </MiCuentaContainer>
  )
};

export default connect(MisDatosPersonalesTemplate);

