import React, {useEffect, useState} from 'react';
import {connect, styled} from "frontity";

import MiCuentaContainer from "../components/pages/mi-cuenta/MiCuentaContainer";
import MiCuentaIntro from "../components/pages/mi-cuenta/Intro/MiCuentaIntro";
import Skeleton, {SkeletonTheme} from "react-loading-skeleton";
import FlexGrid from "../objects/FlexGrid/FlexGrid";
import FlexGridItem from "../objects/FlexGrid/FlexGridItem";
import OrderStatus from "../components/objects/OrderStatus/OrderStatus";
import Text from "../objects/Text/Text";
import Layout from "../objects/Layout/Layout";
import {ApolloError} from "@apollo/client";
import ProductDetail from "../components/pages/carrito/Left/ProductDetail";
import Button from "../objects/Button/Button";
import ProductDetailPrice from "../components/pages/carrito/Left/ProductDetailPrice";


const VerPedidoTemplate = ({state, actions, libraries, ...props}) => {

  const [orderData, setOrderData ] = useState(null);

  useEffect(() => {
    processToken();
  }, []);

  const processToken = async () => {
    const r = await actions.auth.authProcessAuthToken();

    if (state.tokenStatus === 1 && libraries.auth.authIsLogged()) {
      await findOrder();
    } else {
      logOut();
    }
  }

  const findOrder = async (orderId) => {

    const r = await libraries.mi_cuenta.getOrder(state.theme.viewOrder);
    if (r == null || r instanceof ApolloError) {
      logOut();
    } else {
      setOrderData(r);
    }
  }


  const logOut = async () => {
    libraries.auth.authUnregisterJwtToken();
    actions.theme.setLoginName(null);
    actions.theme.logOut();
    await actions.router.set('/');
  }

const date = orderData ?  new Date(orderData.date) : new Date()

  return (
    <MiCuentaContainer current="mis-compras/mis-pedidos">
      <SkeletonTheme color="#ecf5f8" highlightColor="#c3dee7">
        <MiCuentaIntro title="MIS PEDIDOS" texto=""/>
        {orderData === null ? (
          <>
            <Skeleton height={50} count={5}/>
          </>
        ) : (
          <>
            <LineStyled>
              <FlexGrid>

                <FlexGridItem layout={{movil: 6, tablet: 6, desktop: 4}} direction="column">
                  <TitleStyled data={{texto: 'FECHA DE ENVÍO:', font_type: 'SubtitleSmall', font_family: 'Lexend Zetta'}}/>
                  <TextStyled data={{texto: date.getDate() + '/' + (date.getMonth() + 1)  + '/' + date.getFullYear(), font_type: 'SecondaryMedium'}}/>
                </FlexGridItem>

                <FlexGridItem layout={{movil: 6, tablet: 6, desktop: 4}} direction="column">
                  <TitleStyled data={{texto: 'ESTADO:', font_type: 'SubtitleSmall', font_family: 'Lexend Zetta'}}/>
                  <OrderStatus status={orderData.status}/>
                </FlexGridItem>

                <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 4}} direction="column">
                  <Text data={{texto: 'Puedes consultar más información sobre el envío aqui:'}}/>
                  <StyledSeguimiento data={{tipo: 'outlined', tamano: 'full', texto: "Seguimiento"}} />
                </FlexGridItem>

              </FlexGrid>
            </LineStyled>
            <Layout justify={"flex-start"}>
              <MiCuentaIntro title="DETALLE DEL PEDIDO" texto=""/>
            </Layout>
            <Layout justify={"flex-start"} data={{section_background_color: 'gray-base'}}>
              <ProductDetail
                item={null}
                image={orderData.lineItems.nodes[0].product.image.sourceUrl}
                name={orderData.lineItems.nodes[0].product.name}
                category={orderData.lineItems.nodes[0].product.productCategories.nodes[0].name}
                description={orderData.lineItems.nodes[0].product.description}
                price={orderData.total}
                inCheckout={true} initialQuantity={orderData.lineItems.nodes[0].quantity}/>
            </Layout>

            <LineBordered justify={"space-between"} align={"flex-start"}>
              <LeftText data={{texto: 'DIRECIÓN DE ENVÍO', font_type: 'LabelSmall', font_family: 'Lexend Zetta'}}/>
              <DescriptionLine align={"flex-end"} direction={"column"}>
                <RightText data={{texto: orderData.shipping.firstName + ' ' + orderData.shipping.lastName}}/>
                <RightText data={{texto: orderData.shipping.address1}}/>
                <RightText data={{texto: orderData.shipping.postcode + ' ' + orderData.shipping.city + ', España'}}/>
                <RightText data={{texto: orderData.billing.phone }}/>
              </DescriptionLine>
            </LineBordered>

            <LineBordered justify={"space-between"} align={"flex-start"}>
              <LeftText data={{texto: 'DIRECIÓN DE FACTURACIÓN', font_type: 'LabelSmall', font_family: 'Lexend Zetta'}}/>
              <DescriptionLine align={"flex-end"} direction={"column"}>
                <RightText data={{texto: orderData.billing.firstName + ' ' + orderData.billing.lastName}}/>
                <RightText data={{texto: orderData.billing.address1}}/>
                <RightText data={{texto: orderData.billing.postcode + ' ' + orderData.billing.city + ', España'}}/>
                <RightText data={{texto: orderData.billing.phone }}/>
              </DescriptionLine>
            </LineBordered>

            <LineBordered justify={"space-between"} align={"flex-start"}>
              <Text data={{texto: 'MÉTODO DE PAGO', font_type: 'LabelSmall', font_family: 'Lexend Zetta'}}/>
              <DescriptionLine align={"flex-end"} direction={"column"}>
                <Text data={{texto: orderData.paymentMethodTitle}}/>
              </DescriptionLine>
            </LineBordered>

            <LineBordered justify={"space-between"} align={"flex-start"}>
              <Text data={{texto: 'ENVÍO', font_type: 'LabelSmall', font_family: 'Lexend Zetta'}}/>
              <DescriptionLine align={"flex-end"} direction={"column"}>
                <Text data={{texto: "Fecha estimada de entrega:"}}/>
                <Text data={{texto: "Lunes 22 Junio - Martes 23 Junio: "}}/>
              </DescriptionLine>
            </LineBordered>



            <LineBordered justify={"space-between"} align={"flex-start"}>
              <Text data={{texto: 'GASTOS ENVÍO', font_type: 'LabelSmall', font_family: 'Lexend Zetta'}}/>
              <DescriptionLine align={"flex-end"} direction={"column"}>
                <Text data={{texto: orderData.shippingTotal}}/>
              </DescriptionLine>
            </LineBordered>


            {
              orderData && orderData.couponLines && orderData.couponLines?.nodes?.length > 0
                ?
                <LineBordered>
                  <Text data={{texto: 'CUPÓN: '+orderData.couponLines.nodes[0].code, font_type: 'LabelSmall', font_family: 'Lexend Zetta'}}/>
                    <DescriptionLine align={"flex-end"} direction={"column"}>
                      <Text data={{texto: "-"+orderData.couponLines.nodes[0].coupon.amount.toString() + '€'}}/>
                    </DescriptionLine>
                </LineBordered>
                : null
            }

            <LastLine>
              <Line justify={"space-between"} align={"flex-start"}>
                <Text data={{texto: 'IVA', font_type: 'LabelSmall', font_family: 'Lexend Zetta'}}/>
                <DescriptionLine align={"flex-end"} direction={"column"}>
                  <Text data={{texto: orderData.totalTax}}/>
                </DescriptionLine>
              </Line>
              <Line justify={"space-between"} align={"flex-start"}>
                <Text data={{texto: 'TOTAL', font_type: 'LabelSmall', font_family: 'Lexend Zetta'}}/>
                <DescriptionLine align={"flex-end"} direction={"column"}>
                  <Price price={orderData.total} justify={"flex-end"}/>
                </DescriptionLine>
              </Line>
            </LastLine>

          </>
        )}
      </SkeletonTheme>
    </MiCuentaContainer>
  )
};

export default connect(VerPedidoTemplate);


const StyledSeguimiento = styled(Button)`
margin-top: 8px;
`
const LastLine = styled.div`
  padding-top: 16px;
  padding-bottom: 16px;
`
const DescriptionLine = styled(Layout)`
  flex: 1 1;
`

const Line = styled(Layout)`
  flex: 1 1; 
  padding: 0 16px;
`

const RightText = styled(Text)`
  text-align: right;
`

const LeftText = styled(Text)`
  max-width: 110px;
`

const LineBordered = styled(Line)`
  border-bottom: 1px solid #E5EDEF;
   padding: 16px;
`


const LineStyled = styled.div` 
  padding-bottom: 16px;
`;

const TitleStyled = styled(Text)`
  margin-bottom: 8px;
`;

const Price = styled(ProductDetailPrice)`
  justify-content: flex-end !Important;
`;

const TextStyled = styled(Text)`
  margin-bottom: 22px;
  opacity: 0.6;
`;
