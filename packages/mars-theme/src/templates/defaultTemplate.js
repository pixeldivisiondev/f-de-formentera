import React, {useEffect, useState} from "react";
import {connect} from "frontity";
import ContenidoFlexible from '../components/ContenidoFlexible'
import PreFooter from "../components/preFooter/prefooter";
import LoginForm from "../components/pages/login/LoginModal/LoginModalContent";
import Wrapper from "../objects/Wrapper";


const DefaultTemplate = ({ state, actions, libraries, ...props}) => {

    const data = state.source.get(state.router.link);
    const post = state.source[data.type][data.id];

    const [empresaUser, setEmpresaUser] = useState(state.theme.login.role)

    const logOut = async () => {
        libraries.auth.authUnregisterJwtToken();
        actions.theme.setLoginName(null);
        actions.theme.logOut();
        await actions.router.set('/');
    };

    useEffect(() => {
        if (props.forEmpresa && !libraries.auth.authIsLogged()) {
            //SHOW LOFINFORM
            setEmpresaUser(false)
            //logOut();
        }else if(props.forEmpresa && libraries.auth.authIsLogged()) {
            //CHECK IF LOOGED AS EMPRESA
            if (typeof window !== 'undefined') {
                if(localStorage.getItem('Role') === 'empresa') {
                    setEmpresaUser(true)
                }else{
                    setEmpresaUser(false)
                }
            }else{
                setEmpresaUser(false)
            }
        }
    });


    return (
        <>
          <ContenidoFlexible data={post.acf} forEmpresa={empresaUser}/>
          <PreFooter />
        </>
    )
}

export default connect(DefaultTemplate);
