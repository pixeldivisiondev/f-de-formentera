import {connect, styled, css} from "frontity";
import Wrapper from '../objects/Wrapper'
import Buttons from '../components/uiKit/Buttons'
import Colors from '../components/uiKit/Colors'
import Surfaces from '../components/uiKit/Surfaces'
import Texts from '../components/uiKit/Texts'
import Text from "../objects/Text/Text"
import Modal from "../objects/Modal/Modal";
import { ModalWrapper, CloseModal, ModalContent } from '../components/objects/Modals/index'

const UiKitTemplate = () => {
    return (
    <div>
        <Wrapper css={css`padding-top: 150px`} wrapper={"Default"} direction={"column"} align={"flex-start"}>
            <Texts />
            <Colors />
            <Buttons />
            <Surfaces />
            <Modal
              modal={<h1>Modal Content 2</h1>}
              wrapper={ModalWrapper}
              ModalContent={ModalContent }
              CloseModal={CloseModal}
              close={"Cerrar 2"} >
              <Text data={{texto: 'ABRIR MODAL'}} />
            </Modal>
            <ST data={{texto: 'Lorem ipsum.....'}} />
        </Wrapper>
    </div>
    )
}

export default UiKitTemplate;

const ST = styled(Text)`
  background-color: rgba(#000, 0.5)
`
