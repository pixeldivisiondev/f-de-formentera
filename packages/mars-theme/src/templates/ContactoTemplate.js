import {connect, styled} from "frontity";
import Section from "../objects/Section/Section";
import Text from "../objects/Text/Text";
import RichText from "../objects/RichText/RichText";

import {useForm} from "react-hook-form";
import Form from "../objects/Forms/Form";
import FormControl from "../objects/Forms/FormControl";
import Layout from "../objects/Layout/Layout";
import AlertMessage from "../objects/AlertMessage/AlertMessage";
import FlexGrid from "../objects/FlexGrid/FlexGrid";
import FlexGridItem from "../objects/FlexGrid/FlexGridItem";
import ButtonSubmit from "../objects/ButtonSubmit/ButtonSubmit";
import React, {useEffect, useState} from "react";
import {ApolloError} from "@apollo/client";


const ContactoTemplate = ({state, libraries, actions, ...props}) => {

  const data = state.source.get(state.router.link);
  const post = state.source[data.type][data.id];

  const [adding, setAdding] = useState(null);
  const [submitted, setSubmitted] = useState(null)



  const {register, handleSubmit, watch, control, errors} = useForm();

  useEffect(() => {
    register({ name: "tipo_7" }, { required: true });
  }, []);

  const handleOnSubmit = async data => {

    setAdding(true);

    const d = [];
    Object.keys(data).map((field, key) => {

      const exploded  = field.split('_');
      if(data[field].value){
        d.push({id: parseInt(exploded[1]), value: data[field].value})
      }else{
        const parsed = data[field] === true ? 1 : data[field]
        d.push({id: parseInt(exploded[1]), value: parsed.toString()})
      }

    })

    const p = await actions.auth.authProcessAuthToken();
    const r = await libraries.forms.submitForm(d, 1)
    setAdding(false);
    if (r instanceof ApolloError) {
      let errorMessage = r.message;
      actions.theme.setAlertMessage("Ha habido un error al procesar el formulario", false, 'error');
    }else{
      setSubmitted(true)
      actions.theme.setAlertMessage("El formulario se ha enviado correctamente", false, 'success');



    }


  };

  const labelText = "Acepto la <a href='/politica-de-privacidad//'>política de privacidad y cookies</a>"

  return (
    <StyledSection wrapper={"Stretch"} justify={"flex-start"}>
      <StyledPageTitle data={{texto: post.title.rendered, font_type: 'PageTitle', font_family: 'Lexend Zetta'}} />
      <StyledResponsable data={{
        texto: 'En nuestras <a href="/preguntas-frecuentes/">Preguntas Frecuentes</a> podrás encontrar la respuesta a las dudas más habituales. Si eres una empresa o restaurante, nos pondremos en contacto contigo lo antes posible.',
        font_family: 'Lexend Deca',
        font_type: 'PrimaryMedium'}}/>

      <Layout>

        <StyledForm onSubmit={handleSubmit(handleOnSubmit)}>
          <AlertMessage inModal={false}/>
          <FlexGrid>
            <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
              <FormControl
                id={"nombre"} name={"nombre_1"}
                type="text" placeholder={"Nombre"}
                register={register}
                errors={errors}
              />
            </FlexGridItem>

            <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
              <FormControl
                id={"apellidos"} name={"apellidos_6"}
                type="text" placeholder={"Apellidos"}
                register={register}
                errors={errors}
              />
            </FlexGridItem>

            <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
              <FormControl
                id={"email"} name={"email_2"}
                type="email" placeholder={"Email"}
                register={register}
                errors={errors}
              />
            </FlexGridItem>

            <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
              <FormControl
                id={"phone"} name={"telefono_9"}
                type="phone" placeholder={"Teléfono"}
                register={register}
                errors={errors}
              />
            </FlexGridItem>

          </FlexGrid>

          <div style={{height: '20px'}}/>

          <FlexGrid>
            <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
              <FormControl
                id={"ciudad"} name={"ciudad_10"}
                type="text" placeholder={"Ciudad"}
                register={register}
                errors={errors}
              />
            </FlexGridItem>
            <StyledTipo layout={{movil: 12, tablet: 6, desktop: 6}}>
              <FormControl
                id={"tipo"} name={"tipo_7"}
                type="select" placeholder={"Tipo de consulta"}
                selectOptions={[{value: "reclamacion", label: "Reclamación"}, {value: "ayuda", label: "Ayuda"}, {value: 'pedido', label: 'Mi pedido'}]}
                register={register({ required: true, maxLength: 30 })}
                extraValidate={{validate: value => value === 'a' || "Las contraseñas deben coincidir"}}
                errors={errors}
                control={control}
              />
            </StyledTipo>

          </FlexGrid>

          <div style={{height: '20px'}}/>

          <FlexGrid>
            <FlexGridItem layout={{movil: 12, tablet: 12, desktop: 12}}>
              <StyledTextArea
                id={"mensaje"} name={"mensaje_3"}
                type="textarea" placeholder={"Mensaje"}
                register={register}
                errors={errors}
              />
            </FlexGridItem>

            <FlexGridItem  layout={{movil: 12, tablet: 12, desktop: 12}}>
              <StyledResponsable data={
                {
                  texto: '<p>Responsable del Tratamiento: Formentera Mediterranean Spirits; finalidad del tratamiento: gestión de la consulta; legitimación del tratamiento: ejecución de un contrato/acuerdo; destinatarios: no se cederán datos, salvo obligación legal; derechos: acceder, rectificar y suprimir los datos, así como otros derechos, como se explica en la política de privacidad. Más información en la <a href="/politica-de-privacidad//" target="_blank">política de privacidad y</a> <a href="/politica-de-cookies/" target="_blank"> cookies</a></p>',
                  font_family: 'Lexend Deca',
                  font_type: 'PrimarySmall'
                }
              }/>
            </FlexGridItem>

            <StyledCondiciones layout={{movil: 12, tablet: 12, desktop: 12}}>
              <FormControl
                id={"condiciones"} name={"condiciones_8"}
                type="checkbox" label={labelText}
                register={register}
                errors={errors}
              />
            </StyledCondiciones>

            <FlexGridItem layout={{movil: 12, tablet: 12, desktop: 12}}>
              <ButtonSubmit text={adding ? 'ENVIANDO...' : "ENVIAR"} disabled={adding}/>
            </FlexGridItem>

          </FlexGrid>
        </StyledForm>
      </Layout>
    </StyledSection>

  )
};

export default connect(ContactoTemplate);

const StyledForm = styled(Form)`
  margin-top: 22px;
`

const StyledTipo = styled(FlexGridItem)`
& > div {
  color: #F00;
}
`

const StyledCondiciones = styled(FlexGridItem)`
  margin-top: 8px;
  margin-bottom: 24px;
`
const StyledResponsable = styled(RichText)`
  opacity: 0.68;
  a {
    color: var(--color-blue-light);
    text-decoration: none;
  }
`

const StyledSection = styled(Section)`
padding-bottom: 74px;
`

const StyledPageTitle = styled(Text)`
  text-transform: uppercase;
  margin-bottom: 24px;
  margin-top: 16px;
`

const StyledTextArea = styled(FormControl)`
  padding-top: 13px;
  height: 126px;
  & > div {
  color: #F00 !important;
  }
`
