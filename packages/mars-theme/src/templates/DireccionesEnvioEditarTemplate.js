import React from 'react';
import {connect} from "frontity";

import MiCuentaContainer from "../components/pages/mi-cuenta/MiCuentaContainer";
import MisDireccionesEnvioEditar from "../components/pages/mi-cuenta/MisDireccionesEnvioEditar";

const DireccionesEnvioEditarTemplate = ({state, actions, ...props}) => {

  return (
    <MiCuentaContainer current="mi-cuenta/mis-direcciones/envio/editar">
      <MisDireccionesEnvioEditar/>
    </MiCuentaContainer>
  )
};

export default connect(DireccionesEnvioEditarTemplate);

