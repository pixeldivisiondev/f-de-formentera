import React from 'react';
import {connect} from "frontity";

import MiCuentaContainer from "../components/pages/mi-cuenta/MiCuentaContainer";
import Dashboard from "../components/pages/mi-cuenta/Dashboard";

const MiCuentaTemplate = ({state, actions, ...props}) => {

  return (
    <MiCuentaContainer current="dashboard">
      <Dashboard/>
    </MiCuentaContainer>
  )
};

export default connect(MiCuentaTemplate);


