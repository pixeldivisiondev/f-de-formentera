import React from 'react';
import {connect} from "frontity";

import MiCuentaContainer from "../components/pages/mi-cuenta/MiCuentaContainer";
import MisDireccionesFacturacion from "../components/pages/mi-cuenta/MisDireccionesFacturacion";

const DireccionesFacturacionTemplate = ({state, actions, ...props}) => {

  return (
    <MiCuentaContainer current="mi-cuenta/mis-direcciones/facturacion">
      <MisDireccionesFacturacion/>
    </MiCuentaContainer>
  )
};

export default connect(DireccionesFacturacionTemplate);

