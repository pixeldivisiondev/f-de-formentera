import React from 'react';
import {connect} from "frontity";

import MiCuentaContainer from "../components/pages/mi-cuenta/MiCuentaContainer";
import CambiarContrasena from "../components/pages/mi-cuenta/CambiarContrasena";

const CambiarContrasenaTemplate = ({state, actions, ...props}) => {

  return (
    <MiCuentaContainer current="mi-cuenta/cambiar-contrasena">
      <CambiarContrasena/>
    </MiCuentaContainer>
  )
};

export default connect(CambiarContrasenaTemplate);
