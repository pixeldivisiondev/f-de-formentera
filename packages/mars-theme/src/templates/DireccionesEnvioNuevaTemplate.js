import React from 'react';
import {connect} from "frontity";

import MiCuentaContainer from "../components/pages/mi-cuenta/MiCuentaContainer";
import MisDireccionesEnvio from "../components/pages/mi-cuenta/MisDireccionesEnvioNueva";

const DireccionesEnvioNuevaTemplate = ({state, actions, ...props}) => {

  return (
    <MiCuentaContainer current="mi-cuenta/mis-direcciones/envio">
      <MisDireccionesEnvio/>
    </MiCuentaContainer>
  )
};

export default connect(DireccionesEnvioNuevaTemplate);

