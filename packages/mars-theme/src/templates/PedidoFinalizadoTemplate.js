import React, {useEffect} from 'react';
import {connect, styled} from "frontity";

import Wrapper from "../objects/Wrapper";
import PedidoFinalizadoContainer from "../components/pages/pedido-finalizado/PedidoFinalizadoContainer";
import {ApolloError} from "@apollo/client";

const PedidoFinalizadoTemplate = ({state, actions, libraries, ...props}) => {
  WrapperStyled.defaultProps = {
    theme: state.theme
  };

  useEffect(() => {
    emptyCart()
  })

  const emptyCart = async() => {
    const r = await libraries.cart.emptyCart();
    actions.theme.setCheckoutData(null);
    actions.theme.setCheckoutStep(1);
    state.cart = null;
  }

  return (
    <WrapperStyled wrapper="Stretch" align="flex-start" direction="column">
      <PedidoFinalizadoContainer/>
    </WrapperStyled>
  )
};

export default connect(PedidoFinalizadoTemplate);

const WrapperStyled = styled(Wrapper)`
  padding: 28px 16px;

  ${props => {
  return `  
      @media ${props.theme.breakPoints['tablet']} {
        padding: 28px 16px 60px;
      }
    `
}}
`;
