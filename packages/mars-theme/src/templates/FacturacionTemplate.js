import React from 'react';
import {connect} from "frontity";

import MiCuentaContainer from "../components/pages/mi-cuenta/MiCuentaContainer";
import Facturacion from "../components/pages/mi-cuenta/Facturacion";

const FacturacionTemplate = ({state, actions, ...props}) => {

  return (
    <MiCuentaContainer current="mi-cuenta/facturacion">
      <Facturacion/>
    </MiCuentaContainer>
  )
};

export default connect(FacturacionTemplate);

