import React from 'react';
import {connect, styled} from "frontity";

import Wrapper from "../objects/Wrapper";
import FinalizarCompraContainer from "../components/pages/finalizar-compra/FinalizarCompraContainer";

const FinalizarCompraTemplate = ({state, actions, ...props}) => {
  WrapperStyled.defaultProps = {
    theme: state.theme
  };

  return (
    <WrapperStyled wrapper="Default" align="flex-start" direction="column">
      <FinalizarCompraContainer/>
    </WrapperStyled>
  )
};

export default connect(FinalizarCompraTemplate);

const WrapperStyled = styled(Wrapper)`
  padding-top: 28px;
  padding-bottom: 28px;

  ${props => {
  return `  
      @media ${props.theme.breakPoints['tablet']} {
        
      }
    `
}}
`;
