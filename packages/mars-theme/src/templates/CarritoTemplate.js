import React, {useEffect, useState } from 'react';
import {connect, styled} from "frontity";
import Wrapper from "../objects/Wrapper";
import Text from "../objects/Text/Text";
import Layout from "../objects/Layout/Layout";
import {ApolloError} from "@apollo/client";
import LeftContainer from '../components/pages/carrito/Left/LeftContainer'
import RightContainer from "../components/pages/carrito/Right/RightContainer";
import EmptyBag from '../assets/images/icons/emptybag.svg'
import { PayPalButton } from "react-paypal-button-v2";
import Tienda from '../components/modulos/Tienda/tienda'
import Button from "../objects/Button/Button";
import Skeleton, {SkeletonTheme} from "react-loading-skeleton";
import FinalizarCompraSummary from "../components/pages/finalizar-compra/FinalizarCompraSummary";
import StepResumen from "../components/pages/finalizar-compra/Steps/StepResumen";
import Steps from "../components/objects/Steps/Steps";


const CarritoTemplate = ({state, libraries, actions, ...props}) => {

  const [order, setOrder] = useState(false)
  const [orderDetail, setOrderDetail] = useState(false)

  useEffect(() => {
    window.scrollTo(0, 0)

    async function fetchData() {
      const r = await actions.auth.authProcessAuthToken();
      if(state && !state.cart) {
        const r = await libraries.cart.getCart();

        if (r instanceof ApolloError) {
          let errorMessage = r.message;
          actions.theme.setAlertMessage(errorMessage, false, 'error');
        }else{

          state.cart = r.data.cart
        }
      }
    }
    fetchData()
  });

  const OrderDetail = async () => {
    const b =  await order.get();
    setOrderDetail(b);

    state.theme.checkoutData = {
      user: {
        email : b.payer.email_address
      },
      paymentMethodStep: {
        isValid: true,
        method : {
          type: 'paypal_express'
        }
      },
      billingShippingStep : {
        isValid: true,
        shipping : {
          firstName: b.purchase_units[0].shipping.name.full_name,
          lastName: '',
          address: b.purchase_units[0].shipping.address.address_line_1,
          postcode: b.purchase_units[0].shipping.address.postal_code,
          city: b.purchase_units[0].shipping.address.admin_area_2,
          state: b.purchase_units[0].shipping.address.admin_area_1,
        }
      }
    };

    return b;
  }

  order && !orderDetail ? OrderDetail() : null;

  const SquareButtonProps = {
    tipo: "primary",
    tamano: "squared",
    texto: "SEGUIR COMPRANDO",
  }

  Header.defaultProps = {
    breakPoints: state.theme.breakPoints
  }


  HeaderBodyText.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  HeaderBodyTextTramita.defaultProps = {
    breakPoints: state.theme.breakPoints
  }


  CarritoWrapper.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  Content.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  return (
    <CarritoWrapper wrapper={"Default"} align="flex-start" direction="column">

      {order && <Steps />}

      {!order ? <HeaderBodyText data={{texto:  'TU CESTA'}} defaultData={{font_type: 'H1'}}/> : <HeaderBodyTextTramita data={{texto: 'TRAMITAR PEDIDO'}} defaultData={{font_type: 'H1'}}/>}
      {
        state.cart
        ?
          state.cart && state.cart.contents.itemCount > 0 ?
            <Content justify={"space-between"} align={"flex-start"} direction={"column"}>
              {order ?
                <Layout direction={"column"} align={"flex-start"}>

                  <GridStyled>
                    <div>
                      <FinalizarCompraSummary editable={false}/>
                    </div>
                    <div>
                      <StepResumen onSubmit={async () => {
                        actions.theme.setModalOpened(true);
                        const wpOrder = await libraries.cart.checkout(orderDetail);
                        const updateOrder = [{
                            op:    'add',
                            path:  '/purchase_units/@reference_id==\'REF1\'/invoice_id',
                            value: 'WC_'+wpOrder.data.checkout.order.databaseId
                          },
                          {
                            op:    'add',
                            path:  '/purchase_units/@reference_id==\'REF1\'/custom_id',
                            value: JSON.stringify({"order_id":wpOrder.data.checkout.order.databaseId,"order_number":wpOrder.data.checkout.order.databaseId,"order_key":wpOrder.data.checkout.order.orderKey}),
                          }
                        ];
                        await order.patch(updateOrder);
                        const t = await order.capture();

                        const forOrder = {
                          data: wpOrder.data.checkout.order,
                          isGuest: true,
                          cart: state.cart
                        };


                        localStorage.removeItem('for-order');
                        localStorage.setItem('for-order', JSON.stringify(forOrder));
                        await actions.analytics.setTransaction(wpOrder.data.checkout.order.databaseId)
                        actions.theme.setModalOpened(false);
                        await actions.router.set('/pedido-finalizado/');
                      }}/>
                    </div>
                  </GridStyled>
              </Layout> : null}
              <Left order={order}/>
              <Right order={order} setOrder={setOrder} Button={PayPalButton}/>
            </Content>
            :
            <Layout>
              <CestaVacia data={{section_background_color: 'gray-base'}} direction={"column"}>
                <img src={EmptyBag} />
                <Text data={{texto: "Tu cesta está vacía"}} defaultData={{font_type: 'SubtitleLarge', font_family: 'Lexend Zetta'}}/>
                <FullButton data={SquareButtonProps} onClick={ () => { actions.router.set('/') }}/>
              </CestaVacia>
              <Tienda data={{section_config: ''}}/>
            </Layout>
        :
          <SkeletonTheme color="#ecf5f8" highlightColor="#c3dee7">
            <Skeleton height={500} count={2}/>
          </SkeletonTheme>

      }

    </CarritoWrapper>

  )
};

export default connect(CarritoTemplate);



const GridStyled = styled.div`
  --checkout-summary-width: 400px;
  display: grid;
  grid-gap: 125px;
  grid-template-columns: var(--checkout-summary-width) 1fr;
`


const Content = styled(Layout)`
  flex-direction: column;
  ${props => {
  return `
          @media ${props.breakPoints['tablet-wide']} {
            flex-direction: row;
          }`
      }
    }  
`

const Left = styled(LeftContainer)`
  
  ${props =>
    props.order !== false ? `display: none;` : `display: block;`
  }
`

const Right = styled(RightContainer)`  
  ${props =>
  props.order !== false ? `display: none;` : `display: block;`
  }
`


const Header = styled(Layout)`
  padding-top: 10px;
  padding-bottom: 10px;
  ${props => {
  return `
          @media ${props.breakPoints['tablet-wide']} {
            padding-top: 24px;
            padding-bottom: 24px;
          }`
      }
    }  
  
`


const HeaderBodyText = styled(Text)`
  display: none;
  ${props => {    
    return `
          @media ${props.breakPoints['tablet-wide']} {
            display: flex;
            margin-top: 40px;          
          }`
}
  }  
  `

const HeaderBodyTextTramita = styled(HeaderBodyText)`
  margin-bottom: 43px;
`

const CarritoWrapper = styled(Wrapper)`  
  ${props => {
  return `
          @media ${props.breakPoints['tablet-wide']} {
            padding-bottom: 88px;          
          }`
      }
    }  
`

const CestaVacia = styled(Layout)`
  max-width: 820px;
  padding: 32px 16px;
`

const FullButton = styled(Button)`
  width: 100%;
  max-width: 312px;
  margin-top: 24px;
  border: 1px solid var(--color-primary-base)
`

