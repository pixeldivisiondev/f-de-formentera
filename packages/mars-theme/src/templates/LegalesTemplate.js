import {connect, styled} from "frontity";
import Section from "../objects/Section/Section";
import Layout from "../objects/Layout/Layout";
import Text from "../objects/Text/Text";
import RichText from "../objects/RichText/RichText";

const LegalesTemplate = ({state, libraries, actions, ...props}) => {
  const data = state.source.get(state.router.link);
  const post = state.source[data.type][data.id];

  return (
    <StyledSection wrapper={"Stretch"} justify={"flex-start"} align={"flex-start"}>
      <StyledPageTitle data={{texto: post.title.rendered, font_type: 'PageTitle', font_family: 'Lexend Zetta'}} />
      {
        post.acf.bloques.map((bloque, key) => {
          return (
            <Layout  justify={"flex-start"} key={key}>
              <StyledTitleLayout justify={"flex-start"}>
                <StyledTitle data={{texto: bloque.titulo, font_type: 'PrimaryMedium'}} />
                <StyledRichText data={{texto: bloque.text, font_family: 'Lexend Deca', font_type: 'PrimaryMedium'}} />
              </StyledTitleLayout>
            </Layout>
          )
        })
      }
    </StyledSection>

  )
};

export default connect(LegalesTemplate);

const StyledSection = styled(Section)`
padding-bottom: 74px;
`


const StyledTitleLayout = styled(Layout)` 
  
`

const StyledPageTitle = styled(Text)`
  text-transform: uppercase;
  margin-bottom: 16px;
  margin-top: 16px;
`

const StyledTitle = styled(Text)`
  margin-bottom: 8px;
`
const StyledRichText = styled(RichText)`
opacity: 0.6;
p {
  margin-bottom: 16px;
  
}
`
