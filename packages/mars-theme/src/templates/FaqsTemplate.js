import {connect, styled} from "frontity";
import Section from "../objects/Section/Section";
import Layout from "../objects/Layout/Layout";
import Text from "../objects/Text/Text";

import MiCuentaNavigationItem from '../components/pages/faqs/FaqItem'
import RichText from "../objects/RichText/RichText";
const FaqsTemplate = ({state, libraries, actions, ...props}) => {

  const data = state.source.get(state.router.link);
  const post = state.source[data.type][data.id];

  return (
    <StyledSection wrapper={"Stretch"} justify={"flex-start"}>
      <StyledPageTitle data={{texto: post.title.rendered, font_type: 'PageTitle', font_family: 'Lexend Zetta'}} />
      {
        post.acf.faqs.map((section, key) => {
          return (
            <Layout  justify={"flex-start"} key={key}>
              <StyledTitleLayout justify={"flex-start"}>
                <img src={section.icono} />
                <StyledTitle data={{texto: section.titulo, font_type: 'ButtonLarge'}} />
              </StyledTitleLayout>

              <Layout>
                {
                  section.preguntas.map((faq, index) => {
                    return (
                      <MiCuentaNavigationItem
                        title={faq.pregunta}
                        isOpen={false}
                        key={index}
                      >
                        <Layout direction="column" align="flex-start">
                          <StyledRichText data={{font_family: 'Lexend Deca', texto: faq.respuesta}}/>
                        </Layout>
                      </MiCuentaNavigationItem>
                    )
                  })
                }
              </Layout>
            </Layout>
          )
        })
      }
    </StyledSection>

  )
};

export default connect(FaqsTemplate);

const StyledSection = styled(Section)`
padding-bottom: 74px;
`

const StyledRichText = styled(RichText)`
  p {
    margin-bottom: 16px;
  }
  ul {
    margin-bottom: 16px;
    margin-left: 24px;
  }
  a {
    color: var(--color-blue-light);
    text-decoration: none; 
  }
`

const StyledTitleLayout = styled(Layout)`
  margin-top: 32px;
  border-bottom: 1px solid #E5EDEF;
  padding-bottom: 16px;
`

const StyledPageTitle = styled(Text)`
  text-transform: uppercase;
  margin-bottom: 24px;
  margin-top: 16px;
`

const StyledTitle = styled(Text)`
  text-transform: uppercase;  
  margin-left: 8px;
`
