import React from 'react';
import {connect, styled} from "frontity";

import Intro from "../components/pages/reestablecer-password/Intro";
import Wrapper from "../objects/Wrapper";
import LostPasswordModalContent from "../components/pages/login/LoginModal/LostPasswordModalContent";
import FormReestablecerPassword from "../components/pages/reestablecer-password/FormReestablecerPassword";

const ReestablecerContrasenaTemplate = ({state, actions, libraries, ...props}) => {
  WrapperStyled.defaultProps = {
    theme: state.theme
  };

  const links = libraries.source.parse(state.router.link);
  const Reset = links.queryString !== "";

  return (
    <WrapperStyled wrapper={"Stretch"} align="flex-start" direction="column">

      { Reset
        ? <>
            <Intro/>
            <FormReestablecerPassword/>
         </>
        : <LostPasswordModalContent hideLogin={true}/>
      }



    </WrapperStyled>
  )
};

export default connect(ReestablecerContrasenaTemplate);

const WrapperStyled = styled(Wrapper)`
  padding: 28px 16px;

  ${props => {
  return `
      @media ${props.theme.breakPoints['tablet']} {
        padding: 28px 16px 60px;
      }
    `
}}
`;
