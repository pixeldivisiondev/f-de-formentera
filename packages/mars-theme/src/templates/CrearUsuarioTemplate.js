import React from 'react';
import {connect} from "frontity";

import MiCuentaContainer from "../components/pages/mi-cuenta/MiCuentaContainer";
import FormRegistro from "../components/pages/registro/FormRegistro";

const CrearUsuarioTemplate = ({state, actions, ...props}) => {

  return (
    <MiCuentaContainer current="mi-cuenta/crear-usuario/">
      <FormRegistro forEmpresa={true}/>
    </MiCuentaContainer>
  )
};

export default connect(CrearUsuarioTemplate);
