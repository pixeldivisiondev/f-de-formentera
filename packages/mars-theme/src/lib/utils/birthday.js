export const getListDays = () => {
  let r = [];

  for (var i = 1; i <= 31; i++) {
    const value = i.toString();
    r.push({value: value, label: value});
  }

  return r;
}

export const getListMonths = () => {
  return [
    {value: "1", label: "Enero"},
    {value: "2", label: "Febrero"},
    {value: "3", label: "Marzo"},
    {value: "4", label: "Abril"},
    {value: "5", label: "Mayo"},
    {value: "6", label: "Junio"},
    {value: "7", label: "Julio"},
    {value: "8", label: "Agosto"},
    {value: "9", label: "Septiembre"},
    {value: "10", label: "Octubre"},
    {value: "11", label: "Noviembre"},
    {value: "12", label: "Diciembre"}
  ];
}

export const getListYears = () => {
  let r = [];

  var date = new Date();
  date.setDate(date.getDate() - (365 * 18));

  for (var i = 0; i <= 100; i++) {
    const currentYear = date.getFullYear();
    const value = (currentYear - i).toString();

    r.push({value: value, label: value});
  }

  return r;
}

export function findIndex(arr, value) {
  const found = arr.filter(item => item.value === value);

  return arr.indexOf(found[0]);
}
