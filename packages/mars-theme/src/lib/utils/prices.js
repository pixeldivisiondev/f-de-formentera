export const parsePrice = (price) => {
  var explode = price.split(".");
  if (!explode[1]) {
    explode[1] = '00';
  }
  const data = {
    int: explode[0],
    decimal: explode[1]
  }
  return data;
}


export const parsePriceToFloat = (price) => {
  var mPrice = price.replace(/[^0-9.,-]+/g, "");

  return parseFloat(price);
}
