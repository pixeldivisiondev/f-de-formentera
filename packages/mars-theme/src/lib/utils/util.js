export function findMetaData(array, key) {
  if (array) {
    const found = array.find(x => x.key === key);
    return found?.value;
  } else {
    return null;
  }
}