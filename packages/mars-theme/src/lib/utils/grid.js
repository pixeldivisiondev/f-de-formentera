export const getGridWidthByColumns = i => {
  if (i !== undefined) {
    return (i * (100 / 12)).toFixed(2) + '%';
  } else {
    return '100%';
  }
};