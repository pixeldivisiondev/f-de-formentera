import {findMetaData} from "../utils/util";

export const prepareBillingShippingInfo = data => {
  const billingTipoVia = findMetaData(data?.metaData, 'billing_tipo_via');
  const billingStreet = findMetaData(data?.metaData, 'billing_street');
  const billingNumero = findMetaData(data?.metaData, 'billing_numero');
  const billingPiso = findMetaData(data?.metaData, 'billing_piso');

  const shippingTipoVia = findMetaData(data?.metaData, 'shipping_tipo_via');
  const shippingStreet = findMetaData(data?.metaData, 'shipping_street');
  const shippingNumero = findMetaData(data?.metaData, 'shipping_numero');
  const shippingPiso = findMetaData(data?.metaData, 'shipping_piso');
  const shippingPhone = findMetaData(data?.metaData, 'shipping_phone');

  return {
    billing_first_name: data?.billing.firstName,
    billing_last_name: data?.billing.lastName,
    billing_postcode: data?.billing.postcode,
    billing_city: data?.billing.city,
    billing_phone: data?.billing.phone,
    billing_state: data?.billing.state,
    billing_tipo_via: billingTipoVia,
    billing_street: billingStreet,
    billing_numero: billingNumero,
    billing_piso: billingPiso,
    shipping_first_name: data?.shipping.firstName,
    shipping_last_name: data?.shipping.lastName,
    shipping_postcode: data?.shipping.postcode,
    shipping_city: data?.shipping.city,
    shipping_state: data?.shipping.state,
    shipping_tipo_via: shippingTipoVia,
    shipping_street: shippingStreet,
    shipping_numero: shippingNumero,
    shipping_piso: shippingPiso,
    shipping_phone: shippingPhone,
    use_same_address: true
  };
}

export const fillBillingShippingStep = (newCheckoutData, data) => {
  newCheckoutData.billingShippingStep = {
    isValid: false,
    shipping: {
      firstName: data.shipping_first_name,
      lastName: data.shipping_last_name,
      address: data.shippingAddress,
      postcode: data.shipping_postcode,
      city: data.shipping_city,
      state: data.shipping_state,
      phone: data.shipping_phone,
      tipoVia: data.shipping_tipo_via,
      street: data.shipping_street,
      numero: data.shipping_numero,
      piso: data.shipping_piso,
      email: data.shipping_email,
    },
    billing: {
      firstName: !data.use_same_address ? data.billing_first_name : data.shipping_first_name,
      lastName: !data.use_same_address ? data.billing_last_name : data.shipping_last_name,
      address: !data.use_same_address ? data.billingAddress : data.shippingAddress,
      postcode: !data.use_same_address ? data.billing_postcode : data.shipping_postcode,
      phone: !data.use_same_address ? data.billing_phone : data.shipping_phone,
      city: !data.use_same_address ? data.billing_city : data.shipping_city,
      state: !data.use_same_address ? data.billing_state : data.shipping_state,
      tipoVia: !data.use_same_address ? data.billing_tipo_via : data.shipping_tipo_via,
      street: !data.use_same_address ? data.billing_street : data.shipping_street,
      numero: !data.use_same_address ? data.billing_numero : data.shipping_numero,
      piso: !data.use_same_address ? data.billing_piso : data.shipping_piso,
    },
    shipToDifferentAddress: !data.use_same_address
  }



  return newCheckoutData;
}

export const prepareInputCheckoutData = (data, freeShipping) => {
  const userData = data.user;
  const billingData = data.billingShippingStep.billing;
  const shippingData = data.billingShippingStep.shipping;
  const methodType = data.paymentMethodStep.method.type;

  return {
    billing: {
      email: userData.email,
      firstName: billingData.firstName,
      lastName: billingData.lastName,
      address1: billingData.address,
      city: billingData.city,
      phone: billingData.phone,
      postcode: billingData.postcode,
      state: billingData.state,
      country: "ES",
    },
    shipping: {
      email: shippingData.email ? shippingData.email : userData.email,
      firstName: shippingData.firstName,
      lastName: shippingData.lastName,
      address1: shippingData.address,
      city: shippingData.city,
      phone: billingData.phone,
      postcode: shippingData.postcode,
      state: shippingData.state,
      country: "ES",
    },
    shipToDifferentAddress: billingData.shipToDifferentAddress || false,
    paymentMethod: methodType,
    //isPaid: true,
    customerNote: billingData.notes || "",
    shippingMethod: (freeShipping ? "free_shipping" : "mrw"),
    metaData: [
      {key: "is_vat_exempt", value: "no"}
    ]
  };
}

export const formatCard = card => {
  if (!card) return 'ERROR en la tarjeta';

  const expiredDate = card.exp_month.toString().padStart(2, "0") + "/" + card.exp_year;

  return card.brand.toUpperCase() + " que termina en " + card.last4 + " - Cad: " + expiredDate;
}
