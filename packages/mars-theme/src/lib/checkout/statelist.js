export function getStateList() {
  return [
    {value: "C", label: "A Coruña"},
    {value: "VI", label: "Álava"},
    {value: "AB", label: "Albacete"},
    {value: "A", label: "Alicante"},
    {value: "AL", label: "Almería"},
    {value: "O", label: "Asturias"},
    {value: "AV", label: "Ávila"},
    {value: "BA", label: "Badajoz"},
    {value: "PM", label: "Baleares"},
    {value: "B", label: "Barcelona"},
    {value: "BI", label: "Bizkaia"},
    {value: "BU", label: "Burgos"},
    {value: "CC", label: "Cáceres"},
    {value: "CA", label: "Cádiz"},
    {value: "S", label: "Cantabria"},
    {value: "CS", label: "Castellón"},
    {value: "CR", label: "Ciudad Real"},
    {value: "CO", label: "Córdoba"},
    {value: "CU", label: "Cuenca"},
    {value: "SS", label: "Gipuzkoa"},
    {value: "GI", label: "Girona"},
    {value: "GR", label: "Granada"},
    {value: "GU", label: "Guadalajara"},
    {value: "H", label: "Huelva"},
    {value: "HU", label: "Huesca"},
    {value: "J", label: "Jaén"},
    {value: "LO", label: "La Rioja"},
    {value: "GC", label: "Las Palmas"},
    {value: "LE", label: "León"},
    {value: "L", label: "Lleida"},
    {value: "LU", label: "Lugo"},
    {value: "M", label: "Madrid"},
    {value: "MA", label: "Málaga"},
    {value: "MU", label: "Murcia"},
    {value: "NA", label: "Navarra"},
    {value: "OR", label: "Ourense"},
    {value: "P", label: "Palencia"},
    {value: "PO", label: "Pontevedra"},
    {value: "SA", label: "Salamanca"},
    {value: "TF", label: "Santa Cruz de Tenerife"},
    {value: "SG", label: "Segovia"},
    {value: "SE", label: "Sevilla"},
    {value: "SO", label: "Soria"},
    {value: "T", label: "Tarragona"},
    {value: "TE", label: "Teruel"},
    {value: "TO", label: "Toledo"},
    {value: "V", label: "Valencia"},
    {value: "VA", label: "Valladolid"},
    {value: "ZA", label: "Zamora"},
    {value: "Z", label: "Zaragoza"}
  ]
}

export function getStateName(value) {
  const stateList = getStateList();

  const state = stateList.find(s => s.value === value);

  if (state) {
    return state.label;
  } else {
    return value;
  }
}

export function findIndexState(value) {
  const stateList = getStateList();

  const found = stateList.filter(item => item.value === value);

  return stateList.indexOf(found[0]);
}
