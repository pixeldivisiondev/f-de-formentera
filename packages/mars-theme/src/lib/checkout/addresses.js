import React from 'react';
import {getStateName} from "./statelist";
import Text from "../../objects/Text/Text";

export const formatAddress = (data, extraText) => {
  const address = data?.address ? data?.address : data?.address1;

  let text = data?.firstName + " " + data?.lastName + "<br/>" +
    address + ", " + data?.postcode + ", " + data?.city + " (" + getStateName(data?.state) +  ")";

  /*if (!address) {
    text = 'Todavía no has definido ninguna dirección ' + extraText
  }*/

  return (
    <Text data={{texto: text}}/>
  )
}

export const getListTipoVia = () => {
  return [
    {value: "Avenida", label: "Avenida"},
    {value: "Calle", label: "Calle"},
    {value: "Camino", label: "Camino"},
    {value: "Carretera", label: "Carretera"},
    {value: "Parque", label: "Parque"},
    {value: "Plaza", label: "Plaza"},
    {value: "Travesía", label: "Travesía"},
  ];
}
