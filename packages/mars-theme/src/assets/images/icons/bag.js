export default function BagIcon(props) {
  return (
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" className={props.className}>
      <rect x="4" y="7" width="16" height="15" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round"/>
      <path d="M7 7C7 4.23858 9.23858 2 12 2C14.7614 2 17 4.23858 17 7" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round"/>
    </svg>
  )
}
