import axios from 'axios';
import addOAuthInterceptor from "axios-oauth-1.0a";

const instance = axios.create({
    method: 'get',
    baseURL: 'http://localhost/frontityBack/wp-json/wc/v3/',
    auth: {
        'username': 'ck_9b34a462df7a40182e7df19d4eb2ffc7c9878f18',
        'password': 'cs_7fcf8bc682c0f3c5b6da140eda8cb8b99c10424a',
    },
});

addOAuthInterceptor(instance, {
    // OAuth consumer key and secret
    key: "ck_9b34a462df7a40182e7df19d4eb2ffc7c9878f18",
    secret: "cs_7fcf8bc682c0f3c5b6da140eda8cb8b99c10424a",

    // HMAC-SHA1 and HMAC-SHA256 are supported
    algorithm: "HMAC-SHA1",
});

export default instance
