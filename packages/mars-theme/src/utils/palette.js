const palette = ({palette, type, property}) => {
  return palette[property.split("-")[0]][property.split("-")[1]].color
}

export default palette;
