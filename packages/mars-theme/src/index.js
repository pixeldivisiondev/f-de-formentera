import Theme from "./components";
import image from "@frontity/html2react/processors/image";
import iframe from "@frontity/html2react/processors/iframe";
import richtext from './objects/RichText/Processor.tsx'
import axios from './utils/axiosUpdater'

const menuHandler = {
  name: 'menus',
  priority: 10,
  pattern: '/menus/:slug', // You can use something shorter here, you don't need to use the endpoint
  func: async ({ route, params, state, libraries }) => {
    const { api } = libraries.source
    const { slug } = params

    // 1. fetch the data you want from the endpoint page
    const response = await api.get({
       endpoint: `/menus/v1/menus/${slug}` // Instead of using params, this plugin requires a slug be part of the route. We are also using "locations" to get menu locations instead of a specific menu
    })

    // 2. get an array with each item in json format
    const items = await response.json()

    // 3. add data to source
    const currentPageData = state.source.data[route]

    Object.assign(currentPageData, {
      slug,
      items: items.items, // @ni.bonev figured this one out because the "items" property contains "items" that are the menu items we are after
      isMenu: true
    })
  }


}


const viewOrder = {
  name: 'vieworder',
  priority: 10,
  pattern: '/mi-cuenta/ver-pedido/:id', // You can use something shorter here, you don't need to use the endpoint
  func: async ({ route, params, state, libraries, force }) => {
    const { id } = params;
    Object.assign(state.theme, {'viewOrder' :  id});
  }

}
const optionsHandler = {
  name: 'options',
  priority: 10,
  pattern: '/myplugin/v1/options', // You can use something shorter here, you don't need to use the endpoint
  func: async ({ link, params, state, libraries, force }) => {
    // 1. get product
    const response = await libraries.source.api.get({
      endpoint: "/myplugin/v1/options"
    });

    const items = await response.json()

    /* ADD THEME GENERIC MODULES ACF */
    Object.assign(state.theme, {genericModules :  items.grupo_Comunidad});

    /* ADD THEME BREAKPOINTS */
    items.breakpoints.value.map(item => {
      Object.assign(state.theme, {breakPoints : {
          ...state.theme.breakPoints, [item.id] : `(min-width: ${item.anchura})`
        }});
    })

    /* ADD THEME WRAPPERS */
    items.wrappers.value.map(item => {
      let responsive = {}
      item.settings.map(res => {
        responsive[res.breakpoint] = res;
      })
      Object.assign(state.theme, {wrappers : {
          ...state.theme.wrappers, [item.nombre] : responsive
        }});
    })

    /* ADD THEME colors */
    Object.assign(state.theme, {primaryColor : items.color_primary.value});
    Object.assign(state.theme, {secondaryColor : items.color_secundario.value});

    /* ADD HEADERS THEME PROPS */
    items.headers.value.map(item => {
      let responsive = {}
      item.resposive.map(res => {
        responsive[res.breakpoint] =  {...res}  ;
      })
      Object.assign(state.theme, {fontTypes : {
        ...state.theme.fontTypes, [item.tipo] : responsive
      }});
    })

    /* ADD THEME BUTONS TYPES PROPS */
    items.tipos_botone.value.tipos_botons.map(item => {
      let a = {};
      if(state.theme.buttons) {
        a = state.theme.buttons.types;
      }
      Object.assign(state.theme, {buttons : {
        ...state.theme.buttons, types : {...a, [item.nombre] : item.propiedades_botons}
      }});
    })

    /* ADD THEME BUTONS SIZES PROPS */
    items.tamanos_botones.value.tamanos_buttons.map(item => {
      let a = {};
      if(state.theme.buttons) {
        a = state.theme.buttons.sizes;
      }
      Object.assign(state.theme, {buttons : {
          ...state.theme.buttons, sizes : {...a, [item.nombre] : item.popiedades}
        }});
    })

    /* ADD PALETTE COLOR */
    items.paleta_de_colores.value.map(item => {
      let a = {};
      if(state.theme.palette && state.theme.palette[item.nombre] ) {
        a = state.theme.palette[item.nombre]
      }
      item.colores.map(res => {
        Object.assign(state.theme, {palette : {
            ...state.theme.palette, [item.nombre] : {...a, [res.nombre] : {color: res.color}}
        }});
        a = state.theme.palette[item.nombre]
      })
    })
  },
}

const optionsHandlerACF = {
  name: 'options',
  priority: 10,
  pattern: '/myplugin/v1/optionsACF', // You can use something shorter here, you don't need to use the endpoint
  func: async ({ link, params, state, libraries, force }) => {
    const { api } = libraries.source
    const { slug } = params

    // 1. fetch the data you want from the endpoint page
    const response = await api.get({
      endpoint: `/myplugin/v1/optionsACF` // Instead of using params, this plugin requires a slug be part of the route. We are also using "locations" to get menu locations instead of a specific menu
    })

    // 2. get an array with each item in json format
    const items = await response.json()

    Object.assign(state.theme, {'optionsACF' :  items});
  },
}

const getPedidos = {
  name: 'pedido',
  priority: 10,
  pattern: '/pedidos/:slug', // You can use something shorter here, you don't need to use the endpoint
  func: async ({ link, params, state, libraries, force }) => {
    Object.assign(state.theme, {'pedido' :  'asd'});
  },
}

const marsTheme = {
  name: "@frontity/mars-theme",
  roots: {
    /**
     *  In Frontity, any package can add React components to the site.
     *  We use roots for that, scoped to the `theme` namespace.
     */
    theme: Theme,
  },
  state: {
    /**
     * State is where the packages store their default settings and other
     * relevant state. It is scoped to the `theme` namespace.
     */

    tokenStatus: null,
    modalOpened: false,
    theme: {
      modalOpened: false,
      menu: [],
      isMobileMenuOpen: false,
      gridSpacing: '4px',
      featured: {
        showOnList: false,
        showOnPost: false,
      },
      alertMessage: {
        message: null,
        opened: false,
        isModal: false,
        type: 'error'
      },
      login: {
        firstName: null
      },
      checkoutStep: 1,
      checkoutData: {
        user: null,
        billingShippingStep: null,
        paymentMethodStep: null,
      }
    },
  },
  /**
   * Actions are functions that modify the state or deal with other parts of
   * Frontity like libraries.
   */
  actions: {
    theme: {
      updateComponent: ({ state, ...props }) => {


      },
      toggleMobileMenu: ({ state }) => {
        state.theme.isMobileMenuOpen = !state.theme.isMobileMenuOpen;
      },
      closeMobileMenu: ({ state }) => {
        state.theme.isMobileMenuOpen = false;
      },
      beforeSSR: ({ actions }) => async () => {
        await actions.source.fetch("/menus/footer");
        await actions.source.fetch("/menus/SubFooter");
        await actions.source.fetch("/menus/Main");
        await actions.source.fetch("/myplugin/v1/options");
        await actions.source.fetch("/myplugin/v1/optionsACF");

      },
      afterSSR: ({ actions }) => async () => {
        //await actions.source.fetch("/myplugin/v1/options");
        //actions.analytics.sendPageView();

      },
      setModalOpened: ({state}) => (data) => {
        console.log(data, 'OPENEDMODAL')
        state.modalOpened = data;
        state.theme.modalOpened = data
      },
      setAlertMessage: ({ state, actions }) => (message, isModal, type) => {
        state.theme.alertMessage = {
          message: message,
          opened: true,
          isModal: isModal,
          type: type
        };

        setTimeout(() => {
          actions.theme.clearAlertMessage();
        }, 3000);


      },
      setRole: ({state}) => (role) => {
        state.theme.login.role = role
      },
      logOut: ({state}) => {
        state.theme.checkoutData = {
            user: null,
            billingShippingStep: null,
            paymentMethodStep: null,
        }
        state.tokenStatus = null;
      },
      clearAlertMessage: ({ state }) => {
        state.theme.alertMessage = {
          message: null,
          opened: false,
          isModal: false,
          type: 'error'
        };
      },
      setLoginName: ({state}) => name => {
        localStorage.setItem('login-name', name);
        state.theme.login.firstName = name;
      },

      setCheckoutData: ({state}) => data => {
        if(data === null) {
          state.theme.checkoutData = {
            user: null,
            billingShippingStep: null,
            paymentMethodStep: null,
          }
        }else{
          state.theme.checkoutData = data;
        }

      },
      setCheckoutStep: ({state}) => step => {
        state.theme.checkoutStep = step;
      }
    },
  },

  libraries: {
    html2react: {
      /**
       * Add a processor to `html2react` so it processes the `<img>` tags
       * inside the content HTML. You can add your own processors too
       */
      processors: [image, iframe, richtext],
    },
    source: {
      handlers: [menuHandler, optionsHandler, optionsHandlerACF, getPedidos, viewOrder]
    },
    login: {
      getLoginName: () => {
        if (typeof window !== 'undefined') {
          return localStorage.getItem('login-name');
        }


      }
    }
  },
};

export default marsTheme;
