import { styled, connect} from "frontity";
import FrLink from "@frontity/components/link";
import paletteHelper from '../../utils/palette';

const Link = (props) => {

    let {data , defaultData } = props;

    let componentData = data;
    const test = {...data}
    delete(test['enlace']);

     if(defaultData) {
        Object.keys(test).filter(k => {
            return test[k] === 'object' ||  (test[k] === '' )
        }).forEach(k =>  { delete(test[k]) })
    }else{
         defaultData = {
             font_color: "primary-base",
             font_family: "Alliance",
             font_type: "Link",
             texto: "ver todos"
         }
     }

    componentData = {...defaultData, ...test}

    CustomLink.defaultProps = {
        theme: props.state.theme,
        data: componentData
    }

    return (
        <CustomLink  className={props.className} link={ props.link} >
            {props.data.texto}
        </CustomLink>
    )
}


const CustomLink = styled(connect(FrLink))`
    text-decoration: none;
    ${props => {    
        const type = props.data.font_type;
        return Object.keys(props.theme.fontTypes[type]).map((font) => {
            return `
                @media ${props.theme.breakPoints[font]} {
                    font-size: ${props.theme.fontTypes[type][font].tamano_de_fuente} ;
                    line-height: ${props.theme.fontTypes[type][font].line_height} ;
                    letter-spacing: ${props.theme.fontTypes[type][font].letter_spacing} ;
                }`
            
        })        
    }}
    
    ${props => {
        const breakpoints = props.data.layout_breakpoint;
        return breakpoints && breakpoints.map((breakpoint) => `
                @media ${props.theme.breakPoints[breakpoint.breakpoint]} {
                    ${Object.keys(breakpoint.properties).map(property => {
                        return `${property}: ${breakpoint.properties[property]};`;
                    }).join('')              
                }`            
        ).join('')
    }}
    
    color: ${props => paletteHelper({palette: props.theme.palette, type: 'primary', property: props.data.font_color})};
    font-family: ${props => props.data.font_family};
    font-weight: ${props => props.data.font_weight};     
`;

export default connect(Link);
