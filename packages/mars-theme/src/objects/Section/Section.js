import {styled, connect} from "frontity";
import Layout from '../Layout/Layout'
import Wrapper from "../Wrapper";

const Section = (props) => {

    let {data , defaultData } = props;
    if (!defaultData) {
        defaultData = {};
    }


    let componentData = data
    const test = {...data}
    if(defaultData) {
        Object.keys(test).filter(k => {
            return  test[k] === 'object' || (test[k] === '')
        }).forEach(k =>  { delete(test[k]) })
        componentData = {...defaultData, ...test}
    }

    return (
        <Layout className={props.className} type={"section"}  data={componentData}>
            <Wrapper wrapper={props.wrapper} align={props.align} justify={props.justify} direction={props.direction} >
                {props.children}
            </Wrapper>
        </Layout>
    )
}


export default connect(Section);



