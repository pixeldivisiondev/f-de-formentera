import { FlexBox } from '../'
import { styled, connect } from "frontity";
import React, { useState } from 'react';
import  Text from '../Text'

const ConfigForm = ({state, actions, ...props}) => {


    const [backgroundColor, setBackgroundColor] = useState(props.data.section_background_color);
    const [backgroundImage, setBackgroundImage] = useState(props.data.section_background_image);
    const [wrapper, setWrapper] = useState(props.data.wrapper);


    const inputBackgroundColorChangeHandler = (props) => {
        setBackgroundColor(props.target.value);
    }



    const inputBackgroundImageChangeHandler = (e) => {
        //console.log(props.originalStateData.section_background_image, 'ORIGINAL PROPS')

        setBackgroundImage(JSON.parse(e.target.value));
    }



    const inputWrapperChangeHandler = (props) => {
        setWrapper(props.target.value);
    }

    const changeProps = (e) => {        ;
        props.originalStateData.section_background_color = backgroundColor;
        props.originalStateData.wrapper = wrapper;
        let b = {url: 'http://localhost/frontityBack/wp-content/uploads/2020/11/t-shirt-with-logo-1-1.jpg' , id: 113, ID: 113}
        props.originalStateData.section_background_image = backgroundImage
        actions.theme.updateComponent();
        e.preventDefault();

    }



    const data = state.source.get(state.router.link);



    const TitleDefault = {
        color: 'primary-base',
        font_family: 'Pixel',
        font_type:  'Titulo 2',
        texto: 'Formulario De Configuración',
        tipo: 'h3',
    }

    const LabelDefault = {
        color: 'primary-base',
        font_family: 'Pixel',
        font_type:  'Link',
        texto: 'Formulario De Configuración',
        tipo: 'label',
    }


    return (
        <FlexBox align={"column"}>
            <Text data={{texto: 'Formulario De Configuración'}} defaultData={TitleDefault} editable={false}/>
            <FlexBox >
                <FormWrapper type={"form"} >
                    <FlexBox>
                        <Column justify={"space-between"} >
                            <Text data={{texto: 'Background Color'}} defaultData={LabelDefault} editable={false} />
                            <select name={"fontColor"} onChange={(e) => inputBackgroundColorChangeHandler(e)} value={backgroundColor}>
                                <option value={"primary-base"} >primary-base</option>
                                <option value={"secondary-base"} >secondary-base</option>
                                <option value={"orange-base"} >orange-base</option>
                                <option value={"purple-base"} >purple-base</option>
                            </select>
                        </Column>
                        <Column justify={"space-between"} >

                            <Text data={{texto: 'Wrapper'}} defaultData={LabelDefault} editable={false} />
                            <select name={"fontColor"} onChange={(e) => inputWrapperChangeHandler(e)} value={wrapper}>
                                <option value={"Default"} >Default</option>
                                <option value={"Narrow"} >Narrow</option>
                                <option value={"Stretch"} >Stretch</option>
                                <option value={"FullWidth"} >FullWidth</option>

                            </select>
                        </Column>

                    </FlexBox>
                    <FlexBox>
                        <Column justify={"space-between"} >
                            <Text data={{texto: 'Background Image'}} defaultData={LabelDefault} editable={false} />
                            <select name={"fontColor"} onChange={(e) => inputBackgroundImageChangeHandler(e)} >
                                <option value={JSON.stringify({id: null, url: ""})} >Sin imagen de fondo</option>
                                <option value={JSON.stringify({id: 113, ID: 113, url: "http://localhost/frontityBack/wp-content/uploads/2020/11/t-shirt-with-logo-1-1.jpg"})} >Imagen 1</option>
                                <option value={JSON.stringify({id: 112, ID: 112, url: "http://localhost/frontityBack/wp-content/uploads/2020/11/single-1-1.jpg"})} >Imagen 2</option>
                            </select>
                        </Column>
                    </FlexBox>

                    <FlexBox type={"label"} justify={"flex-start"}>
                        <input onClick={(e) => { changeProps(e) }} type="submit" value={"Enviar"} />
                    </FlexBox>
                </FormWrapper>
            </FlexBox>
        </FlexBox>

    )
}

export default connect(ConfigForm)

const Column = styled(FlexBox)`
    padding: 20px;
    flex: 1 1 50%;
    width: 50%;
    font-size: initial;
    font-weight: initial;
`

const FormWrapper = styled(FlexBox)`
    &&& {
        max-width: 600px;
    }
    
`
