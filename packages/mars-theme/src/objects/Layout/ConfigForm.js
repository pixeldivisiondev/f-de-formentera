import { FlexBox } from '../'
import { styled, connect } from "frontity";
import React, { useState } from 'react';
import  Text from '../Text'

const ConfigForm = ({state, actions, ...props}) => {


    const [fontColor, setColor] = useState(props.data.color);
    const [fontFamily, setColorFamily] = useState(props.data.font_family);
    const [fontStyle, setStyle] = useState(props.data.font_type);
    const [fontType, setType] = useState(props.data.tipo);
    const [fontText, setText] = useState(props.data.texto);

    const inputColorChangeHandler = (props) => {
        setColor(props.target.value);
    }

    const inputFamilyChangeHandler = (props) => {
        setColorFamily(props.target.value);
    }

    const inputStyleChangeHandler = (props) => {
        setStyle(props.target.value);
    }

    const inputTypeChangeHandler = (props) => {
        setType(props.target.value);
    }

    const inputTextChangeHandler = (props) => {
        setText(props.target.value);
    }


    const data = state.source.get(state.router.link);

    const changeText = (e) => {
        props.originalStateData.color = fontColor;
        props.originalStateData.font_family = fontFamily;
        props.originalStateData.font_type = fontStyle;
        props.originalStateData.texto = fontText;
        props.originalStateData.fontType = fontType;
        actions.theme.updateComponent();
        e.preventDefault();

    }

    const TitleDefault = {
        color: 'primary-base',
        font_family: 'Pixel',
        font_type:  'Titulo 2',
        texto: 'Formulario De Configuración',
        tipo: 'h3',
    }

    const LabelDefault = {
        color: 'primary-base',
        font_family: 'Pixel',
        font_type:  'Link',
        texto: 'Formulario De Configuración',
        tipo: 'label',
    }


    return (
        <FlexBox align={"column"}>
            <Text data={{texto: 'Formulario De Configuración'}} defaultData={TitleDefault} editable={false}/>
            <FlexBox >
                <FormWrapper type={"form"} >
                    <FlexBox>
                        <Column justify={"space-between"} >

                            <Text data={{texto: 'Font Color'}} defaultData={LabelDefault} editable={false} />
                            <select name={"fontColor"} onChange={(e) => inputColorChangeHandler(e)} value={fontColor}>
                                <option value={"primary-base"} >primary-base</option>
                                <option value={"secondary-base"} >secondary-base</option>
                                <option value={"orange-base"} >orange-base</option>
                                <option value={"purple-base"} >purple-base</option>
                            </select>
                        </Column>
                        <Column type={"label"} justify={"space-between"}>
                            <Text data={{texto: 'Font Family'}} defaultData={LabelDefault} editable={false} />
                            <select name={"fontColor"}  onChange={(e) => inputFamilyChangeHandler(e)} value={fontFamily}>
                                <option value={"Pixel"} >Pixel</option>
                                <option value={"Alliance"} >Alliabce</option>
                            </select>
                        </Column>
                    </FlexBox>
                    <FlexBox>
                        <Column type={"label"} justify={"space-between"} >
                            <Text data={{texto: 'Font Type'}} defaultData={LabelDefault} editable={false} />
                            <select onChange={(e) => inputTypeChangeHandler(e)} value={fontType}>
                                <option value={"h1"} >h1</option>
                                <option value={"h2"} >h2</option>
                            </select>
                        </Column>
                        <Column  justify={"space-between"}>
                            <Text data={{texto: 'Font Style'}} defaultData={LabelDefault} editable={false} />
                            <select onChange={(e) => inputStyleChangeHandler(e)} value={fontStyle}>
                                <option value={"Titulo Hero"} >Titulo Hero</option>
                                <option value={"Titulo 2"} >Titulo 2</option>
                                <option value={"Link"} >Link</option>
                            </select>
                        </Column>
                    </FlexBox>
                    <FlexBox>
                        <Column type={"label"} justify={"space-between"} >
                            <Text data={{texto: 'Text'}} defaultData={LabelDefault} editable={false} />
                            <input type={"text"} onChange={(e) => inputTextChangeHandler(e)} value={fontText} />
                        </Column>
                    </FlexBox>
                    <FlexBox type={"label"} justify={"flex-start"}>
                        <input onClick={(e) => { changeText(e) }} type="submit" value={"Enviar"} />
                    </FlexBox>
                </FormWrapper>
            </FlexBox>
        </FlexBox>

    )
}

export default connect(ConfigForm)

const Column = styled(FlexBox)`
    padding: 20px;
    flex: 1 1 50%;
    width: 50%;
    font-size: initial;
    font-weight: initial;
`

const FormWrapper = styled(FlexBox)`
    &&& {
        max-width: 600px;
    }
    
`
