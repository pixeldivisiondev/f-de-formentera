import FlexBox from '../FlexBox/FlexBox'
import { styled, connect } from "frontity";
import palette from '../../utils/palette'

const Layout = (props) => {

    StyledFlex.defaultProps = {
        theme: props.state.theme
    }

    return (
        <StyledFlex {...props}>
            {props.children}
        </StyledFlex>
    )
}

export default connect(Layout);

const StyledFlex = styled(connect(FlexBox))`  
  ${props => props.data && props.data.section_background_image && props.data.section_background_image.url ? `background-image: url(${props.data.section_background_image.url});` : null }
  ${props =>  props.data && props.data.section_background_color 
              ? 
                `
                background-position: center;
                background-size: cover;
                background-color: ${palette({palette: props.theme.palette, type: null, property: props.data ? props.data.section_background_color : 'transparent-base' })};
                `
            : null};
`
