import { styled} from "frontity";

const Button = (props) => {
    return (
        <ButtonComponent className={props.type} href="#" >{props.text}</ButtonComponent>
    )
}

export default Button;

const ButtonComponent = styled.a`
  width: auto;  
  padding: 8px 16px;
  color: #fff !important;
  background-color: #F00;
  font-size: 16px;
  text-decoration: none !important;  
  position: relative;
  z-index: 1;
  border-radius: 15px;
  transition: all 0.6s;
`;
