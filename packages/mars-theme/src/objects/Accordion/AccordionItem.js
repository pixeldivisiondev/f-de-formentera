import { styled, connect} from "frontity";
import Text from '../Text'
import React, { useState } from 'react';
import {CloseIcon} from "../../components/menu-icon";

const AccordionItem = (props) => {

    return (
        <div className={props.className}>

            <AccordionHeader>
                <AccordionTitle
                    element={"h1"}
                    onClick={() => {props.onClick(props.index)}} text={props.title}

                >asad</AccordionTitle>
                <CloseItem  onClick={() => {props.onClick(props.index)}}>X</CloseItem>
            </AccordionHeader>

            <div>
                {props.active ? props.body : null}
            </div>
        </div>
    )
}

const AccordionHeader = styled.div`
    color: ${props => props.theme.primaryColor} !important;
    position: relative;
`;

const AccordionTitle = styled(Text)`
    color: ${props => props.theme.primaryColor} !important;
`;

const CloseItem = styled.div`
    position: absolute;
    color: #000;
    right: 0; 
    top: 0;
`;

export default connect(AccordionItem);
