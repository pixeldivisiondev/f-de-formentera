import { styled, connect} from "frontity";
import React, { useState } from 'react';
import AccordionItem from './AccordionItem';

const Accordion = (props) => {

    const [active, setActive] = useState(null);

    const toggleAccordionHandler = (index) => {
        if(index == active) {
            setActive(null);
        }else{
            setActive(index);
        }
    }

    return (
        <AccordionWrapper>
            {
                props.items.map( (item, i) => {
                    return <AccordionItem
                        title={item.title}
                        body={item.body}
                        active={active == i ? true : false}
                        index={i}
                        key={i}
                        onClick={toggleAccordionHandler}/>
                })
            }
        </AccordionWrapper>
    )
}


const AccordionWrapper = styled.div`
    box-shadow: 0 0 5px #CCC;
    padding: 35px;
`;

export default connect(Accordion);
