import React, {useEffect} from 'react';
import {connect, styled} from "frontity";

import Layout from "../Layout/Layout";
import Text from "../Text/Text";

export default connect(function AlertMessage({state, actions, ...props}) {
  const alertMessage = state.theme.alertMessage;


  StyledAlertMessage.defaultProps = {
    theme: state.theme,
    inModal: props.inModal,
    type: alertMessage.type
  };


  const textDefault = {
    color: 'secondary-base',
    font_family: 'Lexend Deca',
    font_type: 'PrimarySmall',
    tipo: 'p',
    font_weight: 400,
    texto: alertMessage.message
  };

  const handleClose = () => {
    actions.theme.clearAlertMessage();
  };

  useEffect(() => {

  }, [alertMessage]);

  return (
    <>
      {alertMessage.opened && (alertMessage.isModal === props.inModal) ? (
        <StyledAlertMessage justify="space-between">
          <Text defaultData={textDefault}/>

          <div onClick={handleClose}>
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
              <g id="Add/close">
                <path id="Path 2" d="M17.6569 17.6568L6.34319 6.34314" stroke="currentColor" strokeLinecap="round"/>
                <path id="Path 2_2" d="M17.6568 6.34314L6.34311 17.6568" stroke="currentColor" strokeLinecap="round"/>
              </g>
            </svg>
          </div>
        </StyledAlertMessage>
      ) : ('')}
    </>
  )
});

const StyledAlertMessage = styled(Layout)`
${props => {
  return `
    background-color: ${props.type === 'error' ? `var(--color-error-base)` : `
var(--color-accept-base)`};
    color: white;
    padding: 16px;
    padding-right: 32px;
    position: relative;
    margin-bottom: 24px;
    width: 100%;
    
    svg {
      cursor: pointer;
      position: absolute;
      top: 12px;
      right: 8px;
    }
  `
}}

${props => {
  return props.inModal ? `
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    min-height: 60px;
    z-index: 99;
    ` : ``;
}} 
`;
