import {styled} from "frontity";

const FlexItem = ({className, children, props}) => (
    <FlexBoxComponent  className={className} {...props} >
        {children}
    </FlexBoxComponent>
)


const FlexBoxComponent = styled.div`
  flex: 1 1 100%;
  height: 100%;
`;

export default FlexItem;
