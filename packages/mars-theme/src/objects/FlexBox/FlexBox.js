import {styled, connect} from "frontity";

const FlexBox = ({state, ...props}) => {
        let Element = props.type ? props.type : 'div';

        FlexBoxComponent.defaultProps = {
            theme: state.theme
        }

        return (
            <Element className={props.className} align={props.align} justify={props.justify} direction={props.direction} onClick={() => props.onClick ? props.onClick() : null}>
                {props.children}
            </Element>)


}



const FlexBoxComponent = styled(connect(FlexBox))`
  display: flex;
  flex-wrap: wrap;  
  width: 100%;
  align-self: stretch;
  align-items: ${props => props.align ? props.align : 'center' };
  justify-content: ${props => props.justify ? props.justify : 'center' };
  flex-direction: ${props => props.direction ? props.direction : 'row'};  
  ${props => {
    const breakpoints = props.data ? props.data.layout_breakpoint : null;
    return breakpoints && breakpoints.map((breakpoint) => `
                @media ${props.theme.breakPoints[breakpoint.breakpoint]} {
                    ${Object.keys(breakpoint.properties).map(property => {
                        return `${property}: ${breakpoint.properties[property]};`;
                    }).join('')              
                }`            
        ).join('')
    }}
  
`;

export default FlexBoxComponent;
