import { styled } from "frontity";

const Link = ({ className, children }) => (
    <a href={href} className={className}>
        {children}
    </a>
);

const StyledDiv = styled(Link)`
    width: 100%;
    text-align: center;
    color: white;
`;

export default StyledDiv
