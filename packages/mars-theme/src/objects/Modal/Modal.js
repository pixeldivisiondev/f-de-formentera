import { styled, connect } from "frontity";
import React, {useEffect, useState} from 'react';
import FlexBox from "../FlexBox/FlexBox";

const LinkWithModal = ({actions, ...props}) => {
    const [visible, setCount] = useState(props.state.modalOpened || props.visible);

    const WP = props.wrapper ? props.wrapper  : ModalWrapper;
    const ContenModal = props.ModalContent ? props.ModalContent  : ModalContent;
    const CloseModal = props.CloseModal ? props.CloseModal  : CloseWrapper;


console.log(visible, 'Link With Modal')

    return (
        <>
          {props.children ? <Opener onClick={() => { props.onClick ? props.onClick() : null; setCount(!visible); }}>{props.children}</Opener> : null}

            <WP visible={props.state.modalOpened}>
              <ContenModal>
                  <CloseModal  onClick={() => { props.onClose ? props.onClose() : null;  setCount(!visible); actions.theme.setModalOpened(false) }}>
                    { props.close ? props.close : 'Cerrar' }
                  </CloseModal>
                  {
                    props.state.modalOpened ? props.modal : null
                  }
              </ContenModal>
            </WP>
        </>
    )
};

const Opener = styled.div`
  cursor: pointer;
`

const ModalContent = styled(FlexBox)`
    min-width : 800px;
    width: auto; 
    margin : auto;
    background-color: #FFF;
    position: relative;
`


const ModalWrapper = styled(FlexBox)`
    position: fixed;
    width: 100%;
    height: 100%;
    left: 0;
    top: 0;
    background-color: rgba(0, 0, 0, 0.5);
    z-index: 9999;
    display: ${props => props.visible ? 'flex' : 'none' };
`;

const Container = styled(connect(LinkWithModal))`
    position: relative;
`;

const CloseWrapper = styled.div`
    position: absolute;
    right: 15px;
    top: 15px;
    color: #000;
    cursor: pointer;
    z-index: 99;
`;


export default  connect(LinkWithModal);

