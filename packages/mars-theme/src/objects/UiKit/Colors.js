import React, { useEffect, useState  } from "react";
import ColorBox from "./ColorBox";
import {styled, connect} from "frontity";
import Title from "../Text/Text";

const Buttons = (props) => {

    const TitleDefault = {
        color: 'primary-base',
        font_family: 'LexendZetta',
        font_type:  'Titulo',
        tipo: 'div',
        font_weight: 300,
        texto: '#Colores'
    };

    return (
        <>
                <Text data={TitleDefault} defaultData={TitleDefault} className={props.className}/>

                <p>#Color Primary"</p>
                <div style={{'display' : 'flex'}}>
                   <ColorBox color={props.state.theme.palette.primary["base"]} text={"base"}/>
                </div>

                <p>#Color Secondary</p>
                <div style={{'display' : 'flex'}}>
                   <ColorBox color={props.state.theme.palette.secondary["base"]} text={"base"}/>
                </div>

                 <p>#Color Purple</p>
                 <div style={{'display' : 'flex'}}>
                        <ColorBox color={props.state.theme.palette.purple["300"]} text={"300"}/>
                        <ColorBox color={props.state.theme.palette.purple["400"]} text={"400"}/>
                        <ColorBox color={props.state.theme.palette.purple.base} text={"Base"}/>
                        <ColorBox color={props.state.theme.palette.purple["600"]} text={"600"}/>
                        <ColorBox color={props.state.theme.palette.purple["700"]} text={"700"}/>
                        <ColorBox color={props.state.theme.palette.purple["1000"]} text={"1000"}/>
                 </div>

                 <p>#Color Orange</p>
                 <div style={{'display' : 'flex'}}>
                        <ColorBox color={props.state.theme.palette.orange["300"]} text={"300"}/>
                        <ColorBox color={props.state.theme.palette.orange["400"]} text={"400"}/>
                        <ColorBox color={props.state.theme.palette.orange.base} text={"Base"}/>
                        <ColorBox color={props.state.theme.palette.orange["600"]} text={"600"}/>
                        <ColorBox color={props.state.theme.palette.orange["700"]} text={"700"}/>
                        <ColorBox color={props.state.theme.palette.orange["1000"]} text={"1000"}/>
                 </div>
        </>
    )
}

export default connect(Buttons)
