import React, { useEffect, useState  } from "react";
import {connect, styled} from "frontity";
import Text from '../Text/Text'

const ColorBox = (props) => {

    return (
        <ColorBoxWrapper margin={props.state.theme.gridSpacing}>
            <ColorBoxComponent color={props.color.color}/>
            <Text data={{texto: props.text}} />
        </ColorBoxWrapper>
    )
}

export default connect(ColorBox);

const ColorBoxWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin-right: calc(${props => props.margin} * 4);  
`;

const ColorBoxComponent = styled.div`
  width: 40px;  
  height: 40px;
  background-color: ${props => props.color};
  border: 1px solid #000;
  
  
`;
