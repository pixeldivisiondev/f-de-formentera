import Text from "../Text";
import React, { useEffect, useState  } from "react";
import Button from "../Button/Button";

const Buttons = (props) => {
    return (
        <>
            <p>Button Sizes</p>
            <Button
                data={{tipo: "primary", texto: "Small", tamano: "small"}}
                onClick={() => {alert('bsd')}}/>
            <br />
            <Button
                data={{tipo: "primary", texto: "Normal", tamano: "normal"}}
                onClick={() => {alert('bsd')}}/>
            <br />
            <Button
                data={{tipo: "primary", texto: "Big", tamano: "big"}}
                onClick={() => {alert('bsd')}}/>


            <p>Button Colors</p>
            <Button
                data={{tipo: "primary", texto: "Primary", tamano: "normal"}}
                onClick={() => {alert('bsd')}}/>
            <br />
            <Button
                data={{tipo: "secondary", texto: "Secondary", tamano: "normal"}}
                onClick={() => {alert('bsd')}}/>

            <br />
            <Button
                data={{tipo: "accept", texto: "Accept", tamano: "normal"}}
                onClick={() => {alert('bsd')}}/>
            <br />

            <Button
                data={{tipo: "cancel", texto: "Cancel", tamano: "normal"}}
                onClick={() => {alert('bsd')}}/>

            <br />
            <Button
                data={{tipo: "purple", texto: "Purple", tamano: "normal"}}
                onClick={() => {alert('bsd')}}/>
            <br />
            <Button
                data={{tipo: "orange", texto: "Orange", tamano: "normal"}}
                onClick={() => {alert('bsd')}}/>
            <br />
        </>
    )
}

export default Buttons
