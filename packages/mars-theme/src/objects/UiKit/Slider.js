import React, { useEffect, useState  } from "react";
import Slider from "../Slider/slider";
import Card from "../Cards/Card";

const Buttons = (props) => {

    const responsive = [
        { breakPoint: 1280, cardsToShow: 4 }, // this will be applied if screen size is greater than 1280px. cardsToShow will become 4.
        { breakPoint: 760, cardsToShow: 2 },
    ]

    return (
        <div style={{'position' : 'relative'}}>
                <Slider responsive={responsive} style={{'margin-top': '50px', 'margin-bottom': '50px'}}>
                        <Card
                            image={"https://cdn.photographylife.com/wp-content/uploads/2017/01/What-is-landscape-photography.jpg"}
                            title={"Card 1"}
                            subtitle={"Lorem Ipsum....."}
                            cta={"Ver Más"}
                            onClick={() => {alert('a')}}
                        />
                        <Card
                            image={"https://cdn.photographylife.com/wp-content/uploads/2017/01/What-is-landscape-photography.jpg"}
                            title={"Card 1"}
                            subtitle={"Lorem Ipsum....."}
                            cta={"Ver Más"}
                            onClick={() => {alert('a')}}
                        />
                        <Card
                            image={"https://cdn.photographylife.com/wp-content/uploads/2017/01/What-is-landscape-photography.jpg"}
                            title={"Card 1"}
                            subtitle={"Lorem Ipsum....."}
                            cta={"Ver Más"}
                            onClick={() => {alert('a')}}
                        />
                        <Card
                            image={"https://cdn.photographylife.com/wp-content/uploads/2017/01/What-is-landscape-photography.jpg"}
                            title={"Card 1"}
                            subtitle={"Lorem Ipsum....."}
                            cta={"Ver Más"}
                            onClick={() => {alert('a')}}
                        />
                        <Card
                            image={"https://cdn.photographylife.com/wp-content/uploads/2017/01/What-is-landscape-photography.jpg"}
                            title={"Card 1"}
                            subtitle={"Lorem Ipsum....."}
                            cta={"Ver Más"}
                            onClick={() => {alert('a')}}
                        />

                </Slider>
        </div>
    )
}

export default Buttons
