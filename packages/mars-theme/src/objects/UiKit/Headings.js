import Text from "../Text";
import React, { useEffect, useState  } from "react";

const Headings = (props) => {
    return (
        <>
            <Text
                element="h1"
                text="#Headings"
                color={"primary-base"}/>

            <Text
            element="h1"
            text={<>H1 <a href={"#"} >COn Link</a></>}
            color={"primary-base"}/>

            <Text
                element="h2"
                text="H2"
                color={"primary-base"}/>

            <Text
            element="h1"
            color={"primary-base"}
            text="#ParaGraphs" />
        </>
    )
}

export default Headings
