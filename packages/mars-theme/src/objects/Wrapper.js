import {styled, connect} from "frontity";
import FlexBox from "./FlexBox/FlexBox";

const Wrapper = (props) => {

    WrapperComponent.defaultProps = {
        theme: props.state.theme,
        wrapper: 'Default'
    }


    return (
        <WrapperComponent  className={props.className} { ...props } align={props.align} justify={props.justify} direction={props.direction}>{props.children}</WrapperComponent>
    )
}


const WrapperComponent = styled(FlexBox)`  
  position: relative;
  margin: 0 auto;
  height: 100%;
  z-index: 2;
  ${props => {
    const type = props.wrapper ? props.wrapper : 'Default'
    return Object.keys(props.theme.wrappers[type]).map((item) => {             
         return `@media ${props.theme.breakPoints[item]} { 
            width: 100%;
            max-width: ${props.theme.wrappers[type][item].anchura}; 
            padding-left: calc(${props.theme.wrappers[type][item].padding_lateral} * ${props.theme.gridSpacing}); 
            padding-right: calc(${props.theme.wrappers[type][item].padding_lateral} * ${props.theme.gridSpacing});  
         };`
        }
    );
}}     
`;



export default connect(Wrapper);


