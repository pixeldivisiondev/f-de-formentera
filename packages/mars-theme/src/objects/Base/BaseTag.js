import {connect, styled} from "frontity";
import React from 'react';

const H1 = ({state, libraries, ...props}) => {
    const CustomTag = props.tipo && props.tipo != '0' ? props.tipo : 'p';
    const Html2React = libraries.html2react.Component;
    return (
                <CustomTag className={props.className}>
                    <Html2React html={props.data && props.data.texto ? props.data.texto : '' } />
                </CustomTag>
    )
}

export default connect(H1);



//{props.configModal}
