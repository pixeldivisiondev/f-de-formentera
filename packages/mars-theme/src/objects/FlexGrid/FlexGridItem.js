import React from 'react';
import {connect, styled} from "frontity";
import PropTypes from "prop-types";

import {getGridWidthByColumns} from "../../lib/utils/grid";
import Layout from "../Layout/Layout";

const FlexGridItem = ({state, ...props}) => {
  StyledFlexGridItem.defaultProps = {
    theme: state.theme
  };

  return (
    <StyledFlexGridItem justify="flex-start" align="flex-start" layout={props.layout} {...props}>{props.children}</StyledFlexGridItem>
  )
};

export default connect(FlexGridItem);

FlexGridItem.propTypes = {
  layout: PropTypes.array.isRequired,
};

const StyledFlexGridItem = styled(Layout)`
  position:relative;

  ${props => {
  return Object.keys(props.layout).map(item => {
    return `
     @media ${props.theme.breakPoints[item]} {
        flex-basis: calc(${getGridWidthByColumns(props.layout[item])} - 0.01px - var(--grid-gap, 1em));
        max-width: calc(${getGridWidthByColumns(props.layout[item])} - 0.01px - var(--grid-gap, 1em));
     }`
  })
}}
`;
