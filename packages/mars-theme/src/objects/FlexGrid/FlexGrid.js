import React from 'react';
import {connect, styled} from "frontity";
import Layout from "../Layout/Layout";

export default connect(function FlexGrid({state, ...props}) {
  return (

    <StyledFlexGrid gap={props.gap} justify="flex-start" align="flex-start" data={props.data} {...props} >{props.children}</StyledFlexGrid>
  )
});

const StyledFlexGrid = styled(Layout)`
${props => {
  return `
    --grid-gap: ${props.gap ? props.gap : '16px'};    
    margin-bottom: calc(var(--grid-gap, 1em) * -1);
    margin-left: calc(var(--grid-gap, 1em) * -1);
    width: auto;
    
    > * {
      flex-basis: 100%;
      margin-bottom: var(--grid-gap, 1em);
      margin-left: var(--grid-gap, 1em);
    }
  `
}}
`;