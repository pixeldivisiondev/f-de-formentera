import {styled, connect} from "frontity";

const Clonable = ({state, actions, ...props}) => {

    const clone = (e) => {

        props.parent.push(props.data);
        actions.theme.updateComponent();
    }

    const removeItem = (e) => {

        props.parent.splice(props.index, 1);
        actions.theme.updateComponent();
    }

    return (
        <div style={{'position' : 'relative'}}>
            <Button onClick={(e) => { clone(e) }}>Clonar</Button>
            <ButtonRemove onClick={(e) => { removeItem(e) }}>Eliminar</ButtonRemove>
            {props.children}
        </div>
    )
}


export default connect(Clonable);

const Button = styled.div`
    position: absolute;
    left: 0;
    top: 50%;
    z-index: 9999;
`

const ButtonRemove = styled.div`
    position: absolute;
    left: 0;
    top: 60%;
    z-index: 9999;
`
