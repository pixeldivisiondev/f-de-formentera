import {connect, styled} from "frontity";
import Paragraph from '../Paragraphs/Paragraph'
import Button from '../Button/Button'
import H2 from '../Text/Text'
const Card = (props) => {

    return (
        <div className={props.className} active="false">
            <img src={"https://cdn.photographylife.com/wp-content/uploads/2017/01/What-is-landscape-photography.jpg"} />
            <CardBody>
                <CardTitle
                    text="#Color Orange"
                    element="h1"
                    color={"primary-base"}
                />
                <Paragraph text={props.subtitle} />
                <Button
                    type={"primary"}
                    size={"small"}
                    text={props.cta}
                    onClick={() => {alert('bsd')}}/>
            </CardBody>

        </div>
    )
}

const CardComponent = styled(connect(Card))`
  margin: 0 5px;
  box-shadow: 0 0px 5px #CCC;
  padding-bottom: 17px;
  border-radius: 15px;
  border-radius: 15px;
  position: relative;
  overflow: hidden;
  background-color: #CCC;
`;

const CardTitle = styled(H2)`    
    margin-bottom: 0px;   
    font-size: 21px !important;
`;

const CardBody = styled.div`
  padding: 0 16px; 
`;

export default CardComponent;
