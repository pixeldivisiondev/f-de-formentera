import {styled} from "frontity";

const GridItem = ({className, children, props}) => (
    <GridItemComponent  className={className} {...props} >
        {children}
    </GridItemComponent>
)


const GridItemComponent = styled.div`
      grid-column-start: 1;
      grid-column-end: 3; 
`;

export default GridItem;
