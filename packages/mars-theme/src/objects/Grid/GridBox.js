import {styled} from "frontity";

const GridBox = ({className, children, props}) => (
        <div  className={className} {...props} >
            {children}
        </div>
)


const GridBoxComponent = styled(GridBox)`
  display: grid;  
  width: 100%;
  grid-gap: ${props => props.gap ? props.gap : 0};
  
`;

export default GridBoxComponent;
