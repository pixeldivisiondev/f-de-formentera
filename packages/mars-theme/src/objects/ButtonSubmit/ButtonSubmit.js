import {connect, styled} from "frontity";
import Button from "../../objects/Button/Button";

const ButtonSubmit = (props) => {
  return (
    <StyledSumbit disabled={props.disabled} className={props.className} data={{tipo: 'primary', tamano: props.size || 'submit', texto: props.text}} type="submit"/>
  )
}

export default connect(ButtonSubmit);

const StyledSumbit = styled(Button)`
  @media only screen and (max-width: 600px) {
    width: 100%;
  }
`

