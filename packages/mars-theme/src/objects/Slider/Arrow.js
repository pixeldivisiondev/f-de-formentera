import {styled, connect} from "frontity";

const LeftArrow = (props) => {
    return <StyledArrow className={"fas fa-2x fa-chevron-"+props.direction+" "+ props.className} direction={props.direction} aria-hidden="true" onClick={(e) => {props.onClick(e)}}></StyledArrow>
}



const StyledArrow = styled.i`
    position: absolute;
    ${props => props.direction == 'left' ? 'left: 15px;' : 'right: 15px;'}    
    top: 50%;
    z-index: 1;
    color: #FFF;
    transition: all 0.3s;
    cursor: pointer;
    &:hover {
        ${props => props.direction == 'left' ? 'left: 5px;' : 'right: 5px;'}
    }
`;



export default LeftArrow;


