import {styled, connect} from "frontity";
import Slider2 from 'react-styled-carousel'
import Dot from './Dots';
import DotWrapper from './DotWrapper';
import Arrow from './Arrow'



const Slider = (props) => {

    ThemedSlider.defaultProps = {
        theme: props.state.theme
    }

    return (
        <SliderWrapper>
            <ThemedSlider
                {...props}
                className={props.className}
                responsive={props.responsive}
                DotsWrapper={DotsWrapper}
                showArrows={true}
                Dot={props.Dot ? props.Dot : <Dot />}
                LeftArrow={<h1>Left</h1>}
                RightArrow={<h1>Right</h1>}
                padding={'inherit'}>
                    {props.children}
            </ThemedSlider>
        </SliderWrapper>
    )
}



const ThemedSlider = styled(Slider2)`
    padding: 0;
    position: relative;
`;



const DotsWrapper = styled(DotWrapper)`
  position: absolute;
  bottom: 0;
  width: 100%;
  display: flex;
  justify-content: center; 
  padding: 0; 
`;

const SliderWrapper = styled.div`
    position: relative
`;

export default connect(Slider);
