const DotWrapper = (props) => {

    return (
        <ul {...props} className={props.className}></ul>
    )
}

export  default  DotWrapper
