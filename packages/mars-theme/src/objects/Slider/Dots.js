import {connect, styled} from "frontity";

const Dot = (props) => {


    return <StyledDot className={props.className} onClick={(e) => {props.onClick(e)}} active={props.active}></StyledDot>
}


const StyledDot = styled.div`
    width: 15px;
    height: 15px;
    border: 1px solid #FFF;
    background-color: ${props => props.active ? '#FFF' : 'transparent'};
    border-radius: 50%;    
    margin: 0 5px;
    cursor: pointer;
`;

export default connect(Dot);
