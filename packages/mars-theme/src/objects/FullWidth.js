import {styled, connect, css} from "frontity";

const FullWidth = css`
  width: 100%;
  height: 100vh;  
`;

export {
    FullWidth
};
