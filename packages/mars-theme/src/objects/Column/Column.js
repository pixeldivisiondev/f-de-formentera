import {styled, connect} from "frontity"

import Flexbox from '../FlexBox/FlexBox'

const Column = ({state, ...props}) => {

    ColumnComponent.defaultProps = {
        theme: state.theme
    }

    return (
        <ColumnComponent className={props.className} data={props.data}>
            {props.children}
        </ColumnComponent>
    )
}

const ColumnComponent = styled(connect(Flexbox))`
  ${props => {
    const breakpoints = props.data ? props.data : null;    
    return breakpoints && breakpoints.map((breakpoint) => `          
                @media ${props.theme.breakPoints[breakpoint.breakpoint]} {
                    ${Object.keys(breakpoint.config).map(property => {
                        return `${property}: ${breakpoint.config[property]};`;
                    }).join('')              
                }`            
        ).join('')
    }}
`;

export default connect(Column);
