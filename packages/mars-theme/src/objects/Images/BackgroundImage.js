import { styled} from "frontity";
import FlexBox from '../FlexBox/FlexBox'

const BackgroundImage = props => {
    return (
        <BackgroundWrapper className={props.className} background={props.background}>
            { props.children }
        </BackgroundWrapper>
    )
}


const BackgroundWrapper = styled(FlexBox)`
    position: absolute;
    height: 100%;
    width: 100%;
    left: 0;
    top: 0;
    background-image: url(${props => props.background.url});
    background-position: center;
    background-size: cover;    
    position: relative;
    text-align: center;       
`;


export default  BackgroundImage;
