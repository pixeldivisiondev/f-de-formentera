import {connect, styled } from "frontity";
import H1 from '../Base/BaseTag'
import  paletteHelper  from '../../utils/palette'

const Text = ( {state, ...props}) => {

    Themable.defaultProps = {
        theme: state.theme

    }

    const { data }  = props;
    let defaultData = {...props.defaultData};

    if(!defaultData || !defaultData.color) {
        defaultData.color = 'primary-base'
    }

    if(!defaultData || !defaultData.font_type) {
        defaultData.font_type = 'PrimaryMedium'
    }

    if(!defaultData || !defaultData.font_weight) {
        defaultData.font_weight = 400
    }

    if(!defaultData || !defaultData.tipo) {
        defaultData.tipo = 'p'
    }

    if(!defaultData || !defaultData.font_family) {
        defaultData.font_family = 'Lexend Deca'
    }

    let componentData = data
    const test = {...data}
    if(defaultData) {
        Object.keys(test).filter(k => {
            return  test[k] === 'object' || test[k] === ''
        }).forEach(k =>  { delete(test[k]) })
        componentData = {...defaultData, ...test}
    }
    return <Themable {...props} data={componentData} tipo={componentData.tipo} configModal={null} />



}

const Themable = styled(connect(H1))`
    position: relative; 
    font-family: ${props => props.data.font_family};
    font-weight: ${props => props.data.font_weight};    
    color: ${props => paletteHelper({palette: props.theme.palette, type: 'primary', property: props.data.color})};
    margin: 0;    
    
    ${props => {       
        const type = props.data.font_type;
        return Object.keys(props.theme.fontTypes[type]).map((font) => `
                @media ${props.theme.breakPoints[font]} {
                    font-size: ${props.theme.fontTypes[type][font].tamano_de_fuente} ;
                    line-height: ${props.theme.fontTypes[type][font].line_height} ;
                    letter-spacing: ${props.theme.fontTypes[type][font].letter_spacing} ;
                }`            
        ).join('')        
    }}
    
    ${props => {
        const breakpoints = props.data.layout_breakpoint;       
        return breakpoints && breakpoints.map((breakpoint) => `
                @media ${props.theme.breakPoints[breakpoint.breakpoint]} {
                    ${Object.keys(breakpoint.properties).map(property => {
                        return `${property}: ${breakpoint.properties[property]};`;
                    }).join('')              
                }`            
        ).join('')
    }}
    a {
      cursor: pointer;
    }
`;

export default connect(Text);
