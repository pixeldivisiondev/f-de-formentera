import {styled, connect} from "frontity";
import Text from '../Text/Text'

const RichText = ( {state, libraries, ...props}) => {

    props.data.tipo = 'div';

    return <StyledText data={props.data} defaultData={{font_family: 'Cormorant' }} tipo={"div"} className={props.className} />
}

export default connect(RichText);

const StyledText = styled(Text)`
  
`

