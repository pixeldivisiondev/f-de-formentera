import Text from "../Text/Text";
import React, { ReactNode } from "react";
import {styled, connect} from "frontity";


const ASd = (props) => {
    return <Text className={props.className} data={TitleDefault} defaultData={TitleDefault} {...props} />;

};


const richtext = {
    // We can add a name to identify it later.
    name: "richtext",

    // We can add a priority so it executes before or after other processors.
    priority: 10,

    // Only process the node it if it's an image.
    test: () => {
        return true
    },

    processor: ({ node, ...props }) => {
        //node.component = ASd;
        return node;
    }
};

export default connect(richtext);
