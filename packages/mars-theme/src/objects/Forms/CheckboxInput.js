import React from 'react';
import {connect, styled} from "frontity";
import RichText from "../RichText/RichText";

export default connect(function CheckboxInput({state, actions, ...props}) {
  return (
    <>
      <StyledCheckbox hasError={props.errors[props.name]}>
        <StyledCheckboxInput
          type="checkbox"
          name={props.name}
          id={props.id}
          required={props.validation?.required}
          ref={props.register(props.validation)}
        />
        <StyledCheckboxInputControl aria-hidden="true"/>
      </StyledCheckbox>

      <StyledCheckboxLabel htmlFor={props.id}><RichText data={{texto: props.label, font_family: 'Lexend Deca', font_type: 'PrimaryMedium', tipo: 'span'}}/></StyledCheckboxLabel>
    </>
  )
});

const StyledCheckbox = styled.div`
  --custom-checkbox-size: 20px;
  --custom-checkbox-color-bg: transparent;
  --custom-checkbox-color-border: var(--color-primary-base);
  display: inline-block;
  position: absolute;
  cursor: pointer;
  height: var(--custom-checkbox-size);
  
  ${props => {
  return props.hasError ? `
      --custom-checkbox-color-border: var(--form-error-color);
      ` : ``;
}}
`;

const StyledCheckboxInputControl = styled.div`
  position: absolute;
  width: var(--custom-checkbox-size);
  height: var(--custom-checkbox-size);
  top: 0;
  left: 0;
  z-index: -1;
  pointer-events: none;
  transition: transform 0.2s;
  color: var(--custom-checkbox-color-bg);

  &::before,
  &::after {
    content: '';
    position: absolute;
  }

  &::before { // focus circle
    width: 160%;
    height: 160%;
    background-color: currentColor;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%) scale(0);
    opacity: 0;
    border-radius: 50%;
    transition: transform 0.2s;
    will-change: transform;
  }

  &::after { // custom checkbox
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;

    // custom checkbox style
    background-color: var(--custom-checkbox-color-bg);
    box-shadow: inset 0 0 0 1px var(--custom-checkbox-color-border);
  }
`;

const StyledCheckboxInput = styled.input`
  position: relative;
  // hide input
  margin: 0;
  padding: 0;
  opacity: 0;
  height: var(--custom-checkbox-size);
  width: var(--custom-checkbox-size);
  display: block;
  z-index: 1;
  cursor: pointer;

  &:checked {
    ~ ${StyledCheckboxInputControl} {
      color: var(--color-primary-base); // checked color

      &::after {
        background-color: currentColor;
        background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16'%3E%3Cpolyline points='2.5 8 6.5 12 13.5 3' fill='none' stroke='%23fff' stroke-linecap='round' stroke-linejoin='round' stroke-width='1.5'/%3E%3C/svg%3E");
        background-repeat: no-repeat;
        background-position: center;
        background-size: 0.85em;
        box-shadow: none;
      }
    }
  }

  &:active {
    ~ ${StyledCheckboxInputControl} {
      transform: scale(0.9);
    }
  }

  &:checked:active {
    ~ ${StyledCheckboxInputControl} {
      transform: scale(1);
    }
  }

  &:focus {
    ~ ${StyledCheckboxInputControl}::before  {
      opacity: 0.2;
      transform: translate(-50%, -50%) scale(1);
    }
  }
`;

const StyledCheckboxLabel = styled.label`
  color: var(--color-primary-base);
  display: block;
  margin-left: calc(24px * 1.5);
  font-size: var(--form-label-font-size);
  line-height: var(--custom-checkbox-size);
  pointer-events: auto;
  cursor: pointer;
  user-select: none;
  
  a {
    color: var(--color-blue-light);
    text-decoration: none;
  }
`;

const StyledFormControlError = styled.div`
  color: var(--form-error-color);
  display: inline-flex;
  font-family: inherit;
  font-size: var(--form-error-font-size);
  line-height: 1.5;
  margin-top: var(--form-error-margin-top);
`;
