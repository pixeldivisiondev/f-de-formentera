import React from 'react';
import {connect, styled} from "frontity";

import {hexToRGB} from "../../lib/utils/colors";
import paletteHelper from "../../utils/palette";

const Form = ({state, ...props}) => {
  StyledForm.defaultProps = {
    theme: state.theme
  };

  return (
    <StyledForm
      method="post"
      autoComplete="off"
      onSubmit={props.onSubmit}
      noValidate={true}
      className={props.className}
    >{props.children}</StyledForm>
  )
};

export default connect(Form);

const StyledForm = styled.form`
  ${props => {
  return `
      // Form controls
      --form-control-bg-color: white;
      --form-control-color: ${props.theme.primaryColor};
      --form-control-placeholder-color: ${hexToRGB(props.theme.primaryColor, 0.7)};
      --form-control-font-size: 16px;
      --form-control-font-weight: 400;
      --form-control-padding: 16px;
      --form-control-height: 50px;
      --form-control-radius: 0;
      --form-control-border-color: ${hexToRGB(props.theme.primaryColor, 0.7)};
      --form-control-border-left-width: 1px;
      --form-control-border-right-width: 1px;
      --form-control-border-top-width: 1px;
      --form-control-border-bottom-width: 1px;
      --form-control-margin-bottom: 16px;
      // Form labels
      --form-label-color: ${hexToRGB(props.theme.primaryColor, 0.7)};
      --form-label-font-size: 16px;
      --form-label-font-weight: 400;
      --form-label-margin-bottom: 8px;
      // Form submit
      --form-submit-align: right;
      // Form focus
      --form-focus-bg-color: white;
      --form-focus-color: ${props.theme.primaryColor};
      --form-focus-border-color: ${props.theme.primaryColor};
      // Form errors
      --form-error-color: ${paletteHelper({palette: props.theme.palette, type: 'error-base', property: 'error-base'})};
      --form-error-border-color: ${paletteHelper({palette: props.theme.palette, type: 'error-base', property: 'error-base'})};
      --form-error-font-size: 12px;
      --form-error-margin-top: 4px;
      // Form spacing
      --form-gap: 16px;
      --grid-gap: 16px;
      font-family: 'Lexend Deca';
      width: 100%;
      
      @media ${props.theme.breakPoints['tablet']} {
        --form-gap: 20px;
        --grid-gap: 20px;
      }
    `
  }}
`;
