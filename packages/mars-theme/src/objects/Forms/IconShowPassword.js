import React from 'react';
import {connect, styled} from "frontity";

export default connect(function IconShowPassword({state, actions, ...props}) {
  return (
    <StyledIconShowPassword onClick={props.handleClick}>
      {props.show ? (
        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M22 12C22 12 19 18 12 18C5 18 2 12 2 12C2 12 5 6 12 6C19 6 22 12 22 12Z" stroke="currentColor" strokeLinecap="round"/>
          <circle cx="12" cy="12" r="3" stroke="currentColor" strokeLinecap="round"/>
          <path d="M3 21L20 4" stroke="currentColor" strokeLinecap="round"/>
        </svg>

      ) : (
        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M22 12C22 12 19 18 12 18C5 18 2 12 2 12C2 12 5 6 12 6C19 6 22 12 22 12Z" stroke="currentColor" strokeLinecap="round"/>
          <circle cx="12" cy="12" r="3" stroke="currentColor" strokeLinecap="round"/>
        </svg>

      )}
    </StyledIconShowPassword>
  )
});

const StyledIconShowPassword = styled.div`
    top: calc((var(--form-control-height) / 2) - (24px / 2));
    color: var(--form-control-color);
    cursor: pointer;
    position: absolute;
    right: 18px;
`;