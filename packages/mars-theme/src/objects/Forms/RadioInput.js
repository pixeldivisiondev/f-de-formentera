import React from 'react';
import {connect, styled} from "frontity";
import Text from "../Text/Text";

export default connect(function RadioInput({state, actions, ...props}) {
  return (
    <StyledRadioButton>
      <StyledRadioButtonInput
        id={props.id}
        type="radio"
        name={props.name}
        value={props.value}
        ref={props.register({})}
      />

      <StyledRadioButtonLabel htmlFor={props.id}>
        <Text data={{texto: props.text}}/>
        {props.image || ''}
      </StyledRadioButtonLabel>
    </StyledRadioButton>
  )
});

const StyledRadioButton = styled.div`
  // radios and checkboxes
  --checkbox-radio-size: 18px;
  --checkbox-radio-gap: 4px; // gap between button and label
  --checkbox-radio-border-width: 1px;
  --checkbox-radio-line-height: 1;

  // radio buttons
  --radio-marker-size: 8px;

  // checkboxes
  --checkbox-marker-size: 12px;
  --checkbox-radius: 4px;

  display: flex;
`;

const StyledRadioButtonLabel = styled.label`
  line-height: var(--checkbox-radio-line-height);
  user-select: none;
  cursor: pointer;
  font-size: 16px;
  font-weight: bold;
  display: flex;
  align-items: center;
  justify-content: space-between;

  &::before {
    content: '';
    //display: inline-block;
    position: relative;
    top: calc((1em * var(--checkbox-radio-line-height) - var(--checkbox-radio-size)) / 2);
    flex-shrink: 0;
    width: var(--checkbox-radio-size);
    height: var(--checkbox-radio-size);
    background-color: var(--color-white);
    border-width: var(--checkbox-radio-border-width);
    border-color: var(--color-primary-base);
    border-style: solid;
    background-repeat: no-repeat;
    background-position: center;
    //margin-right: var(--checkbox-radio-gap);
    transition: transform 0.2s, border 0.2s;
    border-radius: 50%;
    //padding: 8px 12px;
    font-weight: normal;
    display: inline-flex;
    margin-right: 16px;
  }
`;

const StyledRadioButtonInput = styled.input`
  position: absolute;
  padding: 0;
  margin: 0;
  opacity: 0;
  height: var(--checkbox-radio-size);
  width: var(--checkbox-radio-size);
  pointer-events: none;
  
  &:checked {
    + ${StyledRadioButtonLabel}::before {
      background-color: white;
      box-shadow: none;
      border-color: var(--color-primary-base);
      transition: transform 0.2s;
      background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16'%3E%3Cg class='nc-icon-wrapper' fill='%23004e66'%3E%3Ccircle cx='8' cy='8' r='8' fill='%23004e66'%3E%3C/circle%3E%3C/g%3E%3C/svg%3E");
      background-size: var(--radio-marker-size);
    }

    &:active,
    &:focus {
      + ${StyledRadioButtonLabel}::before {
        border-color: var(--color-primary-base);
        box-shadow: 0 0 0 3px rgba(#886F68, 0.2);
      }
    }
  }
`;
