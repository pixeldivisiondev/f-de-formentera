import React, {useState} from 'react';
import {connect, styled} from "frontity";
import FormLabel from "./FormLabel";
import {ValidateSpanishID} from "../../lib/utils/validate-spanish-id";
import CheckboxInput from "./CheckboxInput";
import IconShowPassword from "./IconShowPassword";
import SelectInput from "./SelectInput";
import {getStateList} from "../../lib/checkout/statelist";
import Layout from "../Layout/Layout";
import More from '../../assets/images/icons/Add.svg'
import Less from '../../assets/images/icons/Remove.svg'


const FormControl = ({state, ...props}) => {

  const [showPassword, setShowPassword] = useState(false);
  let extraProps = null

  RangeInputWrapper.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  if (!props.validation) props.validation = {};

  if (!props.noRequired) {
    props.validation.required = "Este campo es obligatorio";
  }

  if (props.type === 'email') {
    props.validation.pattern = {
      value: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
      message: "Por favor introduce una dirección de correo electrónico"
    };
  }

  if (props.type === 'phone') {
    props.validation.pattern = {
      value: /^[9|6]{1}([\d]{2}[-]*){3}[\d]{2}$/,
      message: "Por favor introduce un teléfono válido"
    };
  }

  if (props.type === 'password') {
    props.validation.pattern = {
      value: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#\$%\^&\*])(?=.{8,})/,
      message: "La contraseña debe contener 8 caracteres, mayúsculas, minúsculas y algún símbolo especial (@%&$*#!)"
    };
  }

  if (props.type === 'postcode') {
    props.validation.pattern = {
      value: /[0-9]{5}/,
      message: "Debes introducir un código postal válido"
    };

    extraProps = {
      minLength: 5,
      maxLength: 5
    };

    props.type = 'text';
  }

  if (props.type === 'dni') {
    props.validation.validate = {
      isDni: value => ValidateSpanishID(value).valid || 'Debes introducir un DNI válido'
    };

    props.type = 'text';
  }

  if (props.type === 'nif') {
    props.validation.validate = {
      isNif: value => ValidateSpanishID(value).valid || 'Debes introducir un NIF válido'
    };

    props.type = 'text';
  }

  if (props.extraValidate) {
    props.validation.validate = props.extraValidate.validate;
  }

  return (
    <>
      {!['checkbox', 'radio', 'select', 'state', 'number_range', 'textarea'].includes(props.type) ? (
        <>
          {props.label ? (
            <FormLabel id={props.id} text={props.label}/>
          ) : ('')}

          <StyledFormControlInput
            hasError={props.errors[props.name]}
            type={(!showPassword) ? props.type : 'text'}
            name={props.name}
            id={props.id}
            required={props.validation?.required}
            ref={props.register(props.validation)}
            placeholder={props.placeholder}
            className={props.className}
            {...extraProps}
          />

          {props.type === 'password' ? (
            <IconShowPassword show={showPassword} handleClick={() => setShowPassword(!showPassword)}/>
          ) : ('')}

          {props.errors[props.name] ? (
            <StyledFormControlError>{props.errors[props.name].message}</StyledFormControlError>
          ) : ('')}
        </>
      ) : (
        ''
      )}

      {props.type === 'state' ? (
        <>
          <SelectInput
            options={getStateList()}
            menuPlacement={props.menuPlacement ? props.menuPlacement : 'bottom'}
            isSearchable={true}
            defaultIndex={props.selectIndex}
            name={props.name}
            placeholder="Provincia"
            controlForm={props.control}
            error={props.errors[props.name] ? true : false}
          />

          {props.errors[props.name] ? (
            <StyledFormControlError>{props.errors[props.name].message ? props.errors[props.name].message : 'Este campo es obligatorio'}</StyledFormControlError>
          ) : ('')}
        </>
      ) : ('')}

      {props.type === 'number_range' ? (
        <>
          {props.label ? (
            <FormLabel id={id} text={props.label}/>
          ) : ('')}

          <RangeInputWrapper >
            <LessNumberRange src={Less} onClick={() => { props.NumberRangeHandler('remove')}}/>
            { props.quantity }
            <StyledFormControlInput
              hasError={props.errors[props.name]}
              type={"number"}
              name={props.name}
              id={props.id}
              required={props.validation?.required}
              ref={props.register(props.validation)}
              placeholder={props.placeholder}
              className={props.className}
              hidden
              {...extraProps}
            />
            <MoreNumberRange src={More} onClick={() => { props.NumberRangeHandler('add')}}/>
          </RangeInputWrapper>


          {props.errors[props.name] ? (
            <StyledFormControlError>{props.errors[props.name].message}</StyledFormControlError>
          ) : ('')}
        </>
      ) : ('')}

      {props.type === 'checkbox' ? (
        <>
          <CheckboxInput
            id={props.id}
            name={props.name}
            className={props.className}
            label={props.label}
            register={props.register}
            errors={props.errors}
            validation={props.validation}

          />
          {props.errors[props.name] ? (
            <StyledFormControlError>El campo es obligatorio</StyledFormControlError>
          ) : ('')}
        </>
      ) : ('')}

      {props.type === 'select' ? (
        <>
        <SelectInput
          options={props.selectOptions}
          isSearchable={true}
          defaultIndex={props.selectIndex}
          name={props.name}
          rules={{ required: true }}
          controlForm={props.control}
          placeholder={props.placeholder}
          menuPlacement={props.menuPlacement}
          required={true}
          error={props.errors[props.name] ? true : false}
        />
          {props.errors[props.name] ? (
            <StyledFormControlError>El campo es obligatorio</StyledFormControlError>
          ) : ('')}
        </>
      ) : ('')}

      {props.type === 'textarea' ? (
        <>
          {props.label ? (
            <FormLabel id={props.id} text={props.label}/>
          ) : ('')}

          <StyledFormControlTextarea
            hasError={props.errors[props.name]}
            type={(!showPassword) ? props.type : 'text'}
            name={props.name}
            id={props.id}
            required={props.validation?.required}
            ref={props.register(props.validation)}
            placeholder={props.placeholder}
            className={props.className}
            {...extraProps}
          />

          {props.type === 'password' ? (
            <IconShowPassword show={showPassword} handleClick={() => setShowPassword(!showPassword)}/>
          ) : ('')}

          {props.errors[props.name] ? (
            <StyledFormControlError>{props.errors[props.name].message}</StyledFormControlError>
          ) : ('')}
        </>
      ) : ('')}
    </>
  )
};

export default connect(FormControl);

const StyledFormControlTextarea = styled.textarea`
  background-color: var(--form-control-bg-color);
  border-color: var(--form-control-border-color);
  border-left-width: var(--form-control-border-left-width);
  border-right-width: var(--form-control-border-right-width);
  border-top-width: var(--form-control-border-top-width);
  border-bottom-width: var(--form-control-border-bottom-width);
  
  border-style: solid;
  color: var(--form-control-color);
  font-family: inherit;
  font-size: var(--form-control-font-size);
  font-weight: var(--form-control-font-weight);
  height: var(--form-control-height);
  padding: 0 var(--form-control-padding);
  width: 100%;
  line-height: inherit;
  appearance: none;
  -webkit-appearance: none;
  -moz-appearance: none;
  box-shadow: none;
  border-radius: var(--form-control-radius);
  position: relative;

  &::-ms-clear {
    display: none;
  }

  &::-ms-expand {
    background-color: transparent;
    border: 0;
  }

  &:disabled {
    pointer-events: none;
    opacity: 0.5;
    user-select: none;
  }
  
  &:focus {
    color: var(--form-focus-color);
    background-color: var(--form-focus-bg-color);
    border-color: var(--form-focus-border-color);
    outline: 0;
  }
  
  &:-moz-placeholder {
    color: var(--form-control-placeholder-color);
  }

  &::-moz-placeholder {
    color: var(--form-control-placeholder-color);
  }

  &:-ms-input-placeholder {
    color: var(--form-control-placeholder-color);
  }

  &::-webkit-input-placeholder {
    color: var(--form-control-placeholder-color);
  }
  
  ${props => {
  return props.hasError ? `
    border-color: var(--form-error-border-color) !important;
    color: var(--form-error-color) !important;
    ` : ``;
}}
`;

const StyledFormControlInput = styled.input`
  background-color: var(--form-control-bg-color);
  border-color: var(--form-control-border-color);
  border-left-width: var(--form-control-border-left-width);
  border-right-width: var(--form-control-border-right-width);
  border-top-width: var(--form-control-border-top-width);
  border-bottom-width: var(--form-control-border-bottom-width);
  border-style: solid;
  color: var(--form-control-color);
  font-family: inherit;
  font-size: var(--form-control-font-size);
  font-weight: var(--form-control-font-weight);
  height: var(--form-control-height);
  padding: 0 var(--form-control-padding);
  width: 100%;
  line-height: inherit;
  appearance: none;
  -webkit-appearance: none;
  -moz-appearance: none;
  box-shadow: none;
  border-radius: var(--form-control-radius);
  position: relative;

  &::-ms-clear {
    display: none;
  }

  &::-ms-expand {
    background-color: transparent;
    border: 0;
  }

  &:disabled {
    pointer-events: none;
    opacity: 0.5;
    user-select: none;
  }
  
  &:focus {
    color: var(--form-focus-color);
    background-color: var(--form-focus-bg-color);
    border-color: var(--form-focus-border-color);
    outline: 0;
  }
  
  &:-moz-placeholder {
    color: var(--form-control-placeholder-color);
  }

  &::-moz-placeholder {
    color: var(--form-control-placeholder-color);
  }

  &:-ms-input-placeholder {
    color: var(--form-control-placeholder-color);
  }

  &::-webkit-input-placeholder {
    color: var(--form-control-placeholder-color);
  }
  
  ${props => {
  return props.hasError ? `
    border-color: var(--form-error-border-color) !important;
    color: var(--form-error-color) !important;
    ` : ``;
}}
`;

const StyledFormControlError = styled.div`
  color: var(--form-error-color);
  display: flex;
  font-family: inherit;
  font-size: var(--form-error-font-size);
  line-height: 1.5;
  margin-top: var(--form-error-margin-top);
  width: 100%;
`;

const RangeInputWrapper = styled(Layout)`
  position: relative;
  background-color: var(--form-control-bg-color);
  border-color: var(--form-control-border-color);
  border-left-width: var(--form-control-border-left-width);
  border-right-width: var(--form-control-border-right-width);
  border-top-width: var(--form-control-border-top-width);
  border-bottom-width: var(--form-control-border-bottom-width);
  border-style: solid;
  color: var(--form-control-color);
  font-family: inherit;
  font-size: var(--form-control-font-size);
  font-weight: var(--form-control-font-weight);  
  padding: 0 var(--form-control-padding);
  width: 100%;
  line-height: inherit;
  appearance: none;
  -webkit-appearance: none;
  -moz-appearance: none;
  box-shadow: none;
  border-radius: var(--form-control-radius);
  height: 40px;
  ${props => {
    return `
          @media ${props.breakPoints['tablet-wide']} {
                height: var(--form-control-height);       
          }`
    }
  }

  &::-ms-clear {
    display: none;
  }

  &::-ms-expand {
    background-color: transparent;
    border: 0;
  }

  &:disabled {
    pointer-events: none;
    opacity: 0.5;
    user-select: none;
  }
  
  &:focus {
    color: var(--form-focus-color);
    background-color: var(--form-focus-bg-color);
    border-color: var(--form-focus-border-color);
    outline: 0;
  }
  
  &:-moz-placeholder {
    color: var(--form-control-placeholder-color);
  }

  &::-moz-placeholder {
    color: var(--form-control-placeholder-color);
  }

  &:-ms-input-placeholder {
    color: var(--form-control-placeholder-color);
  }

  &::-webkit-input-placeholder {
    color: var(--form-control-placeholder-color);
  }
  
`

const RangeInputControls = styled.img`
  position: absolute;
  z-index: 2;
  top: 12px;
  cursor: pointer;
  &:hover {
    transform: scale(1.1);
  }
`

const LessNumberRange = styled(RangeInputControls)`
  left: 5px;
`

const MoreNumberRange = styled(RangeInputControls)`
  right: 5px;
`
