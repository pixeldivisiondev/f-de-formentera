import React from 'react';
import {connect, styled} from "frontity";

export default connect(function FormLabel({state, actions, ...props}) {
  return (
    <LabelStyled htmlFor={props.id}>{props.text}</LabelStyled>
  )
});

const LabelStyled = styled.label`
    color: red;
    display: inline-block;
    font-size: 16px;
    font-weight: 400;
    margin-bottom: 12px;
`;