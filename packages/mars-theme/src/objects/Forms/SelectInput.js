import React, {useState} from 'react';
import Select from 'react-select';
import {Controller, useForm} from "react-hook-form";
import {connect, styled} from "frontity";
import FormControl from "./FormControl";

export default connect(function SelectInput(props) {

  const methods = useForm();

  const customStyles = {
    control: (provided, state) => ({
      ...provided,
      borderColor: "var(--form-control-border-color)",
      backgroundColor: "var(--form-control-bg-color)",
      color: "var(--form-control-color)"
    }),
    placeholder: (provided, state) => ({
      ...provided,
      color: "rgba(0, 78, 102, 0.7)"
    }),
    input: (provided, state) => ({
      ...provided,
      color: "var(--form-control-color)"
    }),
    option: (provided, state) => ({
      ...provided,
      borderColor: "var(--form-control-border-color)",
      color: state.isSelected ? 'white' : "var(--form-control-color)",
      backgroundColor: state.isSelected ? "var(--color-blue-light)" : state.isFocused ? 'var(--color-blue-base)' : 'white',
    }),
    singleValue: (provided, state) => ({
      ...provided,
      color: "var(--form-control-color)",
      fontSize: "var(--form-control-font-size)",
    }),
    menu: (provided, state) => ({
      ...provided,
      backgroundColor: "var(--form-control-bg-color)",
    }),
    menuList: (provided, state) => ({
      ...provided,
      maxHeight: "150px",
    }),
  };

  const [opened, setOpened] = useState(false)

  const handleMenu = (isOpened) => {
      setOpened(isOpened)
  }

  return (

    <Controller
      as={<SelectStyled
        error={props.error}
        styles={customStyles}
        options={props.options}
        isSearchable={props.isSearchable}
        menuPlacement={props.menuPlacement}
        menuIsOpen={opened}
        onMenuOpen={() => {handleMenu(true)}}
        onMenuClose={() => {handleMenu(false); }}
        onBlur={() => {handleMenu(false); }}

      />}
      options={props.options}
      name={props.name}
      ref={props.ref}
      placeholder={props.placeholder}
      control={props.controlForm}
    />

  )
});

const SelectStyled = styled(Select)`
  height: var(--form-control-height);
  width: 100%;
  z-index: ${props => props.menuIsOpen ? 100 : 10};
  
  & > div {
    color: rgba(0, 78, 102, 0.7) !important;
  } 
    
  & > div:first-of-type {
    border-color: ${props => !props.error ? `var(--form-control-border-color)` : `#F00`} ;
    border-left-width: var(--form-control-border-left-width);
    border-right-width: var(--form-control-border-right-width);
    border-top-width: var(--form-control-border-top-width);
    border-bottom-width: var(--form-control-border-bottom-width);
    border-style: solid;
    border-radius: var(--form-control-border-radius);
    color: var(--form-control-color);
    font-weight: var(--form-control-font-weight);
    height: var(--form-control-height);
    width: 100%;
  }
`
