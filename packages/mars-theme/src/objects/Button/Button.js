import {connect, styled} from "frontity";
import paletteHelper from '../../utils/palette'

const Button = (props) => {
  const {buttons, palette} = props.state.theme;
  const {tipo, tamano, texto} = props.data;

  const type = buttons.types[tipo];

  let extraAttributes = null;

  if (props.onClick !== undefined) {
    extraAttributes = {
      onClick: (e) => { props.preventDefault ? e.preventDefault() : null; props.onClick()}
    }
  }

  const Component = props.type == 'link' ? ButtonComponentLink : ButtonComponent

  return (

    <Component
      color={paletteHelper({palette: palette, type: type, property: type.text_color})}
      backgroundColor={paletteHelper({palette: palette, type: type, property: type.background_color})}
      altbackgroundColor={paletteHelper({palette: palette, type: type, property: type.hover_backfround})}
      altColor={paletteHelper({palette: palette, type: type, property: type.hover_text_color})}
      fontFamily={type.font_family}
      fontWeight={type.font_weight}
      border={type.border}
      className={props.className}
      minWidth={buttons.sizes[tamano].min_width}
      width={buttons.sizes[tamano].width}
      borderRadius={buttons.sizes[tamano].border_radius}
      fontSize={buttons.sizes[tamano].font_size}
      height={buttons.sizes[tamano].height}
      type={props.type ? props.type : 'normal'}
      disabled={props.disabled}
      {...extraAttributes}
    >
      {texto}
    </Component>
  )
}

const ButtonComponent = styled.button`
  background-color: ${props => props.backgroundColor};  
  color: ${props => props.color};  
  border: ${props => props.border};
  border-radius: ${props => props.borderRadius};
  font-size: ${props => props.fontSize};
  height: ${props => props.height};
  line-height: 1.2; 
  min-width: ${props => props.minWidth};
  font-family: ${props => props.fontFamily};
  font-weight: ${props => props.fontWeight};
  letter-spacing: 0.2em;
  text-align: center;
  //margin: 15px 0;
  text-decoration: none !important; 
  text-transform: uppercase;
  display: inline-block;
  cursor: pointer;
  transition: all .6s;  
  position: relative;
  padding: 0 16px;
  width: ${props => props.width};
  z-index: 1;
  &:hover {
    background-color: ${props => props.altbackgroundColor};  
    color: ${props => props.altColor};
  }
  
  
`;

const ButtonComponentLink = styled.a`
 background-color: ${props => props.backgroundColor};  
  color: ${props => props.color};  
  border: ${props => props.border};
  border-radius: ${props => props.borderRadius};
  font-size: ${props => props.fontSize};
  height: ${props => props.height};
  line-height: 1.2; 
  letter-spacing: 0.2em;
  min-width: ${props => props.minWidth};
  font-family: ${props => props.fontFamily};
  font-weight: ${props => props.fontWeight};
  text-align: center;
  //margin: 15px 0;
  text-decoration: none !important; 
  text-transform: uppercase;
  display: inline-block;
  cursor: pointer;
  transition: all .6s;  
  position: relative;
  padding: 0 16px;
  width: ${props => props.width};
  z-index: 1;
  &:hover {
      background-color: ${props => props.altbackgroundColor};  
      color: ${props => props.altColor};
  }  
`;


export default connect(Button);

