import React, {useEffect} from "react";
import {connect, loadable, styled} from "frontity";

const DefaultTemplate = loadable(() => import('../templates/defaultTemplate'));
const UiKitTemplate = loadable(() => import('../templates/UiKitTemplate'));
const RegistroTemplate = loadable(() => import('../templates/RegistroTemplate'));
const MiCuentaTemplate = loadable(() => import('../templates/MiCuentaTemplate'));
const MisDatosPersonalesTemplate = loadable(() => import('../templates/MisDatosPersonalesTemplate'));
const CambiarContrasenaTemplate = loadable(() => import('../templates/CambiarContrasenaTemplate'))
const CrearUsuarioTemplate = loadable(() => import('../templates/CrearUsuarioTemplate'))

const ReestablecerContrasenaTemplate = loadable(() => import('../templates/ReestablecerContrasenaTemplate'));
const PedidosTemplate = loadable(() => import('../templates/PedidosTemplate'));
const DireccionesTemplate = loadable(() => import('../templates/DireccionesTemplate'));
const NuevaDireccionTemplate = loadable(() => import('../templates/DireccionesEnvioNuevaTemplate'));
const EditarDireccionTemplate = loadable(() => import('../templates/DireccionesEnvioEditarTemplate'));
const DireccionesEnvioTemplate = loadable(() => import('../templates/DireccionesEnvioTemplate'));
const DireccionesFacturacionTemplate = loadable(() => import('../templates/DireccionesFacturacionTemplate'));
const CarritoTemplate = loadable(() => import('../templates/CarritoTemplate'))
const FinalizarCompraTemplate= loadable(() => import('../templates/FinalizarCompraTemplate'));
const PedidoFinalizadoTemplate= loadable(() => import('../templates/PedidoFinalizadoTemplate'));
const FaqsTemplate = loadable(() => import('../templates/FaqsTemplate'))
const LegalesTemplate = loadable(() => import('../templates/LegalesTemplate'))
const ContactoTemplate = loadable(() => import('../templates/ContactoTemplate'))
const FacturacionTemplate = loadable(() => import('../templates/FacturacionTemplate'))

const Post = ({state, actions, libraries}) => {
  const data = state.source.get(state.router.link);
  const post = state.source[data.type][data.id];

  return data.isReady ? (
    <Container>
      <Content>
        {(() => {
          switch (post.template) {
            case 'template-nuevo-cliente.php':
              return <CrearUsuarioTemplate/>;
            case 'template-b2b.php':
              return <DefaultTemplate forEmpresa={true}/>;
            case 'template-mi-cuenta.php':
              return <MiCuentaTemplate/>;
            case 'template-mis-datos-personales.php':
              return <MisDatosPersonalesTemplate/>;
            case 'template-cambiar-contrasena.php':
              return <CambiarContrasenaTemplate/>;
            case 'template-mis-pedidos.php':
              return <PedidosTemplate/>;
            case 'template-mis-direcciones.php':
              return <DireccionesTemplate/>;
            case 'template-mis-direcciones-envio.php':
              return <DireccionesEnvioTemplate/>;
            case 'template-mis-direcciones-facturacion.php':
              return <DireccionesFacturacionTemplate/>;
            case 'template-reestablecer-contrasena.php':
              return <ReestablecerContrasenaTemplate/>
            case 'template-registro.php':
              return <RegistroTemplate/>;
            case 'template-finalizar-compra.php':
              return <FinalizarCompraTemplate/>;
            case 'template-pedido-finalizado.php':
              return <PedidoFinalizadoTemplate/>;
            case 'template-uikit.php':
              return <UiKitTemplate />;
            case 'template-carrito.php':
              return <CarritoTemplate />;
            case 'template-nueva_direccion.php':
              return <NuevaDireccionTemplate />;
            case 'template-editar-direccion.php':
              return <EditarDireccionTemplate />;
            case 'template-faqs.php':
              return <FaqsTemplate />;
            case 'template-legales.php':
              return <LegalesTemplate />;
            case 'template-contacto.php':
              return <ContactoTemplate />;
            case 'template-facturacion.php':
              return <FacturacionTemplate />;

            default:
              return <DefaultTemplate/>;
          }
        })()}
      </Content>
    </Container>
  ) : null;
};

export default connect(Post);

const Container = styled.div`
  width: 100%;
  margin: 0;
  padding: 0;
`;

/**
 * This component is the parent of the `content.rendered` HTML. We can use nested
 * selectors to style that HTML.
 */
const Content = styled.div`
 `;
