import Modulos from './modulos/index';

const ContenidoFlexible = ({state, data, ...props}) => {
    return (
        <div>
            {
                data.modulos ? data.modulos.map((item, index) => {
                    return <Modulos {...props} key={index} tag={item.acf_fc_layout} data={item}/>
                }) : null
            }
        </div>

    )
}

export default ContenidoFlexible;
