import React from 'react';
import {connect, styled} from "frontity";

import Text from "../../../objects/Text/Text";

export default connect(function Intro() {
  return (
    <>
      <RegistroTitleStyled data={{texto: 'RESTAURAR CONTRASEÑA', font_type: 'H1', font_family: 'Lexend Zetta'}}/>
      <RegistroCopyStyled data={{texto: 'Introduce tu nueva contraseña'}}/>
    </>
  )
});

const RegistroTitleStyled = styled(Text)`
  margin-bottom: 24px;
`;

const RegistroCopyStyled = styled(Text)`
  margin-bottom: 24px;
  opacity: 0.6;
`;