import React, {useRef} from 'react';
import {connect} from "frontity";
import {useForm} from "react-hook-form";
import {ApolloError} from "@apollo/client";

import Form from "../../../objects/Forms/Form";
import AlertMessage from "../../../objects/AlertMessage/AlertMessage";
import FlexGrid from "../../../objects/FlexGrid/FlexGrid";
import FlexGridItem from "../../../objects/FlexGrid/FlexGridItem";
import FormControl from "../../../objects/Forms/FormControl";
import ButtonSubmit from "../../../objects/ButtonSubmit/ButtonSubmit";

export default connect(function FormReestablecerContrasena({state, actions, libraries, ...props}) {
  const {register, handleSubmit, watch, errors} = useForm();

  const router = libraries.source.parse(state.router.link)
  const keyParameter = router.query.key;
  const loginParameter = router.query.login.replace('/','');

  const password = useRef({});
  password.current = watch("reset_password", "");

  const handleOnSubmit = async (data, e) => {
    actions.theme.clearAlertMessage();

    const r = await libraries.register.resetUserPassword(loginParameter, data.reset_password, keyParameter);

    if (r instanceof ApolloError) {
      let errorMessage = r.message;

      if (errorMessage.includes('Password reset link is invalid.')) {
        errorMessage = 'El enlace para resetear la contraseña es inválido.';
      }

      actions.theme.setAlertMessage(errorMessage, false, 'error');
    } else {
      actions.theme.setAlertMessage("La contraseña se ha actualizado correctamente", false, 'success');
      e.target.reset();
    }
  };

  return (
    <Form onSubmit={handleSubmit(handleOnSubmit)}>
      <AlertMessage inModal={false}/>

      <FlexGrid>
        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
          <FormControl
            id="reset_password" name="reset_password"
            type="password" placeholder="Contraseña"
            register={register}
            errors={errors}
          />
        </FlexGridItem>
      </FlexGrid>

      <div style={{height: '20px'}}/>

      <FlexGrid>
        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
          <FormControl
            id="reset_password_2" name="reset_password_2"
            type="password" placeholder="Confirmar contraseña"
            register={register}
            extraValidate={{validate: value => value === password.current || "Las contraseñas deben coincidir"}}
            errors={errors}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 12, desktop: 12}}>
          <ButtonSubmit text="CONFIRMAR"/>
        </FlexGridItem>
      </FlexGrid>
    </Form>
  )
});
