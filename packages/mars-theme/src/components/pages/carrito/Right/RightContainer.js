import React, {useState} from 'react';
import Text from "../../../../objects/Text/Text";
import Layout from "../../../../objects/Layout/Layout";
import {styled, connect} from "frontity";
import Button from "../../../../objects/Button/Button";
import AddCoupon from "./AddCoupon";
import TotalPrice from "./TotalPrice";
import { parsePriceToFloat } from '../../../../lib/utils/prices'
import { PayPalButton } from "react-paypal-button-v2";
import {fillBillingShippingStep} from "../../../../lib/checkout/util";

const RightContainer = ({state, libraries, actions, ...props}) => {


  const setStateOrder =(data, details) => {

  }
  const SquareButtonProps = {
    tipo: "primary",
    tamano: "squared",
    texto: "comprar ahora",
  }

  ResumeWrapper.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  Resume.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  StyledAddCoupon.defaultProps = {
    breakPoints: state.theme.breakPoints
  }
  ItemLine.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  ButtonsWrapper.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  RightContainerWrapper.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  const PayComp = props.Button;

  return (

    <RightContainerWrapper  className={props.className}>
      <Resume data={{section_background_color: 'blue-base'}}>
        <ResumeWrapper data={{section_background_color: 'secondary-base'}}>
          <ItemLine justify={"space-between"}>
            <Text data={{texto: "SUBTOTAL"}} defaultData={{font_family: 'Lexend Zetta', font_type: 'LabelLarge'}}/>
            <Text data={{texto:  state.cart ? (parsePriceToFloat(state.cart.subtotal) + parsePriceToFloat(state.cart.contentsTax) +  parsePriceToFloat(state.cart.discountTax)).toFixed(2).replace(".",",").toString() + '€': '- €'}} />
          </ItemLine>
          <ItemLine justify={"space-between"}>
            <Text data={{texto: "ENVÍO"}} defaultData={{font_family: 'Lexend Zetta', font_type: 'LabelLarge'}}/>
            <Text data={{texto:  state.cart ? "+"+state.cart.shippingTotal.replace(".",",") : '- €'}} />
          </ItemLine>
          {
            state.cart.appliedCoupons.nodes.map((discount,key) => {
              return (
                <ItemLine justify={"space-between"} key={key}>
                  <Text data={{texto: "CÓDIGO: '"+discount.code+"'"}} defaultData={{color: 'blue-light',font_family: 'Lexend Zetta', font_type: 'LabelLarge'}}/>
                  <Text data={{texto: "-" + discount.amount.toString() + '€' }} defaultData={{color: 'blue-light'}}/>
                </ItemLine>
              )
            })
          }
          <ItemLine justify={"space-between"}>
            <Text data={{texto: "IVA"}}  defaultData={{font_family: 'Lexend Zetta', font_type: 'LabelLarge'}} />
            <Text data={{texto:  state.cart ? state.cart.totalTax.replace(".",",") : '- €'}} />
          </ItemLine>
          <ItemLine justify={"space-between"}>
            <Text data={{texto: "TOTAL"}}  defaultData={{font_family: 'Lexend Zetta', font_type: 'LabelLarge'}} />
            <TotalPrice price={state.cart ? state.cart.total : '0'} />
          </ItemLine>

          <ButtonsWrapper >
            <FullButton data={SquareButtonProps} onClick={ async () => { window.scrollTo(0, 0); await actions.router.set('/finalizar-compra/'); }}/>

            <PayComp amount={ state.cart.total.replace("€","")}
                     style={
                       {height: 52}
                     }
                     createOrder={async (data, actions) => {
                       console.log('Create Order', actions, data);
                       return actions.order.create({
                         purchase_units: [{
                           reference_id: "REF1",
                           amount: {
                             currency_code: "EUR",
                             value: state.cart.total.replace("€",""),
                           }
                         }],
                       });
                     }}
                     options={{
                       currency: "EUR",
                       clientId: "AfexgV2o4V3CPsq3FLL-BJWGgvp1kR-UMR4TcnSaFx4xCAbWWNzvImTyQIOSJhw6q87-45rnT-vhGTye",
                       commit: false,
                       style: {
                         size: 'large'
                       }
                     }}
                     onApprove={async (details, data) => {
                       console.log('Approve', details, data);
                       state.theme.checkoutStep = 4;
                       actions.theme.setCheckoutStep(4)
                       setStateOrder(data, details )
                       props.setOrder(data.order);


                     }}/>
          </ButtonsWrapper>


        </ResumeWrapper>
      </Resume>

      <StyledAddCoupon />

    </RightContainerWrapper>
  );
};

export default connect(RightContainer);

const ButtonsWrapper = styled(Layout)`
  div {
    width: 100%;
  }
  margin-top: 16px;
  ${props => {
  return `
          @media ${props.breakPoints['tablet-wide']} {
            margin-top: 56px;
          }`
      }
    }
`

const FullButton = styled(Button)`
  width: 100%;
  margin-bottom: 24px;
  border: 1px solid var(--color-primary-base);
  font-family: "Lexend Deca" !important;
`

const StyledAddCoupon = styled(AddCoupon)`
   display: none;
   ${props => {
    return `
          @media ${props.breakPoints['tablet-wide']} {
            display: flex;
          }`
      }
    }
`


const ResumeWrapper = styled(Layout)`
  padding: 18px;
  ${props => {
  return `
          @media ${props.breakPoints['tablet-wide']} {
            padding-top: 24px;
            padding-top: 8px;
            padding-left: 32px;
            padding-right: 32px;
          }`
      }
    }
`

const RightContainerWrapper = styled(Layout)`
  flex: 1;
  margin: 0 -16px;
  width: calc(100% + 32px);
  align-self: auto;

  ${props => {
  return `
          @media ${props.breakPoints['tablet']} {
            margin: 0 ;
          }
          @media ${props.breakPoints['tablet-wide']} {
            flex: 0 0 400px;

          }`
      }
    }
`

const Resume = styled(Layout)`
   padding: 24px 16px;
   ${props => {
      return `
          @media ${props.breakPoints['tablet-wide']} {
            padding: 28px;
          }`
      }
    }
`

const ItemLine = styled(Layout)`
 border-bottom: 1px solid var(--color-blue-base);
 padding: 3px 0;
 ${props => {
  return `
          @media ${props.breakPoints['tablet-wide']} {
            padding: 16px 0;
          }`
      }
    }
`
