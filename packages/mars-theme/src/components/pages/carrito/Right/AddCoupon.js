import React, { useState} from 'react';
import {styled, connect} from "frontity";
import Layout from "../../../../objects/Layout/Layout";
import Text from "../../../../objects/Text/Text";
import {useForm} from "react-hook-form";
import Form from "../../../../objects/Forms/Form";
import {ApolloError} from "@apollo/client";
import FormControl from "../../../../objects/Forms/FormControl";
import ButtonSubmit from "../../../../objects/ButtonSubmit/ButtonSubmit";
import Ok from '../../../../assets/images/icons/GreenOk.svg'
import Ko from '../../../../assets/images/icons/RedKo.svg'
import Bag from '../../../../assets/images/icons/bag.js'

const AddCoupon = ({actions, libraries, state, ...props}) => {

  const [coupon, setCoupon] = useState(false)
  const [couponOk, setCouponOk] = useState(false)
  const [adding, setAdding] = useState(false)
  const [couponError, setCouponError] = useState(false)

  const defaultVal = {
    code: ''
  };

  const {register, handleSubmit, errors, setValue, getValues } = useForm({
    defaultValues: defaultVal
  })

  StyledButtonSubmit.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  StyledInputWrapper.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  const handleOnSubmit = async data => {
    setCouponError(false);
    setAdding(true)
    const p = await actions.auth.authProcessAuthToken();
    const r = await libraries.cart.addCoupon(data.code);
    if (r instanceof ApolloError) {
      let errorMessage = r.message;
      if(errorMessage == 'No coupon found with the code provided') {
       errorMessage = 'No se ha encontrado ningún descuento con el cupón introducido'
      }
      setCouponError(errorMessage);
    }else{
      state.cart = r.data.applyCoupon.cart
      setCouponOk(true);
    }
    setAdding(false)
  };

  const handleCouponVisibility = () => {
    setCoupon(!coupon)
    setCouponError(false)
    setCouponOk(false)
  }

  return (
    <CouponWrapper align={"flex-start"} direction={"column"} className={props.className}>
      <div onClick={() => {handleCouponVisibility()}}>
        <Layout justify={"flex-start"} >
          <StyledBag src={Bag} /><TienesCupon data={{texto: "¿Tienes un código de descuento?"}} defaultData={{color: 'blue-light', font_type: 'ButtomSmallLarge'}} />
        </Layout>
      </div>
      {
        coupon === true ?
          <StyledFormLayout justify={"flex-start"}>
            <StyledForm onSubmit={handleSubmit(handleOnSubmit)} >

              <Layout justify={"flex-start"} align={"flex-start"} direction={"row"}>
                <StyledInputWrapper direction={"column"} align={"flex-start"}>
                  <StyledInput
                    id="code"
                    name="code"
                    type="text"
                    placeholder="Introduce el código aquí"
                    register={register}
                    errors={errors}
                  />
                  { couponError !== false ? <CouponMessageWrapper justify={"flex-start"}><img src={Ko} /><TextError data={{texto: couponError}} defaultData={{color: 'error-base'}}/></CouponMessageWrapper> : null }
                  { couponOk === true  ?    <CouponMessageWrapper justify={"flex-start"}><img src={Ok} /> <TextError data={{texto: 'El código se ha añadido correctamente'}} defaultData={{color: 'accept-base'}}/></CouponMessageWrapper> : null }
                </StyledInputWrapper>
                <StyledButtonSubmit disabled={adding} text={adding ? 'Aplicar...' : 'Aplicar'} />
              </Layout>
            </StyledForm>
          </StyledFormLayout>
          : null
      }
    </CouponWrapper>
  );
};

export default connect(AddCoupon);

const CouponWrapper = styled(Layout)`
  margin-top: 24px;  
`

const StyledBag = styled(Bag)`  
  margin-right: 8px;
  width: 18px;
  color: var(--color-blue-light);
  cursor: pointer;
`
const CouponMessageWrapper = styled(Layout)`
  margin-top: 16px;  
`

const TienesCupon = styled(Text)`
  text-transform: uppercase;
  cursor: pointer;
`

const TextError = styled(Text)`
  margin-left: 5px;
  flex: 1;
  word-break: break-all;
`

const StyledFormLayout = styled(Layout)`
  justify-content: flex-start;    
`

const StyledForm = styled(Form)`
   width: 100%;
   margin-top: 15px;
`

const StyledInputWrapper = styled(Layout)`      
      
   flex: 1 1;
   margin-bottom: 18px;
   ${props => {
      return `
          @media ${props.breakPoints['tablet-wide']} {
            max-width: 297px;
          }`
      }
    }
`

const StyledInput = styled(FormControl)`    
   text-align: left;
   min-height: 52px;
   flex: 1 1;
`

const StyledButtonSubmit = styled(ButtonSubmit)`   
    min-width: unset;;
    color: var(--color-primary-base);
    background-color: var(--color-secondary-base);
    &:hover {
      background-color: var(--color-primary-base);
      color: var(--color-secondary-base);
    }
    ${props => {
      return `
          @media ${props.breakPoints['tablet-wide']} {
            margin-left: 17px;
          }`
      }
    }
`
