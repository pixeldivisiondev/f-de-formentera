import React, {useState, useRef} from 'react';
import {styled, connect} from "frontity";
import Layout from "../../../../objects/Layout/Layout";
import Text from "../../../../objects/Text/Text";
import RichText from "../../../../objects/RichText/RichText";
import {useForm} from "react-hook-form";
import Form from "../../../../objects/Forms/Form";
import {ApolloError} from "@apollo/client";
import AlertMessage from "../../../../objects/AlertMessage/AlertMessage";
import FormControl from "../../../../objects/Forms/FormControl";
import ProductDetailPrice from './ProductDetailPrice'
import DeleteModal from "./DeleteModal";
import {CloseModal, ModalContent, ModalWrapper} from "../../../objects/Modals";
import Modal from "../../../../objects/Modal/Modal";


const ProductDetail = ({state, libraries, actions, ...props}) => {
  const item = props.item;
const  [confirmModalOpened, setConfirmModalOpened] = useState(false)
  const defaultVal = {
    quantity: props?.item?.quantity
  };

  const {register, handleSubmit, trigger, errors, setValue, getValues} = useForm({
    defaultValues: defaultVal
  })
  const [quantity, setQuantity] = useState(props?.item?.quantity ? props?.item?.quantity : 0)


  const handleOnSubmit = async data => {
    const p = await actions.auth.authProcessAuthToken();
    const r = await libraries.cart.updateItemQuantities(props.item.key, data.quantity);
    if (r instanceof ApolloError) {
      let errorMessage = r.message;
      actions.theme.setAlertMessage(errorMessage, false, 'error');
    } else {
      state.cart = r.data.updateItemQuantities.cart
    }

  };

  const NumberRangeHandler = async (action) => {
    const singleValue = getValues("quantity");
    console.log(singleValue, 'SINLGE VALUE');

    let forEmpresa = false;
    if (typeof window !== 'undefined') {
      if(localStorage.getItem('Role') === 'empresa') {
        forEmpresa = true
      }
    }

    if (action == 'add' && singleValue < 99) {
      setValue("quantity", parseInt(singleValue) + 1)
      actions.analytics.addToCart(forEmpresa, 1)
    } else if (singleValue > 1) {
      setValue("quantity", parseInt(singleValue) - 1)
      actions.analytics.removeFromCart(forEmpresa, 1)
    }
    console.log(action ,'in')
    if(singleValue == "1" && action == 'remove' ) {
      console.log('in')
      actions.theme.setModalOpened(true)
      setConfirmModalOpened(true)
    }else{
      setQuantity(getValues("quantity"))
      handleSubmit(handleOnSubmit({quantity: getValues("quantity")}))
    }

  }

  StyledProductImage.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  StyledProductDetailPrice.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  ProductDetailLayout.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  ProductSelectorWrapper.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  StyledInput.defaultProps = {
    breakPoints: state.theme.breakPoints
  }
  StyledForm.defaultProps = {
    breakPoints: state.theme.breakPoints
  }


  Name.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  console.log(confirmModalOpened, 'confirmModalOpened in')

  return (
    <ProductDetailLayout justify={"flex-start"}>
      {
        confirmModalOpened ?
          <Modal
            modal={<DeleteModal
              onDelete={async () => {
                await libraries.cart.emptyCart()
                actions.theme.setCheckoutData(null);
                actions.theme.setCheckoutStep(1);
                let forEmpresa = false;
                if (typeof window !== 'undefined') {
                  if(localStorage.getItem('Role') === 'empresa') {
                    forEmpresa = true
                  }
                }
                actions.analytics.removeFromCart(forEmpresa, 1)
                state.cart = null;
                actions.theme.setModalOpened(false)
                setConfirmModalOpened(false)
              }}
              onCancel={() => {
                actions.theme.setModalOpened(false)
                setConfirmModalOpened(false)
              }}/>}
            wrapper={ModalWrapper}
            ModalContent={ModalContent}
            CloseModal={CloseModal}
            onClose={() => {
              actions.theme.setModalOpened(false)
              setConfirmModalOpened(false)
            }}
            visible={confirmModalOpened ? true : false}
            onTop = {false}
          />
        : null
      }

      <StyledProductImage src={props.image ? props.image : item?.product?.node?.image?.sourceUrl}/>

      <ProductDetailWrapper justify={"space-between"} direction={"row"}>

        <ProductDetailContent direction={"column"} justify={"center"} align={"flex-start"}>
          <Category data={{texto: props.category ? props.category : item?.product?.node?.productCategories?.nodes[0]?.name}} defaultData={{font_type: 'Antetitulo'}}/>
          <Name data={{texto: props.name ? props.name : item?.product?.node?.name}} defaultData={{font_type: 'SubtitleLarge', font_family: 'Lexend Zetta'}}/>
          <ProductDescription data={{texto: props.description ? props.description : item?.product?.node?.description, type: 'div', font_family: 'Lexend Deca'}}/>
        </ProductDetailContent>

        <ProductSelectorContent>
          <ProductSelectorWrapper>

            <StyledProductDetailPrice price={props.price ? props.price : item?.product?.node?.price ? item?.product?.node?.price : '50' }/>

            {props.inCheckout ? (
              <RichText data={{texto: 'Cantidad: ' + props.initialQuantity, type: 'div'}}/>
            ) : (
              <StyledFormLayout justify={"center"}>
                <StyledForm onSubmit={handleOnSubmit}>
                  <AlertMessage/>
                  <Layout direction={"row"}>
                    <StyledInput
                      id="quantity" name="quantity"
                      type="number_range" placeholder=""
                      register={register}
                      errors={errors}
                      quantity={quantity}
                      NumberRangeHandler={NumberRangeHandler}
                    />
                  </Layout>
                </StyledForm>
              </StyledFormLayout>
            )}


          </ProductSelectorWrapper>
        </ProductSelectorContent>
      </ProductDetailWrapper>
    </ProductDetailLayout>
  );
};

export default connect(ProductDetail);

const StyledProductDetailPrice = styled(ProductDetailPrice)`
  order: 2;
  justify-content: flex-end;
  ${props => {
    return `
          @media ${props.breakPoints['tablet']} {
            justify-content: flex-start;          
          }`
    }
  }
`

const ProductDescription = styled(RichText)`
  opacity: 0.47;
`
const ProductDetailWrapper = styled(Layout)`
  width: auto;
  flex: 1;
`
const ProductDetailContent = styled(Layout)`
  width: auto;
  flex: 0 0 150px;
`

const ProductSelectorContent = styled(Layout)`
  width: auto;
  flex: 1;
`


const ProductDetailLayout = styled(Layout)`
  padding-left: 16px;
  ${props => {
    return `
          @media ${props.breakPoints['tablet']} {
            padding-left: 50px;          
          }`
  }
  }
`

const Category = styled(Text)`
  text-transform: uppercase;
`

const Name = styled(Text)`
  text-transform: uppercase;
  margin-top: -5px;
  margin-bottom: 0px;
  ${props => {
    return `
          @media ${props.breakPoints['tablet-wide']} {
            margin-top: 3px;
            margin-bottom: 7px;          
          }`
  }
  }
`

const StyledProductImage = styled.img`
  width: 39px;
  margin-right: 24px;
  ${props => {
    return `
          @media ${props.breakPoints['tablet-wide']} {
            width: 127px;
            margin-right: 37px;          
          }`
  }
  }`

const StyledFormLayout = styled(Layout)`
  justify-content: center;
  order: 1;
`

const StyledPrice = styled(Text)`
  justify-content: center;
  ${props => {
    return `
          @media ${props.breakPoints['tablet-wide']} {
            font-size: 28px !important;
            margin-bottom: 18px;         
          }`
  }
  }

`

const ProductSelectorWrapper = styled(Layout)`
  max-width: 92px;
  justify-content: flex-end;
  align-items: flex-end;

  ${StyledFormLayout} {
    order: 1;
  }

  ${StyledPrice} {
    order: 2;
    font-size: 14px
  }

  ${props => {
    return `
          @media ${props.breakPoints['tablet-wide']} {
            max-width: 190px;
            justify-content: flex-start;
            align-items: center;
            align-self: auto;
            ${StyledFormLayout} {
              order: 2;
            }  
            ${StyledPrice} {
              order: 1;
            }          
          }`
  }
  }
`

const StyledForm = styled(Form)`
  width: 188px;

  img {
    top: 8px;
    ${props => {
      return `
          @media ${props.breakPoints['tablet-wide']} {
                top: 12px;          
          }`
    }
    }
  }

`

const StyledInput = styled(FormControl)`
  text-align: center;
  height: 40px !important;
  ${props => {
    return `
          @media ${props.breakPoints['tablet-wide']} {
            width: 100%;              
            height: 50px ;         
          }`
  }
  }



`
