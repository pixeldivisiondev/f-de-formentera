import Layout from "../../../../objects/Layout/Layout";
import Text from "../../../../objects/Text/Text";
import Transporte from "../../../../assets/images/icons/transport.svg";
import Ok from "../../../../assets/images/icons/ok.svg";
import {connect, styled} from "frontity";
import ProductDetail from "./ProductDetail";
import AddCoupon from "../Right/AddCoupon";
import {parsePriceToFloat} from "../../../../lib/utils/prices";
import moment from 'moment';

const LeftContainer = ({state, ...props}) => {

  LeftContainerStyled.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  StyledAddCoupon.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  return (
    <LeftContainerStyled justify={"flex-start"} className={props.className}>
      <ContentWrapper data={{section_background_color: 'gray-base'}}>
        {

            state.cart.contents.nodes.map((item, key) => {
              return (
                <ProductDetail item={item}  key={key}/>
              )

            })

        }
      </ContentWrapper>
      {
        Number(state.cart.shippingTotal.replace(/[^0-9.-]+/g,""))  > 0 ?
          <Envio data={{section_background_color: 'blue-base'}} justify={"flex-start"}>
            <img src={Transporte} />
            <LineItemContainer justify={"flex-start"}>
              <Text data={{texto: 'Aún te faltan ' + (state.cart.availableShippingMethods[0].rates[0].min_amount  - (parsePriceToFloat(state.cart.subtotal) + parsePriceToFloat(state.cart.contentsTax) + parsePriceToFloat(state.cart.discountTax)) ).toFixed(2).replace(".",",") +'€ para tener gastos de envío gratis'}}   defaultData={{font_type: 'PrimarySmallMedium'}}  />
            </LineItemContainer>
          </Envio>
           : null
      }


      <LineItem justify={"flex-start"} align={"flex-start"} direction={"row"}>
        <img src={Ok} />
        <LineItemContainer  align={"flex-start"} direction={"column"}>
          <Text data={{texto: 'Entrega prevista ' + moment().add(1, 'days').format('D/MM')+' o '+moment().add(2, 'days').format('D/MM')}}  defaultData={{font_type: 'PrimaryMediumLarge'}} />
          <LineText data={{texto: '(Excepto festivos y fines de semana; haciendo el pedido antes de las 16:30)'}} defaultData={{font_type: 'PrimarySmall'}}/>
        </LineItemContainer>
      </LineItem>

      {
        Number(state.cart.shippingTotal.replace(/[^0-9.-]+/g,""))  > 0 ?
          <LineItem justify={"flex-start"} align={"flex-start"} direction={"row"}>
            <img src={Ok} />
            <LineItemContainer  align={"flex-start"} direction={"column"}>
              <Text data={{texto: state.cart.shippingTotal.replace(".",",")+' Gastos de envío a domicilio'}}  defaultData={{font_type: 'PrimaryMediumLarge'}} />
            </LineItemContainer>
          </LineItem>
          : null
      }

      <LineItem justify={"flex-start"} align={"flex-start"} direction={"row"}>
        <img src={Ok} />
        <LineItemContainer  align={"flex-start"} direction={"column"}>
          <Text data={{texto: 'Envío gratuito'}} defaultData={{font_type: 'PrimaryMediumLarge'}}/>
          <LineText data={{texto: 'Por compras superiores a '+state.cart.availableShippingMethods[0].rates[0].min_amount +'€'}} defaultData={{font_type: 'PrimarySmall'}} />
        </LineItemContainer>
      </LineItem>

      <StyledAddCoupon />

    </LeftContainerStyled>

  );
};

export default connect(LeftContainer);

const ContentWrapper = styled(Layout)`
  padding-top: 10px;
  padding-bottom: 16px;
  margin-bottom: 16px;
`

const StyledAddCoupon = styled(AddCoupon)`
   padding-top: 15px;
   border-top: 1px solid rgba(0, 78, 102, 0.6);  
   display: flex;
   ${props => {
      return `
          @media ${props.breakPoints['tablet-wide']} {
            display: none;          
          }`
      }
    }

`

const Envio = styled(Layout)`
  img {
    margin-right: 14px;
  }
  padding: 10px 12px;  
  margin-bottom: 16px;
`

const LineItem = styled(Layout)`
  img {
    margin-right: 10px;
  }  
  margin-bottom: 16px;
`

const LineText = styled(Text)`
  opacity: 0.6;
  margin-top: 4px;
`

const LineItemContainer = styled(Layout)`
  width: auto;
  flex: 1;
`

const LeftContainerStyled = styled(Layout)`
  flex: 1;
  width: 100%;
  margin-top: 8px;
  align-self: auto;
  padding-bottom: 24px;
  ${props => {
  return `
          @media ${props.breakPoints['tablet-wide']} {
            max-width: 715px;        
             margin-top: 32px; 
          }`
      }
    }  
  
`
