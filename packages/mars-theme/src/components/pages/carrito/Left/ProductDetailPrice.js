import React from 'react';
import Layout from "../../../../objects/Layout/Layout";
import {styled, connect} from "frontity";
import Text from "../../../../objects/Text/Text";
import { parsePrice } from "../../../../lib/utils/prices";

const ProductDetailPrice = ({state, ...props}) => {

  StyledPrice.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  Entero.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  Decimal.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  const p = parsePrice(props.price);

  return (
    <StyledPrice className={props.className}>
      <Entero data={{texto:  p.int}} defaultData={{font_type: 'H1', font_family: 'Lexend Zetta'}}/>
      <Decimal data={{texto:  ","+p.decimal}} defaultData={{font_type: 'H1', font_family: 'Lexend Zetta'}}/>
    </StyledPrice>
  );
};

export default connect(ProductDetailPrice);

const StyledPrice = styled(Layout)`
  justify-content: flex-start;  
  align-items: flex-end;
  ${props => {
  return `
          @media ${props.breakPoints['tablet-wide']} {
            font-size: 28px !important;
            margin-bottom: 18px;         
          }`
    }
  }
  
`


const Entero = styled(Text)`
  font-size: 18px !important;
  line-height: 24px !important;
  letter-spacing: -0.12em !important;
  ${props => {
  return `
          @media ${props.breakPoints['tablet-wide']} {
            font-size: 28px !important;
            line-height: 24px !important;
            letter-spacing: -0.12em !important;  
          }`
    }
  }
`

const Decimal = styled(Text)`
  font-size: 14px !important;
  line-height: 24px !important;
  letter-spacing: -0.12em !important;
  ${props => {
  return `
          @media ${props.breakPoints['tablet-wide']} {
            font-size: 16px !important;
            line-height: 16px !important;
            letter-spacing: -0.12em !important;  
          }`
    }
  }
`
