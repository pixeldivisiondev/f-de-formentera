import React, {useState, useRef} from 'react';
import {styled, connect} from "frontity";
import Layout from "../../../../objects/Layout/Layout";
import Text from "../../../../objects/Text/Text";
import Button from "../../../../objects/Button/Button";


const DeleteModal = ({state, libraries, actions, ...props}) => {

  const [sending, setSending ] = useState(false)

  return (
    <Layout align={"flex-start"} direction={"column"}>
      <StyledTitle data={{texto: 'vas a eliminar este producto...', font_type: 'H1', font_family: 'Lexend Zetta'}}/>
      <StyledText data={{texto: '¿Estás seguro de que quieres eliminar este producto de tu carrito?', font_type: 'PrimaryMedium'}}/>
      <Layout justify={"flex-start"}>
        <LeftButton disabled={sending} data={{tipo: 'primary', tamano: 'squared', texto: "volver a inicio"}} onClick={props.onCancel}/>
        <RightButton disabled={sending} data={{tipo: 'outlined', tamano: 'squared', texto: sending ? "ELIMINANDO..." : "ELIMINAR"}} onClick={() => {setSending(true); props.onDelete();  }}/>
      </Layout>
    </Layout>
  );
};

export default connect(DeleteModal);


const UpperText = styled(Text)`
  text-transform: uppercase;  
`

const StyledTitle = styled(UpperText)`
  margin-bottom: 24px;  
`

const StyledText = styled(Text)`
  margin-bottom: 40px;
  opacity: 0.6;  
`

const RightButton = styled(Button)`
  min-width: 267px;
  margin-left: 24px;  
`

const LeftButton = styled(Button)`
  min-width: 267px;
`
