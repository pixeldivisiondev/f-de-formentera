import React, {useState} from 'react';
import UserIcon from "../../../assets/images/icons/user";
import BagIcon from "../../../assets/images/icons/bag";
import ChevronDownIcon from "../../../assets/images/icons/chevron-down";
import {styled} from "frontity";
import Layout from "../../../objects/Layout/Layout";
import Text from "../../../objects/Text/Text";

const FaqItem = (props) => {
  const [open, setOpen] = useState(props.isOpen);

  let icon = '';

  switch (props.icon) {
    case 'user':
      icon = <UserIcon/>;
      break;
    case 'orders':
      icon = <BagIcon/>;
      break;
    default:
      break;
  }
  return (
    <LayoutStyled>
      <HeaderStyled justify="space-between" onClick={() => setOpen(!open)}>
        <TitleStyled data={{texto: props.title, font_type: 'PrimaryMedium'}}/>

        <ChevronDownIconStyled isOpen={open}>
          <ChevronDownIcon/>
        </ChevronDownIconStyled>
      </HeaderStyled>
      <BodyStyled isOpen={open}>
        {props.children}
      </BodyStyled>
    </LayoutStyled>
  )
};

export default FaqItem;

const LayoutStyled = styled(Layout)`
  border-bottom: 1px solid #E5EDEF;
`;

const TitleStyled = styled(Text)`
  color: inherit;
  flex-grow: 1;  
  flex: 1 1 calc(100% - 40px);
  padding: 12px 0;
`;

const HeaderStyled = styled(Layout)`
  color: var(--color-blue-light);
  min-height: 60px;
  cursor: pointer;
`;

const BodyStyled = styled(Layout)`
  background-color: var(--color-background-base);
  padding: 0 16px;
  user-select: none;
  max-height: 0;
  height: auto;
  overflow: hidden;
  min-height: initial;
  transition: all 0.5s;
  
  ${props => props.isOpen ? `
      padding: 16px;
      max-height: 50rem;
      transition: all 0.5s;
    ` : ``};
`;

const ChevronDownIconStyled = styled.div
  `
transition: 0.3s all ease-in-out;

  ${props => props.isOpen ? `transform: rotate(180deg);` : `transform: rotate(0deg);`}
`
;
