import React from 'react';
import {connect, styled} from "frontity";
import {useGoogleLogin} from 'react-google-login';

import Layout from "../../../../objects/Layout/Layout";
import Text from "../../../../objects/Text/Text";
import Button from "../../../../objects/Button/Button";
import {ApolloError} from "@apollo/client";

export default connect(function LoginSocialGoogle({libraries, actions, ...props}) {

  const clientId = '335662616864-6hder35g6b0jkf50v7i9ktnt2v5o94vg.apps.googleusercontent.com';

  const onSuccess = async e => {
    const email = e.profileObj.email;
    const googleId = btoa(e.googleId);
    const firstName = e.profileObj.givenName;
    const lastName = e.profileObj.familyName;

    const r = await libraries.register.loginUser(email, googleId);
    if (r instanceof ApolloError) {
      const r2 = await libraries.register.registerUser(firstName, lastName, email, googleId);

      if (r2 instanceof ApolloError) {
        let errorMessage = r2.message;

        if (errorMessage.includes('Ya hay una cuenta registrada')) {
          errorMessage = 'Ya hay una cuenta registrada con tu dirección de correo electrónico. Prueba a iniciar sesión con tu correo electrónico y contraseña.';
        }

        actions.theme.setAlertMessage(errorMessage, false, 'error');
      } else {
        libraries.auth.authRegisterJwtToken(r2.authToken, r2.refreshToken, r2.customer.id);
        actions.theme.setLoginName(firstName);

        await actions.router.set('/mi-cuenta/');
      }
    } else {
      libraries.auth.authRegisterJwtToken(r.authToken, r.refreshToken, r.customer.id);
      actions.theme.setLoginName(firstName);

      await actions.router.set('/mi-cuenta/');
    }
  };

  const onFailure = e => {
    actions.theme.setAlertMessage('Ha ocurrido un error al intentar conectar con Google.', false, 'error');
  };

  const {signIn, loaded} = useGoogleLogin({
    onSuccess,
    onFailure,
    clientId,
    cookiePolicy: 'single_host_origin'
  });

  const handleOnClick = () => {
    signIn();
  }

  const buttonContent = (
    <Layout>
      <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M22.0792 11.9996C22.0792 11.2687 22.0199 10.5339 21.8935 9.81482H11.752V13.9552H17.5595C17.3185 15.2905 16.5442 16.4718 15.4103 17.2225V19.909H18.8751C20.9098 18.0363 22.0792 15.2708 22.0792 11.9996Z" fill="#4285F4"/>
        <path d="M11.7522 22.5046C14.6521 22.5046 17.0976 21.5525 18.8794 19.909L15.4146 17.2225C14.4506 17.8783 13.2061 18.2497 11.7562 18.2497C8.95116 18.2497 6.57282 16.3573 5.71946 13.813H2.14404V16.5825C3.96928 20.2132 7.68693 22.5046 11.7522 22.5046Z" fill="#34A853"/>
        <path d="M5.71521 13.813C5.26482 12.4776 5.26482 11.0316 5.71521 9.69629V6.92682H2.14374C0.618754 9.96494 0.618754 13.5443 2.14374 16.5824L5.71521 13.813Z" fill="#FBBC04"/>
        <path d="M11.7522 5.25567C13.2851 5.23196 14.7667 5.80877 15.8768 6.86757L18.9465 3.79785C17.0028 1.97261 14.4229 0.969119 11.7522 1.00072C7.68693 1.00072 3.96928 3.29215 2.14404 6.92683L5.71551 9.6963C6.56492 7.14807 8.94721 5.25567 11.7522 5.25567Z" fill="#EA4335"/>
      </svg>


      <Text data={{texto: "GOOGLE", font_type: 'ButtonLarge'}}/>
    </Layout>
  );

  return (
    <StyledLoginSocialGoogle preventDefault={true} onClick={handleOnClick} data={{tipo: 'outlined', tamano: 'squared', texto: buttonContent}}/>
  )
});

const StyledLoginSocialGoogle = styled(Button)`
  height: 40px;
  margin-top: 0 !important;
  margin-bottom: 0 !important;
  padding: 0;
  
  svg {
    margin-right: 14px;
  }
`;
