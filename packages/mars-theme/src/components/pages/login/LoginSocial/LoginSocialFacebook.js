import React from 'react';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import {connect, styled} from "frontity";

import Layout from "../../../../objects/Layout/Layout";
import Text from "../../../../objects/Text/Text";
import Button from "../../../../objects/Button/Button";
import {ApolloError} from "@apollo/client";

export default connect(function LoginSocialFacebook({actions, libraries, ...props}) {

  const handleFacebook = async e => {
    //e.preventDefault();
    //e.stopPropagation();
    if (e.status && e.status === 'unknown') {
      actions.theme.setAlertMessage('Ha ocurrido un error inesperado al conectarse con Facebook', false, 'error');
    } else {
      const email = e.email;
      const facebookId = btoa(e.userID);
      let [firstNameStr, ...lastNameStr] = e.name.split(" ");
      lastNameStr = lastNameStr.join(" ");

      const firstName = firstNameStr;
      const lastName = lastNameStr;

      const r = await libraries.register.loginUser(email, facebookId);

      if (r instanceof ApolloError) {
        const r2 = await libraries.register.registerUser(firstName, lastName, email, facebookId);

        if (r2 instanceof ApolloError) {
          let errorMessage = r2.message;

          if (errorMessage.includes('Ya hay una cuenta registrada')) {
            errorMessage = 'Ya hay una cuenta registrada con tu dirección de correo electrónico. Prueba a iniciar sesión con tu correo electrónico y contraseña.';
          }

          actions.theme.setAlertMessage(errorMessage, false, 'error');
        } else {
          libraries.auth.authRegisterJwtToken(r2.authToken, r2.refreshToken, r2.customer.id);
          actions.theme.setLoginName(firstName);

          await actions.router.set('/mi-cuenta/');
        }
      } else {
        libraries.auth.authRegisterJwtToken(r.authToken, r.refreshToken, r.customer.id);
        actions.theme.setLoginName(firstName);

        await actions.router.set('/mi-cuenta/');
      }
    }
  };

  const buttonContent = (
    <Layout>
      <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M15.81 3.985H18.001V0.169C17.623 0.117 16.323 0 14.809 0C11.65 0 9.486 1.987 9.486 5.639V9H6V13.266H9.486V24H13.76V13.267H17.105L17.636 9.001H13.759V6.062C13.76 4.829 14.092 3.985 15.81 3.985Z" fill="#485C94"/>
      </svg>

      <Text data={{texto: "FACEBOOK", font_type: 'ButtonLarge'}}/>
    </Layout>
  );

  return (
    <React.Fragment>
      <FacebookLogin
        appId="3478751168889141"
        fields="name,email"
        callback={handleFacebook}
        render={renderProps => (
          <StyledLoginSocialFacebook preventDefault={true} onClick={(e) => { renderProps.onClick()}} data={{tipo: 'outlined', tamano: 'squared', texto: buttonContent}}/>
        )}
      />
    </React.Fragment>
  )
});

const StyledLoginSocialFacebook = styled(Button)`
  height: 40px;
  margin-top: 0 !important;
  margin-bottom: 0 !important;
  padding: 0px;
  
  svg {
    margin-right: 14px;
  }
`;
