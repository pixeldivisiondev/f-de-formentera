import React from 'react';
import {connect, styled} from "frontity";
import TwitterLogin from 'react-twitter-auth/lib/react-twitter-auth-component.js';

import Layout from "../../../../objects/Layout/Layout";
import Text from "../../../../objects/Text/Text";
import Button from "../../../../objects/Button/Button";

export default connect(function LoginSocialTwitter() {
  const buttonContent = (
    <Layout>
      <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path
          d="M22 5.01994C21.2191 5.3625 20.3869 5.58956 19.5194 5.69981C20.4119 5.16694 21.0931 4.32956 21.4133 3.32025C20.5812 3.81637 19.6624 4.16681 18.6833 4.36238C17.8932 3.52106 16.7671 3 15.5386 3C13.1551 3 11.2362 4.93463 11.2362 7.30631C11.2362 7.64756 11.2651 7.97569 11.3359 8.28806C7.75675 8.1135 4.58969 6.39806 2.46212 3.78488C2.09069 4.42931 1.87281 5.16694 1.87281 5.961C1.87281 7.452 2.64062 8.77369 3.78513 9.53887C3.09344 9.52575 2.41488 9.32494 1.84 9.00863C1.84 9.02175 1.84 9.03881 1.84 9.05587C1.84 11.148 3.33231 12.8857 5.28925 13.2861C4.93881 13.3819 4.55687 13.4278 4.1605 13.4278C3.88488 13.4278 3.60662 13.4121 3.34544 13.3543C3.90325 15.0592 5.48613 16.3127 7.36825 16.3534C5.9035 17.4992 4.04369 18.1896 2.03031 18.1896C1.67725 18.1896 1.33862 18.1738 1 18.1305C2.90706 19.3603 5.16719 20.0625 7.6045 20.0625C15.5267 20.0625 19.858 13.5 19.858 7.81163C19.858 7.62131 19.8514 7.43756 19.8423 7.25513C20.6967 6.64875 21.4146 5.89144 22 5.01994Z"
          fill="#2AA3EF"/>
      </svg>

      <Text data={{texto: "TWITTER", font_type: 'ButtonLarge'}}/>
    </Layout>
  );

  const onFailed = () => {

  }

  const onSuccess = () => {

  }

  return (
    <TwitterLogin
      loginUrl=""
      onFailure={onFailed}
      onSuccess={onSuccess}
      requestTokenUrl=""
    >
      <StyledLoginSocialTwitter data={{tipo: 'outlined', tamano: 'squared', texto: buttonContent}}/>
    </TwitterLogin>
  )
});

const StyledLoginSocialTwitter = styled(Button)`
  height: 40px;
  margin-top: 0 !important;
  margin-bottom: 0 !important;
  padding: 0;
  
  svg {
    margin-right: 14px;
  }
`;