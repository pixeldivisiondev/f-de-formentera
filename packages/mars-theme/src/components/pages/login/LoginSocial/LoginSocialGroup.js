import React from 'react';
import {connect, styled} from "frontity";

import Layout from "../../../../objects/Layout/Layout";
import LoginSocialFacebook from "./LoginSocialFacebook";
import LoginSocialGoogle from "./LoginSocialGoogle";
import LoginSocialTwitter from "./LoginSocialTwitter";

export default connect(function LoginSocialGroup({state, ...props}) {
  StyledLoginSocialGroup.defaultProps = {
    theme: state.theme
  };

  return (
    <StyledLoginSocialGroup justify="flex-start">
      <LoginSocialFacebook/>
      <LoginSocialGoogle/>
    </StyledLoginSocialGroup>
  )
});

const StyledLoginSocialGroup = styled(Layout)`
  margin: 16px 0 8px;
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 20px;
  
  > * {
    min-width: auto !important;
  }
  
  button:not([color]) {
    border: 0;
  }
  
  ${props => {
    return `  
        @media ${props.theme.breakPoints['tablet']} {
        display: flex;
          > * {
            min-width: 140px !important;
          }
          
          button:not([color]) {
            border: 0;
            
            > * {
           d
          }
        }
      `
  }}
`;
