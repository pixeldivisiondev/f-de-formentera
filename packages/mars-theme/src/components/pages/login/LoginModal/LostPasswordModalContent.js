import React, {useState} from 'react';
import {connect, styled} from "frontity";

import Text from "../../../../objects/Text/Text";
import {useForm} from "react-hook-form";
import AlertMessage from "../../../../objects/AlertMessage/AlertMessage";
import FlexGrid from "../../../../objects/FlexGrid/FlexGrid";
import {ApolloError} from "@apollo/client";
import LegalLostPassword from "./LegalLostPassword";
import FlexGridItem from "../../../../objects/FlexGrid/FlexGridItem";
import FormControl from "../../../../objects/Forms/FormControl";
import Form from "../../../../objects/Forms/Form";
import ButtonSubmit from "../../../../objects/ButtonSubmit/ButtonSubmit";

export default connect(function LostPasswordModalContent({state, actions, libraries, ...props}) {
  const {register, handleSubmit, watch, errors} = useForm();
  const [submitting, setSubmitting ] = useState(false);

  const handleOnSubmit = async (data, e) => {
    setSubmitting(true)
    const r = await libraries.register.sendPasswordResetEmail(data.reset_email);

    if (r instanceof ApolloError) {
      let errorMessage = r.message;

      if (errorMessage.includes('There is no user registered with that email address')) {
        errorMessage = 'No existe un usuario registrado con ese email.';
      }

      actions.theme.setAlertMessage(errorMessage, true, 'error');
    } else {
      actions.theme.setAlertMessage('El mensaje se ha enviado correctamente. Revisa tu bandeja de entrada y sigue las instrucciones.', true, 'success');
      e.target.reset();
    }
    setSubmitting(false)
  };

  return (
    <React.Fragment>
      <AlertMessage inModal={true}/>
      <LostPasswordTitleStyled data={{texto: 'NO RECUERDO LA CONTRASEÑA', font_type: 'H1Mobile', font_family: 'Lexend Zetta'}}/>

      <LostPasswordCopyStyled data={{texto: 'Introduce tu dirección de email de tu cuenta de F de Formentera y te enviaremos un link para reestablecer tu contraseña:'}}/>

      <Form onSubmit={handleSubmit(handleOnSubmit)}>
        <FlexGrid justify="flex-start">
          <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
            <FormControl
              id="reset_email" name="reset_email"
              type="email" placeholder="Correo electrónico"
              register={register}
              errors={errors}
            />
          </FlexGridItem>
        </FlexGrid>

        <div style={{height: '20px'}}/>

        <FlexGrid>
          <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
            <ButtonSubmit disabled={submitting} text={submitting ? 'ENVIANDO...' : "Enviar" }/>
          </FlexGridItem>

          {props.hideLogin ? (
            ''
          ) : (
            <div onClick={props.handleOpenLogin}><LostPasswordStyled data={{texto: 'VOLVER AL LOGIN', font_type: 'PrimarySmall'}}/></div>
          )}
        </FlexGrid>
      </Form>

      <LegalLostPassword/>
    </React.Fragment>
  )
});

const LostPasswordStyled = styled(Text)`
  color: var(--color-blue-light); 
  margin-top: 12px;
  letter-spacing: 2px !important;
  cursor: pointer;
`;

const LostPasswordTitleStyled = styled(Text)`
  margin-bottom: 32px;
  padding-right: 32px;
`;

const LostPasswordCopyStyled = styled(Text)`
  margin-bottom: 24px;
  opacity: 0.6;
  max-width: 420px;
`;
