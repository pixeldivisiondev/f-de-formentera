import React from 'react';
import {connect, styled} from "frontity";

import Text from "../../../../objects/Text/Text";

export default connect(function Legal() {
  const legal = 'Al acceder o registrarse desde tu red social, Formentera Mediterranean Spirits SL., recibe datos de carácter personal desde la plataforma elegida, los cuales utilizaremos para crear un perfil de usuario y enviarte comunicaciones personalizadas. Puede acceder, rectificar y suprimir los datos, así como otros derechos, tal y como se explica en la información adicional disponible en nuestra política de privacidad.';

  return (
    <>
      <TitleStyled data={{texto: 'Información básica privacidad:', font_type: 'PrimarySmall'}}/>
      <CopyStyled data={{texto: legal, font_type: 'PrimarySmall'}}/>
    </>
  )
});

const TitleStyled = styled(Text)`
  margin-top: 22px;
  margin-bottom: 12px;
`;

const CopyStyled = styled(Text)`
  opacity: 0.6;
  
  p:not(:last-child) {
    margin-bottom: 16px;
  }
`;
