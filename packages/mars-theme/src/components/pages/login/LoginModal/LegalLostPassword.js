import React from 'react';
import {connect, styled} from "frontity";

import Text from "../../../../objects/Text/Text";

export default connect(function LegalLostPassword() {
  const legal = 'Al pulsar el botón “Enviar”, Formentera Mediterranean Spirits SL., habilitará el acceso con tus credenciales a un sistema de inicio de sesión unificado. Puedes acceder, rectificar y suprimir los datos, así como ejercer otros derechos, tal y como se explica en la información adicional disponible en la política de privacidad.';

  return (
    <>
      <TitleStyled data={{texto: 'Información básica privacidad:', font_type: 'PrimarySmall'}}/>
      <CopyStyled data={{texto: legal, font_type: 'PrimarySmall'}}/>
    </>
  )
});

const TitleStyled = styled(Text)`
  margin-top: 32px;
  margin-bottom: 12px;
`;

const CopyStyled = styled(Text)`
  opacity: 0.6;
  
  p:not(:last-child) {
    margin-bottom: 16px;
  }
`;
