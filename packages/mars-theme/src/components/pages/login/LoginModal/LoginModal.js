import React, {useState} from 'react';
import {connect} from "frontity";

import Layout from "../../../../objects/Layout/Layout";
import LoginModalContent from "./LoginModalContent";
import LostPasswordModalContent from "./LostPasswordModalContent";

export default connect(function LoginModal({state, actions, libraries, ...props}) {
  const [isLogin, setIsLogin] = useState(true);

  return (

    <Layout direction="column" align="flex-start">
      {isLogin ? (
        <LoginModalContent handleOpenLostPassword={() => setIsLogin(false)} onSuccess={props.onSuccess} redirect={props.redirect}/>
      ) : (
        <LostPasswordModalContent handleOpenLogin={() => setIsLogin(true)} onSuccess={props.onSuccess} redirect={props.redirect}/>
      )}
    </Layout>
  )
});
