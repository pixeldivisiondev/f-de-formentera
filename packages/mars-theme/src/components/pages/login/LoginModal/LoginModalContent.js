import React, {useState} from 'react';
import {connect, styled} from "frontity";

import Text from "../../../../objects/Text/Text";
import {useForm} from "react-hook-form";
import AlertMessage from "../../../../objects/AlertMessage/AlertMessage";
import FlexGrid from "../../../../objects/FlexGrid/FlexGrid";
import FlexGridItem from "../../../../objects/FlexGrid/FlexGridItem";
import FormControl from "../../../../objects/Forms/FormControl";
import ButtonSubmit from "../../../../objects/ButtonSubmit/ButtonSubmit";
import LoginSocialGroup from "../LoginSocial/LoginSocialGroup";
import Legal from "./Legal";
import Form from "../../../../objects/Forms/Form";
import Button from "../../../../objects/Button/Button";
import {ApolloError} from "@apollo/client";

export default connect(function LoginModalContent({state, actions, libraries, ...props}) {
  LoginGridStyled.defaultProps = {
    theme: state.theme
  };

  // TODO: Eliminar los datos de prueba
  const defaultVal = {
    log_email: '',
    log_password: ''
  };

  const defaultVal2 = {
    log_email: '',
    log_password: ''
  };

  const {register, handleSubmit, watch, errors} = useForm({
    defaultValues: defaultVal2
  });

  const [submitting, setSubmitting ] = useState(false);

  const handleOnSubmit = async data => {
    setSubmitting(true);
    const r = await libraries.register.loginUser(data.log_email, data.log_password);

    if (r instanceof ApolloError) {
      let errorMessage = r.message;

      if (errorMessage.includes('invalid_email')) {
        errorMessage = 'No existe un usuario registrado con ese email.';
      }

      if (errorMessage.includes('incorrect_password')) {
        errorMessage = 'La contraseña introducida no es correcta';
      }

      actions.theme.setAlertMessage(errorMessage, true, 'error');
      setSubmitting(false);
    } else {

      libraries.auth.authRegisterJwtToken(r.authToken, r.refreshToken, r.customer.id);
      actions.theme.setLoginName(r.customer.firstName);
      if(r.customer.metaData !== null){
        actions.theme.setRole('empresa');
      }else{
        actions.theme.setRole('customer');
      }


      if(props.onSuccess) {
        await props.onSuccess();
      }
      setSubmitting(false);
      props.redirect !== false ? await actions.router.set('/mi-cuenta/') : null;

    }

  };

  const registroText = "¿No tienes cuenta? <a href='/registro/'>Regístrate</a>";

  return (
    <React.Fragment>
      <AlertMessage inModal={true}/>
      <LoginTitleStyled data={{texto: 'INICIA SESIÓN CON TU EMAIL', font_type: 'H1Mobile', font_family: 'Lexend Zetta'}}/>

      <LoginGridStyled forEmpresa={props.forEmpresa}>
        <FlexGridItem layout={{movil: 12, tablet: 12, desktop: props.forEmpresa ? 12 : 6}}>
          <Form onSubmit={handleSubmit(handleOnSubmit)}>
            <FlexGrid>
              <FlexGridItem layout={{movil: 12, tablet: 12, desktop: 12}}>
                <FormControl
                  id="log_email" name="log_email"
                  type="email" placeholder="Correo electrónico"
                  register={register}
                  errors={errors}
                />
              </FlexGridItem>

              <FlexGridItem layout={{movil: 12, tablet: 12, desktop: 12}}>
                <FormControl
                  id="log_password" name="log_password"
                  type="password" placeholder="Contraseña"
                  register={register}
                  errors={errors}
                />
                <div onClick={props.handleOpenLostPassword}><LostPasswordStyled data={{texto: 'NO RECUERDO MI CONTRASEÑA', font_type: 'PrimarySmall'}}/></div>
              </FlexGridItem>

              <FlexGridItem layout={{movil: 12, tablet: 12, desktop: 12}}>
                <ButtonSubmit disabled={submitting} text={submitting ? 'Enviando...' : "Iniciar sesión"} size="full"/>
                
              </FlexGridItem>
            </FlexGrid>
          </Form>
        </FlexGridItem>

        { !props.forEmpresa
          ?
          <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
            <Text data={{texto: "O también puedes iniciar sesión con:"}}/>
            <LoginSocialGroup/>

            <div>
              <LoginLinkRegistroStyled data={{texto: registroText}}/>
              <Legal/>
            </div>
          </FlexGridItem>
          : null
        }

      </LoginGridStyled>
    </React.Fragment>
  )
});

const LoginGridStyled = styled(FlexGrid)`
  
  ${props => {
  return `  
        @media ${props.theme.breakPoints['tablet']} {
        --grid-gap: 0.001px;
        
          > *:first-child {
            border-right: ${props.forEmpresa ? `none`: `2px solid #F5F7F6`} ;
            padding-right: 50px;
          }
          
          > *:last-child {
          padding-left: 50px;
          }
        }
      `
}}
`;

const LostPasswordStyled = styled(Text)`
  color: var(--color-blue-light);
  text-align: right;
  margin-top: 12px;
  margin-bottom: 24px;
  letter-spacing: 2px !important;
  cursor: pointer;
`;

const LoginTitleStyled = styled(Text)`
  margin-bottom: 40px;
  padding-right: 32px;
`;

const LoginLinkRegistroStyled = styled(Text)`
  margin-bottom: 24px;
  margin-top: 24px;
  padding-top: 24px;
  border-top: 2px solid #F5F7F6;
  
  a {
    color: var(--color-blue-light);
    text-decoration: none;
  }
`;
