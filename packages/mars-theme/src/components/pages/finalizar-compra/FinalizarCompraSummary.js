import React from 'react';
import {connect, styled} from "frontity";

import Text from "../../../objects/Text/Text";
import Layout from "../../../objects/Layout/Layout";
import ButtonLink from "../../objects/ButtonLink/ButtonLink";
import {formatAddress} from "../../../lib/checkout/addresses";
import {formatCard} from "../../../lib/checkout/util";
import moment from "moment";

const FinalizarCompraSummary = ({state, actions, ...props}) => {
  LayoutStyled.defaultProps = {
    theme: state.theme
  };

  const checkoutData = state.theme.checkoutData;

  let pm = null;
  let stripeTarjeta = null

  if (checkoutData.paymentMethodStep) {
    if (checkoutData.paymentMethodStep.method.type === 'stripe') {
      pm = checkoutData.paymentMethodStep.method.paymentMethod;
      stripeTarjeta = formatCard(pm?.card);
    }
  }

  return (
    <LayoutStyled>
      <SummaryLineStyled>
        <SummaryTitleStyled data={{texto: 'CONTACTO', font_type: 'LabelSmall', font_family: 'Lexend Zetta'}}/>

        {checkoutData.user ? (
          <GroupLayoutStyled>
            <RightText data={{texto: checkoutData.user.email}}/>
            { props.editable !== false ? <ButtonLink texto="EDITAR" handleOnClick={() => actions.theme.setCheckoutStep(1)}/> : null }

          </GroupLayoutStyled>
        ) : (
          <LinkStyled data={{texto: "INTRODUCE TUS DATOS", font_type: 'LabelSmall', font_family: 'Lexend Zetta'}}/>
        )}

      </SummaryLineStyled>

      <SummaryLineStyled>
        <SummaryTitleStyled data={{texto: 'DIRECCIÓN', font_type: 'LabelSmall', font_family: 'Lexend Zetta'}}/>

        {checkoutData.billingShippingStep?.isValid ? (
          <GroupLayoutStyled>
            {formatAddress(checkoutData.billingShippingStep.shipping)}
            { props.editable !== false ? <ButtonLink texto="EDITAR" handleOnClick={() => actions.theme.setCheckoutStep(2)}/> : null }
          </GroupLayoutStyled>
        ) : (
          <LinkStyled data={{texto: "INTRODUCE TU DIRECCIÓN", font_type: 'LabelSmall', font_family: 'Lexend Zetta'}}/>
        )}
      </SummaryLineStyled>

      <SummaryLineStyled>
        <SummaryTitleStyled data={{texto: 'PAGO', font_type: 'LabelSmall', font_family: 'Lexend Zetta'}}/>

        {checkoutData.paymentMethodStep?.isValid ? (
          <GroupLayoutStyled>
            {checkoutData.paymentMethodStep.method.type === 'paypal' ? (
              <RightText data={{texto: 'PayPal'}}/>
            ) : ('')}

            {checkoutData.paymentMethodStep.method.type === 'paypal_express' ? (
              <RightText data={{texto: 'PayPal Express'}}/>
            ) : ('')}

            {checkoutData.paymentMethodStep.method.type === 'stripe' ? (
              <RightText data={{texto: stripeTarjeta}}/>
            ) : ('')}

            {
              props.editable !== false ? <ButtonLink texto="EDITAR" handleOnClick={() => actions.theme.setCheckoutStep(3)}/> : null
            }

          </GroupLayoutStyled>
        ) : (
          <LinkStyled data={{texto: "AÑADE MÉTODO DE PAGO", font_type: 'LabelSmall', font_family: 'Lexend Zetta'}}/>
        )}
      </SummaryLineStyled>

      <SummaryLineStyled>
        <SummaryTitleStyled data={{texto: 'ENVÍO', font_type: 'LabelSmall', font_family: 'Lexend Zetta'}}/>
        <RightText data={{texto: 'Entrega prevista ' + moment().add(1, 'days').format('D/MM')+' o '+moment().add(2, 'days').format('D/MM')}} />
      </SummaryLineStyled>

      <SummaryLineStyled>
        <div>
          <SummaryTitleStyled data={{texto: 'IVA', font_type: 'LabelSmall', font_family: 'Lexend Zetta'}}/>
          <SummaryTitleStyled data={{texto: 'TOTAL', font_type: 'LabelSmall', font_family: 'Lexend Zetta'}}/>
        </div>

        <div>
          <RightText data={{texto: state.cart?.totalTax}}/>
          <RightText data={{texto: state.cart?.total, font_type: 'TotalPrice'}}/>
        </div>
      </SummaryLineStyled>
    </LayoutStyled>
  )
};

export default connect(FinalizarCompraSummary);

const LayoutStyled = styled(Layout)`
  background-color: white;
  height: fit-content;
  width: var(--checkout-summary-width);

  ${props => {
    return `
    @media ${props.theme.breakPoints['tablet-wide']} {
      background-color: var(--color-background-base);
      padding: 8px 24px;
    }
  `
  }}
`;

const ButtonNoClick = styled(ButtonLink)`
  cursor: auto;
`

const GroupLayoutStyled = styled(Layout)`
  flex-direction: column;
  justify-content: flex-end;
  align-items: flex-end;
  width: initial;
  text-align: right;
  max-width: 200px;
  word-break: break-all;
`;

const SummaryLineStyled = styled(Layout)`
  align-items: flex-start;
  justify-content: space-between;
  padding: 16px 4px;
  flex-wrap: nowrap;

  &:not(:last-child) {
    border-bottom: 1px solid #E5EDEF;
  }
`;

const SummaryTitleStyled = styled(Text)`
  margin-right: 4px;
  letter-spacing: -1px !important;
  padding-right: 16px;
`;

const LinkStyled = styled(Text)`
  color: var(--color-blue-light);
  cursor: pointer;
  letter-spacing: -1px !important;
  pointer-events: none;
  opacity: 0.5;
`;

const RightText = styled(Text)`
  text-align: right;
`
