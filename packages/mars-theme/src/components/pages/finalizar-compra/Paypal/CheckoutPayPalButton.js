import React from "react";
import ReactDOM from 'react-dom'
import {connect} from "frontity";
//import {PayPalButton} from "react-paypal-button-v2";

export default connect(function CheckoutPayPalButton({state, actions, ...props}) {
  const PayPalButton = paypal.Buttons.driver("react", {React, ReactDOM});

  const onApprove = (data, actions) => {
    props.handlePaypalOnApprove(data, actions);
  }

  const onCancel = () => {
    actions.theme.setAlertMessage('Vaya, el pago en Paypal se ha cancelado y no hemos podido terminar su pedido', false, 'error');
  }

  const onError = err => {
    actions.theme.setAlertMessage('Ha ocurrido un error al pagar con PayPal. Prueba con otro método de pago', false, 'error');
  }

  return (
    <>
      <PayPalButton
        style={{
          color: 'gold',
          shape: 'rect',
          height: 50
        }}
        createOrder={async (data, actions) => {
          return actions.order.create({
            purchase_units: [{
              reference_id: "REF1",
              amount: {
                currency_code: "EUR",
                value: props.price,
              }
            }],
          });
        }}
        onApprove={(data, actions) => onApprove(data, actions)}
        onError={(err) => onError(err)}
        onCancel={onCancel}
      />
    </>
  )
});
