import React, {useEffect, useState} from 'react';
import {connect, styled} from "frontity";

import Text from "../../../../objects/Text/Text";
import CheckoutFormGuest from "../Forms/CheckoutFormGuest";
import CheckoutFormLogin from "../Forms/CheckoutFormLogin";

const StepContacto = ({state, actions, ...props}) => {
  const [isGuest, setIsGuest] = useState(true);
  useEffect(() => {

    let forEmpresa = false;
    if (typeof window !== 'undefined') {
      if(localStorage.getItem('Role') === 'empresa') {
        forEmpresa = true
      }
    }
    if(state.cart) {
      actions.analytics.checkoutStep(forEmpresa, 1, 'UNDEFINED')
    }


  })
  return (
    <React.Fragment>
      <TitleStyled data={{texto: 'ACCEDE A TU CUENTA', font_type: 'H1Mobile', font_family: 'Lexend Zetta'}}/>
        <CheckoutFormLogin handleChange={() => setIsGuest(!isGuest)}/>
    </React.Fragment>
  )
};

export default connect(StepContacto);

const TitleStyled = styled(Text)`
  margin-bottom: 42px;
`;
