import React, {useEffect, useState} from 'react';
import {connect, styled} from "frontity";

import Text from "../../../../objects/Text/Text";
import CheckoutFormGuest from "../Forms/CheckoutFormGuest";
import CheckoutFormLogin from "../Forms/CheckoutFormLogin";
import CheckoutFormDirecciones from "../Forms/CheckoutFormDirecciones";
import CheckoutFormDireccionesSelect from "../Forms/CheckoutFormDireccionesSelect";

const StepDirecciones = ({state, actions, libraries, ...props}) => {
  useEffect(() => {

    let forEmpresa = false;
    if (typeof window !== 'undefined') {
      if(localStorage.getItem('Role') === 'empresa') {
        forEmpresa = true
      }
    }
    if(state.cart) {
      actions.analytics.checkoutStep(forEmpresa, 2, 'UNDEFINED')
    }
  })


  return (
    <React.Fragment>
      <TitleStyled data={{texto: 'DIRECCIÓN', font_type: 'H1Mobile', font_family: 'Lexend Zetta'}}/>


      {props.isLogged ? (
        <CheckoutFormDireccionesSelect />
      ) : (
        <CheckoutFormDirecciones/>
      )}

    </React.Fragment>
  )
};

export default connect(StepDirecciones);

const TitleStyled = styled(Text)`
  margin-bottom: 24px;
`;
