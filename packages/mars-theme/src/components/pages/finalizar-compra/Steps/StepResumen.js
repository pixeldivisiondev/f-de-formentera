import React, {useEffect} from 'react';
import {connect, styled} from "frontity";

import Text from "../../../../objects/Text/Text";
import CheckoutFormSubmit from "../Forms/CheckoutFormSubmit";
import Summary from "../../../modulos/Checkout/Summary";

const StepResumen = ({state, actions, ...props}) => {
  GroupStyled.defaultProps = {
    theme: state.theme
  };

  const checkoutData = state.theme.checkoutData

  useEffect(() => {

    let forEmpresa = false;
    if (typeof window !== 'undefined') {
      if(localStorage.getItem('Role') === 'empresa') {
        forEmpresa = true
      }
    }

    if(state.cart) {
      actions.analytics.checkoutStep(forEmpresa, state.theme.checkoutStep, checkoutData?.paymentMethodStep?.method?.type)
    }
  })

  return (
    <React.Fragment>
      <GroupStyled>
        <TitleStyled data={{texto: 'RESUMEN DEL PEDIDO', font_type: 'H1Mobile', font_family: 'Lexend Zetta'}}/>

        <Summary/>
      </GroupStyled>

      <CheckoutFormSubmit onSubmit={props.onSubmit}/>
    </React.Fragment>
  )
};

export default connect(StepResumen);

const TitleStyled = styled(Text)`
  margin-bottom: 42px;
`;

const GroupStyled = styled.div`
  display: none;

  ${props => {
  return `
    @media ${props.theme.breakPoints['desktop']} {
      display: block;
    }
  `
}}
`;
