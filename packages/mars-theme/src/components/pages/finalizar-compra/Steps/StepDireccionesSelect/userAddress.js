import React from 'react';
import Text from "../../../../../objects/Text/Text";
import Layout from "../../../../../objects/Layout/Layout";
import {connect, styled} from "frontity";

const UserAddress = ({actions, ...props}) => {
  let name = props.item.firstName + " " + props.item.lastName;

  return (
    <StyledContent direction={"row"}>

      <LayoutStyledInput align={"flex-start"} justify={"flex-start"}>

        <StyledRadioButton>
            <StyledRadioButtonInput
              type="radio"
              name="test"
              onClick={() => {props.onClick(props.index);}}
              value={props.index}


            />


          <StyledRadioButtonLabel htmlFor={"test"}>
            <LayoutStyled align={"flex-start"} direction={"column"}>
              <TextStyled >
                <Nombre data={{texto: name}}/>
                <Layout direction={"column"} align={"flex-start"}>
                  <Text data={{texto: props.item.address1}}/>
                  <Text data={{texto: props.item.postcode + ' ' + props.item.city + ',España'}}/>
                  <Text data={{texto: props.item.phone}}/>
                </Layout>
              </TextStyled>

              <Layout justify={"space-between"}>
                <div onClick={() => { props.setEditting(props.index); actions.theme.setModalOpened(true) }}>
                  <EditText data={{texto: "EDITAR", font_type: 'ButtonLarge', color: 'blue-light'}}/>
                </div>
                {
                  props.addressesData.shipping.extra_shipping_addresses.length > 1 ?
                    <div onClick={(e) => {
                      e.preventDefault();
                      props.handleDelete(props.item.key, props.index)
                    }}>
                      <CursorText data={{texto: "ELIMINAR", font_type: 'ButtonLarge', color: 'blue-light'}}/>
                    </div>
                    : null
                }
              </Layout>
            </LayoutStyled>
          </StyledRadioButtonLabel>
        </StyledRadioButton>

      </LayoutStyledInput>




    </StyledContent>
  );
};

export default connect(UserAddress);

const StyledContent = styled(Layout)`
  margin-top: 10px;
`

const CursorText = styled(Text)`
  cursor: pointer;
`

const EditText = styled(CursorText)`
margin-right: 50px;
`

const Nombre = styled(Text)`
  margin-bottom: 5px; 
`

const StyledInput = styled.input`
  position: relative;
  top: 3px;
  margin-right: 10px;
`
const TextStyled = styled.div`  
  margin-bottom: 8px;
`;

const LayoutStyled = styled(Layout)`
  flex: 1 1;
`;
const LayoutStyledInput = styled(Layout)`
  flex: 1;
`;

const StyledRadioButton = styled.div`
  // radios and checkboxes
  --checkbox-radio-size: 18px;
  --checkbox-radio-gap: 4px; // gap between button and label
  --checkbox-radio-border-width: 1px;
  --checkbox-radio-line-height: 1;

  // radio buttons
  --radio-marker-size: 8px;

  // checkboxes
  --checkbox-marker-size: 12px;
  --checkbox-radius: 4px;

  display: flex;
`;

const StyledRadioButtonLabel = styled.label`
  line-height: var(--checkbox-radio-line-height);
  user-select: none;
  
  font-size: 16px;
  font-weight: bold;
  display: flex;
  align-items: flex-start;
  justify-content: space-between;
  
  &::before {
    content: '';
    //display: inline-block;
    cursor: pointer;
    position: relative;
    top: calc((1em * var(--checkbox-radio-line-height) - var(--checkbox-radio-size)) / 2);
    flex-shrink: 0;
    width: var(--checkbox-radio-size);
    height: var(--checkbox-radio-size);
    background-color: var(--color-white);
    border-width: var(--checkbox-radio-border-width);
    border-color: var(--color-primary-base);
    border-style: solid;
    background-repeat: no-repeat;
    background-position: center;
    //margin-right: var(--checkbox-radio-gap);
    transition: transform 0.2s, border 0.2s;
    border-radius: 50%;
    //padding: 8px 12px;
    font-weight: normal;
    display: inline-flex;
    margin-right: 16px;
  }
`;

const StyledRadioButtonInput = styled.input`
  position: absolute;
  padding: 0;
  margin: 0;
  opacity: 0;
  z-index: 1;
  cursor: pointer;
  height: var(--checkbox-radio-size);
  width: var(--checkbox-radio-size);  
  
  &:checked {
    + ${StyledRadioButtonLabel}::before {
      background-color: white;
      box-shadow: none;
      border-color: var(--color-primary-base);
      transition: transform 0.2s;
      background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16'%3E%3Cg class='nc-icon-wrapper' fill='%23004e66'%3E%3Ccircle cx='8' cy='8' r='8' fill='%23004e66'%3E%3C/circle%3E%3C/g%3E%3C/svg%3E");
      background-size: var(--radio-marker-size);
    }

    &:active,
    &:focus {
      + ${StyledRadioButtonLabel}::before {
        border-color: var(--color-primary-base);
        box-shadow: 0 0 0 3px rgba(#886F68, 0.2);
      }
    }
  }
`;
