import React, {useEffect} from 'react';
import {connect, styled} from "frontity";

import Text from "../../../../objects/Text/Text";
import CheckoutFormMetodoPago from "../Forms/CheckoutFormMetodoPago";

const StepMetodoPago = ({state, actions, ...props}) => {
  useEffect(() => {

    let forEmpresa = false;
    if (typeof window !== 'undefined') {
      if(localStorage.getItem('Role') === 'empresa') {
        forEmpresa = true
      }
    }
    if(state.cart) {
      actions.analytics.checkoutStep(forEmpresa, 3, 'UNDEFINED')
    }
  })

  return (
    <React.Fragment>
      <TitleStyled data={{texto: 'MÉTODO DE PAGO', font_type: 'H1Mobile', font_family: 'Lexend Zetta'}}/>
      <CheckoutFormMetodoPago/>
    </React.Fragment>
  )
};

export default connect(StepMetodoPago);

const TitleStyled = styled(Text)`
  margin-bottom: 42px;
`;
