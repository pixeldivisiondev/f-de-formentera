import React, {useEffect, useState} from 'react';
import {connect, styled} from "frontity";
import {ApolloError} from "@apollo/client";
import {findMetaData} from "../../../../lib/utils/util";
import {fillBillingShippingStep, prepareBillingShippingInfo} from "../../../../lib/checkout/util";
import Text from "../../../../objects/Text/Text";
import {formatAddress, getListTipoVia} from "../../../../lib/checkout/addresses";
import Link from "../../../link";
import  CheckoutFormDirecciones from './CheckoutFormDirecciones'
import Layout from "../../../../objects/Layout/Layout";
import Form from "../../../../objects/Forms/Form";
import {useForm} from "react-hook-form";
import FlexGrid from "../../../../objects/FlexGrid/FlexGrid";
import FlexGridItem from "../../../../objects/FlexGrid/FlexGridItem";
import FormControl from "../../../../objects/Forms/FormControl";
import {findIndex} from "../../../../lib/utils/birthday";
import {getStateList} from "../../../../lib/checkout/statelist";
import ButtonSubmit from "../../../../objects/ButtonSubmit/ButtonSubmit";
import UserAddress from "../Steps/StepDireccionesSelect/userAddress";
import Modal from "../../../../objects/Modal/Modal";
import FormDireccionEnvio from "../../mi-cuenta/Forms/FormDireccionEditarEnvio";
import FormDireccionEnvioNueva from "../../mi-cuenta/Forms/FormDireccionEnvioNueva";
import {CloseModal, ModalContent, ModalWrapper} from "../../../objects/Modals";
import Skeleton, {SkeletonTheme} from "react-loading-skeleton";
import MiCuentaIntro from "../../mi-cuenta/Intro/MiCuentaIntro";


export default connect(function CheckoutFormDireccionesSelect({state, actions, libraries, ...props}) {
  const formData = state.theme.checkoutData.billingShippingStep;

  const shippingTipoViaList = getListTipoVia();
  const shippingTipoViaIndex = findIndex(shippingTipoViaList, findMetaData(shippingTipoViaList, 'shipping_tipo_via'));
  const shippingTipoViaIndex2 = (shippingTipoViaIndex !== -1) ? shippingTipoViaIndex : 0;

  const billingTipoViaList = getListTipoVia();
  const billingTipoViaIndex = findIndex(billingTipoViaList, findMetaData(billingTipoViaList, 'billing_tipo_via'));
  const billingTipoViaIndex2 = (billingTipoViaIndex !== -1) ? billingTipoViaIndex : 0;

  const stateList = getStateList();

  const billingStateIndex = findIndex(stateList, formData?.billing.state);
  const billingStateIndex2 = (billingStateIndex !== -1) ? billingStateIndex : 0;

  const shippingStateIndex = findIndex(stateList, formData?.shipping.state);
  const shippingStateIndex2 = (shippingStateIndex !== -1) ? shippingStateIndex : 0;

  const [addressesData, setAddressesData] = useState(null);
  const [isFormMe, setIsForMe] = useState(false)

  const [editting, setEditting] = useState(false)
  const [adding, setAdding] = useState(false)

  const {register, handleSubmit, watch, errors, control} = useForm({
    defaultValues: {
      billing_first_name: formData?.billing.firstName,
      billing_last_name: formData?.billing.lastName,
      billing_postcode: formData?.billing.postcode,
      billing_city: formData?.billing.city,
      billing_phone: formData?.billing.phone,
      billing_state: stateList[billingStateIndex2],
      billing_tipo_via: billingTipoViaList[billingTipoViaIndex2],
      billing_street: formData?.billing.street,
      billing_numero: formData?.billing.numero,
      billing_piso: formData?.billing.piso,
      shipping_first_name: formData?.shipping.firstName,
      shipping_last_name: formData?.shipping.lastName,
      shipping_postcode: formData?.shipping.postcode,
      shipping_city: formData?.shipping.city,
      shipping_state: stateList[shippingStateIndex2],
      shipping_tipo_via: shippingTipoViaList[shippingTipoViaIndex2],
      shipping_street: formData?.shipping.street,
      shipping_numero: formData?.shipping.numero,
      shipping_piso: formData?.shipping.piso,
      shipping_phone: formData?.shipping.phone,
      use_same_address: !formData?.shipToDifferentAddress
    },
    mode: "onChange"
  });

  const handleOnSubmit = data => {
    let newCheckoutData = {...state.theme.checkoutData};

    data.shipping_first_name = data.shipping_first_name
      ? data.shipping_first_name
      : addressesData.shipping.extra_shipping_addresses[isFormMe].firstName;

    data.shipping_last_name = data.shipping_last_name
      ? data.shipping_last_name
      : addressesData.shipping.extra_shipping_addresses[isFormMe].lastName;

    data.shipping_postcode = data.shipping_postcode
      ? data.shipping_postcode
      : addressesData.shipping.extra_shipping_addresses[isFormMe].postcode;

    data.shipping_city = data.shipping_city
      ? data.shipping_city
      : addressesData.shipping.extra_shipping_addresses[isFormMe].city;

    data.shipping_state = data.shipping_state
      ? data.shipping_state.value
      : addressesData.shipping.extra_shipping_addresses[isFormMe].state;

    data.shipping_tipo_via = data.shipping_tipo_via
      ? data.shipping_tipo_via
      : addressesData.shipping.extra_shipping_addresses[isFormMe].tipoVia;

    data.shipping_tipo_via = data.shipping_tipo_via
      ? data.shipping_tipo_via
      : addressesData.shipping.extra_shipping_addresses[isFormMe].tipoVia;


    data.shipping_street = data.shipping_street
      ? data.shipping_street
      : addressesData.shipping.extra_shipping_addresses[isFormMe].street;


    data.shipping_numero = data.shipping_numero
      ? data.shipping_numero
      : addressesData.shipping.extra_shipping_addresses[isFormMe].numero;

    data.shipping_piso = data.shipping_piso
      ? data.shipping_piso
      : addressesData.shipping.extra_shipping_addresses[isFormMe]  ? addressesData.shipping.extra_shipping_addresses[isFormMe].piso : null;

    data.shipping_phone = data.shipping_phone
      ? data.shipping_phone
      : addressesData.shipping.extra_shipping_addresses[isFormMe].phone;


    data.shipping_address =  isFormMe === -1
      ? data.shipping_tipo_via?.value + " " + data.shipping_street + " " + data.shipping_numero + " " + data.shipping_piso
      : addressesData.shipping.extra_shipping_addresses[isFormMe].address1;


    data.shippingAddress = data.shipping_address;

    if (data.billing_state) {
      data.billingAddress = data.billing_tipo_via.value + " " + data.billing_street + " " + data.billing_numero + " " + data.billing_piso;
      data.billing_state = data.billing_state.value;
    }

    const result = fillBillingShippingStep(newCheckoutData, data);
    result.billingShippingStep.isValid = true;
    window.scrollTo(0, 0)
    actions.theme.setCheckoutData(result);
    actions.theme.setCheckoutStep(3)
  }

  const useSameAddress = watch("use_same_address");

  useEffect(() => {
    !addressesData ? findAddresses() : null;
  })

  const findAddresses = async () => {
    //setAddressesData(null);

    if (state.tokenStatus === 1 && libraries.auth.authIsLogged()) {
      const p = await actions.auth.authProcessAuthToken();
    }

    const authId = libraries.auth.authGetUserAuthId();
    const r = await libraries.mi_cuenta.getAddresses(authId);

    if (r == null || r instanceof ApolloError) {
      //logOut();
    } else {
      setAddressesData(r);
    }
    setEditting(false)
    setAdding(false)
    actions.theme.setModalOpened(false)
    actions.theme.clearAlertMessage()
  };

  const handleDelete = async (id, index) => {

    if (state.tokenStatus === 1 && libraries.auth.authIsLogged()) {
      const p = await actions.auth.authProcessAuthToken();
    }

    const newAddressData = {...addressesData}
    newAddressData.shipping.extra_shipping_addresses.splice(index,1);
    setAddressesData(newAddressData);
    const r = await libraries.mi_cuenta.deleteAddressEnvio(id);
    if (r instanceof ApolloError) {
      actions.theme.setAlertMessage("Ha ocurrido un error al eliminar la dirección", false, 'error');
      findAddresses();
    } else {
      actions.theme.setAlertMessage("Direción eliminada correctamente", false, 'success');
    }

  }

  const editVisible = editting === false ? false : true;
  const addVisible = adding === false ? false : true;

  return (
    <React.Fragment>

      <TitleStyled data={{texto: 'TUS DIRECCIONES', font_type: 'SubtitleSmall', font_family: 'Lexend Zetta'}}/>

      <Form onSubmit={handleSubmit(handleOnSubmit)}>

        <Layout justify={"flex-start"}>
          <Layout justify={"flex-start"}>
            <StyledRadioButton>

              <StyledRadioButtonInput
                type="radio"
                name="test"
                //checked={props.index === props.isChecked ? true : false}
                onClick={() => {setIsForMe(-1)}}
                value={props.index}
                //onChange={(e) => { console.log('changed'); console.log(e.target.checked)}}
              />


              <StyledRadioButtonLabel htmlFor={"test"}>
                <Text data={{texto: 'No es para mí'}}/>
              </StyledRadioButtonLabel>
            </StyledRadioButton>

          </Layout>
          {isFormMe === -1 ?

            <NotForMeForm>

              <StyledTextNotForMe data={{texto: 'Introduce el email del destinatario y le avisaremos que recibirá un pedido de parte tuya.'}}/>

              <FlexGrid>
                <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
                  <FormControl
                    id="shipping_first_name" name="shipping_first_name"
                    type="text" placeholder="Nombre"
                    register={register}
                    errors={errors}
                  />
                </FlexGridItem>

                <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
                  <FormControl
                    id="shipping_last_name" name="shipping_last_name"
                    type="text" placeholder="Apellidos"
                    register={register}
                    errors={errors}
                  />
                </FlexGridItem>

              </FlexGrid>
              <div style={{height: '20px'}}></div>
              <FlexGrid>
                <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
                  <FormControl
                    id="shipping_email" name="shipping_email"
                    type="text" placeholder="Email"
                    register={register}
                    errors={errors}
                  />
                </FlexGridItem>
              </FlexGrid>
              <div style={{height: '20px'}}></div>
              <FlexGrid>

                <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 3}}>
                  <FormControl
                    id="shipping_tipo_via" name="shipping_tipo_via"
                    type="select" placeholder="Tipo vía"
                    selectOptions={shippingTipoViaList} selectIndex={shippingTipoViaIndex}
                    register={register}
                    errors={errors}
                    control={control}
                  />
                </FlexGridItem>

                <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 5}}>
                  <FormControl
                    id="shipping_street" name="shipping_street"
                    type="text" placeholder="Dirección"
                    register={register}
                    errors={errors}
                  />
                </FlexGridItem>

                <FlexGridItem layout={{movil: 6, tablet: 6, desktop: 2}}>
                  <FormControl
                    id="shipping_numero" name="shipping_numero"
                    type="text" placeholder="Nº"
                    register={register}
                    errors={errors}
                  />
                </FlexGridItem>

                <FlexGridItem layout={{movil: 6, tablet: 6, desktop: 2}}>
                  <FormControl
                    id="shipping_piso" name="shipping_piso"
                    type="text" placeholder="Piso"
                    register={register}
                    errors={errors}
                    noRequired={true}
                  />
                </FlexGridItem>

                <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 3}}>
                  <FormControl
                    id="shipping_postcode" name="shipping_postcode"
                    type="postcode" placeholder="Código postal"
                    register={register}
                    errors={errors}
                  />
                </FlexGridItem>

                <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 3}}>
                  <FormControl
                    id="shipping_city" name="shipping_city"
                    type="text" placeholder="Ciudad"
                    register={register}
                    errors={errors}
                  />
                </FlexGridItem>

                <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 3}}>
                  <FormControl
                    id="shipping_state" name="shipping_state"
                    type="state" placeholder="Provincia" selectIndex={shippingStateIndex}
                    register={register}
                    errors={errors}
                    control={control}
                  />
                </FlexGridItem>

                <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 3}}>
                  <FormControl
                    id="shipping_phone" name="shipping_phone"
                    type="text" placeholder="Teléfono"
                    register={register}
                    errors={errors}
                  />
                </FlexGridItem>
              </FlexGrid>
            </NotForMeForm>
            :
            null
          }

        </Layout>
        {
          addressesData?.shipping.extra_shipping_addresses.map((item, key) => {
            return (
              <UserAddress  isChecked={isFormMe} setEditting={setEditting} handleDelete={handleDelete} addressesData={addressesData} onClick={setIsForMe} key={key} index={key} item={item}/>
            )

          })
        }

        {
          !addressesData ?
            <SkeletonTheme color="#ecf5f8" highlightColor="#c3dee7">
              <Skeleton height={50} count={5}/>
            </SkeletonTheme>            :

            null
        }

        <div onClick={() => {actions.theme.setModalOpened(true); setAdding(true) }}>
          <AddAddress data={{texto: "AÑADIR UNA DIRECCIÓN", font_type: 'ButtonLarge', color: 'blue-light'}}/>
        </div>

        <Facturacion data={{texto: 'DIRECCIÓN DE FACTURACIÓN', font_type: 'SubtitleSmall', font_family: 'Lexend Zetta'}}/>

        <FlexGrid>
          <FlexGridItem layout={{movil: 12, tablet: 12, desktop: 12}}>
            <FormControl
              id="use_same_address" name="use_same_address"
              type="checkbox" label="Misma dirección de envío"
              register={register}
              defaultChecked={true}
              errors={errors}
              noRequired={true}
            />
          </FlexGridItem>

          {!useSameAddress ? (
            <>
              <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
                <FormControl
                  id="billing_first_name" name="billing_first_name"
                  type="text" placeholder="Nombre"
                  register={register}
                  errors={errors}
                />
              </FlexGridItem>

              <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
                <FormControl
                  id="billing_last_name" name="billing_last_name"
                  type="text" placeholder="Apellidos"
                  register={register}
                  errors={errors}
                />
              </FlexGridItem>
            </>
          ) : ('')}
        </FlexGrid>

        <div style={{height: '20px'}}/>

        <FlexGrid>
          {!useSameAddress ? (
            <>
              <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 2}}>
                <FormControl
                  id="billing_tipo_via" name="billing_tipo_via"
                  type="select" placeholder="Tipo vía"
                  selectOptions={billingTipoViaList} selectIndex={billingTipoViaIndex}
                  register={register}
                  errors={errors}
                  control={control}
                />
              </FlexGridItem>

              <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
                <FormControl
                  id="billing_street" name="billing_street"
                  type="text" placeholder="Dirección"
                  register={register}
                  errors={errors}
                />
              </FlexGridItem>

              <FlexGridItem layout={{movil: 6, tablet: 6, desktop: 2}}>
                <FormControl
                  id="billing_numero" name="billing_numero"
                  type="text" placeholder="Nº"
                  register={register}
                  errors={errors}
                />
              </FlexGridItem>

              <FlexGridItem layout={{movil: 6, tablet: 6, desktop: 2}}>
                <FormControl
                  id="billing_piso" name="billing_piso"
                  type="text" placeholder="Piso"
                  register={register}
                  noRequired={true}
                  errors={errors}
                />
              </FlexGridItem>

              <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 3}}>
                <FormControl
                  id="billing_postcode" name="billing_postcode"
                  type="postcode" placeholder="Código postal"
                  register={register}
                  errors={errors}
                />
              </FlexGridItem>

              <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 3}}>
                <FormControl
                  id="billing_city" name="billing_city"
                  type="text" placeholder="Ciudad"
                  register={register}
                  errors={errors}
                />
              </FlexGridItem>

              <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 3}}>
                <FormControl
                  id="billing_state" name="billing_state"
                  type="state" placeholder="Provincia" selectIndex={billingStateIndex}
                  register={register}
                  errors={errors}
                  control={control}
                />
              </FlexGridItem>

              <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 3}}>
                <FormControl
                  id="billing_phone" name="billing_phone"
                  type="text" placeholder="Teléfono"
                  register={register}
                  errors={errors}
                />
              </FlexGridItem>
            </>
          ) : ('')}

          <FlexGridItem layout={{movil: 12, tablet: 12, desktop: 12}}>
            <StyledSubmit text="CONTINUAR"/>
          </FlexGridItem>
        </FlexGrid>

      </Form>

      { addressesData && editVisible ?
        <Layout justify="flex-start">
          <Modal
            modal={<>
              <TitleForm data={{texto: 'Editar Dirección', font_type: 'H1'}}/>
              <FormDireccionEnvio data={addressesData?.shipping.extra_shipping_addresses[editting]} onSubmitted={findAddresses} inPopUp={true}/>
            </>}
            wrapper={ModalWrapper}
            onClose={ () => {setEditting(false); actions.theme.setModalOpened(false)}}
            onClick={() => {  actions.theme.setModalOpened(true)}}
            onTop = {true}
            ModalContent={ModalContent}
            CloseModal={CloseModal}
            visible={true}
          >
          </Modal>
        </Layout> :
        null}

      {addressesData && addVisible ?
        <Layout justify="flex-start">
          <Modal
            modal={
              <>
                <TitleForm data={{texto: 'Añadir Dirección', font_type: 'H1'}}/>
                <FormDireccionEnvioNueva redirectAfterSubmmit={false} onSubmitted={findAddresses} inPopUp={true}/>
              </>}
            onClick={() => { actions.theme.setModalOpened(true)}}
            wrapper={ModalWrapper}
            onTop = {true}
            onClose={() => {
              setAdding(false);
              actions.theme.setModalOpened(false)
            }}
            ModalContent={ModalContent}
            CloseModal={CloseModal}
            visible={true}
          >
          </Modal>
        </Layout>
        : null
      }


    </React.Fragment>
  )
});

const AddAddress = styled(Text)`
  margin-top: 24px;
  cursor: pointer;
`

const TitleForm = styled(Text)`
  text-transform: uppercase;
  margin-bottom: 24px;
`
const StyledInput = styled.input`
  margin-right: 10px;
`

const NotForMeForm = styled.div`
  margin: 15px 0;
`

const TitleStyled = styled(Text)`
 margin-bottom: 16px;
`

const Facturacion = styled(Text)`
  margin-top: 64px;
  margin-bottom: 18px;
`

const StyledSubmit = styled(ButtonSubmit)`
  margin-top: 34px;
`

const StyledRadioButton = styled.div`
  // radios and checkboxes
  --checkbox-radio-size: 18px;
  --checkbox-radio-gap: 4px; // gap between button and label
  --checkbox-radio-border-width: 1px;
  --checkbox-radio-line-height: 1;

  // radio buttons
  --radio-marker-size: 8px;

  // checkboxes
  --checkbox-marker-size: 12px;
  --checkbox-radius: 4px;

  display: flex;
`;

const StyledRadioButtonLabel = styled.label`
  line-height: var(--checkbox-radio-line-height);
  user-select: none;  
  font-size: 16px;
  font-weight: bold;
  display: flex;
  align-items: flex-start;
  justify-content: space-between;
  
  &::before {
    content: '';
    //display: inline-block;
    cursor: pointer;
    position: relative;
    top: calc((1em * var(--checkbox-radio-line-height) - var(--checkbox-radio-size)) / 2);
    flex-shrink: 0;
    width: var(--checkbox-radio-size);
    height: var(--checkbox-radio-size);
    background-color: var(--color-white);
    border-width: var(--checkbox-radio-border-width);
    border-color: var(--color-primary-base);
    border-style: solid;
    background-repeat: no-repeat;
    background-position: center;
    //margin-right: var(--checkbox-radio-gap);
    transition: transform 0.2s, border 0.2s;
    border-radius: 50%;
    //padding: 8px 12px;
    font-weight: normal;
    display: inline-flex;
    margin-right: 16px;
  }
`;

const StyledRadioButtonInput = styled.input`
  position: absolute;
  padding: 0;
  margin: 0;
  opacity: 0;
  z-index: 1;
  height: var(--checkbox-radio-size);
  width: var(--checkbox-radio-size);  
  cursor: pointer;
  
  &:checked {
    + ${StyledRadioButtonLabel}::before {
      background-color: white;
      box-shadow: none;
      border-color: var(--color-primary-base);
      transition: transform 0.2s;
      background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16'%3E%3Cg class='nc-icon-wrapper' fill='%23004e66'%3E%3Ccircle cx='8' cy='8' r='8' fill='%23004e66'%3E%3C/circle%3E%3C/g%3E%3C/svg%3E");
      background-size: var(--radio-marker-size);
    }

    &:active,
    &:focus {
      + ${StyledRadioButtonLabel}::before {
        border-color: var(--color-primary-base);
        box-shadow: 0 0 0 3px rgba(#886F68, 0.2);
      }
    }
  }
`;

const StyledTextNotForMe = styled(Text)`
  color: #8A8A8A;
  margin-bottom: 15px;
`
