import React from 'react';
import {connect, styled} from "frontity";
import {useForm} from "react-hook-form";
import Image from "@frontity/components/image";
import {CardElement, useElements, useStripe} from '@stripe/react-stripe-js';

import Text from "../../../../objects/Text/Text";
import AlertMessage from "../../../../objects/AlertMessage/AlertMessage";
import FlexGrid from "../../../../objects/FlexGrid/FlexGrid";
import Form from "../../../../objects/Forms/Form";

import RadioInput from "../../../../objects/Forms/RadioInput";
import TarjetasImage from "../../../../assets/images/checkout/tarjetas.png";
import PayPalImage from "../../../../assets/images/checkout/paypal-logo.png";
import ButtonSubmit from "../../../../objects/ButtonSubmit/ButtonSubmit";
import FlexGridItem from "../../../../objects/FlexGrid/FlexGridItem";

export default connect(function CheckoutFormMetodoPago({state, actions, libraries, ...props}) {
  const stripe = useStripe();
  const elements = useElements();
  const {register, handleSubmit, watch, errors} = useForm({
    defaultValues: {
      payment_method: 'stripe'
    }
  });

  const paymentMethod = watch("payment_method");

  const handleOnSubmit = async data => {
    if (data.payment_method === 'stripe') {
      const c = await handleStripe();
    } else {
      let newCheckoutData = {...state.theme.checkoutData};

      newCheckoutData.paymentMethodStep = {
        isValid: true,
        method: {
          type: data.payment_method
        }
      };
      window.scrollTo(0, 0)
      actions.theme.setCheckoutData(newCheckoutData);
      actions.theme.setCheckoutStep(4);
    }
  };

  const handleStripe = async data => {
    const billingInfo = state.theme.checkoutData?.billingShippingStep?.billing;

    let billingDetails = null;

    if (billingInfo) {
      billingDetails = {
        name: billingInfo.firstName + " " + billingInfo.lastName,
        email: state.theme.checkoutData.user.email,
        address: {
          city: billingInfo.city,
          line1: billingInfo.address,
          state: billingInfo.state,
          postal_code: billingInfo.postcode
        }
      };
    }

    const cardElement = elements.getElement("card");

    try {

      const paymentMethodTest = await stripe.createSource(cardElement)

      if (paymentMethodTest.error) {
        actions.theme.setAlertMessage(paymentMethodTest.error.message, false, 'error');
      } else {


        let newCheckoutData = {...state.theme.checkoutData};


        newCheckoutData.paymentMethodStep = {
          isValid: true,
          method: {
            type: 'stripe',
            paymentMethod: paymentMethodTest.source,
            source: paymentMethodTest.source.id
          }
        };
        window.scrollTo(0, 0)
        actions.theme.setCheckoutData(newCheckoutData);
        actions.theme.setCheckoutStep(4);
      }
    } catch (err) {
      actions.theme.setAlertMessage(err.message, false, 'error');
    }
  }

  return (
    <Form onSubmit={handleSubmit(handleOnSubmit)}>
      <AlertMessage inModal={false}/>

      <TitleStyled data={{texto: 'PAGO', font_type: 'SubtitleSmall', font_family: 'Lexend Zetta'}}/>

      <FlexGrid>
        <FlexGridItem layout={{movil: 12, tablet: 12, desktop: 12}}>
          <RadioInput
            id="stripe"
            name="payment_method"
            value="stripe"
            text="Tarjeta de crédito"
            image={<ImageStyled src={TarjetasImage}/>}
            register={register}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 12, desktop: 12}}>
          <DivStyled show={paymentMethod === 'stripe'}>
            <CardElement
              options={{
                hidePostalCode: true,
                style: {
                  base: {
                    fontSize: '14px',
                    fontFamily: 'Lexend Deca, Open Sans, Segoe UI, sans-serif',
                    color: '#004E66',
                    '::placeholder': {
                      color: '#749fab',
                    },
                  },
                  invalid: {
                    color: '#e03b58',
                  },
                },
              }}
            />
          </DivStyled>
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 12, desktop: 12}}>
          <RadioInput
            id="paypal"
            name="payment_method"
            value="paypal"
            text="PayPal"
            image={<ImageStyled src={PayPalImage}/>}
            register={register}
          />
        </FlexGridItem>

        <FlexGridItemStyled layout={{movil: 12, tablet: 12, desktop: 12}}>
          <ButtonSubmit text="CONTINUAR"/>
        </FlexGridItemStyled>
      </FlexGrid>
    </Form>
  )
});

const TitleStyled = styled(Text)`
  margin-bottom: 20px;
`;

const FlexGridItemStyled = styled(FlexGridItem)`
  margin-top: 30px;
`;

const ImageStyled = styled(Image)`
  margin-left: 24px;
`;

const DivStyled = styled.div`
  width: 100%;
  max-width: 600px;
  border: 1px solid rgba(0, 78, 102, 0.7);
  padding: 12px;
  margin: 12px 0;

  > * {
    width: 100%;
  }

  ${props => {
    return props.show ? `display: block;` : `display: none;`;
  }}
`;
