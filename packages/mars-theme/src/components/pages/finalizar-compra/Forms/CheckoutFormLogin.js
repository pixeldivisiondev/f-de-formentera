import React, {useState} from 'react';
import {connect, styled} from "frontity";
import {useForm} from "react-hook-form";
import {ApolloError} from "@apollo/client";

import Text from "../../../../objects/Text/Text";
import AlertMessage from "../../../../objects/AlertMessage/AlertMessage";
import FlexGrid from "../../../../objects/FlexGrid/FlexGrid";
import FlexGridItem from "../../../../objects/FlexGrid/FlexGridItem";
import FormControl from "../../../../objects/Forms/FormControl";
import ButtonSubmit from "../../../../objects/ButtonSubmit/ButtonSubmit";
import Form from "../../../../objects/Forms/Form";
import Layout from "../../../../objects/Layout/Layout";
import {findMetaData} from "../../../../lib/utils/util";
import {fillBillingShippingStep, prepareBillingShippingInfo} from "../../../../lib/checkout/util";

import LaunchLogin from "../../registro/LaunchLogin";
import LoginModal from "../../login/LoginModal/LoginModal";
import {CloseModal, ModalContent, ModalWrapper} from "../../../objects/Modals";
import Modal from "../../../../objects/Modal/Modal";
import LostPasswordModalContent from "../../login/LoginModal/LostPasswordModalContent";

export default connect(function CheckoutFormLogin({state, actions, libraries, ...props}) {

  const [submitting, setSubmitting] = useState(false);
  const defaultVal = {
    login_email: '',
    login_password: ''
  };

  const {register, handleSubmit, watch, errors} = useForm({
    defaultValues: defaultVal
  });

  GuestGridStyled.defaultProps = {
    breakPoints: state.theme.breakPoints
  }


  const handleOnSubmit = async data => {
    setSubmitting(true)
    const r = await libraries.register.loginUser(data.login_email, data.login_password);

    if (r instanceof ApolloError) {
      let errorMessage = r.message;

      if (errorMessage.includes('invalid_email')) {
        errorMessage = 'No existe un usuario registrado con ese email.';
      }

      if (errorMessage.includes('incorrect_password')) {
        errorMessage = 'La contraseña introducida no es correcta';
      }
      setSubmitting(false)
      actions.theme.setAlertMessage(errorMessage, false, 'error');
    } else {
      libraries.auth.authRegisterJwtToken(r.authToken, r.refreshToken, r.customer.id);

      const r2 = await libraries.mi_cuenta.getAddresses(r.customer.id);

      setSubmitting(false)
      if (r2 instanceof ApolloError) {
        actions.theme.setAlertMessage('Ha ocurrido un error', false, 'error');
      } else {
        let newCheckoutData = {...state.theme.checkoutData};

        newCheckoutData.user = {
          email: data.login_email,
          isGuest: false
        };

        const dataPrepared = prepareBillingShippingInfo(r2);
        const result = fillBillingShippingStep(newCheckoutData, dataPrepared);
        window.scrollTo(0, 0)
        actions.theme.setCheckoutData(result);
        actions.theme.setCheckoutStep(2);

        actions.theme.setCheckoutData(newCheckoutData);

        processToken();
      }
    }
  };

  const processToken = async () => {
    const r = await actions.auth.authProcessAuthToken();

    if (state.tokenStatus === 1 && libraries.auth.authIsLogged()) {
      await findAddresses();
    } else {
      libraries.auth.authUnregisterJwtToken();
    }
  }

  const findAddresses = async () => {
    const authId = libraries.auth.authGetUserAuthId();

    const r = await libraries.mi_cuenta.getAddresses(authId);
    if (r == null || r instanceof ApolloError) {
      libraries.auth.authUnregisterJwtToken();
    } else {
      let newCheckoutData = {...state.theme.checkoutData};

      const stripeCustomerId = findMetaData(r.metaData, "_stripe_customer_id");

      newCheckoutData.user.stripeCustomerId = stripeCustomerId || null;

      const dataPrepared = prepareBillingShippingInfo(r);
      const result = fillBillingShippingStep(newCheckoutData, dataPrepared);

      //actions.theme.setCheckoutData(result);
    }
  };

  return (
    <GuestGridStyled>




      <Form onSubmit={handleSubmit(handleOnSubmit)}>
        <AlertMessage inModal={false}/>

        <FlexGrid>
          <FlexGridItem layout={{movil: 12, tablet: 12, desktop: 12}}>
            <FormControl
              id="login_email" name="login_email"
              type="email" placeholder="Correo electrónico"
              register={register}
              errors={errors}
            />
          </FlexGridItem>

          <FlexGridItem layout={{movil: 12, tablet: 12, desktop: 12}}>
            <FormControl
              id="login_password" name="login_password"
              type="password" placeholder="Contraseña"
              register={register}
              errors={errors}
            />




          </FlexGridItem>

          <FlexGridItem layout={{movil: 12, tablet: 12, desktop: 12}}>
            <ButtonSubmit disabled={submitting} text={!submitting ? "Continuar" : "Enviando..."} size="full"/>
          </FlexGridItem>
        </FlexGrid>
      </Form>
      <Modal
        modal={<LostPasswordModalContent hideLogin={true}  handleOpenLogin={() => alert('redirect')}  />}
        wrapper={ModalWrapper}
        onClick={() => { actions.theme.setModalOpened(true)}}
        onTop = {true}
        ModalContent={ModalContent}
        CloseModal={CloseModal}
      >
        <LostPasswordStyled data={{texto: 'NO RECUERDO MI CONTRASEÑA', font_type: 'PrimarySmall'}}/>
      </Modal>

    </GuestGridStyled>
  )
});

const GuestGridStyled = styled(Layout)`
  ${props => {
  return ` @media ${props.breakPoints['tablet-wide']}
              {
                  max-width: 295px;
              }`
    }
  }
`;

const LostPasswordStyled = styled(Text)`
  color: var(--color-blue-light);
  text-align: right;
  margin-top: 12px;
  margin-bottom: 24px;
  letter-spacing: 2px !important;
  cursor: pointer;
`;
