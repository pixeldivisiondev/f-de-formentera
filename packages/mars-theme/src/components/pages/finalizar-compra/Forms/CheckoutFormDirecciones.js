import React from 'react';
import {connect, styled} from "frontity";
import {useForm} from "react-hook-form";

import Text from "../../../../objects/Text/Text";
import AlertMessage from "../../../../objects/AlertMessage/AlertMessage";
import FlexGrid from "../../../../objects/FlexGrid/FlexGrid";
import FlexGridItem from "../../../../objects/FlexGrid/FlexGridItem";
import FormControl from "../../../../objects/Forms/FormControl";
import ButtonSubmit from "../../../../objects/ButtonSubmit/ButtonSubmit";
import Form from "../../../../objects/Forms/Form";

import {findMetaData} from "../../../../lib/utils/util";
import {fillBillingShippingStep, prepareBillingShippingInfo} from "../../../../lib/checkout/util";
import {getListTipoVia} from "../../../../lib/checkout/addresses";
import {findIndex} from "../../../../lib/utils/birthday";
import {getStateList} from "../../../../lib/checkout/statelist";

export default connect(function CheckoutFormDirecciones({state, actions, libraries, ...props}) {
  const formData = state.theme.checkoutData.billingShippingStep;

  const shippingTipoViaList = getListTipoVia();
  const shippingTipoViaIndex = findIndex(shippingTipoViaList, findMetaData(shippingTipoViaList, 'shipping_tipo_via'));
  const shippingTipoViaIndex2 = (shippingTipoViaIndex !== -1) ? shippingTipoViaIndex : 0;

  const billingTipoViaList = getListTipoVia();
  const billingTipoViaIndex = findIndex(billingTipoViaList, findMetaData(billingTipoViaList, 'billing_tipo_via'));
  const billingTipoViaIndex2 = (billingTipoViaIndex !== -1) ? billingTipoViaIndex : 0;

  const stateList = getStateList();

  const billingStateIndex = findIndex(stateList, formData?.billing.state);
  const billingStateIndex2 = (billingStateIndex !== -1) ? billingStateIndex : 0;

  const shippingStateIndex = findIndex(stateList, formData?.shipping.state);
  const shippingStateIndex2 = (shippingStateIndex !== -1) ? shippingStateIndex : 0;

  const {register, handleSubmit, watch, errors, control} = useForm({
    defaultValues: {
      billing_first_name: formData?.billing.firstName,
      billing_last_name: formData?.billing.lastName,
      billing_postcode: formData?.billing.postcode,
      billing_city: formData?.billing.city,
      billing_phone: formData?.billing.phone,
      billing_state: stateList[billingStateIndex2],
      billing_tipo_via: billingTipoViaList[billingTipoViaIndex2],
      billing_street: formData?.billing.street,
      billing_numero: formData?.billing.numero,
      billing_piso: formData?.billing.piso,
      shipping_first_name: formData?.shipping.firstName,
      shipping_last_name: formData?.shipping.lastName,
      shipping_postcode: formData?.shipping.postcode,
      shipping_city: formData?.shipping.city,
      shipping_state: stateList[shippingStateIndex2],
      shipping_tipo_via: shippingTipoViaList[shippingTipoViaIndex2],
      shipping_street: formData?.shipping.street,
      shipping_numero: formData?.shipping.numero,
      shipping_piso: formData?.shipping.piso,
      shipping_email: formData?.shipping.email,
      shipping_phone: formData?.shipping.phone,
      use_same_address: !formData?.shipToDifferentAddress
    },
    mode: "onChange"
  });

  const handleOnSubmit = data => {
    let newCheckoutData = {...state.theme.checkoutData};

    data.shippingAddress = data.shipping_tipo_via.value + " " + data.shipping_street + " " + data.shipping_numero;

    if(data.shipping_piso) {
      data.shippingAddress += data.shipping_piso;
    }

    data.shipping_state = data.shipping_state.value;

    if (data.billing_state) {
      data.billingAddress = data.billing_tipo_via.value + " " + data.billing_street + " " + data.billing_numero;
      data.billing_state = data.billing_state.value;
    }

    if(data.billing_piso) {
      data.billingAddress += data.billing_piso;
    }

    const result = fillBillingShippingStep(newCheckoutData, data);
    result.billingShippingStep.isValid = true;
    window.scrollTo(0, 0)
    actions.theme.setCheckoutData(result);
    actions.theme.setCheckoutStep(3);
  }

  const useSameAddress = watch("use_same_address");
  const notForMe = watch("not_for_me");

  return (
    <React.Fragment>
      <Form onSubmit={handleSubmit(handleOnSubmit)}>
        <AlertMessage inModal={false}/>

        <TitleStyled data={{texto: 'DIRECCIÓN DE ENVÍO', font_type: 'SubtitleSmall', font_family: 'Lexend Zetta'}}/>

        <FlexGrid>

          <FlexGridItem layout={{movil: 12, tablet: 12, desktop: 12}}>
            <FormControl
              id="not_for_me" name="not_for_me"
              type="checkbox" label="No es para mí"
              register={register}
              errors={errors}
              noRequired={true}
            />
          </FlexGridItem>

          <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
            <FormControl
              id="shipping_first_name" name="shipping_first_name"
              type="text" placeholder="Nombre"
              register={register}
              errors={errors}
            />
          </FlexGridItem>

          <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
            <FormControl
              id="shipping_last_name" name="shipping_last_name"
              type="text" placeholder="Apellidos"
              register={register}
              errors={errors}
            />
          </FlexGridItem>
          { notForMe ?
            <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
              <FormControl
                id="shipping_email" name="shipping_email"
                type="text" placeholder="Correo electrónico del destinatario"
                register={register}
                errors={errors}
              />
            </FlexGridItem>
            : null }
        </FlexGrid>

        <div style={{height: '40px'}}/>

        <FlexGrid>
          <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 3}}>
            <FormControl
              id="shipping_tipo_via" name="shipping_tipo_via"
              type="select" placeholder="Tipo vía"
              selectOptions={shippingTipoViaList} selectIndex={shippingTipoViaIndex}
              register={register}
              errors={errors}
              control={control}
            />
          </FlexGridItem>

          <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 5}}>
            <FormControl
              id="shipping_street" name="shipping_street"
              type="text" placeholder="Dirección"
              register={register}
              errors={errors}
            />
          </FlexGridItem>

          <FlexGridItem layout={{movil: 6, tablet: 6, desktop: 2}}>
            <FormControl
              id="shipping_numero" name="shipping_numero"
              type="text" placeholder="Nº"
              register={register}
              errors={errors}
            />
          </FlexGridItem>

          <FlexGridItem layout={{movil: 6, tablet: 6, desktop: 2}}>
            <FormControl
              id="shipping_piso" name="shipping_piso"
              type="text" placeholder="Piso"
              register={register}
              errors={errors}
              noRequired={true}
            />
          </FlexGridItem>

          <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 3}}>
            <FormControl
              id="shipping_postcode" name="shipping_postcode"
              type="postcode" placeholder="Código postal"
              register={register}
              errors={errors}
            />
          </FlexGridItem>

          <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 3}}>
            <FormControl
              id="shipping_city" name="shipping_city"
              type="text" placeholder="Ciudad"
              register={register}
              errors={errors}
            />
          </FlexGridItem>

          <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 3}}>
            <FormControl
              id="shipping_state" name="shipping_state"
              type="state" placeholder="Provincia" selectIndex={shippingStateIndex}
              register={register}
              errors={errors}
              control={control}
            />
          </FlexGridItem>

          <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 3}}>
            <FormControl
              id="shipping_phone" name="shipping_phone"
              type="text" placeholder="Teléfono"
              register={register}
              errors={errors}
            />
          </FlexGridItem>
        </FlexGrid>

        <Title2Styled data={{texto: 'DIRECCIÓN DE FACTURACIÓN', font_type: 'SubtitleSmall', font_family: 'Lexend Zetta'}}/>

        <FlexGrid>
          <FlexGridItem layout={{movil: 12, tablet: 12, desktop: 12}}>
            <FormControl
              id="use_same_address" name="use_same_address"
              type="checkbox" label="Misma dirección de envío"
              register={register}
              errors={errors}
              noRequired={true}
            />
          </FlexGridItem>

          {!useSameAddress ? (
            <>
              <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
                <FormControl
                  id="billing_first_name" name="billing_first_name"
                  type="text" placeholder="Nombre"
                  register={register}
                  errors={errors}
                />
              </FlexGridItem>

              <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
                <FormControl
                  id="billing_last_name" name="billing_last_name"
                  type="text" placeholder="Apellidos"
                  register={register}
                  errors={errors}
                />
              </FlexGridItem>
            </>
          ) : ('')}
        </FlexGrid>

        <div style={{height: '40px'}}/>

        <FlexGrid>
          {!useSameAddress ? (
            <>
              <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 2}}>
                <FormControl
                  id="billing_tipo_via" name="billing_tipo_via"
                  type="select" placeholder="Tipo vía"
                  selectOptions={billingTipoViaList} selectIndex={billingTipoViaIndex}
                  register={register}
                  errors={errors}
                  control={control}
                />
              </FlexGridItem>

              <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
                <FormControl
                  id="billing_street" name="billing_street"
                  type="text" placeholder="Dirección"
                  register={register}
                  errors={errors}
                />
              </FlexGridItem>

              <FlexGridItem layout={{movil: 6, tablet: 6, desktop: 2}}>
                <FormControl
                  id="billing_numero" name="billing_numero"
                  type="text" placeholder="Nº"
                  register={register}
                  errors={errors}
                />
              </FlexGridItem>

              <FlexGridItem layout={{movil: 6, tablet: 6, desktop: 2}}>
                <FormControl
                  id="billing_piso" name="billing_piso"
                  type="text" placeholder="Piso"
                  register={register}
                  errors={errors}
                  noRequired={true}
                />
              </FlexGridItem>

              <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 3}}>
                <FormControl
                  id="billing_postcode" name="billing_postcode"
                  type="postcode" placeholder="Código postal"
                  register={register}
                  errors={errors}
                />
              </FlexGridItem>

              <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 3}}>
                <FormControl
                  id="billing_city" name="billing_city"
                  type="text" placeholder="Ciudad"
                  register={register}
                  errors={errors}
                />
              </FlexGridItem>

              <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 3}}>
                <FormControl
                  id="billing_state" name="billing_state"
                  type="state" placeholder="Provincia" selectIndex={billingStateIndex}
                  register={register}
                  errors={errors}
                  control={control}
                />
              </FlexGridItem>

              <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 3}}>
                <FormControl
                  id="billing_phone" name="billing_phone"
                  type="text" placeholder="Teléfono"
                  register={register}
                  errors={errors}
                />
              </FlexGridItem>
            </>
          ) : ('')}

          <FlexGridItem layout={{movil: 12, tablet: 12, desktop: 12}}>
            <ButtonSubmit text="CONTINUAR"/>
          </FlexGridItem>
        </FlexGrid>
      </Form>
    </React.Fragment>
  )
});

const TitleStyled = styled(Text)`
  margin-bottom: 20px;
`;

const Title2Styled = styled(Text)`
  margin-top: 20px;
  margin-bottom: 20px;
`;
