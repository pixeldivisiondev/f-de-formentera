import React, {useState} from 'react';
import {connect, styled} from "frontity";
import {useForm} from "react-hook-form";
import {ApolloError} from "@apollo/client";

import Text from "../../../../objects/Text/Text";
import AlertMessage from "../../../../objects/AlertMessage/AlertMessage";
import FlexGrid from "../../../../objects/FlexGrid/FlexGrid";
import FlexGridItem from "../../../../objects/FlexGrid/FlexGridItem";
import FormControl from "../../../../objects/Forms/FormControl";
import ButtonSubmit from "../../../../objects/ButtonSubmit/ButtonSubmit";
import Form from "../../../../objects/Forms/Form";
import Layout from "../../../../objects/Layout/Layout";

import {fillBillingShippingStep, prepareBillingShippingInfo} from "../../../../lib/checkout/util";

export default connect(function CheckoutFormGuest({state, actions, libraries, ...props}) {

  const [submitting, setSubmitting] = useState(false);

  const defaultVal = {
    guest_email: ''
  };

  GuestGridStyled.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  const {register, handleSubmit, watch, errors} = useForm({
    defaultValues: defaultVal
  });

  const handleOnSubmit = async data => {
    setSubmitting(true)
    const r = await libraries.register.loginUser(data.guest_email, '-');

    if (r instanceof ApolloError) {
      let errorMessage = r.message;

      if (errorMessage.includes('invalid_email')) {
        setSubmitting(false)
        initCheckout(data.guest_email);
      } else {
        setSubmitting(false)
        actions.theme.setAlertMessage('Vaya, parece que ya existe un usuario con ese email. ¿Has olvidado tu contraseña?', false, 'error');
      }
    } else {
      setSubmitting(false)
      initCheckout(data.guest_email);
      window.scrollTo(0, 0)
    }

  };

  const initCheckout = email => {
    let newCheckoutData = {...state.theme.checkoutData};

    newCheckoutData.user = {
      email: email,
      isGuest: true,
      stripeCustomerId: null
    };

    const dataPrepared = prepareBillingShippingInfo(null);
    const result = fillBillingShippingStep(newCheckoutData, dataPrepared);
    setSubmitting(false)
    actions.theme.setCheckoutData(result);
    window.scrollTo(0, 0)
    actions.theme.setCheckoutStep(2);
  }

  return (
    <GuestGridStyled>
      <Form onSubmit={handleSubmit(handleOnSubmit)}>
        <AlertMessage inModal={false}/>
        <Layout justify="flex-end" onClick={() => props.handleChange()}>
          <OpenLoginStyled data={{texto: 'YA SOY USUARIO', font_type: 'PrimarySmall'}}/>
        </Layout>

        <FlexGrid>
          <FlexGridItem layout={{movil: 12, tablet: 12, desktop: 12}}>
            <FormControl
              id="guest_email" name="guest_email"
              type="email" placeholder="Correo electrónico"
              register={register}
              errors={errors}
            />
          </FlexGridItem>

          <FlexGridItem layout={{movil: 12, tablet: 12, desktop: 12}}>
            <ButtonSubmit disabled={submitting} text={!submitting ? "Continuar" : 'Enviando...'} size="full"/>
          </FlexGridItem>
        </FlexGrid>
      </Form>
    </GuestGridStyled>
  )
});

const GuestGridStyled = styled(Layout)`
  
  ${props => {
    return ` @media ${props.breakPoints['tablet-wide']}
              {
                  max-width: 295px;
              }`
    }
  }
`;

const OpenLoginStyled = styled(Text)`
  color: var(--color-blue-light);
  cursor: pointer;
  text-align: right;
  margin-bottom: 8px;
  letter-spacing: 2px !important;
`;
