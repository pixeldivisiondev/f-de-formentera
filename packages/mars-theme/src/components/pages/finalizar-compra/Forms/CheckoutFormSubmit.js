import React, {useState} from 'react';
import {connect, styled} from "frontity";
import {useForm} from "react-hook-form";
import {useStripe} from "@stripe/react-stripe-js";
import axios from 'axios';
import {ApolloError} from "@apollo/client";

import Form from "../../../../objects/Forms/Form";
import FlexGrid from "../../../../objects/FlexGrid/FlexGrid";
import FormControl from "../../../../objects/Forms/FormControl";
import AlertMessage from "../../../../objects/AlertMessage/AlertMessage";
import ButtonSubmit from "../../../../objects/ButtonSubmit/ButtonSubmit";
import FlexGridItem from "../../../../objects/FlexGrid/FlexGridItem";
import CheckoutPayPalButton from "../Paypal/CheckoutPayPalButton";

import {prepareInputCheckoutData} from "../../../../lib/checkout/util";

export default connect(function CheckoutFormSubmit({state, actions, libraries, ...props}) {
  const [processing, isProcessing] = useState(false);
  const [stripeData, setStripeData] = useState(null);

  const {register, handleSubmit, watch, errors} = useForm();

  const checkoutData = state.theme.checkoutData;
  const isPaypal = checkoutData.paymentMethodStep?.method?.type === 'paypal';
  const isStripe = checkoutData.paymentMethodStep?.method?.type === 'stripe';
  const totalPrice = state.cart?.total.replace('€', '');

  const privacyPolicy = watch("privacy_policy");

  //const stripe = isStripe ? useStripe() : null;


  const handleOnSubmit = async data => {
    isProcessing(true);

    if (checkoutData.billingShippingStep && checkoutData.billingShippingStep.isValid) {
      if (checkoutData.paymentMethodStep && checkoutData.paymentMethodStep.isValid) {
        if (checkoutData.paymentMethodStep.method.type === 'stripe') {
          try {
            // Recuperamos el método de pago
            const pm = checkoutData.paymentMethodStep.method.paymentMethod;


            // Generamos el secret del payment intent pasándole los datos del customer y la cantidad
            const url = 'https://gestion.fdeformentera.com/wp-admin/admin-ajax.php';

            const amount = parseInt(totalPrice * 100);

            let form_data = new FormData;
            form_data.append('action', 'paymentIntents');
            form_data.append('amount', amount);
            form_data.append('customerId', checkoutData.user.stripeCustomerId);
            form_data.append('name', checkoutData.billingShippingStep.billing.firstName + " " + checkoutData.billingShippingStep.billing.lastName);
            form_data.append('email', checkoutData.user.email);
            form_data.append('source', checkoutData.paymentMethodStep.method.source);

            const {data} = await axios.post(url, form_data);

            setStripeData({
              customerId: data.customer_id
            });

            const p = await ifPaymentIntentSucceeded('', data.customer_id);

          } catch (err) {
            actions.theme.setAlertMessage(err.message, false, 'error');

            isProcessing(false);
          }
        }
      } else {
        // Método de pago no seleccionado
        isProcessing(false);
      }
    } else {
      // Dirección no seleccionado
      isProcessing(false);
    }

    if(props.onSubmit) {
      props.onSubmit();
    }
  };

  const setStripeCustomerIdFunc = (customerId) => {
    const authId = libraries.auth.authGetUserAuthId;

    libraries.checkout.setStripeCustomerId(authId, customerId)
  }

  const ifPaymentIntentSucceeded = async (pi, customerId) => {
    const checkoutInputData = prepareInputCheckoutData(checkoutData, true);

    // Y rellenamos todos los campos meta data para Woocommerce
    checkoutInputData.metaData.push({key: "_stripe_customer_id", value: customerId || stripeData.customerId});
    checkoutInputData.metaData.push({key: "_stripe_charge_captured", value: "no"});
    checkoutInputData.metaData.push({key: "_stripe_currency", value: "EUR"});
    checkoutInputData.metaData.push({key: "_stripe_source_id", value: checkoutData.paymentMethodStep.method.source});

    if (!checkoutData.user.isGuest) {
      // Añadimos el customer id al cliente
      const p = await processToken2(customerId || stripeData.customerId);
    }

    // Terminamos el checkout
    if (!checkoutData.user.isGuest) {
      // Solamente comprobamos el token si es usuario logado
      const p = await processToken(checkoutInputData);
    } else {
      const p = await sendCheckout(checkoutInputData);
    }
  }

  const sendCheckout = async (checkoutInputData) => {
    actions.theme.setModalOpened(true);
    if(checkoutInputData.shipping.email){
      checkoutInputData.metaData.push({key: "_shipping_email", value: checkoutInputData.shipping.email});
    }

    console.log(checkoutInputData, 'ERROR DATA')
    checkoutInputData.metaData.push({key: "_shipping_phone", value: '941505050'});

    const rcheckout = await libraries.checkout.sendCheckout(checkoutInputData);

    if (rcheckout instanceof ApolloError) {
      console.log(rcheckout, 'ApolloERror')
      actions.theme.setAlertMessage('Ha ocurrido un error al finalizar el pago', false, 'error');
      isProcessing(false);
    } else {
      const forOrder = {
        data: rcheckout.order,
        isGuest: checkoutData.user.isGuest,
        cart: state.cart
      };

      localStorage.removeItem('for-order');
      localStorage.setItem('for-order', JSON.stringify(forOrder));

      isProcessing(false);
      await actions.analytics.setTransaction(rcheckout.order.databaseId)
      actions.theme.setModalOpened(false);
      await actions.router.set('/pedido-finalizado/');
    }
  }

  const handlePaypalOnApprove = async (data, actionsR) => {
    actions.theme.setModalOpened(true);
    const checkoutInputData = prepareInputCheckoutData(checkoutData, true);

    const wpOrder = await libraries.checkout.sendCheckout(checkoutInputData);

    if (wpOrder) {
      const databaseId = wpOrder.order.databaseId;
      const orderKey = wpOrder.order.orderKey;

      const updateOrder = [
        {
          op: 'add',
          path: '/purchase_units/@reference_id==\'REF1\'/invoice_id',
          value: 'WC_' + databaseId
        },
        {
          op: 'add',
          path: '/purchase_units/@reference_id==\'REF1\'/custom_id',
          value: JSON.stringify({"order_id": databaseId, "order_number": databaseId, "order_key": orderKey}),
        }
      ];

      await actionsR.order.patch(updateOrder);
      const t = await actionsR.order.capture();

      const forOrder = {
        data: wpOrder.order,
        isGuest: checkoutData.user.isGuest,
        cart: state.cart
      };

      localStorage.removeItem('for-order');
      localStorage.setItem('for-order', JSON.stringify(forOrder));
      await actions.analytics.setTransaction(databaseId)
      actions.theme.setModalOpened(false);
      await actions.router.set('/pedido-finalizado/');
    }
  }

  const processToken = async (data) => {
    const r = await actions.auth.authProcessAuthToken();

    if (state.tokenStatus === 1 && libraries.auth.authIsLogged()) {
      await sendCheckout(data);
    } else {
      await logOut();
    }
  }

  const processToken2 = async (data) => {
    const r = await actions.auth.authProcessAuthToken();

    if (state.tokenStatus === 1 && libraries.auth.authIsLogged()) {
      setStripeCustomerIdFunc(data);
    } else {
      await logOut();
    }
  }

  const logOut = async () => {
    libraries.auth.authUnregisterJwtToken();
    actions.theme.setLoginName(null);
    actions.theme.logOut();
    await actions.router.set('/');
  };

  return (
    <Form onSubmit={handleSubmit(handleOnSubmit)}>
      <AlertMessage inModal={false}/>

      <FlexGrid>
        <FlexGridItem layout={{movil: 12, tablet: 12, desktop: 12}}>
          <FormControl
            id="privacy_policy" name="privacy_policy"
            type="checkbox" label={'*Soy mayor de 18 años y he leído y acepto la <a href="/politica-de-privacidad/" target="_blank">política de privacidad</a>'}
            register={register}
            errors={errors}
          />
        </FlexGridItem>

        <FlexGridItemStyled layout={{movil: 12, tablet: 12, desktop: 12}}>
          <BlockStyled show={isPaypal} disabled={!privacyPolicy}>
            {totalPrice && <CheckoutPayPalButton price={totalPrice} handlePaypalOnApprove={handlePaypalOnApprove}/>}
          </BlockStyled>

          <BlockStyled show={!isPaypal}>
            <ButtonSubmit text="COMPRAR AHORA"/>
          </BlockStyled>
        </FlexGridItemStyled>
      </FlexGrid>

      {processing && !isPaypal && (
        <BackdropStyled>
          <StyledWeapper className="u-wrapper u-wrapper--size-xs u-text--align-center">
            <h4 className="u-title u-title--size-sm u-margin--bottom-sm">Estamos procesando tu pago</h4>
            <div>Por favor, espere un momento y no cierre la ventana, el proceso puede tardar unos segundos</div>
          </StyledWeapper>
        </BackdropStyled>
      )}
    </Form>
  )
});

const StyledWeapper = styled.div`
  padding: 16px;
`

const FlexGridItemStyled = styled(FlexGridItem)`
  margin-top: 30px;
`;

const BlockStyled = styled.div`

  width: 100%;
  ${props => {
    return props.show ? `display: block;` : `display: none;`;
  }}

  ${props => {
    return props.disabled ? `
    opacity: 0.6;
    pointer-events: none;
    ` : ``;
  }}
`;

const BackdropStyled = styled.div`
  background-color: rgba(0, 0, 0, 0.85);
  backdrop-filter: blur(5px);
  position: fixed;
  color: white;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  z-index: 999;
  padding: space-mult(8);
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  width: 100%;
  height: 100%;
`;
