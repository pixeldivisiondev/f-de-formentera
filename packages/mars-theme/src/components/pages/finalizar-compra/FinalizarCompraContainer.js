import React, {useEffect, useState} from 'react';
import {connect, loadable, styled} from "frontity";

import {Elements} from '@stripe/react-stripe-js';
import {loadStripe} from '@stripe/stripe-js';

import Text from "../../../objects/Text/Text";
import FinalizarCompraSummary from "./FinalizarCompraSummary";
const StepResumen = loadable(() => import('./Steps/StepResumen'))
const StepMetodoPago = loadable(() => import('./Steps/StepMetodoPago'))
const StepDirecciones = loadable(() => import('./Steps/StepDirecciones'))
const StepContacto = loadable(() => import('./Steps/StepContacto'))



import Steps from "../../objects/Steps/Steps";
import Wrapper from "../../../objects/Wrapper";
import SummaryMobile from "../../modulos/Checkout/SummaryMobile";
import {fillBillingShippingStep, prepareBillingShippingInfo} from "../../../lib/checkout/util";

const stripePromiseTest = loadStripe('pk_test_51IQWUOAONSdZ0433emh9qnhg2FxQsJpgKvoXovsggxRwnFCZFXnKOPvB01L4UbS3bLEwK6Q2ShduB6TiIgy0Gp1000cDqyMtdV');
const stripePromise = loadStripe('pk_live_51IQWUOAONSdZ0433hiBqDnHl07qsErsMioel7sqeEFyWuJ49sEbBFDv16tH8VxVc3BLz5g0i8lfT4Xlr7CVUR3QI00TXrljZsO');

const FinalizarCompraContainer = ({state, actions, libraries, ...props}) => {
  const [isLoaded, setIsLoaded] = useState(false);
  const [isLogged, setLogged ] = useState(false)

  GridStyled.defaultProps = {
    theme: state.theme
  };

  TitleStyled.defaultProps = {
    theme: state.theme
  };

  FinalizarCompraSummaryStyled.defaultProps = {
    theme: state.theme
  };

  GroupStyled.defaultProps = {
    theme: state.theme
  };

  useEffect(() => {
    const p = processToken();
    setIsLoaded(true);

  })

  const processToken = async () => {
    const r = await actions.auth.authProcessAuthToken();
    if (state.tokenStatus === 1 && libraries.auth.authIsLogged()) {
      setLogged(true);

      if(state.theme.checkoutStep == 1) {
        let newCheckoutData = {...state.theme.checkoutData};


        const data = await libraries.register.getDatosPersonales(localStorage.getItem('UserID'))
        newCheckoutData.user = {
          email: data.email,
          isGuest: false
        };
        actions.theme.setCheckoutStep(2);
        actions.theme.setCheckoutData(newCheckoutData);
      }
    } else {
      libraries.auth.authUnregisterJwtToken();
    }
  }

  return (
    <Elements stripe={stripePromise}>
      <StepsWrapperStyled wrapper={"Default"} align="flex-start" direction="column">
        <Steps/>
      </StepsWrapperStyled>

      <TitleStyled data={{texto: 'TRAMITAR PEDIDO', font_type: 'H1', font_family: 'Lexend Zetta'}}/>

      <GridStyled>
        {isLoaded ? (
          <React.Fragment>
            <FinalizarCompraSummaryStyled forceShow={state.theme.checkoutStep === 4}>

              <GroupStyled>
                <TitleSummaryStyled data={{texto: 'RESUMEN DEL PEDIDO', font_type: 'H1Mobile', font_family: 'Lexend Zetta'}}/>

                <SummaryMobile/>
              </GroupStyled>

              <FinalizarCompraSummary/>
            </FinalizarCompraSummaryStyled>

            {
              state.theme.checkoutStep === 1 ?
                <BlockStyled show={state.theme.checkoutStep === 1}>
                  <StepContacto isLogged={isLogged}/>
                </BlockStyled>
                : null

            }

            {
              state.theme.checkoutStep === 2 ?
                <BlockStyled show={state.theme.checkoutStep === 2}>
                  <StepDirecciones isLogged={isLogged} setLogged={setLogged} />
                </BlockStyled>

                : null

            }

            {
              state.theme.checkoutStep === 3 ?
                <BlockStyled show={state.theme.checkoutStep === 3}>
                  <StepMetodoPago/>
                </BlockStyled>

                : null

            }
            {
              state.theme.checkoutStep === 4 ?
                <BlockStyled show={state.theme.checkoutStep === 4}>
                  <StepResumen/>
                </BlockStyled>

                : null

            }




          </React.Fragment>
        ) : ('')}
      </GridStyled>
    </Elements>
  )
};

export default connect(FinalizarCompraContainer);

const TitleSummaryStyled = styled(Text)`
  margin-bottom: 42px;
`;

const TitleStyled = styled(Text)`
  margin-bottom: 42px;
  display: none;

  ${props => {
    return `
    @media ${props.theme.breakPoints['tablet-wide']} {
      display: block;
    }
  `
  }}
`;

const FinalizarCompraSummaryStyled = styled.div`
  display: none;
  flex-direction: column;

  ${props => {
    return `
    @media ${props.theme.breakPoints['tablet-wide']} {
      display: flex;
    }
  `
  }}

  ${props => {
    return props.forceShow ? `display: flex !important;` : ``;
  }}
`;

const BlockStyled = styled.div`
  ${props => {
    return props.show ? `display: block;` : `display: none;`;
  }}
`;

const GroupStyled = styled.div`
  display: flex;
  flex-direction: column;

  ${props => {
  return `
    @media ${props.theme.breakPoints['tablet-wide']} {
      display: none;
    }
  `
}}
`;

const StepsWrapperStyled = styled(Wrapper)`
  margin-bottom: 28px;
  padding: 0 !important;
`;

const GridStyled = styled.div`
  --checkout-summary-width: 100%;
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 40px;

  ${props => {
    return `
    @media ${props.theme.breakPoints['tablet-wide']} {
      --checkout-summary-width: 400px;
      grid-gap: 60px;
      grid-template-columns: var(--checkout-summary-width) 1fr;
    }
    
    @media ${props.theme.breakPoints['desktop-wide']} {
      grid-gap: 125px;   
    }
  `
  }}
`;
