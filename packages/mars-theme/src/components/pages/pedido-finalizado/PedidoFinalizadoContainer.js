import React, {useEffect, useState} from "react";
import {connect, styled} from "frontity";
import Skeleton, {SkeletonTheme} from "react-loading-skeleton";

import Text from "../../../objects/Text/Text";
import RichText from "../../../objects/RichText/RichText";
import Layout from "../../../objects/Layout/Layout";
import PedidoFinalizadoSummary from "./PedidoFinalizadoSummary";

const PedidoFinalizadoContainer = ({state, actions, ...props}) => {

  const [orderData, setOrderData] = useState(null);
  const [cartData, setCartData] = useState(null);
  const [isGuest, setIsGuest] = useState(false);
  const [summaryText, setSummaryText] = useState("Tu número de pedido es . <br/><br/>Te mandaremos un mail para confirmarte que hemos recibido correctamente tu pedido y ya nos hemos puesto manos a la obra para preparártelo.");

  useEffect(() => {
    const order = localStorage.getItem('for-order');

    if (order) {
      const mysOrder = JSON.parse(order);

      setOrderData(mysOrder.data);
      setCartData(mysOrder.cart);
      setIsGuest(mysOrder.isGuest);

      setSummaryText("Tu número de pedido es <strong>"+mysOrder.data.orderNumber+"</strong>. <br/><br/>Te mandaremos un mail para confirmarte que hemos recibido correctamente tu pedido y ya nos hemos puesto manos a la obra para preparártelo.");

    } else {
      //actions.router.set('/registro/');
    }
  }, []);

  return (
    <SkeletonTheme color="#ecf5f8" highlightColor="#c3dee7">
      <TitleStyled data={{texto: '¡GRACIAS POR TU COMPRA!', font_type: 'H1', font_family: 'Lexend Zetta'}}/>

      <SummaryStyled justify="flex-start">
        <RichText data={{texto: summaryText, font_family: 'Lexend Deca'}}/>
      </SummaryStyled>

      {orderData === null ? (
        <Skeleton height={50} count={5}/>
      ) : (
        <PedidoFinalizadoSummary orderData={orderData} cartData={cartData}/>
      )}
    </SkeletonTheme>
  )
};

export default connect(PedidoFinalizadoContainer);

const TitleStyled = styled(Text)`
  margin-bottom: 42px;
`;

const SummaryStyled = styled(Layout)`
  background-color: var(--color-gray-base);
  padding: 8px;
  margin-bottom: 32px;

  strong {
    color: var(--color-blue-light);
  }
`;
