import React from "react";
import {connect, styled} from "frontity";

import Text from "../../../objects/Text/Text";
import Layout from "../../../objects/Layout/Layout";
import {formatAddress} from "../../../lib/checkout/addresses";
import ProductDetail from "../carrito/Left/ProductDetail";
import ButtonLink from "../../objects/ButtonLink/ButtonLink";

const PedidoFinalizadoSummary = ({state, actions, ...props}) => {

  return (
    <React.Fragment>
      <SubtitleStyled data={{texto: 'DETALLE DEL PEDIDO', font_type: 'H1Mobile', font_family: 'Lexend Zetta'}}/>

      <SummaryLineStyled>
        <SummaryTitleStyled data={{texto: 'DIRECCIÓN DE ENVÍO', font_type: 'LabelSmall', font_family: 'Lexend Zetta'}}/>

        <GroupLayoutStyled>
          {formatAddress(props.orderData.shipping)}
        </GroupLayoutStyled>
      </SummaryLineStyled>

      <SummaryLineStyled>
        <SummaryTitleStyled data={{texto: 'DIRECCIÓN DE FACTURACIÓN', font_type: 'LabelSmall', font_family: 'Lexend Zetta'}}/>

        <GroupLayoutStyled>
          {formatAddress(props.orderData.billing)}
        </GroupLayoutStyled>
      </SummaryLineStyled>

      <SummaryLineStyled>
        <SummaryTitleStyled data={{texto: 'MÉTODO DE PAGO', font_type: 'LabelSmall', font_family: 'Lexend Zetta'}}/>

        <GroupLayoutStyled>
          {props.orderData.paymentMethod === 'paypal' ? (
            <Text data={{texto: 'PayPal'}}/>
          ) : ('')}

          {props.orderData.paymentMethod === 'paypal_express' ? (
            <Text data={{texto: 'PayPal Express'}}/>
          ) : ('')}

          {props.orderData.paymentMethod === 'stripe' ? (
            <Text data={{texto: 'Tarjeta de crédito'}}/>
          ) : ('')}
        </GroupLayoutStyled>
      </SummaryLineStyled>

      <SummaryLineStyled>
        <SummaryTitleStyled data={{texto: 'ENVÍO', font_type: 'LabelSmall', font_family: 'Lexend Zetta'}}/>

        <GroupLayoutStyled>
          <Text data={{texto: 'Fecha estimada de entrega:<br/>Lunes 22 Junio - Martes 23 Junio'}}/>
        </GroupLayoutStyled>
      </SummaryLineStyled>


      {
        props.orderData && props.orderData.couponLines && props.orderData.couponLines.edges.length > 0
          ?
          <SummaryLineStyled>
            <SummaryTitleStyled data={{texto: 'Cupon: '+props.orderData.couponLines.edges[0].node.code, font_type: 'LabelSmall', font_family: 'Lexend Zetta'}}/>
            <GroupLayoutStyled>
              <Text data={{texto: "-"+props.orderData.couponLines.edges[0].node.amount?.toString() + '€'}}/>
            </GroupLayoutStyled>
          </SummaryLineStyled>
          : null
      }



      <SummaryLineStyled>
        <div>
          <SummaryTitleStyled data={{texto: 'IVA', font_type: 'LabelSmall', font_family: 'Lexend Zetta'}}/>
          <SummaryTitleStyled data={{texto: 'TOTAL', font_type: 'LabelSmall', font_family: 'Lexend Zetta'}}/>
        </div>

        <div>
          <RightText data={{texto: props.orderData.totalTax}}/>
          <RightText data={{texto: props.orderData.total, font_type: 'TotalPrice', font_family: 'Lexend Zetta'}}/>
        </div>
      </SummaryLineStyled>

      <Subtitle2Styled data={{texto: 'RESUMEN DEL PEDIDO', font_type: 'H1Mobile', font_family: 'Lexend Zetta'}}/>

      <ContentWrapper data={{section_background_color: 'gray-base'}}>
        {props.cartData?.contents.nodes.map((item, key) => (
          <ProductDetail item={item} key={key} inCheckout={true} initialQuantity={item.quantity}/>
        ))}
      </ContentWrapper>

      <ButtonLink texto={"VOLVER AL INICIO"} handleOnClick={() => actions.router.set('/')}/>
    </React.Fragment>
  )
};

export default connect(PedidoFinalizadoSummary);

const ContentWrapper = styled(Layout)`
  padding-top: 10px;
  padding-bottom: 16px;
  margin-bottom: 40px;
`;

const RightText = styled(Text)`
  text-align: right;
`
const SubtitleStyled = styled(Text)`
  margin-bottom: 20px;
`;

const Subtitle2Styled = styled(Text)`
  margin-top: 43px;
  margin-bottom: 30px;
`;

const SummaryLineStyled = styled(Layout)`
  align-items: flex-start;
  justify-content: space-between;
  padding: 16px 4px;
  flex-wrap: nowrap;

  &:not(:last-child) {
    border-bottom: 1px solid #E5EDEF;
  }
`;

const SummaryTitleStyled = styled(Text)`
  margin-right: 4px;
  letter-spacing: -1px !important;
  padding-right: 16px;
`;

const GroupLayoutStyled = styled(Layout)`
  flex-direction: column;
  justify-content: flex-end;
  align-items: flex-end;
  width: initial;
  text-align: right;
`;

