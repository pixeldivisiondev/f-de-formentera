import React, {useState, useEffect} from 'react';
import {connect} from "frontity";
import {ApolloError} from "@apollo/client";
import Skeleton, {SkeletonTheme} from "react-loading-skeleton";

import MiCuentaIntro from "./Intro/MiCuentaIntro";
import FormDatosPersonales from "./Forms/FormDatosPersonales";

export default connect(function MisDatosPersonales({state, libraries, actions, ...props}) {
  const [userData, setUserData] = useState(null);

  useEffect(() => {
    processToken();
  }, []);

  const processToken = async () => {
    const r = await actions.auth.authProcessAuthToken();

    if (state.tokenStatus === 1 && libraries.auth.authIsLogged()) {
      findDatosPersonales();
    } else {
      logOut();
    }
  }

  const findDatosPersonales = async () => {
    const authId = libraries.auth.authGetUserAuthId();

    const r = await libraries.register.getDatosPersonales(authId);
    if (r == null || r instanceof ApolloError) {
      logOut();
    } else {
      setUserData(r);
    }
  };

  const logOut = async () => {
    libraries.auth.authUnregisterJwtToken();
    actions.theme.setLoginName(null);
    actions.theme.logOut();
    await actions.router.set('/');
  };

  return (
    <SkeletonTheme color="#ecf5f8" highlightColor="#c3dee7">
      <MiCuentaIntro title="MIS DATOS PERSONALES" texto="Puedes modificar aquí tus datos pesonales para que tu cuenta esté siempre actualizada."/>
      {userData === null ? (
        <React.Fragment>
          <Skeleton height={50} count={2}/>
          <Skeleton height={20} count={1}/>
        </React.Fragment>
      ) : (
        <FormDatosPersonales formData={userData}/>
      )}
    </SkeletonTheme>
  )
});
