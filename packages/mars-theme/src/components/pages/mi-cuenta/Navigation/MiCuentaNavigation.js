import React from 'react';
import {connect, styled} from "frontity";
import MiCuentaNavigationItem from "./MiCuentaNavigationItem";
import Layout from "../../../../objects/Layout/Layout";
import Text from "../../../../objects/Text/Text";
import FrLink from "@frontity/components/link";

export default connect(function MiCuentaNavigation(props) {
  const userRole = typeof window !== 'undefined' ? localStorage.getItem('Role') : null;


  return (
    <NavStyled>
      <MiCuentaNavigationItem
        icon="user"
        title="MI CUENTA"
        isOpen={props.current.includes('mi-cuenta/mis-datos-personales') || props.current.includes('mi-cuenta/cambiar-contrasena') || props.current.includes('mi-cuenta/mis-direcciones')}
      >
        <Layout direction="column" align="flex-start">
          <FrNavLinkStyled
            link="/mi-cuenta/mis-datos-personales/"
            active={props.current.includes('mis-datos-personales')}
          >
            <Text data={{texto: "Mis datos personales"}}/>
          </FrNavLinkStyled>

          <FrNavLinkStyled
            link="/mi-cuenta/cambiar-contrasena/"
            active={props.current.includes('cambiar-contrasena')}
          >
            <Text data={{texto: "Cambiar contraseña"}}/>
          </FrNavLinkStyled>

          <FrNavLinkStyled
            link="/mi-cuenta/mis-direcciones/"
            active={props.current.includes('mis-direcciones')}
          >
            <Text data={{texto: "Mis direcciones"}}/>
          </FrNavLinkStyled>
        </Layout>
      </MiCuentaNavigationItem>

      <MiCuentaNavigationItem
        icon="orders"
        title="MIS COMPRAS"
        isOpen={props.current.includes('mis-compras/')}
      >
        <Layout direction="column" align="flex-start">
          <FrNavLinkStyled
            link="/mi-cuenta/mis-pedidos/"
            active={props.current.includes('mis-pedidos')}
          >
            <Text data={{texto: "Mis pedidos"}}/>
          </FrNavLinkStyled>
        </Layout>
      </MiCuentaNavigationItem>

      {
        userRole === 'empresa' ? <MiCuentaNavigationItem
          icon="file"
          title="FACTURACIÓN"
          isOpen={props.current.includes('mi-cuenta/facturacion')}
        >
          <Layout direction="column" align="flex-start">
            <FrNavLinkStyled
              link="/mi-cuenta/facturacion/"
              active={props.current.includes('mi-cuenta/facturacion')}
            >
              <Text data={{texto: "Ver Facturas"}}/>
            </FrNavLinkStyled>
          </Layout>
        </MiCuentaNavigationItem> : null
      }

      <MiCuentaNavigationItem
        icon="contacto"
        title="CONTACTO"
        isOpen={props.current.includes('contacto/')}
      >
        <Layout direction="column" align="flex-start">
          <FrNavLinkStyled
            link="/contacto"
            active={props.current.includes('Contacto')}
          >
            <Text data={{texto: "Contacto"}}/>
          </FrNavLinkStyled>
        </Layout>
      </MiCuentaNavigationItem>

      {
        userRole === 'administrator' ? <MiCuentaNavigationItem
          icon="orders"
          title="COMERCIALES"
          isOpen={props.current.includes('mis-compras/')}
        >
          <Layout direction="column" align="flex-start">
            <FrNavLinkStyled
              link="/mi-cuenta/crear-usuario/"
              active={props.current.includes('anadir-cliente')}
            >
              <Text data={{texto: "Crear Usuario"}}/>
            </FrNavLinkStyled>
          </Layout>
        </MiCuentaNavigationItem> : null
      }


    </NavStyled>
  )
});

const NavStyled = styled.nav`
  margin-top: 12px;
  margin-bottom: 40px;
`;

const FrNavLinkStyled = styled(FrLink)`
  text-decoration: none;
  
  > * {
    color: inherit !important;
  }
  
  &:not(:last-child) {
    margin-bottom: 12px;
  }
  
  ${props => props.active ? `color: var(--color-blue-light);` : `color: var(--color-primary-base);`}
`;
