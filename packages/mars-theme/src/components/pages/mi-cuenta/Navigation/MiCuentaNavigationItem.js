import React, {useState} from 'react';
import {connect, styled} from "frontity";
import Layout from "../../../../objects/Layout/Layout";
import Text from "../../../../objects/Text/Text";

import UserIcon from '../../../../assets/images/icons/user.js';
import BagIcon from '../../../../assets/images/icons/bag.js';
import ContactoIcon from '../../../../assets/images/icons/chat.js';
import FileIcon from '../../../../assets/images/icons/file.js';
import ChevronDownIcon from "../../../../assets/images/icons/chevron-down";

export default connect(function MiCuentaNavigationItem({state, ...props}) {
  const [open, setOpen] = useState(props.isOpen);

  let icon = '';

  switch (props.icon) {
    case 'user':
      icon = <UserIcon/>;
      break;
    case 'orders':
      icon = <BagIcon/>;
      break;
    case 'contacto':
      icon = <ContactoIcon/>;
      break;
    case 'file':
      icon = <FileIcon/>;
      break;
    default:
      break;
  }
  return (
    <LayoutStyled>
      <HeaderStyled justify="space-between" onClick={() => setOpen(!open)}>
        {icon}
        <TitleStyled data={{texto: props.title, font_type: 'ButtonLarge'}}/>

        <ChevronDownIconStyled isOpen={open}>
          <ChevronDownIcon/>
        </ChevronDownIconStyled>
      </HeaderStyled>
      <BodyStyled isOpen={open}>
        {props.children}
      </BodyStyled>
    </LayoutStyled>
  )
});

const LayoutStyled = styled(Layout)`
  border-bottom: 1px solid #E5EDEF;
`;

const TitleStyled = styled(Text)`
  color: inherit;
  flex-grow: 1;
  margin-left: 10px;
`;

const HeaderStyled = styled(Layout)`
  color: var(--color-blue-light);
  height: 60px;
  cursor: pointer;
`;

const BodyStyled = styled(Layout)`
  background-color: var(--color-background-base);
  padding: 0 16px;
  user-select: none;
  max-height: 0;
  height: auto;
  overflow: hidden;
  min-height: initial;
  transition: all 0.5s;
  
  ${props => props.isOpen ? `
      padding: 16px;
      max-height: 50rem;
      transition: all 0.5s;
    ` : ``};
`;

const ChevronDownIconStyled = styled.div
  `
transition: 0.3s all ease-in-out;

  ${props => props.isOpen ? `transform: rotate(180deg);` : `transform: rotate(0deg);`}
`
;
