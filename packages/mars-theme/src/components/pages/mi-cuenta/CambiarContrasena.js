import React from 'react';
import {connect} from "frontity";

import MiCuentaIntro from "./Intro/MiCuentaIntro";
import FormCambiarContrasena from "./Forms/FormCambiarContrasena";

export default connect(function CambiarContrasena({libraries, actions, ...props}) {
  return (
    <React.Fragment>
      <MiCuentaIntro title="CAMBIAR CONTRASEÑA" texto="Si deseas actualizar tu contraseña, puedes hacerlo fácilmente desde aquí para mantener la seguridad de tu cuenta"/>

      <FormCambiarContrasena/>
    </React.Fragment>
  )
});