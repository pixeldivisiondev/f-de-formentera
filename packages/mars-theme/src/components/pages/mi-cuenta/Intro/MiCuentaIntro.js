import React from 'react';
import {connect, styled} from "frontity";

import Text from "../../../../objects/Text/Text";

export default connect(function MiCuentaIntro(props) {
  return (
    <React.Fragment>
      <TitleStyled data={{texto: props.title, font_type: 'H1Mobile', font_family: 'Lexend Zetta'}}/>
      <TextStyled data={{texto: props.texto}}/>
    </React.Fragment>
  )
});

const TitleStyled = styled(Text)`
  margin-bottom: 24px;
`;

const TextStyled = styled(Text)`
  margin-bottom: 22px;
  opacity: 0.6;
`;