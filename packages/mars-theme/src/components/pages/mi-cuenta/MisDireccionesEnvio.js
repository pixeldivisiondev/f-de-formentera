import React, {useState, useEffect} from 'react';
import {connect} from "frontity";
import {ApolloError} from "@apollo/client";
import Skeleton, {SkeletonTheme} from "react-loading-skeleton";

import MiCuentaIntro from "./Intro/MiCuentaIntro";
import FormDireccionEnvio from "./Forms/FormDireccionEnvio";

export default connect(function MisDireccionesEnvio({libraries, state, actions, ...props}) {
  const [addressesData, setAddressesData] = useState(null);

  useEffect(() => {
    processToken();
  }, []);

  const processToken = async () => {
    const r = await actions.auth.authProcessAuthToken();

    if (state.tokenStatus === 1 && libraries.auth.authIsLogged()) {
      findAddresses();
    } else {
      logOut();
    }
  }

  const findAddresses = async () => {
    const authId = libraries.auth.authGetUserAuthId();

    const r = await libraries.mi_cuenta.getAddresses(authId);
    if (r == null || r instanceof ApolloError) {
      logOut();
    } else {
      setAddressesData(r);
    }
  };

  const logOut = async () => {
    libraries.auth.authUnregisterJwtToken();
    actions.theme.setLoginName(null);
    actions.theme.logOut();
    await actions.router.set('/');
  };

  return (
    <SkeletonTheme color="#ecf5f8" highlightColor="#c3dee7">
      <MiCuentaIntro title="EDITAR DIRECCIÓN" texto=""/>
      {addressesData === null ? (
        <>
          <Skeleton height={50} count={5}/>
        </>
      ) : (
        <FormDireccionEnvio data={addressesData}/>
      )}
    </SkeletonTheme>
  )
});
