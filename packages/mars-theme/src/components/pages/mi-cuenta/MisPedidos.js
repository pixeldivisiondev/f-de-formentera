import React, {useState, useEffect} from 'react';
import {connect, styled} from "frontity";
import {ApolloError} from "@apollo/client";
import Skeleton, {SkeletonTheme} from "react-loading-skeleton";

import MiCuentaIntro from "./Intro/MiCuentaIntro";
import PedidosLine from "./pedidos/PedidosLine";
import Text from "../../../objects/Text/Text";

export default connect(function MisPedidos({libraries, actions, state, ...props}) {
  const [ordersData, setOrdersData] = useState(null);

  useEffect(() => {
    processToken();
  }, []);

  const processToken = async () => {
    const r = await actions.auth.authProcessAuthToken();

    if (state.tokenStatus === 1 && libraries.auth.authIsLogged()) {
      findOrders();
    } else {
      logOut();
    }
  }

  const findOrders = async () => {
    const customerId = libraries.auth.authGetCustomerId();

    const r = await libraries.mi_cuenta.getOrders(customerId);
    if (r == null || r instanceof ApolloError) {
      logOut();
    } else {
      setOrdersData(r);
    }
  };

  const logOut = async () => {
    libraries.auth.authUnregisterJwtToken();
    actions.theme.setLoginName(null);
    actions.theme.logOut();
    await actions.router.set('/');
  };

  return (
    <SkeletonTheme color="#ecf5f8" highlightColor="#c3dee7">
      <MiCuentaIntro title="MIS PEDIDOS" texto=""/>
      {ordersData === null ? (
        <>
          <Skeleton height={50} count={5}/>
        </>
      ) : (
        <>
          {ordersData.edges.length > 0?(
            <>
              {ordersData.edges.map(item => (
                <PedidosLine key={item.node.orderNumber} data={item.node}/>
              ))}
            </>
          ):(
            <TextStyled data={{texto: "No se ha hecho ningún pedido todavía."}}/>
          )}

        </>
      )}
    </SkeletonTheme>
  )
});

const TextStyled = styled(Text)`
  margin-bottom: 22px;
  opacity: 0.6;
`;
