import React, {useEffect} from 'react';
import {connect, styled} from "frontity";
import FrLink from "@frontity/components/link";

import Button from "../../../objects/Button/Button";
import Text from "../../../objects/Text/Text";
import Layout from "../../../objects/Layout/Layout";
import MiCuentaNavigation from "./Navigation/MiCuentaNavigation";
import Wrapper from "../../../objects/Wrapper";

export default connect(function MiCuentaContainer({state, actions, libraries, ...props}) {
  StyledContainer.defaultProps = {
    theme: state.theme
  };

  WrapperStyled.defaultProps = {
    theme: state.theme
  };

  StyledSidebar.defaultProps = {
    theme: state.theme
  };

  StyledContent.defaultProps = {
    theme: state.theme
  };

  FrNavLinkStyled.defaultProps = {
    theme: state.theme
  };

  const logOut = async () => {
    libraries.auth.authUnregisterJwtToken();
    actions.theme.setLoginName(null);
    actions.theme.logOut();
    await actions.router.set('/');
  };

  useEffect(() => {
    if (!libraries.auth.authIsLogged()) {
      logOut();
    }
  });

  const name = libraries.login.getLoginName();
  const welcomeMessage = (name)? 'HOLA ' + name.toUpperCase() : 'HOLA';
  const areYouMessage = (name)? '¿No eres ' + name + '?' : '¿No eres tú?';



  return (
    <WrapperStyled align="flex-start" direction="column">
      <StyledContainer align="flex-start">
        <StyledSidebar hideOnMobile={props.current !== 'dashboard'}>
          <TitleStyled data={{texto: welcomeMessage, font_type: 'H1', font_family: 'Lexend Zetta'}}/>
          <Layout justify="space-between" align="center">
            <Text data={{texto: areYouMessage}}/>
            <div onClick={logOut}><StyledCerrarSesion data={{texto: 'CERRAR SESIÓN', font_type: 'PrimarySmall'}}/></div>
          </Layout>

          <MiCuentaNavigation current={props.current}/>

          <StyledCerrarSesionButton data={{tipo: 'outlined', tamano: 'full', texto: "CERRAR SESIÓN"}} onClick={logOut}/>
        </StyledSidebar>

        <StyledContent>
          {props.current !== 'dashboard' ? (
            <FrNavLinkStyled link={"/mi-cuenta/"}>
              <Layout justify="flex-start">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M14 6L8 12L14 18" stroke="#00AFE6" strokeLinecap="round"/>
                </svg>

                <Text data={{texto: "ATRÁS", font_type: 'ButtonLarge'}}/>
              </Layout>
            </FrNavLinkStyled>
          ) : ('')}

          {props.children}
        </StyledContent>
      </StyledContainer>
    </WrapperStyled>
  )
});

const WrapperStyled = styled(Wrapper)`
  padding: 28px 16px;
  
  ${props => {
  return `  
      @media ${props.theme.breakPoints['tablet']} {
        height: initial;
      }
    `
}}
`;

const StyledContainer = styled(Layout)`
  --container-sidebar-width: 100%;
  display: grid;
  grid-template-columns: 1fr;
  min-height: 50vh;
  
  ${props => {
  return `
      @media ${props.theme.breakPoints['desktop']} {
        grid-gap: 130px;
      }
      
      @media ${props.theme.breakPoints['tablet-wide']} {
        grid-gap: 60px;
      }
        
      @media ${props.theme.breakPoints['tablet']} {
        --container-sidebar-width: 275px;
        grid-template-columns: var(--container-sidebar-width) 1fr;
        padding-bottom: 150px;
        grid-gap: 60px;
      }
    `
}}
`;

const TitleStyled = styled(Text)`
  margin-bottom: 24px;
`;

const StyledSidebar = styled.aside`
  display: flex;
  flex-direction: column;
  
  width: var(--container-sidebar-width);
  
  > *:last-child {
    margin-top: auto;
  }
  
  ${props => {
  return props.hideOnMobile ? `display: none;` : ``;
}}
  
  ${props => {
  return `  
        @media ${props.theme.breakPoints['tablet']} {
          height: initial;
          display: flex !important;
          height: calc(100vh - 69px - (2 * 28px));        
          > *:last-child {
            margin-top: initial;
          }
        }
      `
}}
`;

const StyledContent = styled.div`
  ${props => {
  return `  
      @media ${props.theme.breakPoints['tablet']} {
        padding-top: 50px;
      }
    `
}}
`;

const StyledCerrarSesion = styled(Text)`
  color: var(--color-blue-light);
  text-align: right;
  letter-spacing: 2px !important;
  cursor: pointer;
`;

const StyledCerrarSesionButton = styled(Button)`
  margin-top: 48px;
  letter-spacing: 1.2px;
`;

const FrNavLinkStyled = styled(FrLink)`
  color: var(--color-blue-light);
  display: block;
  margin-bottom: 12px;
  text-decoration: none;

  > *, p {
    color: inherit !important;
  }
  
  ${props => {
  return `  
    @media ${props.theme.breakPoints['tablet']} {
      display: none;
    }
  `
}}
`;
