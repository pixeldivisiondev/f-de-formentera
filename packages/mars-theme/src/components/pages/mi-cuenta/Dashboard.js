import React from 'react';
import {connect, styled} from "frontity";
import Text from "../../../objects/Text/Text";

export default connect(function Dashboard({state, actions, libraries, ...props}) {
  StyledContainer.defaultProps = {
    theme: state.theme
  };


  const texto = "Recuerda que desde el panel de control de tu cuenta puedes ver tus <a href='/mi-cuenta/mis-pedidos'> pedidos recientes</a>."

  return (
    <StyledContainer>

      <TitleStyled data={{texto: 'BIENVENID@ OTRA VEZ', font_type: 'H1Mobile', font_family: 'Lexend Zetta'}}/>
      <TextStyled data={{texto: texto}}/>
    </StyledContainer>
  )
});

const StyledContainer = styled.div`
  display: none;
  
  ${props => {
  return `  
      @media ${props.theme.breakPoints['tablet']} {
        display: block;
      }
    `
}}
`;

const TitleStyled = styled(Text)`
  margin-bottom: 16px;
`;

const TextStyled = styled(Text)`
  max-width: 500px;

  a {
    color: var(--color-blue-light);
    text-decoration: none;
  }
`;
