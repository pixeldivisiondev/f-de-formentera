import React, {useEffect} from 'react';
import {connect} from "frontity";


import MiCuentaIntro from "./Intro/MiCuentaIntro";
import FormDireccionEnvio from "./Forms/FormDireccionEnvioNueva";

export default connect(function MisDireccionesEnvioNueva({libraries, state, actions, ...props}) {

  useEffect(() => {
    processToken();
  }, []);

  const processToken = async () => {
    const r = await actions.auth.authProcessAuthToken();

    if (state.tokenStatus === 1 && libraries.auth.authIsLogged()) {
      //findAddresses();
    } else {
      logOut();
    }
  }

  const logOut = async () => {
    libraries.auth.authUnregisterJwtToken();
    actions.theme.setLoginName(null);
    actions.theme.logOut();
    await actions.router.set('/');
  };

  return (
      <>
        <MiCuentaIntro title="EDITAR DIRECCIÓN" texto=""/>
        <FormDireccionEnvio />
      </>

  )
});
