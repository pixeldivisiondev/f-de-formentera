import React from 'react';
import {connect, styled} from "frontity";
import moment from 'moment';
import FrLink from "@frontity/components/link";
import Text from "../../../../objects/Text/Text";
import Layout from "../../../../objects/Layout/Layout";

export default connect(function FacturacionLine({libraries, actions, ...props}) {

  moment.locale('es');

  const date = moment(props.data.date).format('d MMMM YYYY')
  ;
  return (
    <LineStyled>
      <Layout justify={"space-between"} align={"flex-start"}>
        <FlexLayout direction={"column"} align={"flex-start"}>
          <TitleStyled data={{texto: 'Factura '+date, font_type: 'SubtitleSmall', font_family: 'Lexend Zetta'}}/>
          <OrderNumberStyled data={{texto: "Pedido #" + props.data.orderNumber, font_type: 'SecondarySmall'}}/>
        </FlexLayout>
        <FlexLayout justify={"flex-end"} align={"flex-start"}>
          <TitleStyled data={{texto: '42,86€', font_type: 'SubtitleSmall', font_family: 'Lexend Zetta'}}/>
        </FlexLayout>
      </Layout>

      <Layout justify={"flex-start"}>
        <FrNavLinkStyled link={"/mi-cuenta/ver-pedido/" + props.data.orderNumber}>
          <Text data={{texto: "DESCARGAR FACTURA", font_type: 'ButtomSmall'}}/>
        </FrNavLinkStyled>
      </Layout>

    </LineStyled>
  )
});

const LineStyled = styled.div`
  border-bottom: 1px solid #E5EDEF;
  padding-bottom: 16px;
`;

const TitleStyled = styled(Text)`
  margin-bottom: 0px;
`;

const OrderNumberStyled = styled(Text)`
  color: #004E66;
  opacity: 0.6;
  margin-bottom: 17px;
`;

const FlexLayout = styled(Layout)`
  flex: 1;
`

const FrNavLinkStyled = styled(FrLink)`
  text-decoration: none;
  color: var(--color-blue-light);
  
  > * {
    color: inherit !important;
  }
`;



