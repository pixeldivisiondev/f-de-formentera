import React, {useState} from 'react';
import {connect, styled} from "frontity";
import {useForm} from "react-hook-form";
import {ApolloError} from "@apollo/client";

import Form from "../../../../objects/Forms/Form";
import AlertMessage from "../../../../objects/AlertMessage/AlertMessage";
import FlexGrid from "../../../../objects/FlexGrid/FlexGrid";
import FlexGridItem from "../../../../objects/FlexGrid/FlexGridItem";
import FormControl from "../../../../objects/Forms/FormControl";
import ButtonSubmit from "../../../../objects/ButtonSubmit/ButtonSubmit";

import {findIndex} from "../../../../lib/utils/birthday";
import {findMetaData} from "../../../../lib/utils/util";
import {getListTipoVia} from "../../../../lib/checkout/addresses";
import {findIndexState, getStateList} from "../../../../lib/checkout/statelist";

export default connect(function FormDireccionFacturacion({state, actions, libraries, ...props}) {
  const tipoViaList = getListTipoVia();
  const tipoViaIndex = findIndex(tipoViaList, findMetaData(tipoViaList, 'billing_tipo_via'));
  const tipoViaIndex2 = (tipoViaIndex !== -1) ? tipoViaIndex : 0;
  const [submitting, setSubmitting ] = useState(false);
  const last  = props.data.billing.extra_billing_addresses.length - 1;
  const stateList = getStateList();
  const stateIndex = findIndexState(props.data.billing?.extra_billing_addresses[last]?.state);
  const stateIndex2 = (stateIndex !== -1) ? stateIndex : 0;
console.log(props.data.billing.extra_billing_addresses)

  const {register, handleSubmit, watch, errors, control} = useForm({
    defaultValues: {
      billing_first_name: props.data.billing?.extra_billing_addresses[last]?.firstName,
      billing_last_name: props.data.billing?.extra_billing_addresses[last]?.lastName,
      billing_postcode: props.data.billing?.extra_billing_addresses[last]?.postcode,
      billing_city: props.data.billing?.extra_billing_addresses[last]?.city,
      billing_state: stateList[stateIndex2],
      billing_phone: props.data.billing?.extra_billing_addresses[last]?.phone,
      billing_tipo_via: tipoViaList[tipoViaIndex2],
      billing_street: props.data.billing?.extra_billing_addresses[last]?.street,
      billing_numero: props.data.billing?.extra_billing_addresses[last]?.numero,
      billing_piso: props.data.billing?.extra_billing_addresses[last]?.piso,
    }
  });

  const handleOnSubmit = async (data) => {
    setSubmitting(true)
    actions.theme.clearAlertMessage();

    await actions.auth.authProcessAuthToken();

    let addressLine = data.billing_tipo_via.value + " " + data.billing_street + " " + data.billing_numero;

    if(data.shipping_piso) {
      addressLine += data.billing_piso
    }

    const address = {
      firstName: data.billing_first_name,
      lastName: data.billing_last_name,
      address1: addressLine,
      postcode: data.billing_postcode,
      city: data.billing_city,
      phone: data.billing_phone,
      piso: data.billing_piso,
      state: data.billing_state?.value,
      tipoVia: data.billing_tipo_via.value,
      street: data.billing_street,
      numero: data.billing_numero,
    };

    if(data.billing_piso) {
      address.piso = data.billing_piso
    }

    if (libraries.auth.authIsLogged()) {
      await updateAddressFacturacion(address);
      setSubmitting(false)
    } else {
      setSubmitting(false)
      await logOut();
    }
  };

  const updateAddressFacturacion = async address => {
    const authId = libraries.auth.authGetUserAuthId();

    const r = await libraries.mi_cuenta.updateAddressFacturacion(authId, address);
    if (r instanceof ApolloError) {
      actions.theme.setAlertMessage("Ha ocurrido un error al actualizar los datos de facturación", false, 'error');
    } else {
      actions.theme.setAlertMessage("Los cambios se han guardado correctamente", false, 'success');
    }
  };

  const logOut = async () => {
    libraries.auth.authUnregisterJwtToken();
    actions.theme.setLoginName(null);
    actions.theme.logOut();
    await actions.router.set('/');
  };

  return (
    <Form onSubmit={handleSubmit(handleOnSubmit)}>
      <AlertMessage inModal={false}/>

      <FlexGrid>
        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
          <FormControl
            id="billing_first_name" name="billing_first_name"
            type="text" placeholder="Nombre"
            register={register}
            errors={errors}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
          <FormControl
            id="billing_last_name" name="billing_last_name"
            type="text" placeholder="Apellidos"
            register={register}
            errors={errors}
          />
        </FlexGridItem>

        <div style={{height: '20px'}}/>

        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 2}}>
          <FormControl
            id="billing_tipo_via" name="billing_tipo_via"
            type="select" placeholder="Tipo vía"
            selectOptions={tipoViaList} selectIndex={tipoViaIndex}
            register={register}
            errors={errors}
            control={control}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
          <FormControl
            id="billing_street" name="billing_street"
            type="text" placeholder="Dirección"
            register={register}
            errors={errors}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 6, tablet: 6, desktop: 2}}>
          <FormControl
            id="billing_numero" name="billing_numero"
            type="text" placeholder="Nº"
            register={register}
            errors={errors}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 6, tablet: 6, desktop: 2}}>
          <FormControl
            id="billing_piso" name="billing_piso"
            type="text" placeholder="Piso"
            register={register}
            noRequired={true}
            errors={errors}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 3}}>
          <FormControl
            id="billing_postcode" name="billing_postcode"
            type="postcode" placeholder="Código postal"
            register={register}
            errors={errors}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 3}}>
          <FormControl
            id="billing_city" name="billing_city"
            type="text" placeholder="Ciudad"
            register={register}
            errors={errors}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 3}}>
          <FormControl
            id="billing_state" name="billing_state"
            type="state" placeholder="Provincia" selectIndex={stateIndex}
            register={register}
            errors={errors}
            control={control}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 3}}>
          <FormControl
            id="billing_phone" name="billing_phone"
            type="text" placeholder="Teléfono"
            register={register}
            errors={errors}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 12, desktop: 12}}>
          <ButtonSubmit disabled={submitting} text={submitting ? 'ENVIANDO...' : "GUARDAR" }/>
        </FlexGridItem>
      </FlexGrid>
    </Form>
  )
});
