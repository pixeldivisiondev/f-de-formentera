import React, {useRef, useState} from 'react';
import {connect, styled} from "frontity";
import {useForm} from "react-hook-form";
import {ApolloError} from "@apollo/client";

import Form from "../../../../objects/Forms/Form";
import AlertMessage from "../../../../objects/AlertMessage/AlertMessage";
import FlexGrid from "../../../../objects/FlexGrid/FlexGrid";
import FlexGridItem from "../../../../objects/FlexGrid/FlexGridItem";
import FormControl from "../../../../objects/Forms/FormControl";
import ButtonSubmit from "../../../../objects/ButtonSubmit/ButtonSubmit";
import Text from "../../../../objects/Text/Text";
import Modal from "../../../../objects/Modal/Modal";

import {CloseModal, ModalContent, ModalWrapper} from "../../../objects/Modals";

import LostPasswordModalContent from "../../login/LoginModal/LostPasswordModalContent";

export default connect(function FormCambiarContrasena({state, actions, libraries, ...props}) {
  const {register, handleSubmit, watch, errors} = useForm();

  const password = useRef({});
  password.current = watch("password_1", "");

  const [submitting, setSubmitting ] = useState(false);

  const handleOnSubmit = async (data, e) => {
    setSubmitting(true);
    actions.theme.clearAlertMessage();
    await processToken(data.password_1);
    e.target.reset();
    setSubmitting(false);
  };

  const processToken = async (data) => {
    const r = await actions.auth.authProcessAuthToken();

    if (state.tokenStatus === 1 && libraries.auth.authIsLogged()) {
      await updatePassword(data);
    } else {
      await logOut();
    }
  }

  const updatePassword = async pass => {

    const authId = libraries.auth.authGetUserAuthId();

    const r = await libraries.mi_cuenta.updatePassword(authId, pass);
    if (r instanceof ApolloError) {
      actions.theme.setAlertMessage("Ha ocurrido un error al actualizar la contraseña", false, 'error');
    } else {
      actions.theme.setAlertMessage("La contraseña se ha actualizado correctamente", false, 'success');
    }
  };

  const logOut = async () => {
    libraries.auth.authUnregisterJwtToken();
    actions.theme.setLoginName(null);
    actions.theme.logOut();
    await actions.router.set('/');
  };

  return (
    <Form onSubmit={handleSubmit(handleOnSubmit)}>
      <AlertMessage inModal={false}/>

      <FlexGrid>
        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
          <FormControl
            id="password_1" name="password_1"
            type="password" placeholder="Nueva contraseña"
            register={register}
            errors={errors}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
          <FormControl
            id="password_2" name="password_2"
            type="password" placeholder="Confirmar nueva contraseña"
            register={register}
            extraValidate={{validate: value => value === password.current || "Las contraseñas deben coincidir"}}
            errors={errors}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
          <Modal
            modal={<LostPasswordModalContent hideLogin={true}/>}
            wrapper={ModalWrapper}
            ModalContent={ModalContent}
            CloseModal={CloseModal}
          >
            <LostPasswordStyled data={{texto: 'NO RECUERDO MI CONTRASEÑA', font_type: 'PrimarySmall'}}/>
          </Modal>
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 12, desktop: 12}}>
          <ButtonSubmit disabled={submitting} text={submitting ? 'ENVIANDO...' : 'GUARDAR CONTRASEÑA'} />
        </FlexGridItem>
      </FlexGrid>
    </Form>
  )
});

const LostPasswordStyled = styled(Text)`
  color: var(--color-blue-light);
  text-align: right;
  margin-bottom: 24px;
  letter-spacing: 2px !important;
  cursor: pointer;
`;
