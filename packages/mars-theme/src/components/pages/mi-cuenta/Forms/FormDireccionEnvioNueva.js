import React, {useEffect, useState} from 'react';
import {connect, styled} from "frontity";
import {useForm} from "react-hook-form";
import {ApolloError} from "@apollo/client";

import Form from "../../../../objects/Forms/Form";
import AlertMessage from "../../../../objects/AlertMessage/AlertMessage";
import FlexGrid from "../../../../objects/FlexGrid/FlexGrid";
import FlexGridItem from "../../../../objects/FlexGrid/FlexGridItem";
import FormControl from "../../../../objects/Forms/FormControl";
import ButtonSubmit from "../../../../objects/ButtonSubmit/ButtonSubmit";

import {getListTipoVia} from "../../../../lib/checkout/addresses";

export default connect(function FormDireccionEnvioNueva({state, actions, libraries, ...props}) {
  const tipoViaList = getListTipoVia();
  const [isMobil, setIsMobil ] = useState(false)

  useEffect(() => {
    register({ name: "shipping_tipo_via" }, { required: true });
    register({ name: "shipping_state" }, { required: true });
    if (typeof window !== 'undefined') {
      if(window.innerWidth <= 768) {
        setIsMobil(true)
      }
    }
  }, []);


  const {register, handleSubmit, watch, errors, control} = useForm();
  const [sending, setSending ] = useState(false)

  const handleOnSubmit = async (data) => {
    actions.theme.clearAlertMessage();
    setSending(true);
    actions.auth.authProcessAuthToken();
    let addressLine = data.shipping_tipo_via.value + " " + data.shipping_street + " " + data.shipping_numero;

    if(data.shipping_piso) {
      addressLine += data.shipping_piso
    }
    const address = {
      firstName: data.shipping_first_name,
      lastName: data.shipping_last_name,
      address1: addressLine,
      postcode: data.shipping_postcode,
      city: data.shipping_city,
      phone: data.shipping_phone,
      state: data.shipping_state?.value,
      tipoVia: data.shipping_tipo_via.value,
      street: data.shipping_street,
      numero: data.shipping_numero,
      piso: data.shipping_piso
    };

    if (libraries.auth.authIsLogged()) {
      await updateAddressEnvio(address);
      if(props.redirectAfterSubmmit === true){
        actions.router.set('/mi-cuenta/mis-direcciones');
      }
    } else {
      logOut();
    }
    if(props.onSubmitted) {
      props.onSubmitted();
    }
    setSending(false);
  };

  const updateAddressEnvio = async address => {
    const r = await libraries.mi_cuenta.addExtraAddressEnvio( address);

    if (r instanceof ApolloError) {
      actions.theme.setAlertMessage("Ha ocurrido un error al guardar los datos de envío", false, 'error');
    } else {
      actions.theme.setAlertMessage("Los cambios se han guardado correctamente", false, 'success');

    }
  };

  const logOut = async () => {
    libraries.auth.authUnregisterJwtToken();
    actions.theme.setLoginName(null);
    actions.theme.logOut();
    await actions.router.set('/');
  };

  return (
    <Form onSubmit={handleSubmit(handleOnSubmit)}>
      <AlertMessage inModal={false}/>

      <FlexGrid>
        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
          <FormControl
            id="shipping_first_name" name="shipping_first_name"
            type="text" placeholder="Nombre"
            register={register}
            errors={errors}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
          <FormControl
            id="shipping_last_name" name="shipping_last_name"
            type="text" placeholder="Apellidos"
            register={register}
            errors={errors}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 2}}>
          <FormControl
            id="shipping_tipo_via" name="shipping_tipo_via"
            type="select" placeholder="Tipo vía"
            selectOptions={tipoViaList}
            register={register}
            errors={errors}
            control={control}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
          <FormControl
            id="shipping_street" name="shipping_street"
            type="text" placeholder="Dirección"
            register={register}
            errors={errors}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 6, tablet: 6, desktop: 2}}>
          <FormControl
            id="shipping_numero" name="shipping_numero"
            type="text" placeholder="Nº"
            register={register}
            errors={errors}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 6, tablet: 6, desktop: 2}}>
          <FormControl
            id="shipping_piso" name="shipping_piso"
            type="text" placeholder="Piso"
            register={register}
            noRequired={true}
            errors={errors}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 3}}>
          <FormControl
            id="shipping_postcode" name="shipping_postcode"
            type="postcode" placeholder="Código postal"
            register={register}
            errors={errors}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 3}}>
          <FormControl
            id="shipping_city" name="shipping_city"
            type="text" placeholder="Ciudad"
            register={register}
            errors={errors}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 3}}>
          <FormControl
            id="shipping_state" name="shipping_state"
            type="state" placeholder="Provincia"
            register={register}
            menuPlacement={props.inPopUp && isMobil ? 'top' : 'bottom'}
            errors={errors}
            control={control}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 3}}>
          <FormControl
            id="shipping_phone" name="shipping_phone"
            type="text" placeholder="Teléfono"
            register={register}
            errors={errors}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 12, desktop: 12}}>
          <ButtonSubmit disabled={sending} text={ sending ? 'GUARDANDO...' : "GUARDAR" } />
        </FlexGridItem>
      </FlexGrid>
    </Form>
  )
});
