import React from 'react';
import {connect, styled} from "frontity";
import {useForm} from "react-hook-form";
import {ApolloError} from "@apollo/client";

import Form from "../../../../objects/Forms/Form";
import AlertMessage from "../../../../objects/AlertMessage/AlertMessage";
import FlexGrid from "../../../../objects/FlexGrid/FlexGrid";
import FlexGridItem from "../../../../objects/FlexGrid/FlexGridItem";
import FormControl from "../../../../objects/Forms/FormControl";
import ButtonSubmit from "../../../../objects/ButtonSubmit/ButtonSubmit";

import {findIndex} from "../../../../lib/utils/birthday";
import {findMetaData} from "../../../../lib/utils/util";
import {getListTipoVia} from "../../../../lib/checkout/addresses";
import {findIndexState, getStateList} from "../../../../lib/checkout/statelist";

export default connect(function FormDireccionEnvio({state, actions, libraries, ...props}) {
  const tipoViaList = getListTipoVia();
  const tipoViaIndex = findIndex(tipoViaList, findMetaData(tipoViaList, 'shipping_tipo_via'));
  const tipoViaIndex2 = (tipoViaIndex !== -1) ? tipoViaIndex : 0;

  const stateList = getStateList();
  const stateIndex = findIndexState(props.data.shipping?.state);
  const stateIndex2 = (stateIndex !== -1) ? stateIndex : 0;

  const {register, handleSubmit, watch, errors, control} = useForm({
    defaultValues: {
      shipping_first_name: props.data.shipping?.firstName,
      shipping_last_name: props.data.shipping?.lastName,
      shipping_postcode: props.data.shipping?.postcode,
      shipping_city: props.data.shipping?.city,
      shipping_state: stateList[stateIndex2],
      shipping_phone: findMetaData(props.data.metaData, "shipping_phone"),
      shipping_tipo_via: tipoViaList[tipoViaIndex2],
      shipping_street: findMetaData(props.data.metaData, "shipping_street"),
      shipping_numero: findMetaData(props.data.metaData, "shipping_numero"),
      shipping_piso: findMetaData(props.data.metaData, "shipping_piso"),
    }
  });

  const handleOnSubmit = async (data) => {
    actions.theme.clearAlertMessage();

    actions.auth.authProcessAuthToken();

    let addressLine = data.shipping_tipo_via.value + " " + data.shipping_street + " " + data.shipping_numero;

    if(data.shipping_piso) {
      addressLine += data.shipping_piso
    }

    const address = {
      firstName: data.shipping_first_name,
      lastName: data.shipping_last_name,
      address1: addressLine,
      postcode: data.shipping_postcode,
      city: data.shipping_city,
      phone: data.shipping_phone,
      state: data.shipping_state?.value,
      tipoVia: data.shipping_tipo_via.value,
      street: data.shipping_street,
      numero: data.shipping_numero,
      piso: data.shipping_piso
    };

    if (libraries.auth.authIsLogged()) {
      updateAddressEnvio(address);
    } else {
      logOut();
    }
  };

  const updateAddressEnvio = async address => {
    const authId = libraries.auth.authGetUserAuthId();

    const r = await libraries.mi_cuenta.updateAddressEnvio(authId, address);
    if (r instanceof ApolloError) {
      actions.theme.setAlertMessage("Ha ocurrido un error al actualizar los datos de envío", false, 'error');
    } else {
      actions.theme.setAlertMessage("Los cambios se han guardado correctamente", false, 'success');
    }
  };

  const logOut = async () => {
    libraries.auth.authUnregisterJwtToken();
    actions.theme.setLoginName(null);
    actions.theme.logOut();
    await actions.router.set('/');
  };

  return (
    <Form onSubmit={handleSubmit(handleOnSubmit)}>
      <AlertMessage inModal={false}/>

      <FlexGrid>
        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
          <FormControl
            id="shipping_first_name" name="shipping_first_name"
            type="text" placeholder="Nombre"
            register={register}
            errors={errors}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
          <FormControl
            id="shipping_last_name" name="shipping_last_name"
            type="text" placeholder="Apellidos"
            register={register}
            errors={errors}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 2}}>
          <FormControl
            id="shipping_tipo_via" name="shipping_tipo_via"
            type="select" placeholder="Tipo vía"
            selectOptions={tipoViaList} selectIndex={tipoViaIndex}
            register={register}
            errors={errors}
            control={control}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
          <FormControl
            id="shipping_street" name="shipping_street"
            type="text" placeholder="Dirección"
            register={register}
            errors={errors}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 6, tablet: 6, desktop: 2}}>
          <FormControl
            id="shipping_numero" name="shipping_numero"
            type="text" placeholder="Nº"
            register={register}
            errors={errors}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 6, tablet: 6, desktop: 2}}>
          <FormControl
            id="shipping_piso" name="shipping_piso"
            type="text" placeholder="Piso"
            register={register}
            errors={errors}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 3}}>
          <FormControl
            id="shipping_postcode" name="shipping_postcode"
            type="postcode" placeholder="Código postal"
            register={register}
            errors={errors}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 3}}>
          <FormControl
            id="shipping_city" name="shipping_city"
            type="text" placeholder="Ciudad"
            register={register}
            errors={errors}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 3}}>
          <FormControl
            id="shipping_state" name="shipping_state"
            type="state" placeholder="Provincia" selectIndex={stateIndex}
            register={register}
            errors={errors}
            control={control}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 3}}>
          <FormControl
            id="shipping_phone" name="shipping_phone"
            type="text" placeholder="Teléfono"
            register={register}
            errors={errors}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 12, desktop: 12}}>
          <ButtonSubmit text="GUARDAR"/>
        </FlexGridItem>
      </FlexGrid>
    </Form>
  )
});
