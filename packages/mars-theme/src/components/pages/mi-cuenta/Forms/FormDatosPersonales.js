import React, {useEffect, useState} from 'react';
import {connect} from "frontity";
import {useForm} from "react-hook-form";
import {ApolloError} from "@apollo/client";

import Form from "../../../../objects/Forms/Form";
import AlertMessage from "../../../../objects/AlertMessage/AlertMessage";
import FlexGrid from "../../../../objects/FlexGrid/FlexGrid";
import FlexGridItem from "../../../../objects/FlexGrid/FlexGridItem";
import FormControl from "../../../../objects/Forms/FormControl";
import ButtonSubmit from "../../../../objects/ButtonSubmit/ButtonSubmit";
import Text from "../../../../objects/Text/Text";

import {findIndex, getListDays, getListMonths, getListYears} from "../../../../lib/utils/birthday";
import {findMetaData} from "../../../../lib/utils/util";

export default connect(function FormDatosPersonales({state, actions, libraries, ...props}) {
  const user = props.formData;

  const dayList = getListDays();
  const dayIndex = findIndex(dayList, findMetaData(user.metaData, 'birthday_day'));
  const dayIndex2 = (dayIndex !== -1) ? dayIndex : 0;

  const monthList = getListMonths();
  const monthIndex = findIndex(monthList, findMetaData(user.metaData, 'birthday_month'));
  const monthIndex2 = (monthIndex !== -1) ? monthIndex : 0;

  const yearList = getListYears();
  const yearIndex = findIndex(yearList, findMetaData(user.metaData, 'birthday_year'));
  const yearIndex2 = (yearIndex !== -1) ? yearIndex : 0;

  const [submitting, setSubmitting ] = useState(false);

  const defaultVal = {
    account_first_name: user.firstName,
    account_last_name: user.lastName,
    account_email: user.email,
    account_birthday_day: dayList[dayIndex2],
    account_birthday_month: monthList[monthIndex2],
    account_birthday_year: yearList[yearIndex2]
  };

  const {register, handleSubmit, control, watch, errors, setError} = useForm({
    defaultValues: defaultVal
  });

  const handleOnSubmit = async data => {




    setSubmitting(true)
    actions.theme.clearAlertMessage();

    const user = {
      firstName: data.account_first_name,
      lastName: data.account_last_name,
      email: data.account_email,
      birthdayDay: data.account_birthday_day?.value,
      birthdayMonth: data.account_birthday_month?.value,
      birthdayYear: data.account_birthday_year?.value
    };

    actions.auth.authProcessAuthToken();

    if (libraries.auth.authIsLogged()) {
      await updateDatosPersonales(user);
      setSubmitting(false)
    } else {
      await logOut();
      setSubmitting(false)
    }
  };

  const updateDatosPersonales = async user => {
    const authId = libraries.auth.authGetUserAuthId();

    const r = await libraries.mi_cuenta.updateDatosPersonales(authId, user);
    if (r instanceof ApolloError) {
      actions.theme.setAlertMessage("Ha ocurrido un error al actualizar los datos personales", false, 'error');
    } else {
      actions.theme.setLoginName(user.firstName);
      actions.theme.setAlertMessage("Los cambios se han guardado correctamente", false, 'success');
    }
  };

  const logOut = async () => {
    libraries.auth.authUnregisterJwtToken();
    actions.theme.setLoginName(null);
    actions.theme.logOut();
    await actions.router.set('/');
  };

  useEffect(() => {
    register({ name: "account_birthday_year" }, { required: true });
  }, []);

  return (
    <Form onSubmit={handleSubmit(handleOnSubmit)}>
      <AlertMessage inModal={false}/>

      <FlexGrid>
        <FlexGridItem layout={{movil: 12, tablet: 12, desktop: 12}}>
          <Text data={{texto: "TUS DATOS", font_type: 'SubtitleSmall', font_family: 'Lexend Zetta'}}/>
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
          <FormControl
            id="account_first_name" name="account_first_name"
            type="text" placeholder="Nombre"
            register={register}
            errors={errors}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
          <FormControl
            id="account_last_name" name="account_last_name"
            type="text" placeholder="Apellidos"
            register={register}
            errors={errors}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
          <FormControl
            id="account_email" name="account_email"
            type="email" placeholder="Correo electrónico"
            register={register}
            errors={errors}
            onChange={() => {

              setError("account_birthday_year", {
                type: "manual",
                message: "Dont Forget Your Username Should Be Cool!"
              });
            }}
          />
        </FlexGridItem>

        <div/>

        <FlexGridItem layout={{movil: 12, tablet: 12, desktop: 12}}>
          <Text data={{texto: "FECHA DE NACIMIENTO", font_type: 'SubtitleSmall', font_family: 'Lexend Zetta'}}/>
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 4, desktop: 4}}>
          <FormControl
            id="account_birthday_day" name="account_birthday_day"
            type="select" placeholder="Día"
            selectOptions={dayList} selectIndex={dayIndex}
            register={register}
            errors={errors}
            control={control}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 4, desktop: 4}}>
          <FormControl
            id="account_birthday_month" name="account_birthday_month"
            type="select" placeholder="Mes"
            selectOptions={monthList} selectIndex={monthIndex}
            register={register}
            errors={errors}
            control={control}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 4, desktop: 4}}>
          <FormControl
            id="account_birthday_year" name="account_birthday_year"
            type="select" placeholder="Año"
            selectOptions={yearList} selectIndex={yearIndex}
            register={register}
            errors={errors}

            control={control}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 12, desktop: 12}}>
          <ButtonSubmit disabled={submitting} text={submitting ? 'ENVIANDO...' : "GUARDAR" }/>
        </FlexGridItem>
      </FlexGrid>
    </Form>
  )
});
