import React from 'react';
import {connect, styled} from "frontity";
import moment from 'moment';
import FrLink from "@frontity/components/link";

import FlexGrid from "../../../../objects/FlexGrid/FlexGrid";
import FlexGridItem from "../../../../objects/FlexGrid/FlexGridItem";
import Text from "../../../../objects/Text/Text";
import OrderStatus from "../../../objects/OrderStatus/OrderStatus";

export default connect(function PedidosLine({libraries, actions, ...props}) {
  return (
    <LineStyled>
      <FlexGrid>
        <FlexGridItem layout={{movil: 6, tablet: 6, desktop: 4}} direction="column">
          <TitleStyled data={{texto: 'NÚMERO DE PEDIDO:', font_type: 'SubtitleSmall', font_family: 'Lexend Zetta'}}/>
          <OrderNumberStyled data={{texto: "#" + props.data.orderNumber, font_type: 'SecondaryMedium'}}/>
        </FlexGridItem>

        <FlexGridItem layout={{movil: 6, tablet: 6, desktop: 4}} direction="column">
          <TitleStyled data={{texto: 'FECHA DE ENVÍO:', font_type: 'SubtitleSmall', font_family: 'Lexend Zetta'}}/>
          <TextStyled data={{texto: moment(props.data.date).format('DD/MM/YYYY'), font_type: 'SecondaryMedium'}}/>
        </FlexGridItem>

        <FlexGridItem layout={{movil: 6, tablet: 6, desktop: 4}} direction="column">
          <TitleStyled data={{texto: 'ESTADO:', font_type: 'SubtitleSmall', font_family: 'Lexend Zetta'}}/>
          <OrderStatus status={props.data.status}/>

          <FrNavLinkStyled link={"/mi-cuenta/ver-pedido/" + props.data.orderNumber}>
            <Text data={{texto: "VER PEDIDO", font_type: 'ButtonLarge'}}/>
          </FrNavLinkStyled>
        </FlexGridItem>
      </FlexGrid>
    </LineStyled>
  )
});

const LineStyled = styled.div`
  border-bottom: 1px solid #E5EDEF;
  padding-bottom: 16px;
`;

const TitleStyled = styled(Text)`
  margin-bottom: 8px;
`;

const OrderNumberStyled = styled(Text)`
  color: var(--color-blue-light)
`;

const TextStyled = styled(Text)`
  margin-bottom: 22px;
  opacity: 0.6;
`;

const FrNavLinkStyled = styled(FrLink)`
  text-decoration: none;
  color: var(--color-blue-light);
  
  > * {
    color: inherit !important;
  }
`;
