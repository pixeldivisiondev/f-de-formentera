import React, {useState, useEffect} from 'react';
import {connect, styled} from "frontity";
import {ApolloError} from "@apollo/client";
import Skeleton, {SkeletonTheme} from "react-loading-skeleton";
import FrLink from "@frontity/components/link";

import MiCuentaIntro from "./Intro/MiCuentaIntro";
import Text from "../../../objects/Text/Text";
import {formatAddress} from "../../../lib/checkout/addresses";
import AlertMessage from "../../../objects/AlertMessage/AlertMessage";
export default connect(function MisDirecciones({libraries, actions, state, ...props}) {
  const [addressesData, setAddressesData] = useState(null);

  useEffect(() => {
    processToken();
  }, []);

  const processToken = async () => {
    const r = await actions.auth.authProcessAuthToken();

    if (state.tokenStatus === 1 && libraries.auth.authIsLogged()) {
      findAddresses();
    } else {
      logOut();
    }
  }

  const handleDelete = async (id, index) => {
    const newAddressData = {...addressesData}
    newAddressData.shipping.extra_shipping_addresses.splice(index,1);
    setAddressesData(newAddressData);
    const r = await libraries.mi_cuenta.deleteAddressEnvio(id);
    if (r instanceof ApolloError) {
      actions.theme.setAlertMessage("Ha ocurrido un error al eliminar la dirección", false, 'error');
      findAddresses();
    } else {
      actions.theme.setAlertMessage("Direción eliminada correctamente", false, 'success');
    }
  }

  const handleDeleteBilling = async () => {
    const r = await libraries.mi_cuenta.deleteAddressFacturacion(1);
    if (r instanceof ApolloError) {
      actions.theme.setAlertMessage("Ha ocurrido un error al eliminar la dirección", false, 'error');
      findAddresses();
    } else {
      actions.theme.setAlertMessage("Direción eliminada correctamente", false, 'success');
      findAddresses()
    }

  }

  const findAddresses = async () => {
    const authId = libraries.auth.authGetUserAuthId();

    const r = await libraries.mi_cuenta.getAddresses(authId);
    if (r == null || r instanceof ApolloError) {
      logOut();
    } else {
      setAddressesData(r);
    }
  };

  const logOut = async () => {
    libraries.auth.authUnregisterJwtToken();
    actions.theme.setLoginName(null);
    actions.theme.logOut();
    await actions.router.set('/');
  };

  const hasBillingAddress = addressesData && addressesData?.billing.extra_billing_addresses
  console.log(addressesData, 'ADDRES DATA')
  const hasShippingAddress = addressesData?.shipping.firstName !== null;


  return (
    <SkeletonTheme color="#ecf5f8" highlightColor="#c3dee7">
      <MiCuentaIntro title="MIS DIRECCIONES" texto=""/>
      {addressesData === null ? (
        <>
          <Skeleton height={50} count={5}/>
        </>
      ) : (
        <>



          <TitleStyled data={{texto: "DIRECCIÓN DE ENVÍO", font_type: 'SubtitleSmall', font_family: 'Lexend Zetta'}}/>

          <div>
            {
              addressesData?.shipping.extra_shipping_addresses.map((item, key) => {
                return (
                  <div  key={key}>
                    <TextStyled>
                      {formatAddress(item, 'de facturación')}
                    </TextStyled>
                    <FrNavLinkStyled link={"/mi-cuenta/mis-direcciones/envio/editar?id="+item.key}>
                      <Text data={{texto: "EDITAR", font_type: 'ButtonLarge'}}/>
                    </FrNavLinkStyled>

                    <FrNavLinkStyled scroll={false} link={"/mi-cuenta/mis-direcciones/"} onClick={(e) => {
                      e.preventDefault();
                      handleDelete(item.key, key)
                    }}>
                      <Text data={{texto: "ELIMINAR", font_type: 'ButtonLarge'}}/>
                    </FrNavLinkStyled>


                  </div>
                )

              })
            }
            <AlertMessage inModal={false}/>
          </div>

          <FrNavLinkStyled link={"/mi-cuenta/mis-direcciones/envio/nueva/"}>
            <Text data={{texto: "AÑADIR UNA DIRECCIÓN", font_type: 'ButtonLarge'}}/>
          </FrNavLinkStyled>

          <TitleStyled data={{texto: "DIRECCIÓN DE FACTURACIÓN", font_type: 'SubtitleSmall', font_family: 'Lexend Zetta'}}/>

          {hasBillingAddress && addressesData.billing.extra_billing_addresses[addressesData?.billing.extra_billing_addresses.length - 1].firstName ?
            <TextStyled>
              {formatAddress(addressesData.billing.extra_billing_addresses[addressesData?.billing.extra_billing_addresses.length - 1], 'de facturación')}
            </TextStyled>
            : null }


          <FrNavLinkStyled link={"/mi-cuenta/mis-direcciones/facturacion/"}>
            <Text data={{texto: hasBillingAddress ? "EDITAR" : "AÑADIR", font_type: 'ButtonLarge'}}/>
          </FrNavLinkStyled>

          {hasBillingAddress ?
            <FrNavLinkStyled scroll={false} link={"/mi-cuenta/mis-direcciones/"} onClick={(e) => {
              e.preventDefault();
              handleDeleteBilling()
            }}>

            </FrNavLinkStyled>
            : null }
        </>
      )}
    </SkeletonTheme>
  )
});

const TitleStyled = styled(Text)`
  margin-bottom: 12px;
`;

const TextStyled = styled.div`
  opacity: 0.6;
  margin-bottom: 8px;
`;

const FrNavLinkStyled = styled(FrLink)`
  color: var(--color-blue-light);
  display: inline-flex;
  text-decoration: none;
  margin-bottom: 24px;
  
  &:not(:last-child) {
  margin-right: 32px;
  }
  
  > * {
    color: inherit !important;
  }
`;
