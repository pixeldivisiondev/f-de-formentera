import React, {useState, useEffect} from 'react';
import {connect} from "frontity";
import {ApolloError} from "@apollo/client";
import Skeleton, {SkeletonTheme} from "react-loading-skeleton";

import MiCuentaIntro from "./Intro/MiCuentaIntro";
import FormDireccionEditarEnvio from "./Forms/FormDireccionEditarEnvio";

export default connect(function MisDireccionesEnvio({libraries, state, actions, ...props}) {
  const [addressesData, setAddressesData] = useState(null);

  useEffect(() => {
    processToken();
  }, []);

  const processToken = async () => {
    const r = await actions.auth.authProcessAuthToken();

    if (state.tokenStatus === 1 && libraries.auth.authIsLogged()) {
      findAddresses();
    } else {
      logOut();
    }
  }

  const findAddresses = async () => {
    const authId = libraries.auth.authGetUserAuthId();

    const r = await libraries.mi_cuenta.getAddresses(authId);
    if (r == null || r instanceof ApolloError) {
      logOut();
    } else {
      setAddressesData(r);
    }
  };

  const logOut = async () => {
    libraries.auth.authUnregisterJwtToken();
    actions.theme.setLoginName(null);
    actions.theme.logOut();
    await actions.router.set('/');
  };

  const links = libraries.source.parse(state.router.link);

  const UpdatingAddress = addressesData?.shipping.extra_shipping_addresses.find(item => {
    return item.key == links.query.id
  })

  return (
    <SkeletonTheme color="#ecf5f8" highlightColor="#c3dee7">
      <MiCuentaIntro title="EDITAR DIRECCIÓN" texto=""/>
      {addressesData === null ? (
        <>
          <Skeleton height={50} count={5}/>
        </>
      ) : (
        <FormDireccionEditarEnvio data={UpdatingAddress}/>
      )}
    </SkeletonTheme>
  )
});
