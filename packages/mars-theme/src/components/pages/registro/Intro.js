import React from 'react';
import {connect, styled} from "frontity";

import Text from "../../../objects/Text/Text";

export default connect(function Intro() {
  return (
    <>
      <RegistroTitleStyled data={{texto: 'REGISTRO', font_type: 'H1', font_family: 'Lexend Zetta'}}/>
      <RegistroCopyStyled data={{texto: 'Al crear una cuenta en F de Formentera podrás realizar tus compras rapidamente, revisar el estado de tus pedidos y recibir las últimas novedades y descuentos.'}}/>
    </>
  )
});

const RegistroTitleStyled = styled(Text)`
  margin-bottom: 24px;
`;

const RegistroCopyStyled = styled(Text)`
  margin-bottom: 24px;
  opacity: 0.6;
`;