import React, {useState} from 'react';
import {useRef} from "react";
import {ApolloError} from "@apollo/client";
import {connect} from "frontity";

import {useForm} from "react-hook-form";
import Form from "../../../objects/Forms/Form";
import FormControl from "../../../objects/Forms/FormControl";
import FlexGrid from "../../../objects/FlexGrid/FlexGrid";
import FlexGridItem from "../../../objects/FlexGrid/FlexGridItem";
import AlertMessage from "../../../objects/AlertMessage/AlertMessage";
import Legal from "./Legal";
import ButtonSubmit from "../../../objects/ButtonSubmit/ButtonSubmit";

import Text from "../../../objects/Text/Text";
import LoginSocialGroup from "../login/LoginSocial/LoginSocialGroup";

export default connect(function FormRegistro({state, actions, libraries, ...props}) {
  // TODO: Eliminar los datos de prueba
  const defaultVal = {
    reg_cif: '',
    reg_empresa: '',
    reg_firstname: '',
    reg_lastname: '',
    reg_email: '',
    reg_password: '',
    reg_password_r: ''
  };

  const {register, handleSubmit, watch, errors} = useForm({
    defaultValues: defaultVal
  });

  const password = useRef({});
  password.current = watch("reg_password", "");

  const [submitting, setSubmitting] = useState(false)

  const handleOnSubmit = async data => {
    setSubmitting(true);
    const r = await libraries.register.registerUser(data.reg_firstname, data.reg_lastname, data.reg_email, data.reg_password, data.reg_cif, data.reg_empresa);
    if (r instanceof ApolloError ) {
      let errorMessage = r.message;

        if (errorMessage.includes('Only the user requesting a token can get a token issued for them')) {
          const successMessage = 'Usuario Añadido Correctamente';
          actions.theme.setAlertMessage(successMessage, false, 'success');
        }else {
          if (errorMessage.includes('Ya hay una cuenta registrada')) {
            errorMessage = 'Ya hay una cuenta registrada con tu dirección de correo electrónico.';
          }
          actions.theme.setAlertMessage(errorMessage, false, 'error');
        }
        setSubmitting(false);
    } else {
      if(!props.forEmpresa){
        libraries.auth.authRegisterJwtToken(r.authToken, r.refreshToken, r.customer.id);
        actions.theme.setLoginName(r.customer.firstName);
        setSubmitting(false);
        await actions.router.set('/mi-cuenta/');
      }

    }
  };

  const labelText = "*Soy mayor de 18 años y he leído y acepto la <a href='/politica-de-privacidad//'>política de privacidad</a>"

  return (
    <Form onSubmit={handleSubmit(handleOnSubmit)}>
      <AlertMessage inModal={false}/>

      <FlexGrid>
        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
          <FormControl
            id="reg_firstname" name="reg_firstname"
            type="text" placeholder="Nombre"
            register={register}
            errors={errors}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
          <FormControl
            id="reg_lastname" name="reg_lastname"
            type="text" placeholder="Apellidos"
            register={register}
            errors={errors}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
          <FormControl
            id="reg_email" name="reg_email"
            type="email" placeholder="Correo electrónico"
            register={register}
            errors={errors}
          />
        </FlexGridItem>
      </FlexGrid>

      <div style={{height: '20px'}}/>

      <FlexGrid>
        {!props.pageRegistro ?
          <>
            <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
              <FormControl
                id="reg_empresa" name="reg_empresa"
                type="text" placeholder="Empresa"
                register={register}
                errors={errors}
              />
            </FlexGridItem>

            <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
              <FormControl
                id="reg_cif" name="reg_cif"
                type="text" placeholder="CIF"
                register={register}
                errors={errors}
              />
            </FlexGridItem>
          </>
          : null }



        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
          <FormControl
            id="reg_password" name="reg_password"
            type="password" placeholder="Contraseña"
            extraValidate={{validate: value => value === password.current || "Las contraseñas deben coincidir"}}
            register={register}
            errors={errors}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 6, desktop: 6}}>
          <FormControl
            id="reg_password_r" name="reg_password_r"
            type="password" placeholder="Confirmar contraseña"
            register={register}
            errors={errors}
          />
        </FlexGridItem>

        <FlexGridItem layout={{movil: 12, tablet: 12, desktop: 12}}>
          <ButtonSubmit text={submitting ? 'ENVIANDO...' : "CONFIRMAR"} disabled={submitting}/>
        </FlexGridItem>
        {
          !props.forEmpresa ?
            <>
              <FlexGridItem layout={{movil: 12, tablet: 12, desktop: 12}}>
                <Text data={{texto: "REGÍSTRATE CON UNA RED SOCIAL", font_type: 'SubtitleSmall', font_family: 'Lexend Zetta'}}/>
                <LoginSocialGroup/>
              </FlexGridItem>
            </>
          : null
        }
        <FlexGridItem layout={{movil: 12, tablet: 12, desktop: 12}}>
          <FormControl
            id="privacidad" name="privacidad"
            type="checkbox" label={labelText}
            register={register}
            errors={errors}
          />
        </FlexGridItem>

      </FlexGrid>
      <Legal/>
    </Form>
  )
});
