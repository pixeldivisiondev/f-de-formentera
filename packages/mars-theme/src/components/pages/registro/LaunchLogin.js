import React, {useState} from 'react';
import {connect, styled} from "frontity";

import Text from "../../../objects/Text/Text";
import Layout from "../../../objects/Layout/Layout";
import Modal from "../../../objects/Modal/Modal";
import LoginModal from "../login/LoginModal/LoginModal";
import {CloseModal, ModalContent, ModalWrapper} from "../../objects/Modals";

export default connect(function LaunchLogin({state, actions}) {

  const [openedModal, setopenedModal] = useState(false);

  return (
    <Layout justify="flex-end">
      <Modal
        modal={<LoginModal redirect={true}/>}
        wrapper={ModalWrapper}
        ModalContent={ModalContent}
        CloseModal={CloseModal}
        onClick={() => {actions.theme.setModalOpened(true)}}
      >
        <LaunchLoginStyled data={{texto: 'YA SOY USUARIO', font_type: 'PrimarySmall'}}/>
      </Modal>
    </Layout>
  )
});

const LaunchLoginStyled = styled(Text)`
  color: var(--color-blue-light);
  text-align: right;
  margin-bottom: 8px;
  letter-spacing: 2px !important;
`;
