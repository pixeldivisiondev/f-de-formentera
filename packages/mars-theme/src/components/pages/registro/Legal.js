import React from 'react';
import {connect, styled} from "frontity";

import Text from "../../../objects/Text/Text";
import RichText from "../../../objects/RichText/RichText";

export default connect(function Legal() {
  const legal = '<p>Formentera Mediterranean Spirits SL., utiliza la información facilitada para remitir comunicaciones de interés. Puede acceder, rectificar y suprimir los datos, así como otros derechos, tal y como se explica en la información adicional disponible en nuestra política de privacidad.</p>' + '<p>Al acceder o registrarse desde tu red social, Galician Original Drinks S.A, recibe datos de carácter personal desde la plataforma elegida, los cuales utilizaremos para crear un perfil de usuario y enviarte comunicaciones personalizadas. Puede acceder, rectificar y suprimir los datos, así como otros derechos, tal y como se explica en la información adicional disponible en nuestra política de privacidad.</p>';

  return (
    <>
      <TitleStyled data={{texto: 'Información básica privacidad:', font_type: 'PrimarySmall'}}/>
      <CopyStyled data={{texto: legal, font_type: 'PrimarySmall', font_family: 'Lexend Deca'}}/>
    </>
  )
});

const TitleStyled = styled(Text)`
  margin-top: 22px;
  margin-bottom: 12px;
`;

const CopyStyled = styled(RichText)`
  opacity: 0.6;
  
  p:not(:last-child) {
    margin-bottom: 16px;
  }
`;
