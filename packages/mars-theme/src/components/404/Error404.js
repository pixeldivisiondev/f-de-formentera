import React, {useEffect} from 'react';
import {connect, styled} from "frontity";
import Layout from "../../objects/Layout/Layout";
import Text from "../../objects/Text/Text";
import Logo from "../../assets/images/logos/Logotipo.svg"
import SombraAgeGate from "../../assets/images/sombraAgeGate.png"
import Button from "../../objects/Button/Button";

const Error404 = ({state,actions, ...props}) => {

  useEffect(() => {
    actions.theme.setModalOpened(true);
  })
  return (
    <StyledAgeGate visible={true}>
        <StyledAgeGateWrapper direction={"column"}>
          <img src={Logo} />
          <Welcome data={{texto: '¡Vaya! Parece que hay un problema. ', font_type: 'H1Mobile', font_family: 'Lexend Zetta'}}/>
          <Question data={{texto: 'Lo sentimos, no hemos podido encontrar la página que buscas.' }}/>
          <ButtonWrapper>
            <ComfirmButton onClick={ async () => { await actions.router.set('/'); }} data={{tipo: 'primary', tamano: 'squared', texto: "VOLVER A INICIO"}}/>
          </ButtonWrapper>
        </StyledAgeGateWrapper>
    </StyledAgeGate>

  );
};

export default connect(Error404);

const StyledAgeGate = styled(Layout)`
  position: fixed;
  display: ${props => props.visible ? 'flex' : 'none'};
  z-index: 99999;
  background-color: #FFF;  
  width: 100%;
  height: 100%;
  left: 0;
  top: 0;
  background-image: url(${SombraAgeGate});
  background-position: center;
  background-repeat: no-repeat;  
`

const Welcome = styled(Text)`
  margin-top: 20px;
  margin-bottom: 2px;
  text-align: center;
  text-transform: uppercase;
  
`

const Question = styled(Text)`
  text-align: center;
  max-width: 250px;
  text-transform: none;
  margin-top: 25px;
`

const StyledAgeGateWrapper = styled(Layout)`
  max-width: 400px;
  background-color: #FFF;
  align-self: center;
  padding: 30px;
  margin-left: 15px;
  margin-right: 15px;
`

const ButtonWrapper = styled(Layout)`
  max-width: 400px;
  background-color: #FFF;
  align-self: center;
  padding: 40px;
`

const ComfirmButton = styled(Button)`
  max-width: 264px !important;
  min-width: unset !important;
  margin: 0 10px;
`
