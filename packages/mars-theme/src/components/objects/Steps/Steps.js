import React from 'react';
import { styled} from "frontity";
import Step from './Step'
import Layout from "../../../objects/Layout/Layout";

const Steps = ({...props}) => {
  return (

    <StyledLayout justify={"space-between"}>
        <Step { ...props} number={1} />
        <Step { ...props} number={2} />
        <Step { ...props} number={3} />
        <Step { ...props} number={4} />
    </StyledLayout>
  );
};

export default Steps;

const StyledLayout = styled(Layout)`
  position: relative;
  width: 100%;
`
