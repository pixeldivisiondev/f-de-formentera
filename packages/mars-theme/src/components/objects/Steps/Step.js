import React from 'react';
import {connect, styled} from "frontity";

const Step = ({state, ...props}) => {

  return (
    <>
      <Bullet active={state.theme.checkoutStep <= props.number - 1 }>
      </Bullet>
      {
        props.number <= 3 ?
          <Line position={props.number - 1} active={state.theme.checkoutStep <= props.number} />
          : null
      }
    </>
  );
};

export default connect(Step);

const Bullet = styled.div`
  width: 24px;
  height: 24px;
  border: 1px solid var(--color-primary-base);
  border-radius: 50%;  
  position: relative;
  z-index: 2;
  background-color: #FFF;
  
  &:before {
    content: ' ';
    position: absolute;
    left: 8px;
    top: 8px;
    width: 6px;
    height: 6px;
    background-color: ${props => props.active ? `#FFF` : `var(--color-primary-base)` } ;
    border-radius: 50%;    
  }
  
  
`

const Line = styled.span`
    content: ' ';
    z-index: 1;
    position: absolute;
    left: ${props => props.position * 33.3333333333}%;
    width: 33.3333333%;
    height: 1px;
    background-color: ${props => props.active ?  `rgba(0, 64, 134, 0.47);` : `var(--color-primary-base)` } ;    
    opacity: ${props => props.active ?  `0.47` : `1` } ;
`
