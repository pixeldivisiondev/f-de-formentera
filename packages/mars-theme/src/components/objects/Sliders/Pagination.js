import {styled, connect } from "frontity";

const Pagination = (props) => {
  StyledPagination.defaultProps = {
    breakPoints: props.state.theme.breakPoints
  }

  return <StyledPagination className={props.className}/>
}


const StyledPagination= styled.div`
  position: relative !important;
  margin: 0 10px;
  margin-top: -5px;
  display: flex;
  
  .swiper-pagination-bullet {
    margin: 0 3px;
    position: relative;
    background-color: var(--color-primary-base);
    border: 1px solid var(--color-primary-base);
    width: 5px;
    height: 5px;
    opacity: 0.2;        
    &:after {      
      
      position: absolute;
      border-radius: 50%;
      left: 3px;
      top: 3px;      
      width: 2px;
      height: 2px;  
      background-color: var(--color-primary-base);
      z-index: 3;
      ${props => {
      return `
          @media ${props.breakPoints['tablet']} {
            content: ' ';
          } 
        ` 
      }
    } 
    }
    &-active {
      border-color: var(--color-primary-base) !important;      
      opacity: 1;
    } 
    
    ${props => {
      return `
          @media ${props.breakPoints['tablet']} {
            display: flex;
            background-color: transparent;      
            border-color: transparent;      
            opacity: 1;
            width: 10px;
            height: 10px;
            &:after {
              display: block;
            }
          } 
        ` 
      }
    } 
  }
`

export  default connect(Pagination);
