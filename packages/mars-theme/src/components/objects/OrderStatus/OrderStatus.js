import {connect, styled} from "frontity";

import Text from "../../../objects/Text/Text";

const OrderStatus = props => {
  let type = 'normal';
  let text = '';

  switch (props.status.toLowerCase()) {
    case 'pending':
      text = "Pendiente";
      break;
    case 'processing':
      text = "Procesando";
      type = "warning";
      break;
    case 'on_hold':
      text = "En espera";
      break;
    case 'completed':
      text = "Completado";
      type = "success";
      break;
    case 'cancelled':
      text = "Cancelado";
      type = "error";
      break;
    case 'failed':
      text = "Fallido";
      type = "error";
      break;
    case 'refunded':
      text = "Reembolsado";
      break;
    default:
      break;
  }

  return (
    <TextStyled type={type} data={{texto: text, font_type: 'SecondaryMedium'}}/>
  )
};

export default connect(OrderStatus);

const TextStyled = styled(Text)`
  margin-bottom: 22px;
  
  ${props => {
  switch (props.type) {
    case 'warning':
      return `color: #E6A90E;`;
    case 'success':
      return `color: #23A657;`;
    case 'error':
      return `color: #E03B58;`;
    default:
      break;
  }
}}
`;