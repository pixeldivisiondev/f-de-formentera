import React from 'react';
import {connect, styled} from "frontity";

import Layout from "../../../objects/Layout/Layout";

export default connect(function Column({state, actions, ...props}) {

  StyledLayout.defaultProps = {
    theme: state.theme
  };

  return (
    <StyledLayout layout={props.layout}>
      {props.children}
    </StyledLayout>
  )
});

const StyledLayout = styled(Layout)`
${props => {
  return ` @media ${props.theme.breakPoints['tablet']}
  {
    flex: 1 1 ${props.layout.tablet};
    width: ${props.layout.tablet};
  }`
}}`;