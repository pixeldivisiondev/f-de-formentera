import { connect, styled } from "frontity";
import Layout from '../../../objects/Layout/Layout';
import Image from "@frontity/components/image";
import BgImage from '../../../objects/Images/BackgroundImage'
import Section from "../../../objects/Section/Section";
import sello from "../../../assets/images/dev/selloFormenteraNewsletter.png";
import Form from "../../modulos/Profesionales/Content/form";

const Card = ({ state, actions, ...props}) => {

    CardColumn.defaultProps = {
        theme: state.theme
    }

    CardWrapper.defaultProps = {
        theme: state.theme
    }

  const SectionNewsletterDefault = {
    section_background_color: 'secondary-base'

  }

  StyledNewsletter.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  StyledNewsletterWrapper.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  StyledNewsletterContent.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  StyledNewsletterContentRight.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  StyledNewsletterContentLeft.defaultProps = {
    breakPoints: state.theme.breakPoints
  }


    return (
      <StyledNewsletter
        justify={"flex-end"}
        wrapper={"FullWidth"}
        defaultData={SectionNewsletterDefault}
        className={props.className}>
        <StyledNewsletterWrapper >

          {
            !props.video
              ?
                <StyledNewsletterContentLeft backGround={props.backGround} />
              :
                <StyledNewsletterContentLeft>
                  <StyledVideo src={props.video.url} loop autoPlay mute />
                </StyledNewsletterContentLeft>

          }
          <StyledNewsletterContentRight direction={"column"} justify={"center"}>
            {props.children}
          </StyledNewsletterContentRight>
        </StyledNewsletterWrapper>
      </StyledNewsletter>
    )

};

export default connect(Card);


const StyledVideo = styled.video`
position: absolute;
left: 0;
top: 0;
height: 100%;
width: auto;
`

const CardWrapper = styled(Layout)`
    ${props => {
        return` @media ${props.theme.breakPoints['tablet-wide']}
        {
            flex-direction: row;
            height: 340px;
        }`
    }} 
    
`

const CardColumn = styled(Layout)`
    ${props => {
      return` @media ${props.theme.breakPoints['tablet-wide']}
        {
            width: 50%;
            height: 100%;
            flex: 1 1 50%;
        }`
    }}  
`


const StyledNewsletter = styled(Section)`
  padding-top: 50px;
  padding-bottom: 50px;
  ${props => {
  return `@media ${props.breakPoints['tablet']} {          
              width: 50%;
              padding-top: 0px;
              padding-bottom: 0px;
              margin-top: 0;
            }`
}
  }    
`


const StyledNewsletterContent = styled(Layout)`
  ${props => {
    
  return `@media ${props.breakPoints['tablet-wide']} {          
        flex: 1;       
      }`
}
  }

`

const StyledNewsletterContentLeft = styled(StyledNewsletterContent)`
  overflow: hidden;
  position: relative;
  background-image: url(${props => props.backGround});
  background-size: cover;
  display: none;
  ${props => {
  return `@media ${props.breakPoints['tablet-wide']} {
        display: flex;     
      }`
    }
  }
`

const StyledNewsletterContentRight = styled(StyledNewsletterContent)`
  width: 247px;  
  ${props => {
  return `@media ${props.breakPoints['tablet-wide']} {
        width: 341px;            
        flex: 0 0 50%;
        padding: 0 16px;     
      }`
}
  }
`

const StyledNewsletterWrapper = styled(Layout)`
  flex: 1 1 100%;  
  ${props => {
  return `@media ${props.breakPoints['tablet-wide']} {
              flex: 1 1 100%;          
              max-width: calc(100%);
              height: 341px;
            }`
}
  }    
`
