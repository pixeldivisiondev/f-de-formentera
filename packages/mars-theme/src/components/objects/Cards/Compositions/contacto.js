import { connect, styled } from "frontity";
import Layout from '../../../../objects/Layout/Layout';
import Text from '../../../../objects/Text/Text';
import Button from "../../../../objects/Button/Button";

const CompositionContacto = ({ state, actions,  ...props}) => {

    const TitleDefault = {
        color: 'primary-base',
        font_family: 'Lexend Zetta',
        font_type:  'H1',
        tipo: 'p',
        font_weight: 400,
        texto: ''
    };

    const BodyDefault = {
        color: 'primary-base',
        font_family: 'Cormorant',
        font_type:  'SecondaryMedium',
        tipo: 'p',
        font_weight: 600,
        texto: ''
    };


    const NornalButtonProps = {
        tipo: "primary",
        tamano: "small",
        texto: "contacta con nosotros",
    }


    return (
        <StyledLayout>
            <StyledText data={{texto: '¿Tienes alguna duda?' }} defaultData={TitleDefault} className={props.className}/>
            <StyledTitle data={{texto: 'Si quieres hablar con nosotros, no pierdas el tiempo y escríbenos.' }} defaultData={BodyDefault} className={props.className}/>
            <Button data={NornalButtonProps} onClick={async () => { await actions.router.set('/contacto');  window.scrollTo(0, 0); }}/>
        </StyledLayout>
    )

};

export default connect(CompositionContacto);

const StyledLayout = styled(Layout)`    
    text-align: center;
    max-width: 290px;
    padding: 16px;
`

const StyledText = styled(Text)`
  text-transform: uppercase;
`

const StyledTitle = styled(Text)`
    margin-top: 20px;
    margin-bottom: 20px;
`
