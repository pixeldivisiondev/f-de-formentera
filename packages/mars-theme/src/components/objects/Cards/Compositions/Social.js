import {styled, connect} from "frontity";
import Layout from "../../../../objects/Layout/Layout";
import Text from "../../../../objects/Text/Text";
import TwitterSrc from '../../../../assets/images/icons/Twitter.svg'
import FacebookSrc from '../../../../assets/images/icons/Facebook.svg'
import InstagramSrc from '../../../../assets/images/icons/Instagram.svg'

const Social = (props) => {

  StyledSocialLayout.defaultProps = {
    breakPoints: props.state.theme.breakPoints
  }
  return (
    <StyledSocialLayout justify={"space-between"}>
      <SocialIcons justify={"center"}>
        <SocialIcon href={"#"} target={"_blank"}>
          <img src={FacebookSrc} />
        </SocialIcon>
        <SocialIcon href={"#"} target={"_blank"}>
          <img src={TwitterSrc} />
        </SocialIcon>
        <SocialIcon href={"#"} target={"_blank"}>
          <img src={InstagramSrc} />
        </SocialIcon>
      </SocialIcons>
    </StyledSocialLayout>
  );
};

export default connect(Social);


const SocialIcons = styled(Layout)`
  width: 100%;
`

const SocialIcon = styled.a`
  margin: 0 20px;
`

const StyledSocialLayout = styled(Layout)`
   ${props => {
    return `@media ${props.breakPoints['tablet-wide']} {
        display: none;     
      }`
    }
  }
`
