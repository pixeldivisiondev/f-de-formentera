import { connect, styled } from "frontity";
import Layout from '../../../../objects/Layout/Layout';
import Text from '../../../../objects/Text/Text';
import Social from './Social'

const CompositionComunidad = ({ state, ...props}) => {

    StyledImage.defaultProps = {
        breakPoints: state.theme.breakPoints
    }

    StyledText.defaultProps = {
        breakPoints: state.theme.breakPoints
    }

    StyledTitle.defaultProps = {
        breakPoints: state.theme.breakPoints
    }

    const AnteTitleDefault = {
        color: 'primary-base',
        font_family: 'Lexend Zetta',
        font_type:  'Antetitulo',
        tipo: 'p',
        font_weight: 400,
        texto: ''
    };

    const TitleDefault = {
        color: 'primary-base',
        font_family: 'Lexend Zetta',
        font_type:  'H1',
        tipo: 'p',
        font_weight: 400,
        texto: ''
    };

    const SocialDefault = {
        color: 'primary-base',
        font_family: 'Cormorant',
        font_type:  'SecondaryMedium',
        tipo: 'p',
        font_weight: 400,
        texto: ''
    };


    return (
        <StyledLayout>
            <Text data={{texto: 'COMUNIDAD' }} defaultData={AnteTitleDefault} className={props.className}/>
            <StyledTitle data={{texto: '¡Atrévete a seguirnos!' }} defaultData={TitleDefault} className={props.className}/>
            <StyledText data={{texto: '@fdeformentera' }} defaultData={SocialDefault} className={props.className}/>
            <Social />
            <StyledImage src={props.data.imagen_mobile} />
        </StyledLayout>
    )

};

export default connect(CompositionComunidad);

const StyledLayout = styled(Layout)`
    width: 240px;
    margin: auto;
    text-align: center;
    padding: 16px;    
`

const StyledTitle = styled(Text)`
    margin-bottom: 30px;
    text-transform: uppercase;
    ${props => {
        return` @media ${props.breakPoints['tablet-wide']}
        {
            margin-top: 23px;
            margin-bottom: 63px;
        }`
      }
    }
`

const StyledImage = styled.img`
    max-width: 217px;
    margin-top: 54px;
    ${props => {
        return` @media ${props.breakPoints['tablet-wide']}
        {
            display: none;
        }`
    }}
`

const StyledText= styled(Text)`
    display: none;    
    ${props => {
    return` @media ${props.breakPoints['tablet-wide']}
        {
            display: flex;
        }`
}}
`
