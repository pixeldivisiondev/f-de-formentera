import {styled} from "frontity";

const ModalContent = (props) => {
  return (
    <StyledModalContent>
      <div>
        {props.children}
      </div>
    </StyledModalContent>
  )
};

const StyledModalContent = styled.div`
    background-color: #FFF;
    max-width : 820px;
    margin : auto;
    padding: 32px 24px;
    position: relative;
    width: 100%; 
    z-index: 10;
    
    @media only screen and (max-width: 600px) {
      padding: 16px;
      height: auto;
      max-height: calc(100vh - 32px);
      overflow: auto;
    }
`;

export default ModalContent
