import {styled} from "frontity";

const ModalWrapper = styled.div`
    position: fixed;
    width: 100%;
    height: 100%;
    left: 0;
    top: 0;
    background-color: rgba(0, 0, 0, 0.7);
    z-index: 9999;
    padding: 16px;
    display: ${props => props.visible ? 'flex' : 'none' };
`;

export default ModalWrapper;
