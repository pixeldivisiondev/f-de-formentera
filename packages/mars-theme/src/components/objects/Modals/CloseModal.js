import {styled} from "frontity";

const CloseWrapper = (props) => {
  return (
    <StyledCloseWrapper onClick={props.onClick}>
      <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <g id="Add/close">
          <path id="Path 2" d="M17.6569 17.6568L6.34316 6.34309" stroke="currentColor" strokeLinecap="round"/>
          <path id="Path 2_2" d="M17.6568 6.34309L6.34314 17.6568" stroke="currentColor" strokeLinecap="round"/>
        </g>
      </svg>
    </StyledCloseWrapper>
  )
};

const StyledCloseWrapper = styled.div`
    position: absolute;
    right: 24px;
    top: 32px;
    color: var(--color-primary-base);
    cursor: pointer;
    z-index: 99;
    
    @media only screen and (max-width: 600px) {
      right: 16px;
      top: 16px;
    }
`;
export default CloseWrapper;
