import React from "react";
import {styled} from "frontity";

import Text from "../../../objects/Text/Text";

const ButtonLink = props => {
  return (
    <span onClick={() => props.handleOnClick()}>
      <LinkStyled data={{texto: props.texto, font_type: 'LabelSmall', font_family: 'Lexend Zetta'}}/>
    </span>
  )
};

export default ButtonLink;

const LinkStyled = styled(Text)`
  color: var(--color-blue-light);
  cursor: pointer;
  letter-spacing: -1px !important;
`;