import {styled, connect} from "frontity";
import Section from "../../objects/Section/Section";
import Superior from './Superior/Superior'
import Inferior from './Inferior/Inferior'

const Footer = ({state}) => {

    const SectionDefault = {
        section_background_color: 'primary-base',
    }

    FooterWrapper.defaultProps = {
        breakPoints: state.theme.breakPoints
    }


    return (
        <FooterWrapper
            wrapper={"Default"}
            direction={"column"}
            data={SectionDefault}>
            <Superior />
            <Inferior />
        </FooterWrapper>
    )
}

export default connect(Footer)

const FooterWrapper = styled(Section)`
  
  padding-top: 46px;
  padding-bottom: 20px;  
  ${props => {   
    return `
        @media ${props.breakPoints['tablet']} {
            padding-top: 68px;
            padding-bottom: 22px;             
        }`
  }}
`

