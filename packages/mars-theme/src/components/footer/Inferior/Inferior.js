import {styled, connect} from "frontity";

import Layout from "../../../objects/Layout/Layout";
import Disclaimer from "../common/disclaimer";
import CopyRight from "../common/copyright"
import Text from "../../../objects/Text/Text";

const Inferior = ({state, ...props}) => {

    const defaultData = {
        color: "secondary-base",
        font_family: "Cormorant",
        font_type: "SecondaryMedium",
    }

    StyledLayout.defaultProps = {
        breakPoints: state.theme.breakPoints
    }



    return (
        <StyledLayout direction={"row"} justify={"space-between"} align={"center"}>
            <CopyRight />
            <Disclaimer />
            <RightSideText data={{texto: 'F de Formentera'}} defaultData={defaultData}/>
        </StyledLayout>
    )
}

export default connect(Inferior);

const StyledLayout = styled(Layout)`
  padding-top: 13px;
  border-top: 1px solid rgba(255, 255, 255, 0.2);
  display: none;
  ${props => {
    return `
        @media ${props.breakPoints['tablet']} {
            display: flex;
        }`
  }}
`

const StyledText = styled(Text)`
  opacity: 0.5;
`
const SideText = styled(StyledText)`
    flex: 0 0 200px;
    width: 200px;
`

const RightSideText = styled(SideText)`
  text-align: right;
`
