import {styled, connect} from "frontity";
import Link from '../../../objects/Link/Link'
import Layout from "../../../objects/Layout/Layout";

const Menu = ({state, ...props}) => {

    const defaultData = {
        font_color: "secondary-base",
        font_family: "Lexend Deca",
        font_type: "ButtonLarge",
        texto: "ver todos"
    }

    StyledLink.defaultProps = {
        gridSpacing: state.theme.gridSpacing,
        breakPoints: state.theme.breakPoints

    }

    StyledMenuWrapper.defaultProps = {
        gridSpacing: state.theme.gridSpacing,
        breakPoints: state.theme.breakPoints
    }

    const {items} = state.source.get('/menus/footer')

    return (
        <StyledMenuWrapper>
            {items.map( (item, index) => {
                const isCurrentPage = state.router.link === item.slug;
                const link = item.slug ? '/'+ item.slug : item.url
                return (
                    <StyledLink data={{texto:  item.title }} key={index} defaultData={defaultData} link={link}/>
                )
            })}
        </StyledMenuWrapper>
    )
}

export default connect(Menu);

const StyledMenuWrapper = styled(Layout)`  
  flex-direction: column;  
  ${props => {
    return `
        @media ${props.breakPoints['tablet']} {
            flex-direction: row;          
        }`
  }}
`

const StyledLink = styled(Link)`
  text-transform: uppercase;
  padding: 0 calc(${props => props.gridSpacing  } * 6);
  position: relative;
  margin-bottom: calc(${props => props.gridSpacing  } * 8);
  ${props => {
    return `
        @media ${props.breakPoints['tablet']} {
             margin-bottom: 0;
        }`
  }}
    
  ::after {        
    ${props => {
      return `
        @media ${props.breakPoints['tablet']} {
            content : ' ';
            width: 2px;
            height: 2px;
            overflow: hidden;
            background-color: #FFF;
            border-radius: 50%;
            position: absolute;
            right: 0;
            top: 11px;                 
        }`
    }}
  }
  
  :last-of-type::after {
    display: none;
  }
`
