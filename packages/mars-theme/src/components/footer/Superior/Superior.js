import {styled, connect} from "frontity";

import Layout from "../../../objects/Layout/Layout";
import LogoWhite from '../../../assets/images/logos/LogoWhite.svg'
import Menu from './Menu'
import Submenu from './SubMenu'
import Atencion from './Atencion'
import Social from './Social'
import Copyright from '../common/copyright'
import Disclaimer from "../common/disclaimer";

const Superior = ({state}) => {

    Logo.defaultProps = {
        breakPoints: state.theme.breakPoints
    }

    StyledDisclaimer.defaultProps = {
        breakPoints: state.theme.breakPoints
    }

    StyledCopyright.defaultProps = {
        breakPoints: state.theme.breakPoints
    }

    return (
        <Layout direction={"column"}>
            <Logo src={LogoWhite} />
            <Menu />
            <Social />
            <Submenu />
            <StyledDisclaimer />
            <Atencion />
            <StyledCopyright />
        </Layout>
    )
}

export default connect(Superior);

const Logo = styled.img`
    margin-bottom: 46px;    
`

const StyledDisclaimer = styled(Disclaimer)`
    max-width: 240px;
    margin-bottom: 32px;
    ${props => {
      return `
        @media ${props.breakPoints['tablet']} {
            display: none;
        }`
    }}    
`

const StyledCopyright = styled(Copyright)`   
    ${props => {
    return `
        @media ${props.breakPoints['tablet']} {
            display: none !important;
        }`
}}
    
`
