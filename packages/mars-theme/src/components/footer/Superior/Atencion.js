import {styled, connect} from "frontity";

import Layout from "../../../objects/Layout/Layout";
import Text from "../../../objects/Text/Text";
import Link from "../../../objects/Link/Link";

const Atencion = ({state}) => {

    const defaultData = {
        font_color: "secondary-base",
        font_family: "Cormorant",
        font_type: "SecondarySmall",
    }

    const texto =  state.theme.optionsACF.footer.texto_de_atencion_al_cliente
    const email = state.theme.optionsACF.footer.email_de_atencion_al_cliente

    return (
        <StyledLayout>
            <StyledText data={{color: "secondary-base", texto: texto, font_type: "SecondarySmall", font_family: 'Cormorant'}}/>
            <StyledLink data={{texto: email }}  defaultData={defaultData} link={"mailto:"+email}/>
        </StyledLayout>
    )
}

export default connect(Atencion)

const StyledLayout = styled(Layout)`
  margin-bottom: 42px;
`
const StyledText = styled(Text)`
  opacity: 0.6;
`

const StyledLink = styled(Link)`
  margin-left: 5px;
`

