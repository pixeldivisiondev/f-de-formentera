import {styled, connect} from "frontity";
import Link from '../../../objects/Link/Link'
import Layout from "../../../objects/Layout/Layout";

const SubMenu = ({state, ...props}) => {

    const defaultData = {
        font_color: "secondary-base",
        font_family: "Cormorant",
        font_type: "SecondaryMedium",
        texto: "ver todos"
    }

    StyledLink.defaultProps = {
        gridSpacing: state.theme.gridSpacing,
        breakPoints: state.theme.breakPoints
    }

    StyledMenuWrapper.defaultProps = {
        gridSpacing: state.theme.gridSpacing,
        breakPoints: state.theme.breakPoints
    }

    const {items} = state.source.get('/menus/SubFooter')

    return (
        <StyledMenuWrapper>
            {items.map( (item, index) => {
                const isCurrentPage = state.router.link === item.slug;
                const link = item.slug ? '/'+ item.slug : item.url
                return (
                    <StyledLink data={{texto:  item.title }} key={index} defaultData={defaultData} link={link}/>
                )
            })}
        </StyledMenuWrapper>
    )
}

export default connect(SubMenu);

const StyledMenuWrapper = styled(Layout)`  
  flex-direction: column;
  margin-bottom: 22px;  
  ${props => {    
    return `
        @media ${props.breakPoints['tablet']} {
            flex-direction: row;  
            margin-top: 42px;
            margin-bottom: 49px;        
        }`
  }}
`

const StyledLink = styled(Link)`  
  padding: 0 calc(${props => props.gridSpacing  } * 3);
  opacity: 0.4;
  margin-bottom: 22px;     
  ${props => {
    return `
        @media ${props.breakPoints['tablet']} {
            flex-direction: row;
            margin-bottom: 0; 
           
        }`
  }}
`
