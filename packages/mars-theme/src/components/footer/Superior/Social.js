import { connect, styled } from "frontity";

import Facebook from '../../../assets/images/icons/FacebookNegative.svg'
import Instagram from '../../../assets/images/icons/InstagramNegative.svg'
import Layout from "../../../objects/Layout/Layout";
import Text from "../../../objects/Text/Text";


const Social = ({state, ...props}) => {

    StyledLayout.defaultProps = {
        breakPoints: state.theme.breakPoints
    }
    StyledText.defaultProps = {
        breakPoints: state.theme.breakPoints
    }
    StyledIcon.defaultProps = {
        breakPoints: state.theme.breakPoints
    }

    const TextData = {
        color: "secondary-base",
        font_family: "Laxend Deca",
        font_type: "ButtomSmall",
    }

    TextData.texto = state.theme.optionsACF.footer.texto_siguenos;
    const FbUrl =  state.theme.optionsACF.footer.enlace_facebook
    const InstagramUrl = state.theme.optionsACF.footer.enlace_instagram


    return (
        <StyledLayout>
            <StyledText data={TextData}/>
            <StyledIcon href={FbUrl} target={"_blank"}>
                <img src={Facebook} />
            </StyledIcon>
            <StyledIcon href={InstagramUrl} target={"_blank"}>
                <img src={Instagram} />
            </StyledIcon>
        </StyledLayout>
    )
}

export default connect(Social);

const StyledLayout = styled(Layout)`
  margin-top: 13px;
  margin-bottom: 45px;
  ${props => {
    return `
        @media ${props.breakPoints['tablet']} {
            position: absolute;
            top: 0;
            right: calc(16px * 4);
            width: auto;
        }`
  }}
`

const StyledText = styled(Text)`  
  display: none;
  margin-right: 7px;
  ${props => {
    return `
        @media ${props.breakPoints['tablet']} {
            display: block;
        }`
  }}
`

const StyledIcon = styled.a`
  margin: 0 12px;
  ${props => {
    return `
        @media ${props.breakPoints['tablet']} {
            margin-right: 0;            
            margin-left: 12px;
        }`
  }}`
