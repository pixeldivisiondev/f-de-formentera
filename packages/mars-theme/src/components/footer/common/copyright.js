import Text from "../../../objects/Text/Text";
import {styled, connect} from "frontity";

const Copyright = ({state, ...props}) => {

    const defaultData = {
        color: "secondary-base",
        font_family: "Cormorant",
        font_type: "SecondaryMedium",
    }

    const texto =  state.theme.optionsACF.footer.texto_copyright

    return (
        <StyledText className={props.className} data={{texto: texto}} defaultData={defaultData}/>
    )
}

export default connect(Copyright)

const StyledText = styled(Text)`
  opacity: 0.5;  
`
