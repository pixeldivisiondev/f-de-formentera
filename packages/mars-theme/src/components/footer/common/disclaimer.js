import Text from "../../../objects/Text/Text";
import {styled, connect} from "frontity";

const Disclaimer = ({state, ...props}) => {

    const defaultData = {
        color: "secondary-base",
        font_family: "Cormorant",
        font_type: "SecondarySmall",
    }

    const texto =  state.theme.optionsACF.footer.aviso_consumo_responsable

    return (
        <StyledText className={props.className} data={{texto: texto}} defaultData={defaultData}/>
    )
}

export default connect(Disclaimer)

const StyledText = styled(Text)`
  opacity: 0.5;
  text-align: center;
`
