import React, {useEffect} from "react";
import {connect, loadable, styled} from "frontity";


const DefaultTemplate = loadable(() => import('../templates/defaultTemplate'));

const Home = ({state, actions, libraries}) => {
  const data = state.source.get(state.router.link);
  const post = state.source[data.type][data.id];

  return data.isReady ? (
      <Container>
        <Content>
          <DefaultTemplate forEmpresa={true}/>
        </Content>
      </Container>
  ) : null;
};

export default connect(Home);

const Container = styled.div`
  width: 100%;
  margin: 0;
  padding: 0;
`;

/**
 * This component is the parent of the `content.rendered` HTML. We can use nested
 * selectors to style that HTML.
 */
const Content = styled.div`
 `;
