import React from 'react';
import {connect, styled} from "frontity";
import Layout from "../../objects/Layout/Layout";
import Text from "../../objects/Text/Text";
import Logo from "../../assets/images/logos/Logotipo.svg"
import SombraAgeGate from "../../assets/images/sombraAgeGate.png"
import Button from "../../objects/Button/Button";

const AgeGate = ({state, ...props}) => {
  return (
    <StyledAgeGate visible={props.visible}>
        <StyledAgeGateWrapper direction={"column"}>
          <img src={Logo} />
          <Welcome data={{texto: 'BIENVENIDO', font_type: 'ButtomSmall'}}/>
          <Question data={{texto: '¿ERES MAYOR DE EDAD?', font_type: 'H1', font_family: 'Lexend Zetta'}}/>
          <ButtonWrapper>
            <ComfirmButton onClick={() => {props.onAccept()}} data={{tipo: 'outlined', tamano: 'squared', texto: "SI"}}/>
            <ComfirmButton onClick={() => {props.onCancel()}} data={{tipo: 'outlined', tamano: 'squared', texto: "NO"}}/>
          </ButtonWrapper>
          <Text data={{texto: 'F de Formentera recomienda el consumo responsable.', font_family: 'Cormorant', font_type: 'SecondarySmall'}}/>
        </StyledAgeGateWrapper>
    </StyledAgeGate>

  );
};

export default connect(AgeGate);

const StyledAgeGate = styled(Layout)`
  
  position: fixed;
  display: ${props => props.visible ? 'flex' : 'none'};
  z-index: 99999;
  background-color: #FFF;  
  width: 100%;
  height: 100%;
  left: 0;
  top: 0;
  background-image: url(${SombraAgeGate});
  background-position: center;
  background-repeat: no-repeat;
`

const Welcome = styled(Text)`
  margin-top: 20px;
  margin-bottom: 2px;
  
`

const Question = styled(Text)`
  text-align: center;
  max-width: 250px;
`

const StyledAgeGateWrapper = styled(Layout)`
  max-width: 400px;
  background-color: #FFF;
  align-self: center;
  padding: 40px;
  box-shadow: 0px 0px 20px #ddd;
  margin: 0 32px;
`

const ButtonWrapper = styled(Layout)`
  max-width: 400px;
  background-color: #FFF;
  align-self: center;
  padding: 40px 0;
`

const ComfirmButton = styled(Button)`
  width: 85px !important;
  min-width: unset !important;
  margin: 0 10px;
`
