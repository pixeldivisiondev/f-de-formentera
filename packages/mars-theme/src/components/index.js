import React, {useEffect, useState} from "react";
import {Global, css, connect, styled, Head, loadable} from "frontity";
import FlexBox from '../objects/FlexBox/FlexBox';
import Cookies from 'universal-cookie';
import AnimateCSS from 'animate.css/animate.css';
import LexendDeca from "../assets/fonts/LexendDeca-Regular.ttf";
import LexendZetta from "../assets/fonts/LexendZetta-Regular.ttf";
import Cormorant from  "../assets/fonts/Cormorant-Regular.ttf";
import { ParallaxProvider } from 'react-scroll-parallax';

import CookieBot from 'react-cookiebot';

const Post = loadable(() => import( "./post"));
const Loading = loadable(() => import( "./loading"));
const Title = loadable(() => import( "./title"));
const Error404 = loadable(() => import( './404/Error404'));
const Home = loadable(() => import( "./home"));
const VerPedidoTemplate = loadable(() => import( "../templates/VerPedidoTemplate"));
const AgeGate = loadable(() => import( "./AgeGate/AgeGate"));
const Footer = loadable(() => import( "./footer/Footer"));
const Header = loadable(() => import( "./Header/header"));
const Switch  = loadable(() => import(  "@frontity/components/switch"));
const Maintenance = loadable(() => import( "./Maintenance/Maintenance"));
/**
 * Theme is the root React component of our theme. The one we will export
 * in roots.
 */
const Theme = ({ state, actions, libraries }) => {
  const cookies = new Cookies();
  const data = state.source.get(state.router.link);
  const domainGroupId = '2560e7e9-aa84-4037-865e-26e00e492b2c';
  const [verifiedAge, setVerifiedAge] = useState('undefined');
  const [verifiedAge2, setVerifiedAge2] = useState(true);

  console.log(actions, 'actions')
  useEffect(() => {
    console.log(cookies.get('ageVerification'), 'verification')
    setVerifiedAge(cookies.get('ageVerification') ? cookies.get('ageVerification') : false)

  });

  Main.defaultProps = {
    breakPoints: state.theme.breakPoints
  };

  let colorsCssProperties = '';
  Object.keys(state.theme.palette).forEach(palette => {
    Object.keys(state.theme.palette[palette]).forEach(colorName => {
      colorsCssProperties += '--color-' + palette + '-' + colorName + ': ' +state.theme.palette[palette][colorName].color + ';';
    });
  });

  console.log('index', verifiedAge2);

  return (
    <>
      {/* Add some metatags to the <head> of the HTML. */}
      <Title/>
      <Head>
        <meta name="description" content={state.frontity.description} />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <html lang="es" />
      </Head>

      {/* Add some global styles for the whole site, like body or a's.
      Not classes here because we use CSS-in-JS. Only global HTML tags. */}
      <Global styles={globalStyles} />
      <Global styles={css`:root{${colorsCssProperties}} `}/>
      <Global styles={AnimateCSS} />
      <Global
        styles={css`
        body {
          overflow: ${state.theme.modalOpened || !verifiedAge ? 'hidden' : 'auto'};          
        }
        .CybotCookiebotDialogBodyButton {
          background-color: #FFF !important;
          border: 1px solid #FFF !important;
          color: var(--color-primary-base) !important;
          white-space: break-spaces !important;
          height: 32px !important;
          display: flex !important;
          align-items: center !important;
          float: left !important;
          text-align: center !important;
          justify-content: center !important;                    
        }
        
        @media (max-width: 520px) {
          .CybotCookiebotDialogBodyButton {
            width: 95px !important;             
            font-size: 10px !important;    
          }
        }
        
        #CybotCookiebotDialogBodyLevelButtonLevelOptinDeclineAll {
          width: 115px !important;
        }
        
        #CybotCookiebotDialogBodyLevelButtonLevelOptinAllowallSelectionWrapper {
          float: left !important;
        }
        
        @media (min-width: 520px) {
          #CybotCookiebotDialogBodyLevelButtonLevelOptinAllowallSelectionWrapper {
            padding-left: 67px !important;
          }
        }
        
        #CybotCookiebotDialogBodyLevelButtonsTable {
          width: 100% !important;
        }
        
        #CybotCookiebotDialogPoweredbyLink {
          display: none !important;
        }
        
        #CybotCookiebotDialog {
          background-color: var(--color-primary-base) !important;
        }
        
        
        #CybotCookiebotDialogBodyContentTitle,  #CybotCookiebotDialogBodyContentText  {
          color: #FFF !important;
        }
        
        .CybotCookiebotDialogBodyLevelButtonWrapper label {
            color:#FFF !important;;
        }
        
        .CybotCookiebotDialogDetail {
          background-color: var(--color-primary-base) !important;
        }
        #CybotCookiebotDialogBodyLevelDetailsWrapper {
          background-color: var(--color-primary-base) !important;
          
        }
        #CybotCookiebotDialogBodyLevelDetailsWrapper a {
          color: #FFF !important;
        }
      `}
      />
      <Global
        styles={css` 
        @font-face {
          font-family: "Lexend Deca";
          font-style: normal;
          font-weight: normal;
          font-display: swap;
          src: url("${LexendDeca}") format("truetype");
         
        }
        
        @font-face {
          font-family: "Lexend Zetta";
          font-style: normal;
          font-weight: normal;
          font-display: swap;
          src: url("${LexendZetta}") format("truetype");
        }
        
        @font-face {
          font-family: "Cormorant";
          font-style: normal;
          font-weight: normal;
          font-display: swap;
          src: url("${Cormorant}") format("truetype");
        }
    `}
      />

      
      {!data.isError && verifiedAge === 'true' ?
        <Header logged={state.tokenStatus} withModal={state.theme.modalOpened || !verifiedAge} />
        : null }
      {/* Add the main section. It renders a different component depending
      on the type of URL we are in. */}
      <Main withModal={state.theme.modalOpened || !verifiedAge}>

        {
        /*<Maintenance
          visible={!verifiedAge === true}
          onAccept={() => {cookies.set('ageVerification', true, { path: '/' }); setVerifiedAge(true )}}
          onCancel={() => { window.location.href = "https://www.google.es/?gws_rd=ssl";}}></Maintenance>*/
}
        {
          verifiedAge === false
            ? <AgeGate
              visible={!verifiedAge === true}
              onAccept={() => {cookies.set('ageVerification', true, { path: '/' }); setVerifiedAge(true )}}
              onCancel={() => { window.location.href = "https://www.google.es/?gws_rd=ssl";}}></AgeGate>
            :
            verifiedAge === 'true' ?
              <ParallaxProvider>
              <Switch>
                <Loading when={data.isFetching}/>
                <VerPedidoTemplate when={state.router.link.indexOf('/mi-cuenta/ver-pedido/') >= 0}/>
                <Home when={data.isHome}/>
                <Post when={data.isPostType}/>
                <Error404 when={data.isError}/>
              </Switch>
              </ParallaxProvider>
              : null

        }
        <script defer src={"https://www.paypal.com/sdk/js?client-id=AfexgV2o4V3CPsq3FLL-BJWGgvp1kR-UMR4TcnSaFx4xCAbWWNzvImTyQIOSJhw6q87-45rnT-vhGTye&currency=EUR&disable-funding=credit,card,sofort"}/>
        <CookieBot domainGroupId={domainGroupId} />
      </Main>

      {!data.isError && verifiedAge === 'true' ?
        <Footer />
        : null }


    </>
  );
};

export default connect(Theme);


const globalStyles = css`  
  body {
    margin: 0;
  }
  * {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
    outline: none !important;
  }
`;


const Main = styled(FlexBox)`
  padding-top: 69px;
  align-items: flex-start;
  min-height: calc(100vh - 300px);
  z-index: ${props => props.withModal ? '9' : '1'};
  overflow: ${props => props.withModal ? 'hidden' : 'auto'};
  position: relative;
  ${props => {
  return `
        @media ${props.breakPoints['tablet']} {
          padding-top: 103px;
        }
        @media ${props.breakPoints['desktop']} {
          
        } 
      `
}
  }
`;
