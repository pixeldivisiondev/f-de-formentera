import { loadable } from "frontity";

const Hero = loadable(() => import('./Hero/hero'));
const Origen = loadable(() => import('./Origen/origen'));
const Ginebra = loadable(() => import('./Ginebra/Ginebra'));
const Cocteles = loadable(() => import('./Cocteles/cocteles'));
const Tienda = loadable(() => import('./Tienda/tienda'));
const Profesionales = loadable(() => import('./Profesionales/profesionales'))

const Modulos = (props) => {
    const components = {
        hero: Hero,
        origen: Origen,
        ginebra: Ginebra,
        cocteles: Cocteles,
        tienda: Tienda,
        profesionales: Profesionales
    };
    const TagName = components[props.tag || 'div'];
    return <TagName {...props} />

}
export default Modulos;

