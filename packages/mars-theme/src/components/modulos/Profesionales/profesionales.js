import React, {useState} from 'react';

import {styled, connect} from "frontity";
import LazyLoad from '@frontity/lazyload'
import Content from "./Content/content"
import Newsletter from "./Content/newsletter"
import ScrollAnimation from 'react-animate-on-scroll';

const Profesionales = (props) => {

  const [height, setHeight] = useState(300)
  return (
    <>

      {!props.forEmpresa
        ?
        <LazyLoad offset={250} height={height} onContentVisible={(data) =>setHeight('auto')}>
          <ScrollAnimation animateOnce={true} animateIn={"animate__fadeIn"} >
            <Content data={props.data} />
          </ScrollAnimation>
        </LazyLoad>
        : null
      }
      <LazyLoad offset={250} height={height} onContentVisible={(data) =>setHeight('auto')}>
        <ScrollAnimation animateOnce={true} animateIn={"animate__fadeIn"} >
          <Newsletter data={props.data} forEmpresa={props.forEmpresa}/>
        </ScrollAnimation>
      </LazyLoad>

    </>
  )
}

export default connect(Profesionales);

