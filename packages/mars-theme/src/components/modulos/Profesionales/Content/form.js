import React, {useEffect, useState} from 'react';
import {styled, connect} from "frontity";
import {useForm} from "react-hook-form";
import Form from "../../../../objects/Forms/Form";
import {ApolloError} from "@apollo/client";
import FormControl from "../../../../objects/Forms/FormControl";
import OkIcon from '../../../../assets/images/icons/ok.svg'
import ButtonSubmit from "../../../../objects/ButtonSubmit/ButtonSubmit";
import Layout from "../../../../objects/Layout/Layout";
import SendImage from './message'
import Text from "../../../../objects/Text/Text";

const FormAdd = ({state, actions, libraries, ...props}) => {


  const [form, setForm] = useState(false);
  const [adding, setAdding] = useState(null);
  const [submitted, setSubmitted] = useState(null)

  useEffect(() => {
    getForm();
  }, []);

  const getForm = async () => {
    const r = await actions.auth.authProcessAuthToken();
    const f = await libraries.forms.getForm(2);
    setForm(f);
  }

  const defaultVal = {
    quantity: 1
  };

  StyledFormLayout.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  StyledInput.defaultProps = {
    breakPoints: state.theme.breakPoints
  }





  const {register, handleSubmit, errors, setValue, getValues } = useForm({
    defaultValues: defaultVal
  })


  const handleOnSubmit = async data => {

    setAdding(true);

    const d = [];
    Object.keys(data).map((field, key) => {
      const exploded  = field.split('_');
      d.push({id: parseInt(exploded[1]), value: data[field]})
    })

    const p = await actions.auth.authProcessAuthToken();
    const r = await libraries.forms.submitForm(d, 2)

    if (r instanceof ApolloError) {
      let errorMessage = r.message;
    }else{
      setSubmitted(true)
      //state.cart = r.data.addToCart.cart
    }

    setAdding(false);
  };

  return (
    <StyledFormLayout justify={"center"} align={"center"}>

      {
        submitted ?
          <Submitted >
            <StyledSubmittedIcon src={OkIcon} />
            <Text data={{texto: 'Gracias por unirte a F de Formentera', font_type: 'PrimarySmall'}}/>
          </Submitted>
          :
          <StyledForm onSubmit={handleSubmit(handleOnSubmit)}>

            <Layout>
              {
                form ?
                  form?.data?.form?.fields?.nodes?.map((field, key) => {
                    return <StyledInput
                      key={key}
                      name={"email_"+field.fieldId}
                      type="email" placeholder="Introduce tu correo"
                      register={register}
                      errors={errors}
                    />
                  })
                  :
                  null
              }
            </Layout>
            <ButtonWrapper >
              <StyledButtonSubmit disabled={adding} size="small" text={adding ? 'Enviando...' : props.submitText} />
              <SendImage />
            </ButtonWrapper>

          </StyledForm>


      }
    </StyledFormLayout>
  );
};

export default connect(FormAdd);

const Submitted = styled(Layout)`
  border: 1px solid var(--color-primary-base);
  padding: 12px 0;
  margin-top: 32px;
`

const StyledSubmittedIcon = styled.img`
  margin-right: 5px;
`
const ButtonWrapper = styled(Layout)`
  position: relative;
  margin-top: 23px;
  width: 170px;
  max-width: 170px;    
  svg {
    position: absolute;
    right: 11px;
    top: 6px;
    z-index: 123;    
  }
  &:hover {
    svg {
      path {
        stroke: var(--color-primary-base);
      }
    }
  }
`
const StyledFormLayout = styled(Layout)`
  justify-content: center; 
  
`

const StyledForm = styled(Form)`   
   margin-top: 15px;
   justify-content: center;
`

const StyledInput = styled(FormControl)` 
   text-align: center;   
   background-color: transparent;
   border: none;
   border-bottom: 1px solid var(--color-primary-base);   
   font-size: 10px;
   font-family: 'Lexend Zetta';
   text-transform: uppercase;
   padding-left: 0;
   padding-right: 0;
   &::placeholder {
    text-transform: uppercase; 
   }
   &:focus {
    background-color: transparent;
   }
   ${props => {
      return `@media ${props.breakPoints['tablet-wide']} {          
                font-size: 10px;
            }`
    }
  }    
`

const StyledButtonSubmit = styled(ButtonSubmit)`   
    width: 170px;
    max-width: 170px;    
    min-width: unset;
   
`
