import React from 'react';
import Button from "../../../../objects/Button/Button";
import {styled, connect} from "frontity";
import Text from "../../../../objects/Text/Text";
import Section from "../../../../objects/Section/Section";
import Layout from "../../../../objects/Layout/Layout";
import RichText from "../../../../objects/RichText/RichText";
import ScrollAnimation from 'react-animate-on-scroll';

const Content = ({actions, ...props}) => {

  const SectionDefault = {
    section_background_color: 'secondary-base',
  }

  StyledProfesionalesSection.defaultProps = {
    breakPoints: props.state.theme.breakPoints
  }

  StyledProfesionalesWrapper.defaultProps = {
    breakPoints: props.state.theme.breakPoints
  }

  StyledMobileImage.defaultProps = {
    breakPoints: props.state.theme.breakPoints
  }


  return (
    <StyledProfesionalesSection
      src={props.data.contenido.imagenes.imagen_desktop.url}
      defaultData={SectionDefault}
      wrapper={"FullWidthMobile"}
      justify={"flex-start"}
      direction={"row"}>

      <StyledMobileImage src={props.data.contenido.imagenes.imagen_mobile.url} tabletSrc={props.data.contenido.imagenes.imagen_tablet.url}/>

        <StyledProfesionalesWrapper
          direction={"column"} align={"center"}>
          <ScrollAnimation animateOnce={true} animateIn={"animate__fadeIn"}  delay={100}>
          <UpperText
            data={props.data.contenido.antetitulo.titulo_component}
            defaultData={{font_type: 'Antetitulo'}} />
          <ProfesionalesTitle
            data={props.data.contenido.titulo.titulo_component}
            defaultData={{font_type: 'H1'}} />
          <ProfesionalesText
            data={props.data.contenido.texto.component_text}
            defaultData={{font_type: 'SecondaryMedium', font_family: 'Cormorant'}} />
          <Button
            data={{tipo: 'primary', tamano: 'small', texto: props.data.contenido.boton.button_component.texto}} onClick={async () => { await actions.router.set(props.data.contenido.boton.button_component.enlace.url);  window.scrollTo(0, 0); }}/>
          </ScrollAnimation>
        </StyledProfesionalesWrapper>
    </StyledProfesionalesSection>
  );
};

export default connect(Content);

const UpperText = styled(Text)`
  text-transform: uppercase;
  text-align: center;
`


const ProfesionalesTitle = styled(UpperText)`
  margin-top: 11px;
  margin-bottom: 18px;
  max-width: 290px;
`

const ProfesionalesText = styled(RichText)`
  max-width: 247px;
  margin-bottom: 26px;
`


const StyledProfesionalesSection = styled(Section)`
  ${props => {
    return `@media ${props.breakPoints['tablet-wide']} {          
              background-image:  url(${props.src});
              background-size: auto;
              background-position: 50vw calc(50% - 50px);
              background-repeat: no-repeat;     
            }`
    }
  }    
`


const StyledMobileImage = styled(Layout)`
  background-image: url(${props => props.src});
  height: 400px;
  width: 100%;
  background-repeat: no-repeat;
  background-position: center top;
  background-size: 520px auto;
  ${props => {   
    return`@media (min-width: 520px) {          
        background-image: url(${props.tabletSrc});
        background-size: 1024px auto;
        height: 565px;     
    }`    
  }}
  ${props => {
    return`@media ${props.breakPoints['tablet-wide']} {          
        display: none;     
    }`
   
  }}
`

const StyledProfesionalesWrapper = styled(Layout)`
  margin-top: -50px;   
  padding-bottom: 55px;
  ${props => {
    return `@media ${props.breakPoints['tablet-wide']} {          
      flex: 0 0 50%;
      padding-top: 147px;
      padding-bottom: calc(150px + 341px);   
    }`
  }}
`
