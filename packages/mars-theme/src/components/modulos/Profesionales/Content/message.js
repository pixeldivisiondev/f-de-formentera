import React from 'react';

const MessageIcon = () => {
  return (
    <svg width="16" height="17" viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg" >
      <path fillRule="evenodd" clipRule="evenodd" d="M13.1061 7.91231C13.847 8.27979 13.847 9.33652 13.1061 9.704L3.44437 14.4965C2.77966 14.8263 2 14.3427 2 13.6007V10.1405L8 8.80716L2 7.33674L2 4.01562C2 3.27363 2.77966 2.79006 3.44437 3.11978L13.1061 7.91231Z" stroke="white" strokeLinejoin="round"/>
    </svg>
  );
};

export default MessageIcon;
