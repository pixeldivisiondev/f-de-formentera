import sello from "../../../../assets/images/dev/selloFormenteraNewsletter.png";
import {styled, connect} from "frontity";
import Section from "../../../../objects/Section/Section";
import Layout from "../../../../objects/Layout/Layout";
import Text from "../../../../objects/Text/Text";
import Image from "@frontity/components/image";
import Form from "./form"

const Newsletter = ({libraries, ...props}) => {


  const SectionNewsletterDefault = {
    section_background_color: 'primary-light',
  }

  StyledNewsletter.defaultProps = {
    breakPoints: props.state.theme.breakPoints
  }

  StyledNewsletterWrapper.defaultProps = {
    breakPoints: props.state.theme.breakPoints
  }

  StyledNewsletterContent.defaultProps = {
    breakPoints: props.state.theme.breakPoints
  }

  StyledNewsletterContentRight.defaultProps = {
    breakPoints: props.state.theme.breakPoints
  }

  StyledSello.defaultProps = {
    breakPoints: props.state.theme.breakPoints
  }

  Antetitulo.defaultProps = {
    breakPoints: props.state.theme.breakPoints
  }

  const data = props.state.theme.optionsACF.newsletter;

  return (
    <StyledNewsletter
      justify={"flex-end"}
      wrapper={"DefaultNoPadding"}
      defaultData={SectionNewsletterDefault}
      forEmpresa={props.forEmpresa}>
      <StyledNewsletterWrapper >

        <StyledNewsletterContent>
          <StyledSello src={sello} />
        </StyledNewsletterContent>

        <StyledNewsletterContentRight direction={"column"} justify={"center"}>
          <StyledLayout direction={"column"} justify={"center"}>
            <Antetitulo data={{texto: data.antetitulo }} defaultData={{font_type: 'Antetitulo'}} />
            <UpperText data={{texto: data.titulo}} defaultData={{font_type: 'SubtitleLarge', font_family: 'Lexend Zetta'}} />
            <Form submitText={data.texto_boton}/>
          </StyledLayout>
        </StyledNewsletterContentRight>

      </StyledNewsletterWrapper>
    </StyledNewsletter>
  );
};

export default connect(Newsletter);

const StyledLayout = styled(Layout)`
  max-width: 270px;
  margin: auto;
`

const StyledNewsletter = styled(Section)`
  > div {
    padding: 0;
  }
  padding-top: 50px;
  padding-bottom: 50px;
  
  
  
  ${props => {
    return `@media ${props.breakPoints['tablet-wide']} {         
              width: 50%;
              padding-top: 0px;
              padding-bottom: 0px;
              margin-top: ${props.forEmpresa ? `0px` : `-341px`};
              div > {
                max-width: 616px;
              }
            }`
    }
  }    
`

const StyledSello = styled(Image)`
  margin-left: 15px;
  width: 87px;
  ${props => {
    return `@media ${props.breakPoints['tablet-wide']} {          
              width: auto;              
            }`
    }
  }    
`

const StyledNewsletterContent = styled(Layout)`
  ${props => {
    return `@media ${props.breakPoints['tablet-wide']} {          
        flex: 1;       
      }`
    }
  }

`

const StyledNewsletterContentRight = styled(StyledNewsletterContent)`
  width: 247px;  
  ${props => {
  return `@media ${props.breakPoints['tablet-wide']} {        
        flex: 0 0 50%;
        padding: 0 16px;     
      }`
    }
  }
`

const StyledNewsletterWrapper = styled(Layout)`
  flex: 1 1 100%;
  background-color: var(--color-primary-light);
  ${props => {
    return `@media ${props.breakPoints['tablet-wide']} {
              flex: 0 0 100%;          
              padding: 0 !important;
              max-width: 680px;
              height: 341px;
            }`
    }
  }    
`


const UpperText = styled(Text)`
  text-transform: uppercase;
  text-align: center;
`

const Antetitulo = styled(UpperText)`
  margin-bottom: 20px;
  ${props => {
  return `@media ${props.breakPoints['tablet-wide']} {          
              margin-bottom: 0;
            }`
    }
  }    
`
