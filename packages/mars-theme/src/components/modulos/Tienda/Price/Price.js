import React from 'react';
import Text from "../../../../objects/Text/Text";
import {connect, styled} from "frontity";
import Layout from "../../../../objects/Layout/Layout";

const Price = (props) => {

  StyledPriceWrapper.defaultProps = {
    breakPoints: props.state.theme.breakPoints
  }

  return (
    <StyledPriceWrapper>
      <Text data={{texto: props.forEmpresa ? '72' : '34' }} defaultData={{font_type: 'Display', font_family: 'Lexend Zetta'}}/>
      <Decimal data={{texto: props.forEmpresa ? '€' : ',95€' }} defaultData={{font_type: 'H1', font_family: 'Lexend Zetta'}}/>
    </StyledPriceWrapper>
  );
};

export default connect(Price);



const StyledPriceWrapper = styled(Layout)`
  margin-top: 100px;
  justify-content: center;
  align-items: center;
  ${props => {
    return `
        @media ${props.breakPoints['tablet']} {
          justify-content: flex-start;
          margin-top: 50px;
        }`
    }
  }  
`

const Decimal = styled(Text)`
  margin-top: 12px;
`
