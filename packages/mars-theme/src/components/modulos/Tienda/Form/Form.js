import React, { useState } from 'react';
import {styled, connect} from "frontity";
import {useForm} from "react-hook-form";
import Form from "../../../../objects/Forms/Form";
import {ApolloError} from "@apollo/client";
import FormControl from "../../../../objects/Forms/FormControl";
import AlertMessage from "../../../../objects/AlertMessage/AlertMessage";
import ButtonSubmit from "../../../../objects/ButtonSubmit/ButtonSubmit";
import Layout from "../../../../objects/Layout/Layout";


const FormAdd = ({state, actions, libraries, ...props}) => {


  const defaultVal = {
    quantity: 1
  };

  StyledFormLayout.defaultProps = {
    breakPoints: state.theme.breakPoints
  }


  const [adding, setAdding] = useState(false);


  const {register, handleSubmit, errors, setValue, getValues } = useForm({
    defaultValues: defaultVal
  })

  const [quantity, setQuantity] = useState(1)

  const NumberRangeHandler = (action) => {
    const singleValue = getValues("quantity");
    if(action == 'add' && singleValue < 99) {
      setValue("quantity", parseInt(singleValue) + 1)
    }else if (singleValue > 1) {
      setValue("quantity", parseInt(singleValue) - 1)
    }
    setQuantity(getValues("quantity"));
  }


  const handleOnSubmit = async data => {
    setAdding(true);

    const p = await actions.auth.authProcessAuthToken();
    const r = await libraries.cart.addItem(props.forEmpresa ? 1429 : 306, data.quantity);

    if (r instanceof ApolloError) {
      let errorMessage = r.message;
      actions.theme.setAlertMessage(errorMessage, false, 'error');
    }else{
      state.cart = r.data.addToCart.cart
      actions.analytics.addToCart(props.forEmpresa, data.quantity);
      actions.router.set('/carrito')
    }
    setAdding(false);
  };

  return (
    <StyledFormLayout justify={"center"}>
      <StyledForm onSubmit={handleSubmit(handleOnSubmit)}>
        <AlertMessage />
        <StyledInput
          id="quantity" name="quantity"
          type="number_range" placeholder=""
          register={register}
          errors={errors}
          getValues={getValues}
          quantity={quantity}
          NumberRangeHandler={NumberRangeHandler}
        />
        <StyledButtonSubmit disabled={adding} text={adding ? 'AÑADIENDO...' : 'COMPRAR'} />
      </StyledForm>
    </StyledFormLayout>
  );
};

export default connect(FormAdd);


const StyledFormLayout = styled(Layout)`
  justify-content: center;
  ${props => {
    return `
        @media ${props.breakPoints['tablet']} {
          justify-content: flex-start;          
        }`
    }
  }  
`

const StyledForm = styled(Form)`
   width: 188px;
   margin-top: 15px;
`

const StyledInput = styled(FormControl)`
   text-align: center;   
   
`

const StyledButtonSubmit = styled(ButtonSubmit)`   
    width: 100%;  
    min-width: unset;
`
