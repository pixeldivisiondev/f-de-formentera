import Text from "../../../../objects/Text/Text";
import {styled} from "frontity";
import Layout from "../../../../objects/Layout/Layout";
import ScrollAnimation from "react-animate-on-scroll";
import React from "react";

const Header = (props) => {
  return (
    <ScrollAnimation animateOnce={true} animateIn={"animate__fadeIn"} >
      <StyledContent  justify={"space-between"} className={props.className}>
        <Text data={{texto: 'TIENDA'}} defaultData={{font_type: 'Antetitulo'}}/>
      </StyledContent>
    </ScrollAnimation>
  );
};

export default Header;

const StyledContent = styled(Layout)`
  position: relative;
  height: auto;
`
