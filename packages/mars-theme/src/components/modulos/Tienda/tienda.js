import {styled, connect} from "frontity";
import Layout from "../../../objects/Layout/Layout";
import Section from "../../../objects/Section/Section";
import Header from './Header/header'
import Body from './Body/Body'
import React, {useEffect, useState} from 'react';
import {Parallax} from "react-scroll-parallax";
import Sombra from "../../../assets/images/sombraTienda.png";
import ScrollAnimation from 'react-animate-on-scroll';
import LazyLoad from '@frontity/lazyload'

const Tienda = ({actions, libraries, ...props}) => {
  const [product, setProduct] = useState(null)
  const [height, setHeight] = useState(300)

  const SectionDefault = {
    section_background_color: 'transparent-base',
  }

  StyledSection.defaultProps = {
    breakPoints: props.state.theme.breakPoints
  }

  StyledSombra.defaultProps = {
    breakPoints: props.state.theme.breakPoints,
    sombra: Sombra
  }

  StyledSombraContent.defaultProps = {
    breakPoints: props.state.theme.breakPoints,
    sombra: Sombra
  }

  useEffect(() => {
    if(product === null) {



      const  b = getProduct()
    }

  })

  const getProduct = async () => {
    const r = await libraries.products.getProduct(props.forEmpresa ? 1429 : 306)
    setProduct(r);
    props.data.section_config.section_background_image.url = r.data.product.featuredImage.node.sourceUrl
    return r;
  }
  return (
    <StyledScrollAnimation animateOnce={true} animateIn={"animate__fadeIn"} >
      <LazyLoad
        offset={250}
        height={height}
        onContentVisible={(data) => {
          setHeight('auto');
          actions.analytics.showProduct(props.forEmpresa);
        }
      }>
        <div id={"Tienda"} name={"Tienda"} style={{position: 'relative', overflow: 'hidden'}}>
          <StyledSombraContent y={['150px', '-150px']} tagOuter="figure">
            <StyledSombra ></StyledSombra>
          </StyledSombraContent>
          <StyledSection
            data={props.data.section_config}
            defaultData={SectionDefault}
            wrapper={"Default"}
            bg={product?.data?.product?.featuredImage?.node?.sourceUrl}
          >
            <Layout
              direction={"row"} align={"flex-start"} justify={"flex-start"}>

              <Header />
              <Body forEmpresa={props.forEmpresa} data={product?.data?.product}/>
            </Layout>
          </StyledSection>
        </div>
      </LazyLoad>
    </StyledScrollAnimation>


  )
}
export default connect(Tienda);

const StyledScrollAnimation = styled(ScrollAnimation)`
width: 100%;
`
const StyledSombraContent = styled(Parallax)`
  width: 100%;
  height: 100%;
  content: ' ';
  position: absolute;
  left: 0;
  top: 0;    
  z-index: 0;
  .parallax-inner {
    width: 100%;
    height: 100%;  
    transition: all 0.3s ease-out;
  }
  
`

const StyledSombra = styled.div`
  width: 100%;
  height: 100%;
  content: ' ';
  position: absolute;
  left: 0;
  top: 0;   
  background-image: url(${props => props.sombra});  
  background-position: calc(100% + 250px) -150px;
  background-size: 600px;
  background-repeat: no-repeat;
  ${props => {
  return `
        @media ${props.breakPoints['tablet']} {
             background-position: center;
             background-size: auto;
        } 
        @media ${props.breakPoints['tablet-wide']} {
          
          
        } 
        @media ${props.breakPoints['desktop']} {
          
        } 
      `
}
  }
  
`

const StyledSection = styled(Section)`
  padding-top: 50px;
  background-size: 160px;    
  background-image: url(${props => props.bg});
  background-repeat: no-repeat;
  background-position: right 40px;
  position: relative;
  z-index: 1;
  padding-bottom: 83px;
  ${props => {
    return `
        @media ${props.breakPoints['tablet']} {
        
          background-position: center 150px;
          padding-top: 130px;
          height: 790px;       
          padding-bottom: 28px;
          background-size: auto;    
        }`
    }
  }  
`






