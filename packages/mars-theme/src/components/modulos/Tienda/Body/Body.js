import Text from "../../../../objects/Text/Text";
import Layout from "../../../../objects/Layout/Layout";
import Form from "../Form/Form";
import {connect, styled} from "frontity";
import Price from "../Price/Price";
import ScrollAnimation from "react-animate-on-scroll";
import React from "react";
import RichText from "../../../../objects/RichText/RichText";


const Body = (props) => {

  StyledBody.defaultProps = {
    breakPoints: props.state.theme.breakPoints
  }

  Empresa.defaultProps = {
    breakPoints: props.state.theme.breakPoints
  }

  StyledScrollAnimation.defaultProps = {
    breakPoints: props.state.theme.breakPoints
  }

  return (

      <StyledBody direction={"column"} align={"flex-start"}>
        <ScrollAnimation animateOnce={true} animateIn={"animate__fadeIn"} >
          <StyledTitle data={{texto: 'LLÉVATELA <br>A CASA', font_family: 'Lexend Zetta'}} defaultData={{font_type: 'H1'}}/>
        </ScrollAnimation>
        <ScrollAnimation animateOnce={true} animateIn={"animate__fadeIn"} >
          <StyledName data={{texto: props.data?.description, font_family: 'Lexend Deca'}} defaultData={{font_type: 'ButtonLarge'}} />
        </ScrollAnimation>
        <ScrollAnimation animateOnce={true} animateIn={"animate__fadeIn"} >
          <Text data={{texto: 'Entrega 2/3 días laborables'}} defaultData={{font_type: 'SecondaryMedium', font_family: 'Cormorant'}}/>
        </ScrollAnimation>
        <ScrollAnimation animateOnce={true} animateIn={"animate__fadeIn"} style={{width: '100%'}}>
          <Price forEmpresa={props.forEmpresa}/>
          <Form forEmpresa={props.forEmpresa}/>
        </ScrollAnimation>
        <StyledScrollAnimation animateOnce={true} animateIn={"animate__fadeIn"}>
        </StyledScrollAnimation>
      </StyledBody>

  )
};

export default connect(Body);


const StyledName = styled(RichText)`
  margin: 8px 0;
`

const StyledTitle = styled(Text)`
  text-transform: uppercase;
`

const StyledScrollAnimation = styled(ScrollAnimation)`
  width: 100%;
  ${props => {
  return `@media ${props.breakPoints['tablet']} {
              width: auto;     
            }`
    }
  }
`

const StyledBody = styled(Layout)`
  height: 100%;
  margin-top: 30px;
  align-items: flex-start;
  ${props => {
    return `
        @media ${props.breakPoints['tablet']} {
          margin-top: 0;
          flex: 0 0 240px;
          margin-left: calc(100% - 240px);
          justify-content: center;
          align-items: flex-start;
        }
        @media ${props.breakPoints['tablet-wide']} {
          margin-top: 0;
          flex: 0 0 352px;
          margin-left: calc(100% - 352px);          
        }        
        `
  

  
    }
  }  
`

const StyledLink = styled.a`
  text-decoration: none;
`

const Empresa = styled(Text)`
  text-align: center;
  text-transform: uppercase;
  text-decoration: none !important;
  
  margin: auto;
  margin-top: 17px;
  opacity: 0.6;  
  max-width: 150px;
  ${props => {
    return `@media ${props.breakPoints['tablet']} {
              margin: 0;          
              margin-top: 17px;
              max-width: unset;     
            }`
    }
  }
  &:after {
    content: ' ';
    position: absolute;    
    width: 187px;
    background-color: rgba(0, 78, 102, 0.6);
    height: 1px;
    bottom: 26px;
    left: -18px;
    ${props => {
      return  `
        @media ${props.breakPoints['tablet']} {          
          bottom: 0;
          left: 0;
          
        }`
    }
  }  
    
  }
  
`

