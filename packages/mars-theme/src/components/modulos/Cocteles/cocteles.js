import React, { useState } from 'react';
import {styled, connect} from "frontity";
import Layout from "../../../objects/Layout/Layout";
import Section from "../../../objects/Section/Section";
import Text from "../../../objects/Text/Text";
import LazyLoad from '@frontity/lazyload'
import Slider from './Slider/Slider'


const Cocteles = (props) => {
  const [height, setHeight] = useState(300)
  const SectionDefault = {
    section_background_color: 'primary-light',
  }


  const [actual, setActual]  = useState(1);
  const [total, setTotal]  = useState(1);


  return (
    <>
      <div id={"Cocteles"} name={"Cocteles"}></div>
      <LazyLoad offset={250} height={height} onContentVisible={(data) =>setHeight('auto')}>
        <StyledSection
          data={props.data.section_config}
          defaultData={SectionDefault}
          wrapper={"Default"} >
          <Layout
            direction={"column"} >
                <StyledContent  justify={"space-between"}>
                  <Text data={{texto: 'CÓCTELES'}} defaultData={{font_type: 'Antetitulo'}}/>
                  <Text data={{texto:   actual.toString() + '/' + total.toString() }} defaultData={{font_type: 'SecondaryMedium', font_family: 'Cormorant'}}/>
                  <Slider setTotal={setTotal} setActual={setActual} data={props.data} />
                </StyledContent>
          </Layout>
        </StyledSection>
      </LazyLoad>
    </>


  )
}
export default connect(Cocteles);

const StyledSection = styled(Section)`
  padding-top: 57px;
  padding-bottom: 34px;
  position: relative;
`

const StyledContent = styled(Layout)`
  position: relative;
`
