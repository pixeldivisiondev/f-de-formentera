import {styled, connect} from "frontity";
import Layout from "../../../../../objects/Layout/Layout";
import Text from "../../../../../objects/Text/Text";

const Header = (props) => {

  const defaultTitle = {
    font_type: 'H1',
    font_family: 'Lexend Zetta'
  }
  return (
    <StyleHeaderLayout>
      <StyledTitle data={{texto: props.data}} defaultData={defaultTitle}/>
    </StyleHeaderLayout>
  );
};

export default Header;


const StyleHeaderLayout = styled(Layout)`  
  padding-bottom: 16px;
  border-bottom: 1px solid #CCC;
`

const StyledTitle = styled(Text)`
  text-transform: uppercase;
`

