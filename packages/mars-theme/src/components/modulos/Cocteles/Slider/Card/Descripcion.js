import {styled, connect} from "frontity";
import Layout from "../../../../../objects/Layout/Layout";
import Text from "../../../../../objects/Text/Text";

const Descripcion = (props) => {

  const defaultTitle = {
    font_type: 'LabelLarge',
    font_family: 'Lexend Zetta'
  }

  const defaultText = {
    font_type: 'PrimaryMedium',
    font_family: 'Cormorant',
    font_weight: 600,
  }

  return (
    <StyledDescripcionLayout direction={"row"} justify={"space-between"}>
      <Left direction={"column"} align={"flex-start"}>
        <Text data={{texto: 'DIFICULTAD'}} defaultData={defaultTitle}/>
        <Text data={{texto: props.data.dificultad}} defaultData={defaultText}/>
      </Left>
      <Right direction={"column"} align={"flex-start"}>
        <Text data={{texto: 'SERVIR EN'}} defaultData={defaultTitle}/>
        <Text data={{texto: props.data.servir_en}} defaultData={defaultText}/>
      </Right>
    </StyledDescripcionLayout>
  );
};

export default Descripcion;

const AutoWidthLayout = styled(Layout)`
  width: calc(50% - 9px);
  border-bottom: 1px solid #CCC;
  padding-bottom: 9px;
`

const Left = styled(AutoWidthLayout)`
  margin-right: 9px;
`

const Right = styled(AutoWidthLayout)`
  margin-left: 9px;
`

const StyledDescripcionLayout = styled(Layout)`
  padding-top: 25px;
`
