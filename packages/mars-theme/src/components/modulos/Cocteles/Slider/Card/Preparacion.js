import {styled, connect} from "frontity";
import Layout from "../../../../../objects/Layout/Layout";
import Text from "../../../../../objects/Text/Text";
import RichText from "../../../../../objects/RichText/RichText";

const Preparacion = (props) => {

  const defaultTitle = {
    font_type: 'LabelLarge',
    font_family: 'Lexend Zetta'
  }

  const defaultText = {
    font_type: 'PrimaryMedium',
    font_family: 'Cormorant',
    font_weight: 600,
  }

  return (
    <StyledPreparacionLayout justify={"flex-start"}>
      <StyledPreparacionTitle
        data={{texto: 'PREPARACIÓN'}}
        defaultData={defaultTitle}>
      </StyledPreparacionTitle>
      <StyledPreparacionText
        data={{texto: props.data.preparacion}}
        defaultData={defaultText}   >
      </StyledPreparacionText>
    </StyledPreparacionLayout>
  );
};

export default Preparacion;

const StyledPreparacionLayout = styled(Layout)`
  margin-top: 32px;
  padding-bottom: 28px;
  border-bottom: 1px solid #CCC;
`

const StyledPreparacionTitle = styled(Text)`
  margin-bottom: 8px;
  text-transform: uppercase;
`

const StyledPreparacionText = styled(RichText)`
  text-align: left;
`
