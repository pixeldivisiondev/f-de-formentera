import {styled, connect} from "frontity";
import Layout from "../../../../../objects/Layout/Layout";
import Text from "../../../../../objects/Text/Text";

const Ingredientes = (props) => {

  const defaultTitle = {
    font_type: 'LabelLarge',
    font_family: 'Lexend Zetta'
  }

  const defaultIngredienteText = {
    font_type: 'PrimaryMedium',
    font_family: 'Cormorant',
    font_weight: 600,
  }

  const defaultCantidadText = {
    font_type: 'LabelSmall',

  }

  return (
    <StyledIngredientesLayout justify={"flex-start"}>
      <StyledIngredientsTitle data={{texto: 'INGREDIENTES'}} defaultData={defaultTitle} />
      {
        props.data.ingrediente.map((item, key) => {
          return (
            <StyledIngredientLine justify={"flex-start"} key={key}>
              <StyledCantidadText data={{texto: item.cantidad}} defaultData={defaultCantidadText}/>
              <Text data={{texto: item.nombre_ingrediente}} defaultData={defaultIngredienteText}/>
            </StyledIngredientLine>
          )
        })
      }
    </StyledIngredientesLayout>
  );
};

export default Ingredientes;

const StyledIngredientsTitle = styled(Text)`
  margin-bottom: 5px;
`

const StyledCantidadText = styled(Text)`
  text-align: left;
  width: 80px;  
`

const StyledIngredientLine = styled(Layout)`
  margin-top: 4px;
`

const StyledIngredientesLayout = styled(Layout)`
  padding-top: 25px;
`
