import {styled, connect} from "frontity";
import Layout from "../../../../../objects/Layout/Layout";
import Text from "../../../../../objects/Text/Text";
import TwitterSrc from '../../../../../assets/images/icons/Twitter.svg'
import FacebookSrc from '../../../../../assets/images/icons/Facebook.svg'
import InstagramSrc from '../../../../../assets/images/icons/Instagram.svg'

const Social = (props) => {

  const defaultTitle = {
    font_type: 'LabelLarge',
    font_family: 'Lexend Zetta'
  }

  const openSocial = (url) => {
    if (typeof window !== 'undefined') {
      window.open(url, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
    }
  }
  return (
    <StyledSocialLayout justify={"space-between"}>
      <Text data={{texto: 'COMPARTIR'}} dafaultData={defaultTitle}/>
      <SocialIcons >
        <a href="#"
           onClick={(e) => {
             openSocial("https://www.facebook.com/sharer/sharer.php?u=http://f-de-formentera.vercel.app/&t=TITLE")
             return false;
           }}
           target="_blank" title="Share on Facebook">
          <img src={FacebookSrc} />
        </a>


        <SocialIcon href="https://twitter.com/share?url=http://f-de-formentera.vercel.app&text=TEXT"
            onClick={(e) => {
              openSocial("https://twitter.com/share?url=http://f-de-formentera.vercel.app&text=TEXT")
              return false;
            }}
           target="_blank" title="Share on Twitter">
          <img src={TwitterSrc} />
        </SocialIcon>

        <a href="#" target="_blank" rel="noopener"
           onClick={(e) => {
             openSocial("https://www.instagram.com/?url=http://f-de-formentera.vercel.app")
             return false;
           }}
           target="_blank">
          <img src={InstagramSrc} />
        </a>


      </SocialIcons>
    </StyledSocialLayout>
  );
};

export default Social;


const SocialIcons = styled(Layout)`
  width: auto;
`

const SocialIcon = styled.a`
  margin-left: 20px;
  margin-right    : 20px;
`

const StyledSocialLayout = styled(Layout)`
  padding-top: 21px;
`
