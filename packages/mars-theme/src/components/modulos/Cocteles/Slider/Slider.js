import React from 'react'
import {styled, connect, css, Global} from "frontity";
import Layout from "../../../../objects/Layout/Layout";
import PaginationSlider from "../../../objects/Sliders/Pagination";
import arrowBig from '../../../../assets/images/icons/ArrowBig.svg'
import arrow from '../../../../assets/images/icons/Arrows.svg'
import Card from "./Card";

// Import Swiper styles
import SwiperCore, { Navigation, Pagination,EffectFade } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import Swippertyles from 'swiper/swiper-bundle.css';
import SwiperAnimation from '@cycjimmy/swiper-animation';
import ScrollAnimation from 'react-animate-on-scroll';

SwiperCore.use([Navigation, Pagination, EffectFade]);

const Slider = (props) => {

  StyledLayout.defaultProps = {
    breakPoints: props.state.theme.breakPoints
  }

  let swiperAnimation = {};

  if(typeof window !== 'undefined'){
    swiperAnimation = new SwiperAnimation();
  }

  return (

    <StyledLayout direction={"row"}>
        <Global styles={css(Swippertyles)} />
        <Swiper
          onSwiper={(swiper) => props.setTotal(swiper.slides.length)}
          onSlideChange={(swiper) =>  { props.setActual(swiper.activeIndex + 1); }}
          spaceBetween={0}
          onInit={(swiper) => {swiperAnimation.init(swiper).animate()}}
          onSlideChangeTransitionStart={(swiper) => { swiperAnimation.init(swiper).animate()}}
          effect={"fade"}
          fadeEffect={{crossFade: true}}
          navigation={{
            nextEl: '.swiper-button-next4',
            prevEl: '.swiper-button-prev4',
          }}
          pagination={{
            el: '.swiper-pagination3',
            type: 'bullets',
            clickable: true
          }}
          slidesPerView={1}
        >
          {
            props.data.cocteles.map((card, key) => {
              return (
                <SwiperSlide key={key}>
                  {({ isActive }) => (
                    <ScrollAnimation animateOnce={true} animateIn={"animate__fadeIn"} >
                      <Card active={isActive} data={card}/>
                    </ScrollAnimation>
                  )}
                </SwiperSlide>
              )
            })
          }

        </Swiper>


      <NavigationBar>
        <PaginationSlider className="swiper-pagination3"></PaginationSlider>
      </NavigationBar>

      <Left className="swiper-button-prev4" breakPoints={props.state.theme.breakPoints}><img src={arrow} /></Left>
      <Right className="swiper-button-next4" breakPoints={props.state.theme.breakPoints}><RightArrow src={arrow} /></Right>

      <LeftDesktop className="swiper-button-prev4" breakPoints={props.state.theme.breakPoints}><img src={arrowBig} /></LeftDesktop>
      <RightDesktop className="swiper-button-next4" breakPoints={props.state.theme.breakPoints}><RightArrow src={arrowBig} /></RightDesktop>

    </StyledLayout>


  )
}

export default connect(Slider);

const NavigationBar = styled(Layout)`
  margin-top: 24px;
`

const Arrow = styled.div`
  position: absolute;
  cursor: pointer;
  z-index: 9;
`

const Left = styled(Arrow)`  
  left: 0;  
  top: 140px;
  ${props => {
    return `
        @media ${props.breakPoints['tablet-wide']} {
          display: none;
        }
      ` 
    }
  }     
  
`

const Right = styled(Arrow)`  
  right: 0; 
  top: 140px;
  ${props => {
    return `
        @media ${props.breakPoints['tablet-wide']} {
          display: none;
        }
      ` 
    }
  }     
`

const LeftDesktop = styled(Arrow)`  
  left: 0;
  display: none;
  ${props => {
    return `
        @media ${props.breakPoints['tablet-wide']} {
          display: block;
        }
      ` 
    }
  }     
  
`

const RightDesktop = styled(Arrow)`  
  right: 0;
  display: none; 
  ${props => {
    return `
        @media ${props.breakPoints['tablet-wide']} {
          display: block;
        } 
        
      ` 
    }
  }   
  
`



const RightArrow = styled.img`
  transform: rotate(180deg)
`


const StyledLayout = styled(Layout)`
  padding-bottom: 0px;
  flex: 1 1 100%;
  height: auto;  
  
  .swiper-wrapper {
    transition-delay: 2s !important;
  }
  
  .swiper-slide {
    padding-bottom: 28px;    
  }
  
  .swiper-button-disabled {
    opacity: 0;
  }    
    
  .swiper-container {
    width: 100%;
    
  } 
`
