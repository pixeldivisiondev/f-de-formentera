import {styled, connect, Global } from "frontity";
import Image from "@frontity/components/image";
import Header from "./Card/header";
import Descripcion from "./Card/Descripcion";
import Ingredientes from "./Card/Ingredientes";
import Preparacion from "./Card/Preparacion";
import Social from "./Card/Social";
import Layout from "../../../../objects/Layout/Layout";

import NoteInferior from '../../../../assets/images/card/recorteinferior.svg'
import NoteSuperior from '../../../../assets/images/card/recortesuperior.svg'


const Card = (props) => {

  StyledCard.defaultProps = {
    breakPoints: props.state.theme.breakPoints
  }

  StyledImage.defaultProps = {
    breakPoints: props.state.theme.breakPoints
  }

  return (
      <StyledCard active={props.active} className={props.className}>

        <Cocktel data-swiper-animation="animate__fadeInLeft" data-duration="2s"  data-delay="-0.5s"  data-swiper-out-animation="animate__fadeOutRight"  data-out-duration="1.5s">
          <StyledImage src={props.data.imagen.url} rootMargin={'100%'}/>
        </Cocktel>
       <Animate data-swiper-animation="animate__fadeIn" data-duration="1s"  data-delay="1s"  data-swiper-out-animation="animate__fadeOut"  data-out-duration="1.5s">
        <StyledNote >
          <Header data={props.data.nombre} />
          <Descripcion data={props.data.descripcion} />
          <Ingredientes data={props.data.igngredientes} />
          <Preparacion data={props.data} />
          <Social />
        </StyledNote>
       </Animate>
      </StyledCard>
  );
};

export default connect(Card);

const Cocktel = styled.div`
  transition: all 0.3s;
  visibility: hidden;
  position: relative;
  z-index: 1;
`

const Animate = styled.div`
  transition: all 0.3s;
  visibility: hidden;
  position: relative;
  z-index: 9;
`
const StyledCard = styled(Layout)`
 
  text-align: center;
  margin-top: 0;
  position: relative;  
  flex-direction: column;
  ${props => {
    return `
      @media ${props.breakPoints['tablet-wide']} {
        flex-direction: row;
        margin-top: 28px;          
      }
     `
    }
  }
`

const StyledImage = styled(Image)`
 max-width: 220px;
 margin-bottom: 25px;
 ${props => {
  return `
        @media ${props.breakPoints['tablet-wide']} {
          max-width: 100%;
          margin-bottom: 0px;          
        }
      `
    }
  }   
`

const StyledNote = styled(Layout)`
  text-align: center;
  max-width: 400px;
  height: auto;
  align-self: auto;
  background-color: var(--color-secondary-base);
  padding: 0 25px;
  
  position: relative;
  &:after {
    content: ' ';
    position: absolute;
    width: 100%;
    height: 27px;    
    top: -27px;
    left: 0;    
    background-image: url(${NoteSuperior});
  }
  &:before {
    content: ' ';
    position: absolute;
    width: 100%;
    height: 27px;    
    bottom: -27px;
    left: 0;    
    background-image: url(${NoteInferior});
  }
  
`

