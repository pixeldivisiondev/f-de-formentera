import React from 'react';
import {styled} from "frontity";
import Label from './Label'
import Text from "../../../../objects/Text/Text";

const Bienvenidos = (props) => {
  return (
    <Bienvenido className={props.className}>
      <Text data={{texto: 'BIENVENIDO', font_type: 'ButtomSmall' }}/>
    </Bienvenido>
  );
};

export default Bienvenidos;

const Bienvenido = styled(Label)`
  left: 0;
`
