import React from 'react';
import Image from "@frontity/components/image";
import Sroll from "../../../../assets/images/icons/Scroll.svg";
import {styled} from "frontity";
import Label from './Label'
import Text from "../../../../objects/Text/Text";

const Descubre = (props) => {
  return (
    <DescubreMas onClick={() => { window.scrollTo({top: window.innerHeight,left: 0, behavior: 'smooth'})}}>
      <DescubreText data={{texto: 'DESCUBRE MÁS', font_type: 'ButtomSmall' }} className={props.className} />
      <Image src={Sroll} />
    </DescubreMas>
  );
};

export default Descubre;

const DescubreMas = styled(Label)`
  right: 0;
  cursor: pointer;
`

const DescubreText = styled(Text)`  
    margin-right: 7px;
`
