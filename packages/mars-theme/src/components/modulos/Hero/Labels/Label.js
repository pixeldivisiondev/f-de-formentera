import React from 'react';
import {styled, connect} from "frontity";
import Layout from "../../../../objects/Layout/Layout";

const Label = ({state, ...props}) => {

  StyledLabel.defaultProps = {
    breakPoints: state.theme.breakPoints
  }


  return (
    <StyledLabel className={props.className} onClick={props.onClick}>
      {props.children}
    </StyledLabel>
  );
};

export default connect(Label);

const
  StyledLabel = styled(Layout)`
  position: absolute;
  top: calc(50vh - 160px);
  width: auto;
  padding: 0 28px;
  display: none;
   ${props => {
    return `
        @media ${props.breakPoints['tablet']} {
          display: flex;
        } 
      ` 
    }
  } 
`
