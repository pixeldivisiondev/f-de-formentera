import React from 'react';
import Image from "@frontity/components/image";
import {connect, styled} from "frontity";

const ImageMobile = (props) => {

  StyledImage.defaultProps = {
    breakPoints: props.state.theme.breakPoints
  }

  return (
    <StyledImage src={props.src} />
  );
};

export default connect(ImageMobile);

const StyledImage = styled(Image)`  
  margin-top: -75px;
  max-width: 943px;
  width: 100%;
  display: none;
  ${props => {
  return `
        @media ${props.breakPoints['tablet']} {
          display: none;
        }
      `
  }
} 
`
