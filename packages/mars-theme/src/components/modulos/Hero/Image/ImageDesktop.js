import React from 'react';
import Image from "@frontity/components/image";
import {connect, styled} from "frontity";
import { Parallax } from 'react-scroll-parallax';
import BaseImage from '../../../../assets/images/HeroBase.png'
import BotelleImage from '../../../../assets/images/HeroBotella.png'
import FrutasImage from '../../../../assets/images/HeroFrutas.png'
import ScrollAnimation from "react-animate-on-scroll";

const ImageDesktop = (props) => {
  StyledImage.defaultProps = {
    breakPoints: props.state.theme.breakPoints
  }

  ParalaxItem.defaultProps = {
    breakPoints: props.state.theme.breakPoints
  }

  ParalaxContainer.defaultProps = {
    breakPoints: props.state.theme.breakPoints
  }

  return (
    <ParalaxContainer>
      <ParalaxItem className="custom-class" y={['100px', '-100px']} tagOuter="figure" >
        <ScrollAnimation animateOnce={false} animateIn={"animate__fadeIn"} initiallyVisible={true}>
          <StyledImage src={BaseImage} rootMargin={'-130px'}/>
        </ScrollAnimation>
      </ParalaxItem>
      <ParalaxItem className="custom-class" y={['0', '0']} tagOuter="figure">
        <ScrollAnimation animateOnce={true} animateIn={"animate__fadeIn"} initiallyVisible={true}>
          <StyledImage src={BotelleImage} rootMargin={'-130px'} />
        </ScrollAnimation>
      </ParalaxItem>
      <ParalaxItem className="custom-class" y={['300px', '-300px']} tagOuter="figure">
        <ScrollAnimation animateOnce={true} animateIn={"animate__fadeIn"} initiallyVisible={true}>
          <StyledImage src={FrutasImage} rootMargin={'-130px'}/>
        </ScrollAnimation>
      </ParalaxItem>

    </ParalaxContainer>
  );
};

export default connect(ImageDesktop);

const StyledImage = styled.img`  
  margin: auto;    
  width: 713px;
  position: relative;
  left: calc(50% - 713px / 2);
  margin-top: -40px;
  ${props => {
  return `
      @media ${props.breakPoints['tablet']} {
        display: block;
        width: 100%;
        left: 0;
        margin-top: -60px;
      } 
      @media ${props.breakPoints['desktop']} {        
        margin-top: -130px;
      } 
    ` 
    }
  } 
`

const ParalaxContainer = styled.div`
  position: relative;
  height: 500px;
  width: 100%;
  .parallax-inner {
    transition: all 0.3s ease-out;
  }
  ${props => {
  return `
      @media ${props.breakPoints['tablet']} {
        height:600px;
      } 
      @media ${props.breakPoints['tablet-wide']} {
        height: 820px;
      } 
    ` 
    }
  } 
  
`

const ParalaxItem = styled(Parallax)`
  position: absolute;
  overflow: hidden;
  width: 100%;
  height: 480px;
    left: 0;
  top: 0;
  ${props => {
  return `
      @media ${props.breakPoints['tablet']} {
        height: 940px;
      } 
    ` 
    }
  } 
`
