import React, {useState} from 'react';
import Button from "../../../../objects/Button/Button";
import {styled, connect} from "frontity";
import Text from "../../../../objects/Text/Text";
import {ApolloError} from "@apollo/client";
import Layout from "../../../../objects/Layout/Layout";

const MyComponent = ({state, actions, libraries, ...props}) => {

  const textProps = {
    font_family: 'Cormorant',
    font_type: 'SecondaryMedium',
    font_weight: 600
  }

  StyledTitle.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  StyledText.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  BodyWrapper.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  const [adding, setAdding] = useState(false);

  const handleOnSubmit = async data => {
    setAdding(true);
    const p = await actions.auth.authProcessAuthToken();
    const r = await libraries.cart.addItem(props.forEmpresa ? 1429 : 306, 1);

    if (r instanceof ApolloError) {
      let errorMessage = r.message;
      actions.theme.setAlertMessage(errorMessage, false, 'error');
    }else{
      state.cart = r.data.addToCart.cart
      actions.analytics.addToCart(props.forEmpresa);
      actions.router.set('/carrito/')
    }
    setAdding(false);
  };


  return (
    <BodyWrapper direction="column">
      <StyledTitle
        data={props.data.titulo_titulo_component}
        defaultData={{font_type: 'H1', font_family: 'Lexend Zetta'}}/>
      <StyledText
        data={textProps}
        defaultData={props.data.texto_titulo_component}/>
      <Button
        data={{tipo: 'primary', tamano: 'large', texto: adding ? 'añadiendo...' : 'comprar'}}
        onClick={() => {handleOnSubmit()}}
      />
    </BodyWrapper>
  );
};

export default connect(MyComponent);

const BodyWrapper = styled(Layout)`
  
   ${props => {
  return `
      @media ${props.breakPoints['tablet']} {
        position: absolute;
        width: 380px;
        margin-top: 50px;
        right: 0;
        top: 0;
      } 
    ` 
    }
  } 
`

const StyledTitle = styled(Text)`
  max-width: 250px;
  margin-top: 24px;
  text-align: center;
  ${props => {
  return `
      @media ${props.breakPoints['tablet']} {
        margin-top: 0;
      } 
    ` 
    }
  } 
`

const StyledText = styled(Text)`
  max-width: 260px;
  margin-top: 12px;  
  text-align: center;
  margin-bottom: 20px;
  ${props => {
    return `
        @media ${props.breakPoints['tablet']} {
          max-width: 300px;          
        } 
      ` 
    }
  } 
`




