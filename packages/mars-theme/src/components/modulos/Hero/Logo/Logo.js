import React from 'react';
import Image from "@frontity/components/image";
import LogoSrc from '../../../../assets/images/logos/Logotipo.svg'
import {styled, connect} from "frontity";

const Logo = ({state}) => {

  StyledLogo.defaultProps = {
    breakPoints: state.theme.breakPoints
  }
  return (
    <StyledLogo src={LogoSrc} />
  );
};

export default connect(Logo)

const StyledLogo = styled(Image)`  
 width: 72px;
 margin-bottom: 26px;
 margin-top: 16px;
 display: none;
  ${props => {
      return `
        @media ${props.breakPoints['tablet']} {
          display: block;
        } 
      ` 
    }
  } 
`
