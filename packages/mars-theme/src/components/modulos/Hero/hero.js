import {styled, connect} from "frontity";
import Layout from "../../../objects/Layout/Layout";
import Section from "../../../objects/Section/Section";
import Logo from "./Logo/Logo"
import Body from "./Body/Body"
import Bienvenidos from "./Labels/Bienvenidos"
import Descubre from "./Labels/Descubre"
import ImageDesktop from "./Image/ImageDesktop"
import Nav from "../../Header/nav";
import Wrapper from "../../../objects/Wrapper";


const Hero = ({state, ...props}) => {
  const SectionDefault = {
    section_background_color: 'secondary-base',
  }

  MobileNav.defaultProps = {
    breakPoints: state.theme.breakPoints,
  }

  return (
<div>
    <Section
      data={props.data.section_config}
      defaultData={SectionDefault}
      wrapper={"FullWidthMobile"}>
      <Layout
        direction={"column"}>

        <Body data={props.data} forEmpresa={props.forEmpresa}/>
        <ImageDesktop src={props.data.imagenes.imagen.url} />
        <Bienvenidos />
        <Descubre />
      </Layout>
    </Section>
      <MobileNav wrapper={"Default"}>
        <Nav/>
      </MobileNav>
  </div>


  )
}


export default connect(Hero);

const MobileNav = styled(Wrapper)`  
  margin-bottom: 30px;
  
  ${props => {
    return `
      @media ${props.breakPoints['tablet']} {
          display: none;      
      }      
      `
  }}
`

