import {styled} from "frontity";
import Layout from "../../../../objects/Layout/Layout";
import Text from "../../../../objects/Text/Text";

const Title = (props) => {
  return (
    <Layout direction={"column"} className={props.className}>
      <Text data={props.data.pretitulo_titulo_component} defaultData={{font_type: 'Antetitulo' }}/>
      <StyledTitle data={props.data.titulo_titulo_component} defaultData={{font_type: 'H1', font_family: 'Lexend Zetta' }}/>
    </Layout>
  );
};

export default Title;

const StyledTitle = styled(Text)`
  margin-top: 9px;
  text-align: center;
  max-width: 270px;
`
