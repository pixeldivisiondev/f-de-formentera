import React, {useEffect, useState} from 'react';
import {styled, connect} from "frontity";
import Layout from "../../../objects/Layout/Layout";
import Section from "../../../objects/Section/Section";
import LeftColumn from "./LeftColumn/LeftColumn"
import RightColumn from "./RightColumn/RightColumn"
import Title from "./Title/Title";
import LazyLoad from '@frontity/lazyload'

const Origen = (props) => {
  const [height, setHeight] = useState(400)
  const SectionDefault = {
    section_background_color: 'secondary-base',
  }

  StyledTitle.defaultProps = {
    breakPoints: props.state.theme.breakPoints
  }

  const [slideIndex, setSlideIndex] = useState(0);

  return (
    <>
      <div id={"Origen"} name={"Origen"}></div>
      <LazyLoad offset={150} height={height} onContentVisible={(data) =>setHeight('auto')}>
        <Section
          data={props.data.section_config}
          defaultData={SectionDefault}
          wrapper={"FullWidthMobile"}>
          <Layout>
            <StyledTitle data={props.data}/>
            <Layout>
              <LeftColumn data={props.data} setSlideIndex={setSlideIndex}/>
              <RightColumn data={props.data} slideIndex={slideIndex}/>
            </Layout>
          </Layout>
        </Section>
      </LazyLoad>
    </>

  )
}


export default connect(Origen);

const StyledTitle = styled(Title)`  
  display: flex;
  margin-bottom: 54px;
  ${props => {    
    return `
          @media ${props.breakPoints['tablet']} {
            display: none;
          } 
        ` 
      }
    } 
`



