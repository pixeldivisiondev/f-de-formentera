import {styled, connect} from "frontity";
import Layout from "../../../../objects/Layout/Layout";

import SwiperCore, { Navigation, Pagination, Autoplay, EffectFade } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
SwiperCore.use([Navigation, Pagination, Autoplay, EffectFade]);

import ScrollAnimation from 'react-animate-on-scroll';
import React, {useEffect, useState} from "react";

const RightColumn = (props) => {

  StyledScroll.defaultProps = {
    breakPoints: props.state.theme.breakPoints
  }

  StyledVideo.defaultProps = {
    breakPoints: props.state.theme.breakPoints
  }

  const [swipperInstance, setSwipperInstance ] = useState(null)
  const [swipperIndex, setSwipperIndex ] = useState(props.slideIndex)

  useEffect(() => {
    console.log(swipperInstance, 'swipper Instance')

    if(swipperInstance !== null) {
      console.log(props.slideIndex, 'swipper Move')
      swipperInstance.slideTo(props.slideIndex ? props.slideIndex : 1);
    }


  })
  return (
    <StyledScroll animateOnce={true} animateIn={"animate__fadeInRight"} style={{minHeight: '234px'}}  className={props.className} >
      <StyledLayout >

          <StyledSWipper
            spaceBetween={0}
            slidesPerView={1}
            effect={"fade"}
            loop={true}
            onSwiper={(swiper) => setSwipperInstance(swiper)}
            fadeEffect={
              {crossFade: true}
            }
            speed={1000}
          >
            {
              props.data.imagenes
                ? props.data.imagenes.map((item, key) => {
                  console.log(item, 'ITEM IMAGE VIDEO')
                    return (

                        <SwiperSlide key={key}>
                          {item.es_video === false
                            ? <StyledContent data={{section_background_image: {url: item.imagen.url}}} />
                            : <StyledContent><StyledVideo    src={  item.video} autoPlay muted loop /></StyledContent>
                          }


                        </SwiperSlide>

                    )
                  })
                : null
            }
          </StyledSWipper>

      </StyledLayout>
    </StyledScroll>
  )
}

export default connect(RightColumn);

const StyledVideo = styled.video`
  position: absolute;
  width: auto;
  height: 100%;
  top: 0;
  left: 0;
  ${props => {
     return `
         @media ${props.breakPoints['tablet']} {
           width: 100%;
           height: auto;
         } 
       ` 
     }
  }
  
`
const StyledScroll = styled(ScrollAnimation)`
  @keyframes fadeInRight {
    0% {
      opacity: 0;
      transform: translate3d(10%, 0, 0);
    }
  
    100% {
      opacity: 1;
      transform: translate3d(0, 0, 0);
    }
  }

  &.animate__fadeInRight {
    right: 20%;
  }
  flex: 1 1 135px;
  height: 100%;
  min-height: 234px;
  background-size: cover;
  background-position: center;
  overflow: hidden;
  ${props => {
  return `
        @media ${props.breakPoints['tablet']} {
          flex: 1 1 50%;
        } 
      ` 
    }
  }
`


const StyledLayout = styled(Layout)`
  flex: 1 1 100%;
  height: 100%;
  min-height: 234px;
  background-size: cover;
  background-position: center;
  overflow: hidden;
  
`

const StyledContent = styled(Layout)`  
  height: 100%;
  background-size: cover;
  background-position: center;
`

const StyledSWipper = styled(Swiper)`
    position: relative;    
    height: 100%;
    width: 100%;
    overflow: hidden;
    & > * {
      height: 100%;
      width: 100%;
    }
`

