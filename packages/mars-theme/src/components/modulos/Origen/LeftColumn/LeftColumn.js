import React, { useState } from 'react'
import {styled, connect, css, Global} from "frontity";
import Layout from "../../../../objects/Layout/Layout";
import Title from "../Title/Title"
import Text from "../../../../objects/Text/Text";
import RichText from "../../../../objects/RichText/RichText";
import PaginationSlider from "../../../objects/Sliders/Pagination";

import SwiperCore, { Navigation, Pagination, Autoplay,EffectFade } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
SwiperCore.use([Navigation, Pagination, Autoplay, EffectFade]);
import arrow from '../../../../assets/images/icons/Arrows.svg';

// Import Swiper styles
import Swippertyles from 'swiper/swiper-bundle.css';

import ScrollAnimation from 'react-animate-on-scroll';

const LeftColumn = (props) => {

  SuperiorTitle.defaultProps = {
    breakPoints: props.state.theme.breakPoints
  }
  Arrow.defaultProps = {
    breakPoints: props.state.theme.breakPoints
  }

  return (
      <StyledLayout direction={"row"}>
        <Global styles={css(Swippertyles)} />
        <ScrollAnimation animateOnce={true} animateIn={"animate__fadeIn"}>
          <SuperiorTitle data={props.data}/>
        </ScrollAnimation>

          <StyledSWipper
            spaceBetween={0}
            effect={"fade"}
            fadeEffect={{crossFade: true}}
            speed={1000}
            onSlideChangeTransitionStart={(e) => {console.log(e.snapIndex, 'changed'); props.setSlideIndex(e.snapIndex + 1) }}
            navigation={{
              nextEl: '.swiper-button-next2',
              prevEl: '.swiper-button-prev2',
            }}
            pagination={{
              el: '.swiper-pagination',
              type: 'bullets',
              clickable: true
            }}
            slidesPerView={1}
          >
            {
              props.data.slider_frases.map((frase, key) => {

                return (<SwiperSlide key={key}>
                          <SwipperContent>
                            <ScrollAnimation animateOnce={true} animateIn={"animate__fadeIn"}>
                              <StyledText data={frase.component_text} defaultData={{font_family: 'Cormorant'}}/>
                            </ScrollAnimation>
                          </SwipperContent>
                        </SwiperSlide>)
              })
            }
          </StyledSWipper>
        <ScrollAnimation animateOnce={true} animateIn={"animate__fadeIn"}>
          <NavigationWrapper>
            <Arrow className="swiper-button-prev2"><img src={arrow} /></Arrow>
            <PaginationSlider className="swiper-pagination"></PaginationSlider>
            <Arrow className="swiper-button-next2"><RightArrow src={arrow} /></Arrow>
          </NavigationWrapper>
        </ScrollAnimation>
      </StyledLayout>
  )
}

export default connect(LeftColumn);


const SuperiorTitle = styled(Title)`
  display: none;
  ${props => {  
    return `
        @media ${props.breakPoints['tablet']} {
          display: flex;
        } 
      ` 
    }
  } 
`

const StyledLayout = styled(Layout)`  
  padding-top: 50px;
  overflow: hidden;
  padding-bottom: 50px;
  flex: 1 1 50%;
  height: 100%;  
`

const NavigationWrapper = styled(Layout)`
  margin-top: 20px;
  .swiper-button-prev2, .swiper-button-next2 {
     transition: all 0.3s;
     cursor: pointer;
     &:focus {
      outline: none;
     }
  }
  .swiper-button-disabled {
    opacity: 0;
  }
`

const StyledSWipper = styled(Swiper)`
    position: relative;
`


const Arrow = styled.div`
  display: block;
  ${props => {
    return `
        @media ${props.breakPoints['tablet']} {
          display: block;
        } 
      ` 
    }
  } 
`


const SwipperContent = styled(Layout)`

`

const RightArrow = styled.img`
  transform: rotate(180deg)
`

const StyledText = styled(RichText)`
  padding: 0 26px;
  margin-top: 15px;
  max-width: 300px; 
  text-align: center;
`
