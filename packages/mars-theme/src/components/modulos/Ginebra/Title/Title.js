import {connect, styled} from "frontity";
import Layout from "../../../../objects/Layout/Layout";
import Text from "../../../../objects/Text/Text";

const Title = (props) => {

  StyledText.defaultProps = {
    breakPoints: props.state.theme.breakPoints
  }

  return (
    <Layout direction={"column"} className={props.className}>
      <Text data={props.data.pretitulo_titulo_component} defaultData={{font_type: 'Antetitulo' }}/>
      <StyledTitle data={props.data.titulo_titulo_component}  defaultData={{font_type: 'H1', font_family: 'Lexend Zetta' }}/>
      <StyledText data={props.data.texto_component_text} defaultData={{font_family: 'Cormorant', font_type: 'SecondaryMedium' }}/>
    </Layout>
  );
};

export default connect(Title);

const StyledTitle = styled(Text)`
  margin-top: 9px;
  text-align: center;
  max-width: 280px;
  text-transform: uppercase;
`

const StyledText = styled(Text)`  
  text-align: center;  
  margin-top: 15px;
  max-width: 220px;
  ${props => {
  return `
        @media ${props.breakPoints['tablet']} {
            max-width: 420px;
        }
      `
    }
  }
`
