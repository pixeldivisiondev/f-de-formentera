import {styled, connect} from "frontity";
import Text from "../../../../objects/Text/Text";
import Layout from "../../../../objects/Layout/Layout";

const Card = (props) => {

  StyledCard.defaultProps = {
    breakPoints: props.state.theme.breakPoints
  }

  return (
      <StyledCard active={props.active} className={props.className}>
        <ImageWrapper>
          <StyledImage src={props.data.imagen.url} />
        </ImageWrapper>
        <StyledTitle data={props.data.titulo_component} defaultData={{ font_type: 'SubtitleLarge', font_family: 'Lexend Zetta'}}/>
        <StyledText
          data={props.data.component_text}
          defaultData={{ font_type: 'PrimaryMedium', font_family: 'Cormorant'}}/>
      </StyledCard>
  );
};

export default connect(Card);

const StyledCard = styled.div`
  text-align: center;
  max-width: 250px;
  ${props => 
    props.active 
      ? `
          ${StyledText} {
            opacity: 1;
          }
          ${StyledImage} {           
            margin-top: 0px;
          }
        `
      : `
          ${StyledImage} {
            transform: scale(0.5);
            margin-top: 48px;
          }
          ${StyledText} {
            opacity: 0;
          }
          ${ImageWrapper} {
            align-items: flex-end;
            justify-content: center;
            padding-bottom: 20px;
          }
        `
  }
  
  
`

const ImageWrapper = styled(Layout)`
  height: 174px;
`
const StyledTitle = styled(Text)`
  padding: 0 26px;
  margin: auto;
  margin-top: 24px;
  max-width: 300px; 
  text-align: center;  
  transition: all 0.6s;
`

const StyledText = styled(Text)`
  padding: 0 26px;  
  max-width: 300px; 
  text-align: center;  
  transition: all 0.6s;  
  margin: auto;
`

const StyledImage = styled.img`
  transition: all 0.3s;
  width: 121px;
  margin: auto;  
`
