import React from 'react'
import {styled, connect, css, Global} from "frontity";
import Layout from "../../../../objects/Layout/Layout";
import PaginationSlider from "../../../objects/Sliders/Pagination";
import Card from './Card'
import arrow from '../../../../assets/images/icons/Arrows.svg'

// Import Swiper styles
import SwiperCore, { Navigation, Pagination } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import Swippertyles from 'swiper/swiper-bundle.css';
import ScrollAnimation from "react-animate-on-scroll";
import Button from "../../../../objects/Button/Button";

SwiperCore.use([Navigation, Pagination]);

const Slider = (props) => {

  Arrow.defaultProps = {
    breakPoints: props.state.theme.breakPoints
  }

  StyledLayout5.defaultProps = {
    breakPoints: props.state.theme.breakPoints
  }

  ButtonLayout2.defaultProps = {
    breakPoints: props.state.theme.breakPoints
  }

  return (
    <>
    <StyledLayout5 direction={"row"}>
      <StyledLayout2 >
        <Global styles={css(Swippertyles)} />
        <Swiper
          spaceBetween={0}
          navigation={{
            nextEl: '.swiper-button-next3',
            prevEl: '.swiper-button-prev3',
          }}
          pagination={{
            el: '.swiper-pagination2',
            type: 'bullets',
            clickable: true
          }}
          slidesPerView={"auto"}
          centeredSlides={true}
        >
          {
            props.data.slider_ingredientes.map((card, key) => {
              return (
                <SwiperSlide key={key}>
                  {({ isActive }) => (
                    <ScrollAnimation animateOnce={true} animateIn={"animate__fadeIn"} delay={300 * key}>
                      <Card active={isActive} data={card} className={props.className}/>
                    </ScrollAnimation>
                  )}
                </SwiperSlide>
              )
            })
          }

        </Swiper>
      </StyledLayout2>

      <NavigationBar>
        <Arrow className="swiper-button-prev3"><img src={arrow} /></Arrow>
        <PaginationSlider className="swiper-pagination2"></PaginationSlider>
        <Arrow className="swiper-button-next3"><RightArrow src={arrow} /></Arrow>
      </NavigationBar>

      <ButtonLayout2>
        <StyledButton2 data={{tipo: "primary", tamano: "small", texto: props.adding ? "AÑADIENDO..." : "COMPRAR"}} onClick={ () => { props.handleOnSubmit() }}/>
      </ButtonLayout2>
    </StyledLayout5>

      </>
  )
}

export default connect(Slider);

const ButtonLayout2 = styled(Layout)`
  ${props => {
    return `
        @media ${props.breakPoints['tablet']} {
          display: none;
        } 
      `
    }
  } 
`

const StyledButton2 = styled(Button)`
  margin-top: 16px;
`

const StyledLayout2 = styled(Layout)`
  margin-right: 0;
  overflow: hidden;
  justify-content: flex-start;
  flex: 0 0 calc(50% + 132px);   
`

const NavigationBar = styled(Layout)`
  margin-top: 24px;
`

const Arrow = styled.div`  
  cursor: pointer;
  ${props => {
    return `
        @media ${props.breakPoints['tablet']} {
          display: block;
        } 
      `
    }
  } 
`

const RightArrow = styled.img`
  transform: rotate(180deg)
`


const StyledLayout5 = styled(Layout)`

  .swiper-slide {
    width: 204px;
    ${props => {
      return `
          @media ${props.breakPoints['tablet']} {
            width: 264px;
          } 
        `
      }
    }}
  
  .swiper-container {
    overflow: visible;    
    margin: 0;
    width: 100%;
  }
  
  .swiper-button-disabled {
    opacity: 0;
  }
  
  .swiper-wrapper {
    overflow: visible !important;
    position: relative;
    right: calc(50% - 132px);
    cursor: grab;
  }
  
  padding-bottom: 0px;
  flex: 1 1 100%;
  height: auto;  
  overflow: visible; 
  justify-content: flex-end; 
  padding-left: 113px;
  ${props => {
    return `
      @media ${props.breakPoints['tablet']} {
        padding-left: 0px;
      } 
    `
    }
  }}
`
