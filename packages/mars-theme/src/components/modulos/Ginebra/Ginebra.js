import React, {useState} from 'react';
import Layout from "../../../objects/Layout/Layout";
import Section from "../../../objects/Section/Section";
import {connect, styled} from "frontity";
import Title from "./Title/Title";
import Slider from "./Slider/Slider";
import Sombra from '../../../assets/images/SombraGinebra.png'
import { Parallax } from 'react-scroll-parallax';
import ScrollAnimation from 'react-animate-on-scroll';
import LazyLoad from '@frontity/lazyload'
import Button from "../../../objects/Button/Button";
import {ApolloError} from "@apollo/client";


const Ginebra = ({actions, libraries, ...props}) => {

  const [adding, setAdding] = useState(false)
  const [height, setHeight] = useState(300)

  const handleOnSubmit = async data => {
    setAdding(true);
    const p = await actions.auth.authProcessAuthToken();
    const r = await libraries.cart.addItem(props.forEmpresa ? 1429 : 306, 1);

    if (r instanceof ApolloError) {
      let errorMessage = r.message;
      actions.theme.setAlertMessage(errorMessage, false, 'error');
    }else{
      props.state.cart = r.data.addToCart.cart
      actions.analytics.addToCart(props.forEmpresa);
      actions.router.set('/carrito/')
    }
    setAdding(false);
  };


  const SectionDefault = {
    section_background_color: 'transparent-base',
  }


  StyledSection.defaultProps = {
    breakPoints: props.state.theme.breakPoints,
    sombra: Sombra
  }

  StyledSombra.defaultProps = {
    breakPoints: props.state.theme.breakPoints,
    sombra: Sombra
  }

  StyledSombraContent.defaultProps = {
    breakPoints: props.state.theme.breakPoints,
    sombra: Sombra
  }

  StyledComprar.defaultProps = {
    breakPoints: props.state.theme.breakPoints,
  }


  StyledTitle.defaultProps = {
    breakPoints: props.state.theme.breakPoints
  }

  return (
    <>
      <div id={"Ginebra"} name={"Ginebra"}></div>
      <LazyLoad offset={50} height={height} onContentVisible={(data) =>setHeight('auto')}>
        <ScrollAnimation animateOnce={true} animateIn={"animate__fadeIn"}>
        <div style={{position: 'relative'}}>
          <StyledSection
            data={props.data.section_config}
            defaultData={SectionDefault}
            wrapper={"FullWidth"}>
            <StyledComprar data={{tipo: "primary", tamano: "small", texto: adding ? "AÑADIENDO..." : "COMPRAR"}} onClick={ () => { handleOnSubmit()  }}/>
            <Layout>
              <ScrollAnimation animateOnce={true} animateIn={"animate__fadeIn"}>
                <StyledTitle data={props.data}/>
              </ScrollAnimation>
              <Slider data={props.data} adding={adding} handleOnSubmit={handleOnSubmit}/>
            </Layout>
          </StyledSection>

            <StyledSombraContent y={['-150px', '150px']} tagOuter="figure">
              <ScrollAnimation animateOnce={true} animateIn={"animate__fadeIn"}>
                <StyledSombra ></StyledSombra>
              </ScrollAnimation>
            </StyledSombraContent>

        </div>
        </ScrollAnimation>
      </LazyLoad>
    </>
  );
};

export default connect(Ginebra);

const StyledComprar = styled(Button)`
    position: absolute;
    display: none;
    ${props => {
      return `
        @media ${props.breakPoints['tablet']} {
          display: block;
          left: 85x;
          bottom: 0;
        }
        @media ${props.breakPoints['desktop']} {
          left: calc(22% + 11px);
          bottom: -50px;
        } 
      `
    }
  }
`
const StyledTitle = styled(Title)`
  margin-bottom: 54px;
  margin-top: 50px;    
`

const StyledSombraContent = styled(Parallax)`
  width: 100%;
  height: 100%;
  content: ' ';
  position: absolute;
  left: 0;
  top: 0;    
  .parallax-inner {
    width: 100%;
    height: 100%;  
    transition: all 0.3s ease-out;
  }
  
`

const StyledSombra = styled.div`
  width: 100%;
  height: 100%;
  content: ' ';
  position: absolute;
  left: 0;
  top: 0;   
  background-image: url(${props => props.sombra});  
  background-position: -240px calc(50% + 100px);
  background-repeat: no-repeat;
  ${props => {
  return `
        @media ${props.breakPoints['tablet']} {
            background-position: -120px center;
        } 
        @media ${props.breakPoints['tablet-wide']} {
          background-position: left center;
          
        } 
        @media ${props.breakPoints['desktop']} {
          
        } 
      `
    }
  }
  
`


const StyledSection = styled(Section)`
  background-size: 250px;
  background-position: -80px calc(50% + 150px);
  background-repeat: no-repeat;
  padding-top: 90px;
  padding-bottom: 90px;
  margin-top: -50px;
  background-image:url(https://gestion.fdeformentera.com/wp-content/uploads/2021/01/sombra_botella_retocada-3.png);
  position: relative;
  z-index: 1;
  ${props => {
    return `
        @media ${props.breakPoints['tablet']} {
          padding-top: 112px;
          padding-bottom: 155px;
          background-image: url(https://gestion.fdeformentera.com/wp-content/uploads/2021/01/sombra_botella_retocada-3.png);
          background-position: left center;
          background-size: auto;
        } 
        @media ${props.breakPoints['tablet-wide']} {
          padding-top: 112px;
          padding-bottom: 155px;
          background-image: url(https://gestion.fdeformentera.com/wp-content/uploads/2021/01/sombra_botella_retocada-3.png);
          background-position: left center;
          
        } 
        @media ${props.breakPoints['desktop']} {
          padding-top: 112px;
          padding-bottom: 155px;
          background-image: url(https://gestion.fdeformentera.com/wp-content/uploads/2021/01/sombra_botella_retocada-3.png);
          background-position: 22% 180px;
        } 
      ` 
    }
  }   
  
`


