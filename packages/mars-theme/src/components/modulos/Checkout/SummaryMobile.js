import React, {useState} from 'react';
import {styled, connect} from "frontity";

import Layout from "../../../objects/Layout/Layout";
import ProductDetail from "../../pages/carrito/Left/ProductDetail";
import Text from "../../../objects/Text/Text";
import BagIcon from "../../../assets/images/icons/bag";
import ChevronDownIcon from "../../../assets/images/icons/chevron-down";

const SummaryMobile = ({state, ...props}) => {
  const [open, setOpen] = useState(false);

  let text = 'MOSTRAR DETALLE DE PEDIDO';

  if (open) {
    text = 'OCULTAR DETALLE DE PEDIDO';
  }

  return (
    <div>
      <LayoutStyled justify="space-between" onClick={() => setOpen(!open)}>
        <Layout2Styled justify="flex-start">
          <BagIcon/>
          <TitleStyled data={{texto: text, font_type: 'ButtonLarge', font_family: 'Lexend Deca'}}/>
        </Layout2Styled>

        <ChevronDownIcon/>
      </LayoutStyled>

      {open && (
        <ContentWrapper data={{section_background_color: 'gray-base'}}>
          {state.cart?.contents.nodes.map((item, key) => (
            <ProductDetail item={item} key={key} inCheckout={true} initialQuantity={item.quantity}/>
          ))}
        </ContentWrapper>
      )}
    </div>
  )
}
export default connect(SummaryMobile);

const TitleStyled = styled(Text)`
  color: var(--color-blue-light);
  margin-left: 8px;
`;

const LayoutStyled = styled(Layout)`
  color: var(--color-blue-light);
  width: initial;
  margin-bottom: 20px;
`;

const Layout2Styled = styled(Layout)`
  color: var(--color-blue-light);
  width: initial;
`;

const ContentWrapper = styled(Layout)`
  padding-top: 10px;
  padding-bottom: 16px;
  margin-bottom: 16px;
  width: calc(100% + 32px);
  margin-left: -16px;
`;
