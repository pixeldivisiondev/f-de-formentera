import React from 'react';
import {styled, connect} from "frontity";

import Layout from "../../../objects/Layout/Layout";
import ProductDetail from "../../pages/carrito/Left/ProductDetail";

const Summary = ({state, ...props}) => {

  return (
    <ContentWrapper data={{section_background_color: 'gray-base'}}>
      {state.cart?.contents.nodes.map((item, key) => (
        <ProductDetail item={item} key={key} inCheckout={true} price={state.cart.total} initialQuantity={item.quantity}/>
      ))}
    </ContentWrapper>
  )
}
export default connect(Summary);

const ContentWrapper = styled(Layout)`
  padding-top: 10px;
  padding-bottom: 16px;
  margin-bottom: 16px;
`

