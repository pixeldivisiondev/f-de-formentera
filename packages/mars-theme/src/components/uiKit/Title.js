import Text from "../../objects/Text/Text";
import {connect, styled} from "frontity";

const Title = (props) => {

    const TitleDefault = {
        color: 'primary-base',
        font_family: 'Lexend Zetta',
        font_type:  'Display',
        tipo: 'h2',
        font_weight: 700,
        texto: props.texto
    };


    return (
        <UiKitTitle data={TitleDefault} defaultData={TitleDefault} className={props.className}/>

    )
}


const UiKitTitle = styled(Text)`
    margin-bottom: 16px;
    margin-top: 32px;
`

export default connect(Title)


