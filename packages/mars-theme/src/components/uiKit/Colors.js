import React, { useEffect, useState  } from "react";
import ColorBox from "../../objects/UiKit/ColorBox";
import {styled, connect} from "frontity";
import Title from "./Title"
import Text from "../../objects/Text/Text"
import Layout from '../../objects/FlexBox/FlexBox'

const Colors = (props) => {

    return (
        <>
            <Title texto={"#Colores"}/>

            <Text data={{texto: 'Primary'}} />
            <Layout justify={"flex-start"}>
              <ColorBox color={props.state.theme.palette.primary["base"]} text={"base"}/>
            </Layout>

            <Text data={{texto: 'Secondary'}} />
            <Layout justify={"flex-start"}>
              <ColorBox color={props.state.theme.palette.secondary["base"]} text={"base"}/>
            </Layout>

            <Title texto={"#Palette"}/>

            <Layout justify={"flex-start"} direction={"row"}>

                <PaletteItem justify={"flex-start"}>
                    <Text data={{texto: 'Primary'}}/>
                    <Layout justify={"flex-start"}>
                        <ColorBox color={props.state.theme.palette.primary["light"]} text={"light"}/>
                        <ColorBox color={props.state.theme.palette.primary["base"]} text={"base"}/>
                    </Layout>
                </PaletteItem>

                <PaletteItem justify={"flex-start"}>
                    <Text data={{texto: 'Blue'}}/>
                    <Layout justify={"flex-start"}>
                        <ColorBox color={props.state.theme.palette.blue["light"]} text={"light"}/>
                        <ColorBox color={props.state.theme.palette.blue["dark"]} text={"base"}/>
                    </Layout>
                </PaletteItem>

                <PaletteItem justify={"flex-start"}>
                    <Text data={{texto: 'Error'}}/>
                    <Layout justify={"flex-start"}>
                        <ColorBox color={props.state.theme.palette.error["base"]} text={"base"}/>
                    </Layout>
                </PaletteItem>

                <PaletteItem justify={"flex-start"}>
                    <Text data={{texto: 'Accept'}}/>
                    <Layout justify={"flex-start"}>
                        <ColorBox color={props.state.theme.palette.accept["base"]} text={"base"}/>
                    </Layout>
                </PaletteItem>

                <PaletteItem justify={"flex-start"}>
                    <Text data={{texto: 'Gray'}}/>
                    <Layout justify={"flex-start"}>
                        <ColorBox color={props.state.theme.palette.gray["base"]} text={"base"}/>
                    </Layout>
                </PaletteItem>
            </Layout>
        </>
    )
}

export default connect(Colors)

const PaletteItem = styled(Layout)`
    width: auto
`
