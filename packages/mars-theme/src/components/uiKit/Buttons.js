import { connect, styled } from "frontity";
import Title from "./Title";
import Button from "../../objects/Button/Button";


const Buttons = ({ state, libraries}) => {
    const NornalButtonProps = {
        tipo: "primary",
        tamano: "small",
        texto: "Login",
    }

    const SquareButtonProps = {
        tipo: "primary",
        tamano: "squared",
        texto: "Get Cart",
    }

    return (
        <div>
            <Title texto={"#Buttons"}/>
            <Button data={NornalButtonProps} onClick={ () => { alert( libraries.login.login()) }}/>
            <br />
            <Button data={SquareButtonProps} onClick={ async () => { alert( await libraries.cart.getCart()) } } />

        </div>
    );
};


export default connect(Buttons);
