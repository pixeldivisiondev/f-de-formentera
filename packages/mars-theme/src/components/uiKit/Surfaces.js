import React, { useEffect, useState  } from "react";
import ColorBox from "../../objects/UiKit/ColorBox";
import {styled, connect} from "frontity";
import Title from "./Title"
import Text from "../../objects/Text/Text"
import Layout from "../../objects/Layout/Layout"
import Wrapper from "../../objects/Wrapper"

const Surfaces = (props) => {


    return (
        <>
            <Title texto={"#Surfaces"}/>

            <SurfaceLayout data={ {section_background_color: 'primary-base'}} >
                <Wrapper wrapper={"Default"}>
                    <Text data={{texto: 'Primary Base', color: 'secondary-base'}}/>
                </Wrapper>
            </SurfaceLayout>


            <SurfaceLayout data={ {section_background_color: 'primary-light'}} >
                <Text data={{texto: 'Primary Light'}}/>
            </SurfaceLayout>


            <SurfaceLayout data={ {section_background_color: 'secondary-base'}} >
                <Text data={{texto: 'Secondary Base'}}/>
            </SurfaceLayout>

        </>
    )
}

export default connect(Surfaces)

const SurfaceLayout = styled(Layout)`
    height: 150px;
    margin-bottom: 15px;    
    border: 1px solid #000;
`
