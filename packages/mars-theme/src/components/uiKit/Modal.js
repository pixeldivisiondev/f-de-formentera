import Modal from "../../objects/Modal/Modal";

const ModalComponent = (props) => {
  return (
    <Modal
      modal={props.modal}
      wrapper={props.wrapper}
      ModalContent={props.ModalContent }
      CloseModal={props.CloseModal}
      close={props.close} >
        {props.children}
    </Modal>

  );
};

export default ModalComponent;



