import React from "react";
import {styled, connect} from "frontity";
import Title from "./Title"
import Text from "../../objects/Text/Text"

const Texts = (props) => {

    const TitleDefault = {
        color: 'primary-base',
        font_family: 'Lexend Deca',
        font_type:  'H1',
        tipo: 'p',
        font_weight: 400,
        texto: ''
    };

    return (
        <>
            <Title texto={"#Textos"}/>
            <UiKitTextLayout data={{texto: 'Display',  font_type:  'Display' }} defaultData={TitleDefault} className={props.className}/>
            <UiKitTextLayout data={{texto: 'H1',  font_type:  'H1' }} defaultData={TitleDefault} className={props.className}/>
            <UiKitTextLayout data={{texto: 'SubtitleLarge',  font_type:  'SubtitleLarge'}} defaultData={TitleDefault} className={props.className}/>
            <UiKitTextLayout data={{texto: 'SubtitleSmall',  font_type:  'SubtitleSmall'}} defaultData={TitleDefault} className={props.className}/>

            <UiKitTextLayout data={{texto: 'ANTETITULO',  font_type:  'Antetitulo' }} defaultData={TitleDefault} className={props.className}/>

            <UiKitTextLayout data={{texto: 'PrimaryLarge',  font_type:  'PrimaryLarge' }} defaultData={TitleDefault} className={props.className}/>
            <UiKitTextLayout data={{texto: 'PrimaryMedium',  font_type:  'PrimaryMedium' }} defaultData={TitleDefault} className={props.className}/>
            <UiKitTextLayout data={{texto: 'PrimarySmall',  font_type:  'PrimarySmall' }} defaultData={TitleDefault} className={props.className}/>

            <UiKitTextLayout data={{texto: 'SecondaryLarge',  font_type:  'SecondaryLarge' , font_family: 'Cormorant'}} defaultData={TitleDefault} className={props.className}/>
            <UiKitTextLayout data={{texto: 'SecondaryMedium',  font_type:  'SecondaryMedium', font_family: 'Cormorant' }} defaultData={TitleDefault} className={props.className}/>
            <UiKitTextLayout data={{texto: 'SecondarySmall',  font_type:  'SecondarySmall', font_family: 'Cormorant'  }} defaultData={TitleDefault} className={props.className}/>

            <UiKitTextLayout data={{texto: 'TertiaryLarge',  font_type:  'PrimaryLarge' , font_family: 'Lexend Zetta'}} defaultData={TitleDefault} className={props.className}/>
            <UiKitTextLayout data={{texto: 'TertiaryMedium',  font_type:  'PrimaryMedium', font_family: 'Lexend Zetta' }} defaultData={TitleDefault} className={props.className}/>
            <UiKitTextLayout data={{texto: 'TertiarySmall',  font_type:  'PrimarySmall', font_family: 'Lexend Zetta' }} defaultData={TitleDefault} className={props.className}/>

            <UiKitTextLayout data={{texto: 'LabelLarge',  font_type:  'LabelLarge' }} defaultData={TitleDefault} className={props.className}/>
            <UiKitTextLayout data={{texto: 'LabelMedium',  font_type:  'LabelMedium' }} defaultData={TitleDefault} className={props.className}/>
            <UiKitTextLayout data={{texto: 'LabelSmall',  font_type:  'LabelSmall' }} defaultData={TitleDefault} className={props.className}/>

            <UiKitTextLayout data={{texto: 'Capion',  font_type:  'Capion' }} defaultData={TitleDefault} className={props.className}/>

            <Text data={{texto: 'Texto Base'}} />


        </>
    )
}

export default connect(Texts)

const UiKitTextLayout = styled(Text)`   
    margin-bottom: 15px;
`
