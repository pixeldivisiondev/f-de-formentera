import React from "react";
import { connect, styled } from "frontity";
import Layout from "../../objects/Layout/Layout";
import Text from "../../objects/Text/Text";
import AnchorLink from 'react-anchor-link-smooth-scroll'
import Link from "@frontity/components/link";

const Nav = ({ state, ...props }) => {

  const {items} = state.source.get('/menus/Main')
  StyledLayout.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  NavItem.defaultProps = {
    breakPoints: state.theme.breakPoints
  }


  const LinkData = {
    tipo: 'span',
    font_family: "Lexend Deca",
    font_type: "ButtomSmall"
  }

  return (
    <StyledLayout className={props.className}>
      {items.map( (item, index) => {
        const isCurrentPage = state.router.link === item.slug;
        const link = item.slug ? '/'+ item.slug : item.url
        return (
          <NavItem key={index}>
            {
              props.anchor
                ? <StyledLink offset="20" key={index} href={link } aria-current={isCurrentPage ? "page" : undefined}>
                    <Text data={{texto: item.title}} defaultData={LinkData}/>
                  </StyledLink>
                : <StyledLinkExternal offset="20" key={index} href={'/'+link } aria-current={isCurrentPage ? "page" : undefined}>
                    <Text data={{texto: item.title}} defaultData={LinkData}/>
                  </StyledLinkExternal>


            }

          </NavItem>
        )
      })}
    </StyledLayout>
  );
}

export default connect(Nav);

const NavItem = styled(Layout)`
  width: auto;
  margin-right: 25px;
  padding-top: 21px;
  ${props => {
    return `
      @media ${props.breakPoints['tablet']} {
          padding-top: 0px;     
          margin-right: 50px;      
      }
      @media ${props.breakPoints['desktop']} {
           margin: 0 63px;      
      }
      `
  }}
`

const StyledLayout = styled(Layout)`
  flex: 0 0 100%;
  order: 3;
  flex-wrap: nowrap;
  justify-content: flex-start;
  overflow: auto;
  @media (min-width: 379px) {
    justify-content: center;
  }
  ${props => {
    return `@media ${props.breakPoints['tablet']} {
      flex-wrap: wrap;
      order: 2;
      justify-content: center;
      flex: 1 1;
    }`
  }}  
  `;

const StyledLink = styled(AnchorLink)`
  text-decoration: none;
  text-transform: uppercase;    
  white-space: nowrap;
`
const StyledLinkExternal = styled.a`
  text-decoration: none;
  text-transform: uppercase;    
  white-space: nowrap;
`
