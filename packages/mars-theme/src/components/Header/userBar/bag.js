import React, { useEffect } from 'react';
import BagIcon from "../../../assets/images/icons/bag.svg";
import Layout from "../../../objects/Layout/Layout";
import {styled, connect} from "frontity";
import Link from "@frontity/components/link"
import Text from "../../../objects/Text/Text";
import {ApolloError} from "@apollo/client";

const Bag = ({state, libraries, actions, ...props}) => {

  const ItemCounterData = {
    texto: "1",
    color: 'secondary-base',
    font_type: "PrimarySmall",
  }

  useEffect(() => {
      async function fetchData() {
        const r = await actions.auth.authProcessAuthToken();
        if(state && !state.cart) {
          const r = await libraries.cart.getCart();

          if (r instanceof ApolloError) {
                        
          }else{

            state.cart = r.data.cart
          }
        }

      }
    fetchData()
  });

  const logOut = async () => {
    libraries.auth.authUnregisterJwtToken();
    actions.theme.setLoginName(null);
    actions.theme.logOut();
  };

  return (
    <StyledLayout className={props.className} >
      <Link link={"/carrito"}>
        <img src={BagIcon} />
        {
          state.cart && state.cart.contents
            ? <ItemCounter data={{section_background_color: 'primary-base'}}>
                <Text data={{texto: state.cart.contents.itemCount.toString()}} defaultData={ItemCounterData} />
              </ItemCounter>
            : null
        }

      </Link>
    </StyledLayout>
  )
}

export default connect(Bag)

const StyledLayout = styled(Layout)`
  width: auto;
  position: relative;
`

const ItemCounter = styled(Layout)`
  position: absolute;
  width: 24px;
  height: 24px;
  left: 10px;
  top: 8px;
  border-radius: 50%;
`
