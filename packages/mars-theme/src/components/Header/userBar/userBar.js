import User from "../../../assets/images/icons/user.svg";
import {useState, useEffect} from 'react';
import {styled, connect} from "frontity";
import Layout from "../../../objects/Layout/Layout";
import Link from "../link";
import Bag from "./bag"
import LoginModal from "../../pages/login/LoginModal/LoginModal";
import {CloseModal, ModalContent, ModalWrapper} from "../../objects/Modals";
import Modal from "../../../objects/Modal/Modal";

const UserBar = ({state, libraries, actions, ...props}) => {

  const [openedModal, setopenedModal] = useState(false);

  StyledIcon.defaultProps = {
    breakPoints: state.theme.breakPoints
  }


  console.log(openedModal, 'OPENED CUENTA')

  return (
    <StyledMenuRight style={{textAlign: 'right'}} justify={'flex-end'} className={props.className}>
      {props.logged === 1 ? (
        <StyledIcon link={"/mi-cuenta"}>
          <img src={User}/>
        </StyledIcon>
      ) : (
        <Modal
          modal={<LoginModal redirect={true} onSuccess={() => { console.log('CLOSING'); actions.theme.setModalOpened(false); setopenedModal(false); }}/>}
          wrapper={ModalWrapper}
          ModalContent={ModalContent}
          CloseModal={CloseModal}
          onClick={() => {  setopenedModal(true);  actions.theme.setModalOpened(2)}}
          visible={openedModal}
          onTop = {true}
        >
          <StyledIcon>
            <img src={User}/>
          </StyledIcon>
        </Modal>
      )}

      <Bag/>
    </StyledMenuRight>
  )
}

export default connect(UserBar);

const StyledMenuRight = styled(Layout)`
  width: auto;
`

const StyledIcon = styled(Link)`
  margin-right: 20px;  
  
  ${props => {
    return `@media ${props.breakPoints['tablet']} {
        margin-right: 25px;
      }`
  }}  
`
