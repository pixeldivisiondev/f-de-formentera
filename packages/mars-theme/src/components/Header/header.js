import {connect, styled} from "frontity";
import Nav from "./nav";
import Wrapper from "../../objects/Wrapper";
import Logo from '../../assets/images/logos/Logotipo.svg'
import UserBar from './userBar/userBar';
import Link from "./link";
import Layout from "../../objects/Layout/Layout";
import Text from "../../objects/Text/Text";
import {useState} from "react";

const Header = ({state, ...props}) => {

  const [a, b] = useState(props.witModal);

  console.log(state.theme.modalOpened, 'HEADER MODAL')

  LeftBar.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  RightBar.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  StyledWrapper.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  HeaderText.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  StyledNav.defaultProps = {
    breakPoints: state.theme.breakPoints
  }

  const data = state.source.get(state.router.link);
  let showNav = true;
  let showUserbar = true;
  let post = null;

  if (data.type && data.id) {
    post = state.source[data.type][data.id];
    if(post) {
      showNav = ![
        'template-mi-cuenta.php',
        'template-carrito.php',
        'template-mis-datos-personales.php',
        'template-cambiar-contrasena.php',
        'template-mis-pedidos.php',
        'template-mis-direcciones.php',
        'template-registro.php',
        'template-finalizar-compra.php',

      ].includes(post.template)

      showUserbar = ![
        'template-carrito.php',
        'template-finalizar-compra.php',
      ].includes(post.template)
    }
  }

  return (
    <StyledHeader withModal={state.theme.modalOpened || a}>
    <StyledWrapper wrapper={"Default"} justify={"space-between"}>

      { post && post.template === 'template-carrito.php' ? <HeaderText data={{texto: "TU CESTA"}} defaultData={{font_type: 'H1'}}/> : null }

      <LeftBar link={"/"} rightLogo={post && post.template === 'template-carrito.php'}>
        <img src={Logo}/>
      </LeftBar>

      {showNav ? (
        <StyledNav anchor={data?.isHome}/>
      ) : ('')}

      {showUserbar ? (
        <RightBar logged={props.logged}/>
      ) : ('')}


    </StyledWrapper>
    </StyledHeader>


  );
};

// Connect the Header component to get access to the `state` in it's `props`
export default connect(Header);

const HeaderText = styled(Text)`
  flex: 1 1;
  width: 100%;
  ${props => {
  return `
          @media ${props.breakPoints['tablet-wide']} {
            display: none;          
          }`
}
  }  
  `


const StyledHeader = styled(Layout)`  
  position: fixed;
  background-color: var(--color-secondary-base);
  z-index: ${props => props.withModal && props.withModal != 2 ? '1' : 10};
`

const StyledWrapper = styled(Wrapper)`  
  padding-top: 13px;
  padding-bottom: 13px; 
  z-index: 3; 
  ${props => {
  return `@media ${props.breakPoints['tablet']} {
      padding-top: 30px;
      padding-bottom: 30px;
    }`
}}  
`

const LeftBar = styled(Link)`
  order: 1;
  
  
  ${props => {
    props.rightLogo ? 'flex: 0;' : 'flex: 1 1 50%;' 
    return `@media ${props.breakPoints['tablet']} {
      flex: 0 0 100px;
    }`
}}  
  `;

const RightBar = styled(UserBar)`
  order: 2;
  flex: 1 1 50%;
  ${props => {
  return `@media ${props.breakPoints['tablet']} {
      flex: 0 0 100px;
    }`
}}  
  `;

const StyledNav = styled(Nav)`
  display: none;
  ${props => {
  return `@media ${props.breakPoints['tablet']} {
      display: flex;
    }`
}}  
  `;
