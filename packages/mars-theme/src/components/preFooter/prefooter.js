import { connect, styled } from "frontity";
import CardComunidad from "../objects/Cards/Compositions/comunidad";
import CardContacto from "../objects/Cards/Compositions/contacto";
import Layout from "../../objects/Layout/Layout";
import Card from "../objects/Cards/card";
import Section from "../../objects/Section/Section";
import ScrollAnimation from 'react-animate-on-scroll';
import React, {useState} from "react";
import LazyLoad from '@frontity/lazyload'

const PreFooter = ({ state, ...props}) => {

    const [height, setHeight] = useState(200)

    HalfCardComunidad.defaultProps = {
        theme: state.theme
    }
    HalfCardContacto.defaultProps = {
        theme: state.theme
    }

    const data = state.theme.optionsACF;

    return (
      <LazyLoad offset={150} height={height} onContentVisible={(data) =>setHeight('auto')}>
        <ScrollAnimation animateOnce={true} animateIn={"animate__fadeIn"}  delay={100}>
          <Section wrapper={"DefaultNoPadding"} data={{section_background_color: 'secondary-base'}}>
            <Layout direction={"row"}>
                <HalfCardComunidad backGround={data.comunidad.imagen_desktop} >
                    <ScrollAnimation animateOnce={true} animateIn={"animate__fadeIn"}>
                        <CardComunidad data={data.comunidad}/>
                    </ScrollAnimation>
                </HalfCardComunidad >
                <HalfCardContacto backGround={data.contacto.imagen_desktop} video={data.contacto.video}>
                    <ScrollAnimation animateOnce={true} animateIn={"animate__fadeIn"}>
                    <CardContacto  />
                    </ScrollAnimation>
                </HalfCardContacto>
            </Layout>
          </Section>
        </ScrollAnimation>
      </LazyLoad>

    )

};

export default connect(PreFooter);


const HalfCard = styled(Card)`    
   
    ${props => {
        return` @media ${props.theme.breakPoints['tablet-wide']}
            {
                flex: 1 1 50%;
                width: 50%;
            }`
        }
    }
`

const HalfCardComunidad = styled(HalfCard)`
  
`

const HalfCardContacto = styled(HalfCard)`
  margin-top: -20px;
  padding-top: 0;
  ${props => {
  return ` @media ${props.theme.breakPoints['tablet-wide']}
            {
              margin-top: 0px;            
            }`
        }
    }     
`


