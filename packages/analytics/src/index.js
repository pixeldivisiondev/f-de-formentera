export default {
    actions: {
        analytics: {
            showProduct: ({state, actions}) => (forEmpresa) => {
                actions.googleTagManagerAnalytics.event({
                    name: "showProduct",
                    payload: {
                        'ecommerce': {
                            'currencyCode': 'EUR',
                            'impressions': [
                                {
                                    'name': forEmpresa ? 'GINEBRA, PACK 3 BotellaS 70cl' : 'F de Formentera Botella de 70 cl',
                                    'id': forEmpresa  ? '1429' : '306',
                                    'price': forEmpresa ? '72.00' : '34.95',
                                    'brand': 'F de Formentera',
                                    'category': 'Ginebra',
                                    'list': 'Home',
                                    'position': 1
                                }]
                        }
                    },
                });
            },
            addToCart: ({state, actions}) => (forEmpresa, quantity) => {
                actions.googleTagManagerAnalytics.event({
                    name: "addToCart",
                    payload: {
                        'ecommerce': {
                            'currencyCode': 'EUR',
                            'add': {
                                'products': [{
                                    'name': forEmpresa ? 'GINEBRA, PACK 3 BotellaS 70cl' : 'F de Formentera Botella de 70 cl',
                                    'id': forEmpresa ? '1429' : '306',
                                    'price': forEmpresa ? '72.00' : '34.95',
                                    'brand': 'F de Formentera',
                                    'category': 'Ginebra',
                                    'quantity': quantity ? quantity : 1
                                }]
                            }
                        }
                    },
                });
            },
            removeFromCart: ({state, actions}) => (forEmpresa, quantity) => {
                actions.googleTagManagerAnalytics.event({
                    name: "removeFromCart",
                    payload: {
                        'ecommerce': {
                            'remove': {
                                'products': [{
                                    'name': forEmpresa ? 'GINEBRA, PACK 3 BotellaS 70cl' : 'F de Formentera Botella de 70 cl',
                                    'id': forEmpresa  ? '1429' : '306',
                                    'price': forEmpresa ? '72.00' : '34.95',
                                    'brand': 'F de Formentera',
                                    'category': 'Ginebra',
                                    'quantity': quantity ? quantity : 1
                                }]
                            }
                        }
                    },
                });
            },
            checkoutStep: ({state, actions}) => (forEmpresa, step, option, quantity) => {

                actions.googleTagManagerAnalytics.event({
                    name: "checkout",
                    payload: {
                        'ecommerce': {
                            'checkout': {
                                'actionField': {'step': step, 'option': option},
                                'products': [{
                                    'name': forEmpresa ? 'GINEBRA, PACK 3 BotellaS 70cl' : 'F de Formentera Botella de 70 cl',
                                    'id': forEmpresa  ? '1429' : '306',
                                    'price': forEmpresa ? '72.00' : '34.95',
                                    'brand': 'F de Formentera',
                                    'category': 'Ginebra',
                                    'quantity': quantity ? quantity : state.cart ? state.cart.contents.itemCount : 1
                                }]
                            }
                        }
                    },
                });
            },

            setTransaction: ({state, actions}) => (idTransaction) => {

                console.log(state.cart, 'CART CHECOUT')

                let forEmpresa = false;
                if (typeof window !== 'undefined') {
                    if(localStorage.getItem('Role') === 'empresa') {
                        forEmpresa = true
                    }
                }

                const products = [];
                state.cart.contents.nodes.map(item => {
                    console.log(item, 'ITEM QU')
                    const i = item.product.node
                    const p = {
                        'name': forEmpresa ? 'GINEBRA, PACK 3 BotellaS 70cl' : 'F de Formentera Botella de 70 cl',
                        'id': forEmpresa  ? '1429' : '306',
                        'price': i.price.replace('€', ''),
                        'brand': 'F de Formentera',
                        'category': 'Ginebra',
                        'quantity': item.quantity.toString()
                    }
                    products.push(p);
                })
                actions.googleTagManagerAnalytics.event({
                    name: "transaction",
                    payload: {
                        'ecommerce': {
                            'purchase': {
                                'actionField': {
                                    'id': idTransaction,
                                    'affiliation': 'Online Store',
                                    'revenue': state.cart.total.replace('€', ''),
                                    'tax': state.cart.totalTax.replace('€', ''),
                                    'shipping': state.cart.shippingTotal.replace('€', ''),
                                    'coupon': state.cart.appliedCoupons.nodes[0] ? state.cart.appliedCoupons.nodes[0].code : ''
                                },
                                'products': products
                            }
                        }
                    },
                });
            },
        }
    }
}
